<?php
define('SERVER', $_SERVER['SERVER_NAME']);

switch (SERVER) {
    case 'tienda.local':
    case 'motel.local':
    case 'crm2.local':
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
        // remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG', true);
        define('ENV', 'local');
        break;
    case 'pedidos-dev.giosyst3m.net':
    case 'app01.giosyst3m.net':
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
        // remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG', true);
        define('ENV', 'dev');
        break;
    case 'pedidos-stage.giosyst3m.net':
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
        // remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG', true);
        define('ENV', 'stg');
        break;
    default:
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 1);
        // remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG', false);
        define('ENV', 'liv');
        break;
}
// change the following paths if necessary
$yii = dirname(__FILE__) . '/yii/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';





require_once($yii);
Yii::createWebApplication($config)->run();
