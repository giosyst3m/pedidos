//setInterval(function () {location.reload();}, 50000);
$(document).ready(function () {
    geteChart();
    getChart('getOrderStatus');
    getChart('getOrderBill');
    getChart('getMapCity');
    google.charts.load('current', {'packages': ['corechart', 'table', 'geochart']});
    $('.periodos').selectpicker('hide');

});

$('#Year').on('change', function () {
    if ($('#Year').val().length > 1) {
        $('.periodos').selectpicker('hide');
        $('#Periodos').selectpicker('hide');
        $('.periodos').selectpicker('deselectAll');
    } else if ($('#Year').val().length = 1) {
        $('#Periodos').selectpicker('show');
    }
    getChart('getOrderStatus');
    getChart('getOrderBill');

});
$('#Document').on('change', function () {
    getChart('getOrderStatus');
    getChart('getOrderBill');
});
$('#Chart').on('change', function () {
    getChart('getOrderStatus');
    getChart('getOrderBill');
});
$('#Semestres').on('change', function () {
    getChart('getOrderStatus');
    getChart('getOrderBill');
});
$('#Cuatrimestres').on('change', function () {
    getChart('getOrderStatus');
    getChart('getOrderBill');
});
$('#Trimestres').on('change', function () {
    getChart('getOrderStatus');
    getChart('getOrderBill');
});
$('#Bimestral').on('change', function () {
    getChart('getOrderStatus');
    getChart('getOrderBill');
});
$('#Meses').on('change', function () {
    getChart('getOrderStatus');
    getChart('getOrderBill');
});
$('#Periodos').on('change', function () {
    $('.periodos').selectpicker('hide');
    $('.periodos').selectpicker('deselectAll');
    if ($('#Periodos').selectpicker('val') == 2) {
        $('#Semestres').selectpicker('show');
    } else if ($('#Periodos').selectpicker('val') == 3) {
        $('#Cuatrimestres').selectpicker('show');
    } else if ($('#Periodos').selectpicker('val') == 4) {
        $('#Trimestres').selectpicker('show');
    } else if ($('#Periodos').selectpicker('val') == 5) {
        $('#Bimestral').selectpicker('show');
    } else if ($('#Periodos').selectpicker('val') == 6) {
        $('#Meses').selectpicker('show');
    }

});
$('#Status').on('change', function () {
    getChart('getMapCity');
});

function geteChart() {
    $.ajax({
        url: getHomeUrl() + 'site/geteChart/',
        type: "post",
        dataType: "json",
        data: {
        },
        success: function (data, textStatus, jqXHR)
        {
            var echartLine = echarts.init(document.getElementById('echart_line'), theme);

            echartLine.setOption({
                title: {
                    text: 'Facturado Vs Pagado por mes',
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    x: 220,
                    y: 40,
                    data: ['Facturado', 'Pagado']
                },
                toolbox: {
                    show: true,
                    feature: {
                        magicType: {
                            show: true,
                            title: {
                                line: 'Line',
                                bar: 'Bar',
                                stack: 'Stack',
                                tiled: 'Tiled'
                            },
                            type: ['line', 'bar', 'stack', 'tiled']
                        },
                        restore: {
                            show: true,
                            title: "Restore"
                        },
                        saveAsImage: {
                            show: true,
                            title: "Save Image"
                        }
                    }
                },
                calculable: true,
                xAxis: [{
                        type: 'category',
                        boundaryGap: false,
                        data: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
                    }],
                yAxis: [{
                        type: 'value'
                    }],
                series: [{
                        name: 'Facturado',
                        type: 'line',
                        smooth: true,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    type: 'default'
                                }
                            }
                        },
                        data: data.facturado
                    }, {
                        name: 'Pagado',
                        type: 'line',
                        smooth: true,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    type: 'default'
                                }
                            }
                        },
                        data: data.pagado
                    }]
            });
            var echartPieCollapse = echarts.init(document.getElementById('echart_pie2'), theme);

            echartPieCollapse.setOption({
                title: {
                    text: 'Pedidos por Estatus',
                    subtext: 'No incluye Facturados y pagados'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    x: 'center',
                    y: 'bottom',
                    data: ['rose1', 'rose2', 'rose3', 'rose4', 'rose5', 'rose6']
                },
                toolbox: {
                    show: true,
                    feature: {
                        magicType: {
                            show: true,
                            type: ['pie', 'funnel']
                        },
                        restore: {
                            show: true,
                            title: "Restore"
                        },
                        saveAsImage: {
                            show: true,
                            title: "Save Image"
                        }
                    }
                },
                calculable: true,
                series: [{
                        name: 'Area Mode',
                        type: 'pie',
                        radius: [25, 90],
                        center: ['50%', 170],
                        roseType: 'area',
                        x: '50%',
                        max: 40,
                        sort: 'ascending',
                        data: data.estado
                    }]
            });
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}
function getChart(router) {
    $.ajax(
            {
                url: getHomeUrl() + 'chart/' + router + '/',
                type: "POST",
                dataType: 'json',
                data: {
                    year: $('#Year').val(),
                    periodo: $('#Periodos').val(),
                    chart: $('#Chart').val(),
                    semestral: $('#Semestres').val(),
                    cuatrimestral: $('#Cuatrimestres').val(),
                    trimestral: $('#Trimestres').val(),
                    bimestral: $('#Bimestral').val(),
                    mensual: $('#Meses').val(),
                    document: $('#Document').val(),
                    id_status: $('#Status').val(),
                },
                beforeSend: function (data, textStatus, jqXHR) {
                    $('#ChartOrderStatus').html();
                    $('#ChartOrderStatuTsTable').html();
                },
                success: function (result, textStatus, jqXHR)
                {
                    if (result.code === 200) {
                        if (router === 'getMapCity') {
                            google.charts.setOnLoadCallback(drawMarkersMap);

                            function drawMarkersMap() {
                                var data = google.visualization.arrayToDataTable(result.chartData);

                                var options = {
                                    region: 'CO',
                                    displayMode: 'markers',
                                    colorAxis: {colors: ['blue', 'red']}
                                };
                                var chart = new google.visualization.GeoChart(document.getElementById('chartMap'));
                                chart.draw(data, options);
                            }
                            ;
                        } else {
                            google.charts.setOnLoadCallback(drawChart);
                            function drawChart() {

                                var data = google.visualization.arrayToDataTable(result.chartData);

                                var options = {
                                    title: result.chartTitle,
                                    is3D: true,
                                };
                                if ($('#Chart').val() == 'BarChart') {
                                    var chart = new google.visualization.BarChart(document.getElementById(result.chartId));
                                } else if ($('#Chart').val() == 'PieChart') {
                                    var chart = new google.visualization.PieChart(document.getElementById(result.chartId));
                                } else if ($('#Chart').val() == 'ColumnChart') {
                                    var chart = new google.visualization.ColumnChart(document.getElementById(result.chartId));
                                } else if ($('#Chart').val() == 'LineChart') {
                                    var chart = new google.visualization.LineChart(document.getElementById(result.chartId));
                                }


                                chart.draw(data, options);

                            }
                            google.charts.setOnLoadCallback(drawTable);

                            function drawTable() {
                                var data = new google.visualization.DataTable();
                                $.each(result.tablesName, function (i, item) {
                                    data.addColumn(item.type, item.label);
                                });
                                data.addRows(result.tablesData);
                                var table = new google.visualization.Table(document.getElementById(result.tablesId));

                                table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});
                            }
                        }
                    } else {
                        $.growl(result.menssage, {
                            type: result.type
                        });

                    }
                },
                complete: function () {


                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $.growl("Se ha presentado problemas interno en el sistema Error: " + errorThrown, {
                        type: "danger"
                    });
                }
            });
}
