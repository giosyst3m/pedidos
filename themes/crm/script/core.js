/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var theme = {
    color: [
        '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
        '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
    ],
    title: {
        itemGap: 8,
        textStyle: {
            fontWeight: 'normal',
            color: '#408829'
        }
    },
    dataRange: {
        color: ['#1f610a', '#97b58d']
    },
    toolbox: {
        color: ['#408829', '#408829', '#408829', '#408829']
    },
    tooltip: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        axisPointer: {
            type: 'line',
            lineStyle: {
                color: '#408829',
                type: 'dashed'
            },
            crossStyle: {
                color: '#408829'
            },
            shadowStyle: {
                color: 'rgba(200,200,200,0.3)'
            }
        }
    },
    dataZoom: {
        dataBackgroundColor: '#eee',
        fillerColor: 'rgba(64,136,41,0.2)',
        handleColor: '#408829'
    },
    grid: {
        borderWidth: 0
    },
    categoryAxis: {
        axisLine: {
            lineStyle: {
                color: '#408829'
            }
        },
        splitLine: {
            lineStyle: {
                color: ['#eee']
            }
        }
    },
    valueAxis: {
        axisLine: {
            lineStyle: {
                color: '#408829'
            }
        },
        splitArea: {
            show: true,
            areaStyle: {
                color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
            }
        },
        splitLine: {
            lineStyle: {
                color: ['#eee']
            }
        }
    },
    timeline: {
        lineStyle: {
            color: '#408829'
        },
        controlStyle: {
            normal: {color: '#408829'},
            emphasis: {color: '#408829'}
        }
    },
    k: {
        itemStyle: {
            normal: {
                color: '#68a54a',
                color0: '#a9cba2',
                lineStyle: {
                    width: 1,
                    color: '#408829',
                    color0: '#86b379'
                }
            }
        }
    },
    map: {
        itemStyle: {
            normal: {
                areaStyle: {
                    color: '#ddd'
                },
                label: {
                    textStyle: {
                        color: '#c12e34'
                    }
                }
            },
            emphasis: {
                areaStyle: {
                    color: '#99d2dd'
                },
                label: {
                    textStyle: {
                        color: '#c12e34'
                    }
                }
            }
        }
    },
    force: {
        itemStyle: {
            normal: {
                linkStyle: {
                    strokeColor: '#408829'
                }
            }
        }
    },
    chord: {
        padding: 4,
        itemStyle: {
            normal: {
                lineStyle: {
                    width: 1,
                    color: 'rgba(128, 128, 128, 0.5)'
                },
                chordStyle: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)'
                    }
                }
            },
            emphasis: {
                lineStyle: {
                    width: 1,
                    color: 'rgba(128, 128, 128, 0.5)'
                },
                chordStyle: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)'
                    }
                }
            }
        }
    },
    gauge: {
        startAngle: 225,
        endAngle: -45,
        axisLine: {
            show: true,
            lineStyle: {
                color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                width: 8
            }
        },
        axisTick: {
            splitNumber: 10,
            length: 12,
            lineStyle: {
                color: 'auto'
            }
        },
        axisLabel: {
            textStyle: {
                color: 'auto'
            }
        },
        splitLine: {
            length: 18,
            lineStyle: {
                color: 'auto'
            }
        },
        pointer: {
            length: '90%',
            color: 'auto'
        },
        title: {
            textStyle: {
                color: '#333'
            }
        },
        detail: {
            textStyle: {
                color: 'auto'
            }
        }
    },
    textStyle: {
        fontFamily: 'Arial, Verdana, sans-serif'
    }
};
function udpdateSumary(id, order) {
    $.ajax({
        url: getHomeUrl() + 'OrdDetail/refreshSumaryByClient/id/' + id,
        type: "post",
        dataType: "json",
        data: {
            id: id,
            order: order,
        },
        beforeSend: function(){
          $('.cart-check').hide();  
        },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            $('#cart-total').text(data.quantityItems);
            $('.dropdown-menu-shop').html(data.htlm);
            $('.cart-check').attr('href', getHomeUrl() + 'ordDetail/view/id/' + data.orderNumber);
            if (data.code === 200) {
                getOrderDatail(data.orderNumber);
                $('.cart-check').show();
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });

}
if (($(window).height() + 100) < $(document).height()) {
    $('#top-link-block').removeClass('hidden').affix({
        // how far to scroll down before link "slides" into view
        offset: {top: 100}
    });
}
function chageCompany(obj) {
    $.ajax({
        url: getHomeUrl() + 'site/changeCompany/',
        type: "post",
        dataType: "json",
        data: {
            id: obj.value,
        },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                location.reload();
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}
$(window).on('load', function () {
    $('.cart-check').hide();
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
//    CKEDITOR.replaceAll();
    $('a[title]').tooltip();
    var homeUrl = $("#homeUrl").val();
    $(".fancybox").fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        closeBtn: false,
        helpers: {
            title: {
                type: 'outside'
            },
            thumbs: {
                width: 50,
                height: 50
            },
            buttons: {}
        }
    });
//    window.open(homeUrl+"dashboard","fs","fullscreen=yes");
    initFields();
//    $('.selectpicker').selectpicker();
//    $('.timepicker').timepicker();
//    $('.datetimepicker').datetimepicker({
//         language: "es",
//    });
//    $('.timepicker').datetimepicker({
//        format: 'hh:mm A',
//        language: "es",
//        pickDate: false,
//        pickSeconds: true,
//        pick12HourFormat: false,
//        use24hours: true, 
//    });

    $('.popoverMsg').popover();

    $(".cantidad").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $(this).after('<div class="alert alert-danger alertnumber" role="alert">Valor no permito sólo se permiten <b>números</b></div>').show();

            $('.alertnumber').fadeOut(5000);
            return false;

        }
    });
    $(document).on('click', '.panel-heading span.clickable', function (e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    })
});
$(document).ready(function () {
    //$('#procesando').hide();
});

function initFields() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $('.selectpicker').selectpicker('mobile');
    } else {
        $('.selectpicker').selectpicker();
    }



    $('.datetimepicker').datetimepicker({
        language: "es",
    });
    $('.timepicker').datetimepicker({
        format: 'hh:mm A',
        language: "es",
        pickDate: false,
        pickSeconds: true,
        pick12HourFormat: false,
        use24hours: true,
    });
    $('.date').datetimepicker({
        format: 'YYYY-MM-DD',
        language: "es",
        pickDate: true,
        pickTime: false,
        pickSeconds: false,
        pick12HourFormat: false,
        use24hours: false,
    });
}
function getHomeUrl() {
    return $("#homeUrl").val();
}
$(document).on("click", ".btnAjax", function (e) {
    $.ajax({
        url: $('#' + e.target.id).attr('log'),
        type: "POST",
        beforeSend: function () {
            $('#procesando').removeClass('hidden');
        },
        success: function (data, textStatus, jqXHR)
        {
            $(".modal-body").html(data);
            $('#procesando').hide();
            $("#myModal").modal("show");
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });

    e.stopPropagation();

    e.preventDefault();
});
$(document).on("click", ".CierreCuenta", function (e) {
    $.ajax({
        url: $('#' + e.target.id).attr('log') + "/id/" + $('#CajMovCierre_id_caj_movimientos').val() + '/total/' + $('#valor_total').text(),
        type: "POST",
        beforeSend: function () {
            $('#procesando').removeClass('hidden');
        },
        success: function (data, textStatus, jqXHR)
        {
            $(".modal-body").html(data);
            $('#procesando').hide();
            $("#myModal").modal("show");
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });

    e.stopPropagation();

    e.preventDefault();
});
//
//$(document).on("click",".btnDeletePedidoDetalle",function(e) {
////    console.log(e.target.id);
////    console.log(e);
//    $("#myModal2").modal("show");
//    $('#pedido-detalle').val($('#'+e.target.id).attr('log'));
//    e.stopPropagation();
//    e.preventDefault();
//});
function ajax($id) {

}

function updateHabitacion(data) {
    if (data.log == 1) {//Ocupada
        $("#id-IN-" + data.id).addClass('disabled');
        $("#id-ORDER-" + data.id).addClass('disabled');
        $("#id-OUT-" + data.id).removeClass('disabled');
        $("#id-SUP-IN-" + data.id).removeClass('disabled');
        $("#id-OO-IN-" + data.id).removeClass('disabled');

    } else if (data.log == 2) {//Disponible
        $("#id-IN-" + data.id).removeClass('disabled');
        $("#id-ORDER-" + data.id).addClass('disabled');
        $("#id-OUT-" + data.id).addClass('disabled');
        $("#id-SUP-IN-" + data.id).removeClass('disabled');
        $("#id-OO-IN-" + data.id).removeClass('disabled');

    } else if (data.log == 3) {
        $("#id-IN-" + data.id).addClass('disabled');
        $("#id-ORDER-" + data.id).removeClass('disabled');
        $("#id-ORDER-" + data.id).attr('disabled', false);
        $("#id-OUT-" + data.id).addClass('disabled');
        $("#id-SUP-IN-" + data.id).hide();
        $("#id-SUP-IN-" + data.id).removeClass('disabled');
        $("#id-SUP-OUT-" + data.id).removeClass('hide');
        $("#id-SUP-OUT-" + data.id).show();
        $("#id-OO-IN-" + data.id).removeClass('disabled');

    } else if (data.log == 4) {
        $("#id-IN-" + data.id).removeClass('disabled');
        $("#id-ORDER-" + data.id).addClass('disabled');
        $("#id-OUT-" + data.id).addClass('disabled');
        $("#id-SUP-IN-" + data.id).removeClass('disabled');
        $("#id-SUP-IN-" + data.id).removeClass('hide');
        $("#id-SUP-IN-" + data.id).show();
        $("#id-SUP-OUT-" + data.id).hide()
        $("#id-OO-IN-" + data.id).removeClass('disabled');

    } else if (data.log == 5) {
        $("#id-IN-" + data.id).addClass('disabled');
        $("#id-ORDER-" + data.id).addClass('disabled');
        $("#id-OUT-" + data.id).addClass('disabled');
        $("#id-SUP-IN-" + data.id).addClass('disabled');
        $("#id-OO-IN-" + data.id).hide();
        $("#id-OO-OUT-" + data.id).removeClass('hide');

    } else if (data.log == 6) {
        $("#id-IN-" + data.id).removeClass('disabled');
        $("#id-ORDER-" + data.id).addClass('disabled');
        $("#id-OUT-" + data.id).addClass('disabled');
        $("#id-SUP-IN-" + data.id).removeClass('disabled');
        $("#id-SUP-IN-" + data.id).removeClass('hide');
        $("#id-OO-IN-" + data.id).removeClass('hide');
        $("#id-OO-IN-" + data.id).show();
        $("#id-OO-OUT-" + data.id).hide();
    }
    $('#myModal').modal('hide');
    $("#img-" + data.id + " h1").remove();
    $("#alert-" + data.id).removeClass('alert-sucess alert-info alert-warning alert-primary alert-danger');
    $("#div-" + data.id).before(data.img);
    $("#alert-" + data.id).addClass('alert-' + data.alerta);
    $("#alert-" + data.id).text(data.msg);
}

function validHabitacion() {
    if ($("#HabLog_id_cliente").val() === "") {
        alert("Debe Seleccionar un Cliente");
        $("#HabLog_id_cliente").focus();
        return false;
    }
    if ($("#fechahora").val() === "") {
        alert("Debe Especficar la fecha y hora de entrega de la Habitación");
        $("#fechahora").focus();
        return false;
    }
    if ($("#horarios").val() === "") {
        alert("Debe Seleccionar una Tarifa");
        $("#horarios").focus();
        return false;
    }

    return true;
}

function redirec(Url, obj, id) {
    window.location = getHomeUrl() + Url + obj;
}

function updateShopCart() {
    $.ajax({
        url: getHomeUrl() + 'producto/pedido/updateShopCart',
        type: "POST",
        success: function (data, textStatus, jqXHR)
        {
            $("#div-producto").html(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function updateItems(id) {
    $.ajax({
        url: getHomeUrl() + 'producto/cuenta/updateItems/id/' + id,
        type: "POST",
        success: function (data, textStatus, jqXHR)
        {
            $("#div-producto").html(data).fadeOut().fadeIn();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function updateItemsEmpleado(id) {
    $.ajax({
        url: getHomeUrl() + 'producto/CuentaEmpleado/updateItems/id/' + id,
        type: "POST",
        success: function (data, textStatus, jqXHR)
        {
            $("#div-producto").html(data).fadeOut().fadeIn();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function deletePedDetalle(data) {
    if (data.resp === 1) {
        $('#div-PedDetalle-' + data.id).hide('highlight').remove();
    }
    $('#myModal2').modal('hide');
}

function deletePedidoProducto(idDetalle) {
//    $('#procesando').removeClass('hidden');
    $('#pedido-detalle').val(idDetalle);
    $("#myModal2").modal("show");
}
function deleteCuenta(idCueDetalle, idCuenta, idProducto) {
    $('#idCueDetalle').val(idCueDetalle);
    $('#idCuenta').val(idCuenta);
    $('#idProducto').val(idProducto);
    $("#myModal2").modal("show");
}

function validPedDetalle() {
    var resp = false;
    $('#procesando').removeClass('hidden');
    $("input.number").each(function (index) {
        $('#alert_' + $(this).attr('id')).addClass('hide');
        if ($(this).val() <= 0) {
            $(this).addClass('error');
            $('#alert_' + $(this).attr('id')).removeClass('hide');
            resp = false;
            $('#procesando').addClass('hidden');
        } else {
            resp = true;
        }
    });
    return resp;

}

function validDisponibilidad(url, obj) {
    console.log(url + '/cantidad/' + $('#' + obj.id).val());
    $.ajax({
        url: url + '/cantidad/' + $('#' + obj.id).val(),
        type: "POST",
        dataType: "json",
        success: function (data, textStatus, jqXHR)
        {
            if (data.resp == 0) {
                $(".modal-body").html(data.msg);
                $("#myModal3").modal("show");
                $('#finalizar').attr('disabled', true);
            } else {
                $('#finalizar').attr('disabled', false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function anularOrden(data) {
    console.log(data);
}

function CajMovCierre() {
    $.ajax({
        url: getHomeUrl() + 'CajMovCierre/Admin/id/' + $('#CajMovCierre_id_caj_movimientos').val(),
        type: "POST",
        dataType: "json",
        beforeSend: function () {
            $('#procesando').removeClass('hidden');
        },
        success: function (data, textStatus, jqXHR)
        {
            $('#procesando').hide();
            $('#cuadre').html(data.cuadre);
            $('#pago').html(data.pago);
            $('#total').html(data.total);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function checkHorario() {
    if ($('#TarHabHorario_nombre').val() == "") {
        $('#TarHabHorario_nombre').val(
                'Desde ' +
                $('#TarHabHorario_dia_desde').va() + ' ' +
                $('#TarHabHorario_hora_desde').val() + ' Hasta ' +
                $('#TarHabHorario_dia_hasta').val() + ' ' +
                $('#TarHabHorario_hora_hasta').val()
                );
        return false;
    } else {
        return true;
    }

}

function validAdicional() {
    var hora = $("#horas").val();
    if ($("#habitacion").val() == "") {
        alert("Debe seleccionar una habitacion, cierre la venta y vuelva a intentar");
        return false;
    }
    if ($("#adicionales").val() == "") {
        alert("Debe seleccionar una tarifa");
        $("#adicionales").focus();
        return false;
    }
    if (hora == "") {
        alert("Debe colocar mínimo una hora");
        $("#horas").val(1);
        $("#horas").focus();
        return false;
    } else if (!$.isNumeric(hora)) {
        alert("No es número valido");
        $("#horas").focus();
        return false;
    } else if (hora <= 0) {
        alert("Debe colocar mínimo una hora");
        $("#horas").val(1);
        $("#horas").focus();
        return false;
    } else if (hora > 99) {
        alert("Debe colocar màximo 2 digitos en la hora");
        $("#horas").focus();
    }
}

function openAdicionalCuenta(url) {
    $.ajax({
        url: getHomeUrl() + url,
        type: "POST",
        beforeSend: function () {
            $('#procesando').removeClass('hidden');
        },
        success: function (data, textStatus, jqXHR)
        {
            $(".modal-body").html(data);
            $("#myModal").modal("show");
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function validInvInicial() {
    var res = true;
    $('#finalizar').attr('disabled', true);
    $('.cantidad').each(function () {
        if (!$.isNumeric(this.value)) {
            $('#' + this.id).after('<div class="alert alert-danger" role="alert">Valor no permito sólo se permiten números</div>');
            res = false;
        } else if (this.value < 0) {
            $('#' + this.id).after('<div class="alert alert-danger" role="alert">Valor no permito debe ser igual o mayor a 0</div>');
            res = false;
        }
    });
    if ($("#almacen").val() === "") {
        $('#almacen').after('<div class="alert alert-danger" role="alert">Debe Selecicionar el Almacén</div>');
        $('#almacen').focus();
        res = false;
    }
    if (!res) {
        alert('Favor revisar la información sumistrada hay datos incorrectos');
        $('#finalizar').attr('disabled', false);
    } else {
        alert('Por favor esperar hasta que el sistema temriné de procesar clic en OK o Aceptar para continuar');
    }
    return res;
}
function validInvTraspaso() {
    var res = true;
    $('#finalizar').attr('disabled', true);
    $('.cantidad').each(function () {
        var valor = parseInt(this.value);
        var max = parseInt(this.max);
//        if(!$.isNumeric(valor)){
//            $('#'+this.id).after('<div class="alert alert-danger" role="alert">Valor no permito sólo se permiten <b>números</b></div>');
//            res = false;
//        }else 
        if (valor < 0) {
            $('#' + this.id).after('<div class="alert alert-danger" role="alert">Valor no permito debe ser igual o mayor a <b>0</b></div>');
            res = false;
        }
        if (valor > max) {
            $('#' + this.id).after('<div class="alert alert-danger" role="alert">Valor máximo para traspasar es <b>' + max + '</b> y ud colocó ' + valor + '</div>');
            res = false;
        }
    });
    if ($("#almacenDesde").val() === "") {
        $('#almacenDesde').after('<div class="alert alert-danger" role="alert">Debe Selecicionar el Almacén de Origen</div>');
        $('#almacenDesde').focus();
        res = false;
    }
    if ($("#almacenHasta").val() === "") {
        $('#almacenHasta').after('<div class="alert alert-danger" role="alert">Debe Selecicionar el Almacén Destino</div>');
        $('#almacenHasta').focus();
        res = false;
    }

    if (!res) {
        alert('Favor revisar la información sumistrada hay datos incorrectos');
        $('#finalizar').attr('disabled', false);
    } else {
        alert('Por favor esperar hasta que el sistema temriné de procesar clic en OK o Aceptar para continuar');
    }

    return res;
}
function validInvAjuste() {
    var res = true;
    $('#finalizar').attr('disabled', true);
    $('.cantidad').each(function () {
        var valor = parseInt(this.value);
        var max = parseInt(this.max);
//        if(!$.isNumeric(valor) && valor !== null ){
//            $('#'+this.id).after('<div class="alert alert-danger" role="alert">Valor no permito sólo se permiten <b>números</b></div>');
//            res = false;
//        }else

        if (valor < 0) {
            $('#' + this.id).after('<div class="alert alert-danger" role="alert">Valor no permito debe ser igual o mayor a <b>0</b></div>');
            res = false;
        }
        if (valor > max) {
            $('#' + this.id).after('<div class="alert alert-danger" role="alert">Valor máximo para traspasar es <b>' + max + '</b> y ud colocó ' + valor + '</div>');
            res = false;
        }
    });
    if ($("#almacen").val() === "") {
        $('#almacen').after('<div class="alert alert-danger" role="alert">Debe Selecicionar el Almacén de Origen</div>');
        $('#almacen').focus();
        res = false;
    }
    if ($("#justifiacion").val() === "") {
        $('#justifiacion').after('<div class="alert alert-danger" role="alert">Debe dar una Justifiación</div>');
        $('#justifiacion').focus();
        res = false;
    }

    if (!res) {
        alert('Favor revisar la información sumistrada hay datos incorrectos');
        $('#finalizar').attr('disabled', false);
    } else {
        alert('Por favor esperar hasta que el sistema temriné de procesar clic en OK o Aceptar para continuar');
    }

    return res;
}

