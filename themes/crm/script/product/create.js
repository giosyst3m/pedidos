function getAttr(obj){
    $.ajax(
    {
        url : getHomeUrl()+'product/getAttr/id/'+obj.value,
        type: "POST",
        dataType:'html',
        data : {id: obj.value},
        beforesend:function(data, textStatus, jqXHR){
            $('#proAttr').html('ini');
        },
        success:function(data, textStatus, jqXHR) 
        {
            $('#proAttr').html(data);
        },
        complete: function(){
            initFields();
    
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
//            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
//                type: "danger"
//            }); 
        }
    });
}

function validateForm(){
    $.each($('#proAttr div.form-group div .required '), function(idx, obj) {
        if(obj.value==='' || !$(obj).is(':checked' )){
            $(obj).parents('div.form-group ').addClass('has-error');
            //return false;
        }else{
            $(obj).parents('div.form-group ').addClass('has-success');
        }
    });
}