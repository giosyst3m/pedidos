/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


  jQuery(document).ready(function($) {
 
        $('#myCarousel').carousel({
                interval: 5000
        });
 
        $('#carousel-text').html($('#slide-content-0').html());
 
        //Handles the carousel thumbnails
       $('[id^=carousel-selector-]').click( function(){
            var id = this.id.substr(this.id.lastIndexOf("-") + 1);
            var id = parseInt(id);
            $('#myCarousel').carousel(id);
        });
 
 
        // When the carousel slides, auto update the text
        $('#myCarousel').on('slid.bs.carousel', function (e) {
                 var id = $('.item.active').data('slide-number');
                $('#carousel-text').html($('#slide-content-'+id).html());
        });
});

$('.btn-cart').on('click', function () {
    var id_product = ($(this).attr('id'));
    var id_client = $('#id_client').val();
    $.ajax({
        url: getHomeUrl() + 'order/createOrder/',
        type: "post",
        dataType: "json",
        data: {
            id_product: id_product,
            id_client: id_client,
            quantity: $('#quantity').val(),
            origin: 4,
        },
        success: function (data, textStatus, jqXHR)
        {
            console.log(data);
            $.growl(data.menssage, {
                type: data.type
            });
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
});

