$( document ).ready(function() {
    console.log(1);
    console.log($('.tipo_prod').val());
    getAttr2();
});


function getAttr2(){
    
    $.ajax(
    {
        url : getHomeUrl()+'product/getAttr/id/'+$('.tipo_prod').val()+'/product/'+$('.id_pro').val(),
        type: "POST",
        dataType:'html',
        beforesend:function(data, textStatus, jqXHR){
            $('#proAttr').html('ini');
        },
        success:function(data, textStatus, jqXHR) 
        {
            $('#proAttr').html(data);
        },
        complete: function(){
            $('.selectpicker').selectpicker();
            
            $('.datetimepicker').datetimepicker({
                 language: "es",
            });
            $('.timepicker').datetimepicker({
                format: 'hh:mm A',
                language: "es",
                pickDate: false,
                pickSeconds: true,
                pick12HourFormat: false,
                use24hours: true, 
            });
            $('.date').datetimepicker({
                format: 'MM/DD/YYYY',
                language: "es",
                pickDate: true,
                pickTime:false,
                pickSeconds: false,
                pick12HourFormat: false,
                use24hours: false, 
            });
    
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
//            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
//                type: "danger"
//            }); 
        }
    });
}