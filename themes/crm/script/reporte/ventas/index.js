    
jQuery(document).ready(function(){

    jQuery("#grid").jqGrid('jqPivot', 
        getHomeUrl()+'/reporte/ventas/getData/desde/'+$('#date_from').val()+'/hasta/'+$('#date_to').val()+'/grupo/'+$('#grupo').val(),
        // pivot options
        {
            xDimension : [
                {
                    dataName:'grupo',
                    label:'Grupos',
                    width:15,
                },
                {
                    dataName: 'caja',
                    label:'Sub-Grupo',
                    width:80,
                    
                    
                }
            ],
            yDimension : [{dataName: 'agrupacion'}],
            aggregates : [
                { 
                    member : 'total', 
                    aggregator : 'sum', 
                    width:40, 
                    label:'Suma', 
                    formatter:'number', 
                    align:'right'
                }
            ],
            rowTotals:true,
            colTotals:true,
            groupSummary:true,
            
            frozenStaticCols : true,
            groupSummaryPos :  'footer'
            
        }, 
        // grid options
        {
            width: 1200,
            height:300,
            rowNum : 150,
            pager: "#pager",
            caption: "Reportes de Ventas",
            root: "rows",
        }
    );
    
});