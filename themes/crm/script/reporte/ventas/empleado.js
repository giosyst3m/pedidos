    
jQuery(document).ready(function(){

    $("#grid").jqGrid('jqPivot', 
        getHomeUrl()+'/reporte/ventas/getData/desde/'+$('#date_from').val()+'/hasta/'+$('#date_to').val()+'/grupo/'+$('#grupo').val()+'/rep/3',
        // pivot options
        {
            xDimension : [{dataName:'nombre',label:'Nombre y Apellido',width:30},{dataName:'grupo',label:'Ventas',width:80} ],
            yDimension : [{dataName: 'agrupacion'}],
            aggregates : [
                { 
                    member : 'total', 
                    aggregator : 'sum', 
                    width:40, 
                    style:'font-size:10px',
                    label:'Suma', 
                    formatter:'number', 
                    align:'right'
                }
            ],
            rowTotals:true,
            colTotals:true,
            groupSummary:true,
            groupSummaryPos:'footer',
//            
//            frozenStaticCols : true,
//            groupSummaryPos :  'footer'
        }, 
        // grid options
        {
            width: 1200,
            rowNum : 150,
            height:300,
            pager: "#pager",
            caption: "Reportes de Ventas de Empleados",
        }
    );
    
});