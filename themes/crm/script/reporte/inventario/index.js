    
jQuery(document).ready(function(){
    
    reporteInventarioResumen(jQuery('#almacenes').selectpicker('val'));
});

function reporteInventarioResumen(almacenes){
    $('#datos').html('<div id="report" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">'+
        '<table id="grid" class="table table-responsive" ></table>'+
        '<div id="pager"></div>'+
   ' </div>');
    jQuery("#grid").jqGrid('jqPivot', 
        getHomeUrl()+'/reporte/inventario/getData/almacenes/'+$('#almacenes').selectpicker('val'),
        // pivot options
        {
            xDimension : [{dataName:'nombre',width:200} ],
            yDimension : [{dataName:'alm_nombre',width:300}],
            aggregates : [
                { 
                    member : 'total', 
                    aggregator : 'sum', 
                    width:100, 
                    style:'font-size:8px',
                    label:'Suma', 
                    formatter:'integer', 
                    align:'right'
                }
            ],
            rowTotals: true,
            colTotals: true,
            groupSummaryPos :  'footer',
            frozenStaticCols : true
        }, 
        // grid options
        {
            width: 1200,
            height: 500,
            rowNum : 1500000000000000,
            pager: "#pager",
            caption: "Reporte Resumen de Iventario de Productos",
            root: "rows",
        }
    );
}