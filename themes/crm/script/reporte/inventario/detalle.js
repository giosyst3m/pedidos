function getProductosPorAlmacen(obj) {
    $.ajax({
        url: getHomeUrl() + 'producto/producto/getProductosPorAlmacen/id_almacen/' + obj.value,
        type: "POST",
        dataType: 'json',
        beforeSend: function () {

        },
        success: function (data, textStatus, jqXHR)
        {
            var $select = $('#productos');
            $select.find('option').remove();
            $select.append('<option value="">Seleccionar</option>');
            $.each(data, function (key, obj)
            {
                $select.append('<option value=' + obj.id + '>' + obj.producto + '</option>');
            });
            $('#productos').selectpicker('refresh');
        },
        complete: function () {

        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}

function reporteInventarioDetalle(almacen, producto) {
    $('#datos').html('<div id="report" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">' +
            '<table id="grid" class="table table-responsive" ></table>' +
            '<div id="pager"></div>' +
            ' </div>');
    jQuery("#grid").jqGrid('jqPivot', 
        getHomeUrl()+'/reporte/inventario/getDataDetalle/id_almacen/'+almacen+'/id_producto/'+producto,
        // pivot options
        {
            xDimension : [{dataName:'r_c_d',width:50,label:'Fecha'},
                {dataName:'detalle',label:'Detalle'}
            ],
            yDimension : [{dataName:'almacen_nombre',width:300}],
            aggregates : [
                { 
                    member : 'cantidad', 
                    aggregator : 'sum', 
                    width:100, 
                    style:'font-size:8px',
                    label:'Suma', 
                    formatter:'integer', 
                    align:'right'
                }
            ],
//            rowTotals: true,
            colTotals: true,
            groupSummaryPos :  'footer',
            frozenStaticCols : true
        }, 
        // grid options
        {
            width: 1200,
            height: 500,
            rowNum : 150000000000000,
            pager: "#pager",
            caption: "Reporte Resumen de Iventario de Productos",
            root: "rows",
        }
    );
}