    
jQuery(document).ready(function(){

    jQuery("#grid").jqGrid('jqPivot', 
        getHomeUrl()+'/reporte/pagos/getData/desde/'+$('#date_from').val()+'/hasta/'+$('#date_to').val()+'/grupo/'+$('#grupo').val()+'/rep/2',
        // pivot options
        {
            xDimension : [{dataName:'nombre_completo',label:'Nombre y Apellido'} ],
            yDimension : [{dataName:'agrupacion'}],
            aggregates : [
                { 
                    member : 'total', 
                    aggregator : 'sum', 
                    width:40, 
                    style:'font-size:10px',
                    label:'Suma', 
                    formatter:'number', 
                    align:'right'
                }
            ],
            rowTotals: true,
            colTotals: true,
            groupSummaryPos :  'footer'
        }, 
        // grid options
        {
            width: 1200,
            rowNum : 150,
            pager: "#pager",
            caption: "Reportes de Pagos Recibidos",
            root: "rows",
        }
    );
});