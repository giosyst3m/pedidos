/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function getFormDescuento(id,tipo){
    $('#modal-descuento').modal('show');
    $('.id_habitacion').val(id); 
    if(tipo == 0){
        getTotal(id);
    }else{
        getTotalEmpleado(id);
    }
}

function validDescuento(){
    $.growl(false, {
        z_index:1080
    });
    var valor = parseInt($('#CueDescuento_valor').val());
    var valor_max = parseInt($('#valor_maximo2').val());
    if(valor<=0){
        $.growl("El valor debe ser mayor a 0", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if(!$.isNumeric(valor)){
         $.growl("Debe colocar un valor y pude ser mayor al indicado", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if($('#CueDescuento_concepto').val()==""){
        $.growl("Debe Colocar un Concepto o motivo por el cual esta aplicando el descuento", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if(valor > valor_max){
        $.growl("Favor revisar esta colocando un valor a lo permitido: "+$('#ValorMax').text(), {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else{
        $('.form-group').removeClass('has-error');
        $('.form-group').addClass('has-success');
        $('#modal-descuento').modal('hide');
        return true;
    }
}

$(document).on("submit","#cue-descuento-form",function(e) {

    var postData = $(this).serializeArray();
    $.ajax(
    {
        url : getHomeUrl()+'Dashboard/saveDescuento',
        type: "POST",
        dataType:'json',
        data : postData,
        beforesend:function(data, textStatus, jqXHR){
            
        },
        success:function(data, textStatus, jqXHR) 
        {
            if(data.r == 0){
                $('#modal-descuento').modal('show');
                $.growl(data.m, {
                    type: "danger"
                });
            }else if(data.r == 2){
                $.growl(data.m, {
                    type: "danger"
                });
            }else{
                $.growl(data.m, {
                    type: "success"
                });
                
            }
        },
        complete: function(){
            getTotales();
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
    e.stopPropagation();
    e.preventDefault();
});