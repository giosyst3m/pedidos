function getCatalogoCuenta(id){
     $.growl(false, {
        z_index:1080,
        
    });
    if(id==0){
        id= $('.id_habitacion').val();
        var url = getHomeUrl()+'producto/producto/CatalogoCuentas/id/'+id+'/categoria/0/producto/0/almacenSeleccionado/1/json/1';
        var destino = 0;
    }else{
        var destino = id;
        var url = getHomeUrl()+'producto/producto/CatalogoEmpleado/id/'+id+'/categoria/0/producto/0/almacenSeleccionado/1/json/1';
    }
    var btn = $('.getCatalogoCuenta');
    $.ajax({
       url : url,
       type: "POST",
       dataType:'json',
       beforeSend:function(){
           $.growl("Se han comenzado a cargar las Catalogo", {
                    type: "info"
            });            
            btn.button('loading');
       },
       success: function(data, textStatus, jqXHR)
       {
           $('#modal-catalogo').modal('show');
           
           var html = '<input type="hidden" name="almacen" id="almacen" value="'+data.almacen+'">';
           html = html + '<ul>';
           $.each(data.almacenes, function(idx, obj) {
                html = html + '<li>Almacen: <b>'+obj+'</b></li>';
           });
           html = html + '</ul>';
           html = html + '<div class="row">';
           var i = 0;
                $.each(data.datos, function(idx, obj) {
                    if(i==0){
                        html = html + '<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">';
                    }
                        html = html + '<div class="panel panel-primary panel-productos" id="panel-'+obj.id+'">';
                            html = html + '<div class="panel-heading" style="width:100%;height:50px; dsiplay:block">'+obj.nombre+'</div>';
                            html = html + '<div class="panel-body">';
                                html = html + '<label class="label label-primary" style="float: right  !important;position: relative; font-size: large " id="existencia-'+obj.id+'">'+obj.total+'</label>';
                                html = html + '<div class="" style="width:100px;height:100px; dsiplay:block" >';
                                    if(obj.imagen == null || obj.imagen == ""){
                                        html = html + '<img src="'+data.ruta_sin_imagen+'" class="img-responsive" alt="" width="100px" height="100px" >';
                                    }else{
                                        html = html + '<img src="'+data.ruta_imagen+'/'+obj.imagen+'" class="img-responsive" alt="" width="100px" height="100px">';
                                    }
                                html = html + '</div>';
                                html = html + '<label class="label label-default" style="float: right  !important;position: relative; font-size:medium"><i class="fa fa-usd"></i>'+obj.valor+'</label>';
                                html = html + '<input type="number" class="text-right col-xs-12" value="1" id="cantidad-'+obj.id+'"/>';
                            html = html + '</div>';
                            html = html + '<div class="panel-footer">';
                                html = html + '<button class="btn btn-block btn-success" data-loading-text="Cargando ..." onclick="addProductoCuenta('+obj.id+','+destino+',this);">Agregar</button>';
                            html = html + '</div>';
                        html = html + '</div>';
                    if(i==1){
                        html = html + '</div>';
                        i=0;
                    }else{
                        i++;
                    }
                 });
            html = html + '</div>';
            $('#catalogo').html(html);
       },
       complete: function(){
           $.growl("Se cargo el Catalogo", {
                    type: "success"
            });
            btn.button('reset');
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown+jqXHR, {
                type: "danger"
            }); 
       }
    });
}

function addProductoCuenta(id,destino,obj){
    var existencia =parseInt($('#existencia-'+id).text());
    var cantidad = parseInt($('#cantidad-'+id).val());
    if(cantidad > existencia){
        $.growl("la cantidad solicitada no esta disponble, puede agregar hasta: "+existencia, {
            type: "danger"
        }); 
        $('#cantidad-'+id).addClass('has-error');
    }else{
        if(destino === 0){
            var url = getHomeUrl()+'producto/cuenta/AddProducto/habitacion/'+$('.id_habitacion').val()+'/producto/'+id+'/cantidad/'+cantidad+'/almacen/'+$('#almacen').val();
        }else{
//            $('#almacen').val(1);
            var url = getHomeUrl()+'producto/cuentaEmpleado/AddProducto/habitacion/'+destino+'/producto/'+id+'/cantidad/'+cantidad+'/almacen/'+$('#almacen').val();;
        }
        var btn = $(obj);
        $.ajax({
           url : url,
           type: "POST",
           dataType:'json',
           beforeSend:function(){
               btn.button('loading');
           },
           success: function(data, textStatus, jqXHR)
           {
               $.growl(data.info, {
                        type: "infos"
                });
               getProductoExistencia(id);
           },
           complete: function(){
               btn.button('reset');
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown+jqXHR, {
                    type: "danger"
                }); 
           }
        });
    }
}
function getProductoExistencia(id){
    $.ajax({
       url : getHomeUrl()+'producto/producto/getProductoExistencia/idProducto/'+id+'/idAlmacen/'+$('#almacen').val(),
       type: "POST",
       dataType:'json',
       beforeSend:function(){

       },
       success: function(data, textStatus, jqXHR)
       {
           $('#existencia-'+id).html(data);
           
       },
       complete: function(){

       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown+jqXHR, {
                type: "danger"
            }); 
       }
    }); 
}
function getProductoPorNombre(){
    $.ajax({
       url : getHomeUrl()+'producto/producto/getProductoPorNombre/idAlmacen/'+$('#almacen').val()+'/key/'+$('#texto').val(),
       type: "POST",
       dataType:'json',
       beforeSend:function(){

       },
       success: function(data, textStatus, jqXHR)
       {
           console.log(data);
//           if(data.length > 0){
                $('.panel-productos').addClass('hide');
                $.each(data.productos, function(idx, obj) {
                    $('#panel-'+obj.id).removeClass('hide');
                });
//            }
           
       },
       complete: function(){

       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown+jqXHR, {
                type: "danger"
            }); 
       }
    }); 
}