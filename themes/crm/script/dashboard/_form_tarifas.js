/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function getFormTarifas(id){
    $('#modal-tarifas').modal('show');
    $('#modal-cuenta').modal('hide');
    $('#idTarfaCuenta').val(id);
    $.ajax(
    {
        url : getHomeUrl()+'Dashboard/getHabitacionTarifas/idHabitacion/'+$('.id_habitacion').val()+'/adicionales/0',
        type: "POST",
        dataType:'json',
        beforesend:function(data, textStatus, jqXHR){
            
        },
        success:function(data, textStatus, jqXHR) 
        {
            $('#tarifa')
            .find('option')
            .remove()
            .end();
            $.each(data, function(i, obj) {
                $('#tarifa').append($('<option>').text(obj.nombre).attr('value', obj.id));
            });
            $('#tarifa').selectpicker('refresh');
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
}

function validTarifas(){
    $.growl(false, {
        z_index:1080
    });
     if(!$.isNumeric($('#tarifa').val())){
        $.growl("Debe Seleccionar una tarifa para cambiarla", {
            type: "danger"
        });
    }else{
        $('.form-group').removeClass('has-error');
        $('.form-group').addClass('has-success');
        $('#modal-tarifas').modal('hide');
        changeTarifaCuenta();
    }
}

function changeTarifaCuenta(){
    $.ajax(
    {
        url : getHomeUrl()+'Dashboard/chageTarifacuenta/id/'+$('#idTarfaCuenta').val()+'/tarifa/'+$('#tarifa').val()+'/motivo/'+$('#changeMotivo').val(),
        type: "POST",
        dataType:'json',
        beforesend:function(data, textStatus, jqXHR){
            
        },
        success:function(data, textStatus, jqXHR) 
        {
            if(data.r == 1){
                getCuenta($('.id_habitacion').val());
                $.growl(data.m, {
                    type: "success"
                });
            }else{
                $('#modal-tarifas').modal('show');
                $.growl(data.m, {
                    type: "danger"
                });
                
            }
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
}

