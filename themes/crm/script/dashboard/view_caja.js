    
function getCajaDetalle(caja,caja_destino){
    var btn = $('.getCajaDetalle');
    $.ajax({
       url : getHomeUrl()+'CajMovCierre/CierreDetalle/caja/'+caja,
       type: "POST",
       dataType:'html',
       beforeSend:function(){
           $.growl("Se han comenzado a cargar la caja", {
                    type: "info"
            });
            btn.button('loading');
            $('#'+caja_destino).html('<i class="fa fa-refresh fa-spin fa-5x"></i>');
       },
       success: function(data, textStatus, jqXHR)
       {
           $('#'+caja_destino).html(data);
           
       },
       complete: function(){
           $.growl("Se han cargado la caja correctamente", {
                    type: "success"
            });
            btn.button('reset');
            getCajaApertura();
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}

function getCajaApertura(){
    
    $.ajax({
       url : getHomeUrl()+'CajMovimientos/getCajaApertura',
       type: "POST",
       dataType:'html',
       beforeSend:function(){
           $('#apertura').html('<i class="fa fa-refresh fa-spin"></i>');
       },
       success: function(data, textStatus, jqXHR)
       {
           $('#apertura').html(data);
           
       },
       complete: function(){
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}