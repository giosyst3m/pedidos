/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function getFormHora(id){
    $('#modal-hora').modal('show');
    $('.selectpicker').selectpicker();
    var currentDate = new Date();
    var day = currentDate.getDate();
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear();
    var hour = currentDate.getHours();
    var minute = currentDate.getMinutes();
    $('.datetimepicker').datetimepicker({
        minDate: month + '/' + day + '/' + year,
        defaultDate: month + '/' + day + '/' + year + ' ' + hour + ':' + minute,
    });
    $('.id_habitacion').val(id); 
}

function validHora(){
    $.growl(false, {
        z_index:1080
    });
    dato = $('#fechahora').val();
    if(dato == ''){
        $.growl("El valor es requerido", {
            type: "danger"
        });
        $('#fechahora').addClass('has-error');
    }else{
        $('#fechahora').removeClass('has-error');
        $('#fechahora').addClass('has-success');
        $('#modal-hora').modal('hide');
        return true;
    }
}

$(document).on("submit","#hora-form",function(e) {

    var postData = $(this).serializeArray();
    $.ajax(
    {
        url : getHomeUrl()+'Dashboard/updateHora',
        type: "POST",
        dataType:'json',
        data : postData,
        beforesend:function(data, textStatus, jqXHR){
             
        },
        success:function(data, textStatus, jqXHR) 
        {
            if(data.r == 0){
                $('#modal-hora').modal('show');
                $.growl(data.m, {
                    type: "danger"
                });
            }else if(data.r == 2){
                $.growl(data.m, {
                    type: "danger"
                });
            }else{
                $.growl(data.m, {
                    type: "success"
                });
                
            }
        },
        complete: function(){
            getEmpleadoSaldos();
            getTotales();
           
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
    e.stopPropagation();
    e.preventDefault();
});