function getCuenta(id,cuenta){
    var btn = $('.getCuenta');
    $.ajax({
       url : getHomeUrl()+'Dashboard/getCuenta/idHabitacion/'+id+'/cuenta/'+cuenta,
       type: "POST",
       dataType:'json',
       beforeSend:function(){
           btn.button('loading');
       },
       success: function(data, textStatus, jqXHR)
       {   
           if(data.r == 1){
               $('#modal-cuenta').modal('show');
               var html ='<div class="panel panel-info">'+
                            '<div class="panel-heading">'+
                                '<div class="panel-title">'+
                                    '<div class="row">'+
                                        '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2"><h5>Item</h5></div>'+
                                        '<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"><h5>Producto</h5></div>'+
                                        '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right"><h5>Unidades</h5></div>'+
                                        '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right"><h5>Precio</h5></div>'+
                                        '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right"><h5>Total</h5></div>'+
                                        '<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 text-center"><h5>Acción</h5></div>'+
                                    '</div>'+
                                '</div>'+   
                            '</div>'
                            '<div class="panel-body">';
                $.each(data.detalle, function(idx, obj) {
                    html = html + '<div class="row" id="div-detalle-'+obj.id+'">';
                        html = html + '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">';
                        if(obj.imagen == null || obj.imagen == ""){
                            html = html + '<img src="'+data.ruta_sin_imagen+'" class="img-responsive" alt="" width="100px" height="100px" >';
                        }else{
                            html = html + '<img src="'+data.ruta_imagen+'/'+obj.imagen+'" class="img-responsive" alt="" width="100px" height="100px">';
                        }
                        html = html + '</div>';
                        html = html + '<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">'+
                                            '<h4 class="product-name"><b>'+obj.producto_nombre+'</b></h4>'+
                                            '<h4><small>'+obj.referencia+'</small></h4>'+
                                            '<h4><small>'+obj.r_c_d+'</small></h4>'+                                        
                                            '<h4><small>Usuario: <b>'+obj.nombre_completo+'</b></small></h4>'+                                        
                                        '</div>';
                        html = html + '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right">'+
                                            '<h4>'+obj.cantidad+'</h4>'+
                                        '</div>';
                        html = html + '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right">'+
                                            '<h4>'+obj.valor+'</h4>'+
                                        '</div>';
                        html = html + '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right">'+
                                        '<h4>'+obj.total+'</h4>'+
                                        '</div>';
                        html = html + '<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 text-center">';
                        if(obj.abierta==1){
                            if(data.rcu == obj.r_c_u){
                                if(obj.tipo == 6){
                                    html = html + '<button class="btn btn-warning" onclick="getFormTarifas('+obj.id+')"><span class="glyphicon glyphicon-transfer"></span></button>';
                                }else if(obj.tipo==1){
                                    html = html + '<button class="btn btn-danger" onclick="getFromDeleteProducto('+obj.id+','+obj.id_cuenta+','+obj.id_producto+',0)"><span class="glyphicon glyphicon-trash"></span></button>';
                                }else if(obj.tipo == 5){
                                    html = html + '<button class="btn btn-danger" onclick="getFromDeleteAdicional('+obj.id+')"><span class="glyphicon glyphicon-trash"></span></button>';
                                }else if(obj.tipo == 2){
                                    html = html + '<button class="btn btn-danger" onclick="getFromDeleteHabitacion('+obj.id+')"><span class="glyphicon glyphicon-trash"></span></button>';
                                }else if(obj.tipo == 4){
                                    html = html + '<button class="btn btn-danger" onclick="getFromDeleteDescuento('+obj.id+')"><span class="glyphicon glyphicon-trash"></span></button>';
                                }else if(obj.tipo == 3){
                                    html = html + '<button class="btn btn-danger" onclick="getFromDeletePago('+obj.id+')"><span class="glyphicon glyphicon-remove"></span></button>';
                                }else{
                                    html = html + '<button class="btn"><span class="glyphicon glyphicon-trash"></span></button>';
                                }
                            }
                        }
                        html = html +'</div>';
                    html = html + '</div>';
                });
                html = html + '<div class="panel-footer" id="cuenta_footer"><i class="fa fa-refresh fa-spin fa-5x"></i></div>';  
                html = html + '</div>';
                                    
                $('#factura').html(html); 
                getCuentaFooter($('#id_habitacion').val(),cuenta);
            }else{
               $.growl(data.m, {
                    type: "info"
            }); 
            }
       },
       complete: function(){
           $.growl("Se actualizaron el Tiempo transcurido", {
                    type: "info"
            });
            btn.button('reset');
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}

function getCuentaFooter(id,cuenta){
    $.ajax({
       url : getHomeUrl()+'Dashboard/getCuentaFooter/idHabitacion/'+id+'/cuenta/'+cuenta,
       type: "POST",
       dataType:'json',
       beforeSend:function(){
       },
       success: function(data, textStatus, jqXHR)
       {
           $('#cuenta_footer').html(data.totales);
            
       },
       complete: function(){
           $.growl("Se cargo el Total del Factura ", {
                    type: "info"
            });
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}

function getFromDeleteProducto(id,id_cuenta,id_producto,tipo){
    $('#deleteProductoCuenta').val(id_cuenta);
    $('#deleteProductoID').val(id);
    $('#deleteProductoIDProducto').val(id_producto);
    $('#modal-cuenta').modal('hide');
    $("#modal-delete-producto").modal("show");
    $('#deleteProductoTipo').val(tipo);
}


function validDeleteProducto(){
    $("#modal-delete-producto").modal("hide");
    
    if($('#deleteProductoTipo').val()==0){
        var url = getHomeUrl()+'producto/cuenta/deleteProducto/idCueDetalle/'+$('#deleteProductoID').val()+'/idCuenta/'+$('#deleteProductoCuenta').val()+'/idProducto/'+$('#deleteProductoIDProducto').val()+'/motivo/'+$('#deleteProductoMotivo').val()
    }else{
        var url = getHomeUrl()+'producto/cuentaEmpleado/deleteProducto/idCueDetalle/'+$('#deleteProductoID').val()+'/idCuenta/'+$('#deleteProductoCuenta').val()+'/idProducto/'+$('#deleteProductoIDProducto').val()+'/motivo/'+$('#deleteProductoMotivo').val()
    }
    $.ajax({
       url : url,
       type: "POST",
       dataType:'json',
       beforeSend:function(){
       },
       success: function(data, textStatus, jqXHR)
       {
            $('#modal-cuenta').modal('show');
            $('#div-detalle-'+data.id).hide('slow');
       },
       complete: function(){
           $.growl("Se Eliminó el producto requerdido", {
                    type: "success"
            });
        if($('#deleteProductoTipo').val()==0){
            getCuentaFooter($('#id_habitacion').val());
        }else{
            getCuentaEmpleadoFooter($('#deleteProductoTipo').val());
        }
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}

function getFromDeleteAdicional(id){
    $('#deleteAdicionalID').val(id);
    $('#modal-cuenta').modal('hide');
    $("#modal-delete-adicional").modal("show");
}

function validDeleteAdicional(){
    $("#modal-delete-adicional").modal("hide");
    $.ajax({
       url : getHomeUrl()+'producto/Adicional/deleteAdicional/idCueAdicional/'+$('#deleteAdicionalID').val()+'/motivo/'+$('#deleteAdicionalMotivo').val(),
       type: "POST",
       dataType:'json',
       beforeSend:function(){
       },
       success: function(data, textStatus, jqXHR)
       {
            $('#modal-cuenta').modal('show');
            $('#div-detalle-'+data.id).hide('slow');
            
       },
       complete: function(){
           $.growl("Se Eliminó el Adicional requerdido", {
                    type: "success"
            });
            getCuentaFooter($('#id_habitacion').val());
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}
function getFromDeleteHabitacion(id){
    $('#deleteHabitacionID').val(id);
    $('#modal-cuenta').modal('hide');
    $("#modal-delete-habitacion").modal("show");
}

function validDeleteHabitacion(){
    $("#modal-delete-habitacion").modal("hide");
    $.ajax({
       url : getHomeUrl()+'producto/AdicionalHabitacion/deleteCuentaTarifarioHabitacion/id/'+$('#deleteHabitacionID').val()+'/motivo/'+$('#deleteHabitacionMotivo').val(),
       type: "POST",
       dataType:'json',
       beforeSend:function(){
       },
       success: function(data, textStatus, jqXHR)
       {
            $('#modal-cuenta').modal('show');
            $('#div-detalle-'+data.id).hide('slow');
            
       },
       complete: function(){
           $.growl("Se Eliminó el Adicional requerdido", {
                    type: "success"
            });
            getCuentaFooter($('#id_habitacion').val());
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}
function getFromDeleteDescuento(id){
    $('#deleteDescuentoID').val(id);
    $('#modal-cuenta').modal('hide');
    $("#modal-delete-descuento").modal("show");
}

function validDeleteDescuento(){
    $("#modal-delete-descuento").modal("hide");
    $.ajax({
       url : getHomeUrl()+'producto/descuento/deleteDescuento/id/'+$('#deleteDescuentoID').val()+'/motivo/'+$('#deleteDescuentoMotivo').val(),
       type: "POST",
       dataType:'json',
       beforeSend:function(){
       },
       success: function(data, textStatus, jqXHR)
       {
            $('#modal-cuenta').modal('show');
            $('#div-detalle-'+data.id).hide('slow');
            
       },
       complete: function(){
           $.growl("Se Eliminó el Descuento requerdido", {
                    type: "success"
            });
            getCuentaFooter($('#id_habitacion').val());
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}
function getFromDeletePago(id){
    $('#deletePagoID').val(id);
    $('#modal-cuenta').modal('hide');
    $("#modal-delete-pago").modal("show");
}

function validDeletePago(){
    $("#modal-delete-pago").modal("hide");
    $.ajax({
       url : getHomeUrl()+'producto/pago/deletePago/id/'+$('#deletePagoID').val()+'/motivo/'+$('#deletePagoMotivo').val(),
       type: "POST",
       dataType:'json',
       beforeSend:function(){
       },
       success: function(data, textStatus, jqXHR)
       {
           if(data.r == 1){
                $('#modal-cuenta').modal('show');
                $('#div-detalle-'+data.id).hide('slow');
                $.growl(data.m, {
                    type: "success"
                });
                getCuentaFooter($('#id_habitacion').val());
            }else{
                $("#modal-delete-pago").modal("show");
                $.growl(data.m, {
                    type: "danger"
                });
            }
            
       },
       complete: function(){
           $.growl("Se Eliminó el Pago requerdido", {
                    type: "success"
            });
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}


