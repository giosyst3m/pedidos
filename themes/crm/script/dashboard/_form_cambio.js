/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function getFormCambio(id){
    $.ajax(
    {
        url : getHomeUrl()+'Dashboard/getFormCambio/id/'+id,
        type: "POST",
        dataType:'html',
        beforesend:function(data, textStatus, jqXHR){
             $('#hab_cambio').html('<i class="fa fa-refresh"></i>');
        },
        success:function(data, textStatus, jqXHR) 
        {
            $('#hab_cambio').html(data);
        },
        complete: function(){
           $('#modal-cambio').modal('show');
           $('.selectpicker').selectpicker();
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
}

//function getTarifas(obj){
//    $.ajax(
//    {
//        url : getHomeUrl()+'Dashboard/getHabitacionTarifas/idHabitacion/'+obj.value,
//        type: "POST",
//        dataType:'json',
//        beforesend:function(data, textStatus, jqXHR){
//            
//        },
//        success:function(data, textStatus, jqXHR) 
//        {
//            $('#hab_tarifas')
//            .find('option')
//            .remove()
//            .end();
//            $.each(data, function(i, obj) {
//                $('#hab_tarifas').append($('<option>').text(obj.nombre).attr('value', obj.id));
//            });
//            $('#hab_tarifas').selectpicker('refresh');
//        },
//        complete: function(){
//        },
//        error: function(jqXHR, textStatus, errorThrown) 
//        {
//            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
//                type: "danger"
//            }); 
//        }
//    });
//}


function validCambio(){
    $.growl(false, {
        z_index:20000
    });
    var hab = $('#habitacion_cambio').val();
    var tarifa = $('#hab_tarifas').val();
    if(hab == ''){
        $.growl("Debe seleccionar una Habitación", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
//    }else if(tarifa == ''){
//        $.growl("Debe seleccionar una tarifa", {
//            type: "danger"
//        });
//        $('.form-group').addClass('has-error');
//        return false;
    }else{
        $('.form-group').removeClass('has-error');
        $('.form-group').addClass('has-success');
        $('#modal-cambio').modal('hide');
        return true;
    }
}

$(document).on("submit","#cambio-form",function(e) {

    var postData = $(this).serializeArray();
    $.ajax(
    {
        url : getHomeUrl()+'Dashboard/saveCambioHabitacion',
        type: "POST",
        dataType:'json',
        data : postData,
        beforesend:function(data, textStatus, jqXHR){
             
        },
        success:function(data, textStatus, jqXHR) 
        {
            if(data.r == 0){
                $('#modal-cambio').modal('show');
                $.growl(data.m, {
                    type: "danger"
                });
            }else if(data.r == 2){
                $.growl(data.m, {
                    type: "danger"
                });
            }else{
                $.growl(data.m, {
                    type: "success"
                });
                
            }
        },
        complete: function(){
            getTotales();
            getHabitaciones();
           
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
    e.stopPropagation();
    e.preventDefault();
});