$(window).on('load', function () {
//    window.open(homeUrl+"Dashboard","fs","fullscreen=yes");
    $('.selectpicker').selectpicker();

//    setInterval(function () {getTotales()}, 900000);
    setInterval(function () {getHabitaciones()}, 600000);
//    setInterval(function () {getEmpleadoSaldos()}, 360000);
//    setInterval(function () {getTiempos()}, 600000);
//    setInterval(function () {getCajaDetalle(0,'caja')}, 600000 );
    run();
    
    $.growl(false, {
        z_index:1080,
        
    });
});

function run(){
    getHabitaciones();
    setTimeout(getHabitacionStatusResumen(),30000);
    setTimeout(getEmpleados(),80000);
//    setTimeout(getCajaDetalle(0,'caja'),100000);
}
function getHomeUrl(){
    return $("#homeUrl").val();
};

function getHabitaciones(){
    $('.popoverMsg').each(function() {
        $(this).popover('hide');
    });  
    var btn = $('#refrescar-paneles');
    $.ajax({
       url : getHomeUrl()+'Dashboard/getHabitaciones',
       type: "POST",
       dataType:'json',
       beforeSend:function(){
           $.growl("Se han comenzado a cargar las habitaciones", {
                    type: "info"
            });
            btn.button('loading');
       },
       success: function(data, textStatus, jqXHR)
       {
           if(data.length > 0){
                $.each(data, function(idx, obj) {
                    getPanel(obj); 
                });
            }else{
               $.growl("No se encontraron habitaciones en el sistema", {
                    type: "danger"
                }); 
            }
           
       },
       complete: function(){
           $.growl("Se han cargado las habitaciones correctamente", {
                    type: "success"
            });
            btn.button('reset');
            getTotales();
            getTiempos();
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}

$(document).on("click",".popoverMsg",function(e) {
    
  $(".popoverMsg").popover({
        placement : 'bottom',
        html : true,
        trigger: 'focus click',
        
        template : '<div class="popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
    });
    $('.id_habitacion').val($(this).attr('hab'));

    if($(this).attr('status')==2){
        var btn = $('.btn-group-vertical');
        btn.button('loading');
        getTotal($(this).attr('hab'));
        getHabitacionTarifas($(this).attr('hab'),btn);
        
   //     getCatalogoCuenta($(this).attr('hab'));
    }
     e.stopPropagation();

    e.preventDefault();
});


function getPanel(obj){
    $('#div-dis-'+obj.id).html(
            '<div class="panel panel-default">'+
                '<div class="panel-heading">'+
                    getPopover(obj)+
                '</div>'+
                '<div class="panel-body panel-body-hab-'+obj.id+'" >'
                    +getToolBar(obj)+
                '</div>'+
                '<div class="panel-footer text-center">'+
                        '<h4 class="div-price-'+obj.id+' "><i class="fa fa-usd"></i>0,00</h4>'+
                        '<h4 class="div-time-'+obj.id+'"><i class="fa fa-clock-o"></i>00:00:00</h4> '+
                    '</div>'+
            '</div>')
    if(obj.id_sis_hab_status == 2){
        $('#div-ocu-'+obj.id).html(
                '<div class="panel panel-'+obj.alert+'">'+
                    '<div class="panel-heading">'+
                        '<span class="glyphicon glyphicon-home">&nbsp</span>'+obj.numero+
                    '</div>'+
                    '<div class="panel-body panel-body-hab-'+obj.id+'">'+
                        //getToolBar(obj)+
                    '</div>'+
                    '<div class="panel-footer text-center">'+
                        '<i class="fa fa-usd">&nbsp;</i><label class="div-price-'+obj.id+'">0,00</label>'+
                        '<br>'+
                        '<i class="fa fa-clock-o">&nbsp;</i><label class="div-time-'+obj.id+'">00:00:00</label> '+
                    '</div>'+
                '</div>');
    }else if (obj.id_sis_hab_status == 3){
        $('#div-man-'+obj.id).html(
                '<div class="panel panel-'+obj.alert+'">'+
                    '<div class="panel-heading">'+
                        '<span class="glyphicon glyphicon-home">&nbsp;</span>'+obj.numero+
                    '</div>'+
                    '<div class="panel-body panel-body-hab-'+obj.id+'">'+
                        //getToolBar(obj)+
                    '</div>'+
                '</div>')
    }else if (obj.id_sis_hab_status == 4){
        $('#div-fs-'+obj.id).html(
                '<div class="panel panel-'+obj.alert+'">'+
                    '<div class="panel-heading">'+
                        '<span class="glyphicon glyphicon-home">&nbsp;</span>'+obj.numero+
                    '</div>'+
                    '<div class="panel-body panel-body-hab-'+obj.id+'">'+
                        //getToolBar(obj)+
                    '</div>'+
                '</div>')
    }
    
}

function getToolBar(obj){
    if(obj.id_sis_hab_status == 1){
        return '<div class="btn-toolbar" role="toolbar">'+
                                        '<div class="btn-group" onclick="updaStatus('+obj.id+",'"+obj.numero+"'"+',2,1);"><span class="glyphicon glyphicon-arrow-down btn btn-warning"></span></div>'+
                                        '<div class="btn-group" onclick="updaStatus('+obj.id+",'"+obj.numero+"'"+',3,3);"><span class="glyphicon glyphicon-warning-sign btn btn-info"></span></div>'+
                                        '<div class="btn-group" onclick="updaStatus('+obj.id+",'"+obj.numero+"'"+',4,5);"><span class="glyphicon glyphicon-wrench btn btn-danger"></span></div>'+
                                    '</div>';
    }else if(obj.id_sis_hab_status == 2){
        var html = '';
        html = html + '<div class="btn-toolbar" role="toolbar">';
        html = html + '<div class="btn-group" onclick="updaStatus('+obj.id+",'"+obj.numero+"'"+',1,2);"><span class="glyphicon glyphicon-arrow-up btn btn-success"></span></div>'; 
        if(obj.taxi == 1){                                
            var activo = 'show';
            
        }else{
            var activo = 'hide';
        }
        html = html +'<div class="btn-group"><span class="fa fa-cab  fa-2x '+activo+' " id="taxi-'+obj.id+'" disabled="disabled"></span></div>';
        if(obj.nevera == 1){
            html = html +'<div class="btn-group"><span class="fa fa-plug fa-2x" disabled="disabled"></span></div>';
        }
//        html = html +'<div class="btn-group label label-danger""><span class="fa fa-male fa-3x"></span></div>'; 
        html = html + '</div>';
        
        return html;
    }else if(obj.id_sis_hab_status == 3){
        return '<div class="btn-toolbar" role="toolbar">'+
                                        '<div class="btn-group" onclick="updaStatus('+obj.id+",'"+obj.numero+"'"+',1,4);"><span class="glyphicon glyphicon-arrow-up btn btn-success"></span></div>'+
                                        '<div class="btn-group" onclick="updaStatus('+obj.id+",'"+obj.numero+"'"+',4,5);"><span class="glyphicon glyphicon-wrench btn btn-danger"></span></div>'+
                                    '</div>';
    }else if(obj.id_sis_hab_status == 4){
        return '<div class="btn-toolbar" role="toolbar">'+
                                        '<div class="btn-group" onclick="updaStatus('+obj.id+",'"+obj.numero+"'"+',1,6);"><span class="glyphicon glyphicon-arrow-up btn btn-success"></span></div>'+
                                        '<div class="btn-group" onclick="updaStatus('+obj.id+",'"+obj.numero+"'"+',3,3);"><span class="glyphicon glyphicon-warning-sign btn btn-info"></span></div>'+
                                    '</div>';
    }
    
}

function updaStatus(id,numero,status,log){
    if(status == 2 || status == 1){
        getFormMesonero(id,numero,status,log);
    }else{
        saveUpdateStatus(id,numero,status,log);
    }
}

function saveUpdateStatus(id,numero,status,log,mesonero){
    $.ajax({
       url : getHomeUrl()+'Dashboard/updateStatus/id/'+id+'/status/'+status+'/log/'+log+'/mesonero/'+mesonero,
       type: "POST",
       dataType:'json',
       beforeSend:function(){
          startProcess(id,status); 
       },
       success: function(data, textStatus, jqXHR)
       {
           if(data.resp == 1){
                $.growl("Se han actualizado el estado de la Habitación: "+numero, {
                    type: "success"
                });
            }else{
                $.growl(data.msg, {
                    type: "danger"
                });
            }
           getHabitacion(id,numero);
       },
       complete: function(){
//           getTotales();
//           getTiempos();
            getHabitacionStatusResumen();
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            });
       }
    });
}

function getHabitacion(id,numero){
     $.ajax({
       url : getHomeUrl()+'Dashboard/getHabitacion/id/'+id,
       type: "POST",
       dataType:'json',
       beforeSend:function(){
           
       },
       success: function(data, textStatus, jqXHR)
       {
           if(data.id > 0){
                getPanel(data);
                clearPanel(data); 
            }else{
               $.growl("No se encontraron la habitación numero: "+numero, {
                    type: "danger"
                }); 
            }
           
       },
       complete: function(){
//           $.growl("Se han cargado las habitaciones correctamente : "+numero, {
//                    type: "success"
//            });
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}

function clearPanel(obj){
    
    if(obj.id_sis_hab_status == 1){
        $('#div-ocu-'+obj.id).html(' ');
        $('#div-man-'+obj.id).html(' ');
        $('#div-fs-'+obj.id).html(' ');
    }else if (obj.id_sis_hab_status == 2){
        $('#div-ocu-'+obj.id).html(' ');
        $('#div-man-'+obj.id).html(' ');
        $('#div-fs-'+obj.id).html(' ');
    }else if (obj.id_sis_hab_status == 3){
        $('#div-ocu-'+obj.id).html(' ');
        $('#div-fs-'+obj.id).html(' ');
    }else if (obj.id_sis_hab_status == 4){
        $('#div-ocu-'+obj.id).html(' ');
        $('#div-man-'+obj.id).html(' ');
    }
}

function startProcess(id,idStatus){
    $('#div-dis-'+id).html('<i class="fa fa-refresh fa-spin fa-5x"></i>').fadeIn(3000); 
    $('#div-ocu-'+id).html('<i class="fa fa-refresh fa-spin fa-5x"></i>').fadeIn(3000); 
    $('#div-man-'+id).html('<i class="fa fa-refresh fa-spin fa-5x"></i>').fadeIn(3000);
    $('#div-fs-'+id).html('<i class="fa fa-refresh fa-spin fa-5x"></i>').fadeIn(3000);
}

function getTotales(){
    var btn = $('#refrescar-totales');
     $.ajax({
       url : getHomeUrl()+'Dashboard/getTotales',
       type: "POST",
       dataType:'json',
       beforeSend:function(){
           btn.button('loading');
       },
       success: function(data, textStatus, jqXHR)
       {
           if(data.length > 0){
                $.each(data, function(idx, obj) {
                    $('.div-price-'+obj.id_habitacion).html('<i class="fa fa-refresh fa-spin"></i>').fadeIn(3000);
                    $('.div-price-'+obj.id_habitacion).html('<label class="label label-default"><i class="fa fa-usd">&nbsp;</i>'+obj.valor_formato+'</label>'); 
//                    $('.div-price-'+obj.id_habitacion).html('<div class="popoverMsg">'+obj.valor_formato+'</div>'); 
                });
            }
       },
       complete: function(){
//           $.growl("Se actualizaron Totales", {
//                    type: "info"
//            });
            btn.button('reset');
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}

function getTotal(id){
    var btn = $('.btnGuardar');
    $.ajax(
    {
        url : getHomeUrl()+'dashboard/getTotal/idHabitacion/'+id,
        type: "POST",
        dataType:'json',
//        data : postData,
        beforesend:function(data, textStatus, jqXHR){
            btn.button('loading');
            $('.pagos').attr('disabled',true);
        },
        success:function(data, textStatus, jqXHR) 
        {
//            $('#Pago_id_cuenta').val(data.id_cuenta);
            $('.id_cuenta').val(data.id_cuenta);
            $('#Pago_valor').val(data.valor);
            $('#Pago_valor').attr('max',data.valor);
            $('.ValorMax').html(data.valor_formato);
            $('.valor_maximo').val(data.valor);
            $('.numero').val(data.numero);
            $('.titulo').html('<h1> Cuenta Nro.: '+data.id_cuenta+'<small> Habitación: '+data.numero+'</small></h1>');
        },
        complete:function(){
            btn.button('reset');
            $('.pagos').attr('disabled',false);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
}
function getPopover(obj){
   return '<button type="button"  data-loading-text="Cargando ..." class="btn btn-block btn-'+obj.alert+'  popoverMsg" data-html="true" hab="'+obj.id+'" status="'+obj.id_sis_hab_status+'" data-container="body" title="Opciones" tabindex="0" data-toggle="popover" data-placement="bottom" data-trigger="focus" '+
                'data-content="'+
                    getPopoveropciones(obj)+
                '"</button>'+
                    '<span class="glyphicon glyphicon-home ">&nbsp</span>'+
                    '<b>'+obj.numero+'</b>&nbsp;'+obj.nombre+
            '</button>';

}
function getPopoveropciones(obj){
    
    if(obj.id_sis_hab_status == 1){
        return "<div class='btn-group-vertical' data-loading-text='Cargando ...'>"+
                "<button type='button' class='btn btn-primary' data-loading-text='Cargando ...' onclick='getPedidos("+obj.id+",1)'><i class='fa fa-pencil'></i>&nbsp;Pedido</button>"+
                "</div>"; 
    }else if(obj.id_sis_hab_status == 2){
        return "<div class='btn-group-vertical' data-loading-text='Cargando ...'>"+
                    "<button type='button' class='btn btn-primary getFormPagos' onclick='getFormPagos("+obj.id+",0)' data-loading-text='Cargando ...'><i class='fa fa-usd'></i>&nbsp;Pagos</button>"+
                    "<button type='button' class='btn btn-primary getCatalogoCuenta' onclick='getCatalogoCuenta(0)' data-loading-text='Cargando ...'><i class='fa fa-shopping-cart'></i>&nbsp;Agregar Productos</button>"+
                    "<button type='button' class='btn btn-primary getCuenta' onclick='getCuenta("+obj.id+",0)' data-loading-text='Cargando ...'><i class='fa fa-eye'></i>&nbsp;Ver Factura</button>"+
                    "<button type='button' class='btn btn-primary getFormAdicional' onclick='getFormAdicional("+obj.id+")' data-loading-text='Cargando ...'><i class='fa fa-plus'></i>&nbsp;Adicionales</button>"+
                    "<button type='button' class='btn btn-primary getFormDescuento' onclick='getFormDescuento("+obj.id+",0)' data-loading-text='Cargando ...'><i class='fa fa-minus'></i>&nbsp;Descuentos</button>"+
                    "<button type='button' class='btn btn-primary getFormHabitacion' onclick='getFormHabitacion("+obj.id+")' data-loading-text='Cargando ...'><i class='fa fa-heart-o'></i>&nbsp;Habitación</button>"+
                    "<button type='button' class='btn btn-primary getFormVale' onclick='getFormVale("+obj.id+")' data-loading-text='Cargando ...'><i class='fa fa-cab'></i>&nbsp;Taxi</button>"+
                    "<button type='button' class='btn btn-primary getFormCambio' onclick='getFormCambio("+obj.id+")' data-loading-text='Cargando ...'><i class='fa fa-exchange'></i>&nbsp;Cambio Habitación</button>"+
              "</div>";    
    }
    return '';
    
}




function getTiempos(){
    var btn = $('#refrescar-tiempos');
     $.ajax({
       url : getHomeUrl()+'Dashboard/getTiempos',
       type: "POST",
       dataType:'json',
       beforeSend:function(){
           btn.button('loading');
       },
       success: function(data, textStatus, jqXHR)
       {
           if(data.length > 0){
                $.each(data, function(idx, obj) {
                    $('.div-time-'+obj.id_habitacion).html('<i class="fa fa-refresh fa-spin"></i>').fadeIn(3000);
                    if(parseInt(obj.en_hora) == 1){
                        $('.div-time-'+obj.id_habitacion).html('<label class="label label-primary"><i class="fa fa-clock-o">&nbsp;</i>'+obj.tiempo_transcurrido+'<label>'); 
                    }else{
                        $('.div-time-'+obj.id_habitacion).html('<label class="label label-danger"><i class="fa fa-clock-o fa-spin"></i>&nbsp;'+obj.tiempo_transcurrido+'<label>'); 
                    }
                    
                });
            }
       },
       complete: function(){
//           $.growl("Se actualizaron el Tiempo transcurido", {
//                    type: "info"
//            });
            btn.button('reset');
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
//           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
//                type: "danger"
//            }); 
       }
    });
}

function getHabitacionStatusResumen(){
     $.ajax({
       url : getHomeUrl()+'entidad/Habitacion/getHabitacionStatusResumen',
       type: "POST",
       dataType:'json',
       beforeSend:function(){
       },
       success: function(data, textStatus, jqXHR)
       {
           if(data.length > 0){
               $('.habitacion-status').html('');
                $.each(data, function(idx, obj) {
                    $('.habitacion-'+obj.id).html(obj.nombre+' <span class="badge">'+obj.cantidad+'</span>&nbsp;');
                    
                });
            }
       },
       complete: function(){
//           $.growl("Se actualizaron el Tiempo transcurido", {
//                    type: "growl"
//            });
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}

function getPedidos(id,modal){
    
    $('#modal-large').modal('show');
    
     $.ajax({
       url : getHomeUrl()+'producto/pedido/Admin/idHabitacion/'+$('.id_habitacion').val()+'/json/1',
       type: "POST",
       dataType:'json',
       beforeSend:function(){
           
       },
       success: function(data, textStatus, jqXHR)
       {
            
            $('#large-content').html(data.list);
       },
       complete: function(){
           $.growl("Pedidos cargados", {
                    type: "growl"
            });
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}


