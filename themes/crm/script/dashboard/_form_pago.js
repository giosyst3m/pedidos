/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function getFormPagos(id,tipo){
    $('.pagos').attr('disabled',true);
    $('#modal-pagos').modal('show');
    $('.id_habitacion').val(id); 
    if(tipo==0){
        getTotal(id);
    }else{
        getTotalEmpleado(id);
    }
}

function validPagos(){
    $.growl(false, {
        z_index:1080
    });
    var valor = parseInt($('#Pago_valor').val());
    var valor_max = parseInt($('#valor_maximo').val());
    if(valor<=0){
        $.growl("El valor debe ser mayor a 0", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if(!$.isNumeric(valor)){
         $.growl("Debe colocar un valor y pude ser mayor al indicado", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if(!$.isNumeric($('#Pago_id_pag_tipo').val())){
        $.growl("Debe Seleccionar un tipo de pago", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if(valor > valor_max){
        $.growl("Favor revisar esta colocando un valor a lo permitido: "+$('#ValorMax').text(), {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
    }else{
        $('.form-group').removeClass('has-error');
        $('.form-group').addClass('has-success');
        $('#modal-pagos').modal('hide');
        return true;
    }
}

$(document).on("submit","#pago-form",function(e) {

    var postData = $(this).serializeArray();
    $.ajax(
    {
        url : getHomeUrl()+'Dashboard/savePago',
        type: "POST",
        dataType:'json',
        data : postData,
        beforesend:function(data, textStatus, jqXHR){
             
        },
        success:function(data, textStatus, jqXHR) 
        {
            if(data.r == 0){
                $('#modal-pagos').modal('show');
                $.growl(data.m, {
                    type: "danger"
                });
            }else if(data.r == 2){
                $.growl(data.m, {
                    type: "danger"
                });
            }else{
                $.growl(data.m, {
                    type: "success"
                });
                
            }
        },
        complete: function(){
            getEmpleadoSaldos();
            getTotales();
           
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
    e.stopPropagation();
    e.preventDefault();
});