function getFormMesonero(id,numero,status,log){
    $('#MesoneroIDHabitacion').val(id);
    $('#MesoneroNumero').val(numero);
    $('#MesoneroStatus').val(status);
    $('#MesoneroLOG').val(log);
    $('#modal-mesonero').modal('show');
}

function ValidMesonero(){
    $('#modal-mesonero').modal('hide');
    saveUpdateStatus( $('#MesoneroIDHabitacion').val(),$('#MesoneroNumero').val(),$('#MesoneroStatus').val(),$('#MesoneroLOG').val(),$('input[name=mesonero]:checked', '#MesoneroFrom').val());
}

function getCuentaMesoneros(id){
    $.ajax({
       url : getHomeUrl()+'producto/cuenta/getCuentaMesoneros/idCuenta/'+id,
       type: "POST",
       dataType:'html',
       beforeSend:function(){
           $.growl("Se han comenzado a cargar las Mesoneros de la cuenta: "+id, {
                    type: "info"
            });
       },
       success: function(data, textStatus, jqXHR)
       {
           $('#MainModal').modal('show');
           $('#MainContent').html(data);
       },
       complete: function(){
           $.growl("Se han cargado los mesoneros encontrados", {
                    type: "success"
            });
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}