function getEmpleados(){
    $.ajax({
       url : getHomeUrl()+'Dashboard/getEmpleados',
       type: "POST",
       dataType:'html',
       beforeSend:function(){
           $.growl("Se han comenzado a cargar Empleados", {
                    type: "info"
            });
       },
       success: function(data, textStatus, jqXHR)
       {
            $('#empleados').html(data);
           
       },
       complete: function(){
           $.growl("Se han cargado las Empleados correctamente", {
                    type: "success"
            });
            getEmpleadoSaldos();
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}

function getEmpleadoModal(id){
    console.log(id);
    $('#modal-empleados').modal('show');
}

function getEmpleadoFactura(id){
    
}

function getEmpleadoSaldos(){
    $.ajax({
       url : getHomeUrl()+'Dashboard/getEmpleadoTotales',
       type: "POST",
       dataType:'json',
       beforeSend:function(){
           $('.empleado-saldo').html('<i class="fa fa-refresh fa-spin"></i>').fadeIn(3000);
       },
       success: function(data, textStatus, jqXHR)
       {
           if(data.length > 0){
               
               $('.empleado-saldo').html('<h4><label class="label label-default"><i class="fa fa-usd">&nbsp;</i>0,00</label></h4>');
                $.each(data, function(idx, obj) {
                    if(!$.isNumeric(obj.numero)){
                        $('#empleado-saldo-'+obj.id_sis_usuario).html('<h4><label class="label label-danger"><i class="fa fa-usd">&nbsp;</i>'+obj.valor_formato+'</label></h4>'); 
                    }
                });
                
            }
       },
       complete: function(){
//           $.growl("Se actualizaron Totales", {
//                    type: "grow"
//            });
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    })  
}

function getEmpleadoCuenta(id,cuenta){
    $.ajax({
       url : getHomeUrl()+'Dashboard/getCuentaEmpleado/id/'+id+'/cuenta/'+cuenta,
       type: "POST",
       dataType:'json',
       beforeSend:function(){
       },
       success: function(data, textStatus, jqXHR)
       {   
           if(data.r == 1){
               $('#modal-cuenta').modal('show');
               var html ='<div class="panel panel-info">'+
                            '<div class="panel-heading">'+
                                '<div class="panel-title">'+
                                    '<div class="row">'+
                                        '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2"><h5>Item</h5></div>'+
                                        '<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"><h5>Producto</h5></div>'+
                                        '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right"><h5>Unidades</h5></div>'+
                                        '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right"><h5>Precio</h5></div>'+
                                        '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right"><h5>Total</h5></div>'+
                                        '<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 text-center"><h5>Acción</h5></div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'
                            '<div class="panel-body">';
                $.each(data.detalle, function(idx, obj) {
                    html = html + '<div class="row" id="div-detalle-'+obj.id+'">';
                        html = html + '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">';
                        if(obj.imagen == null || obj.imagen == ""){
                            html = html + '<img src="'+data.ruta_sin_imagen+'" class="img-responsive" alt="" width="100px" height="100px" >';
                        }else{
                            html = html + '<img src="'+data.ruta_imagen+'/'+obj.imagen+'" class="img-responsive" alt="" width="100px" height="100px">';
                        }
                        html = html + '</div>';
                        html = html + '<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">'+
                                            '<h4 class="product-name"><b>'+obj.producto_nombre+'</b></h4>'+
                                            '<h4><small>'+obj.referencia+'</small></h4>'+
                                            '<h4><small>'+obj.r_c_d+'</small></h4>'+                                        
                                        '</div>';
                        html = html + '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right">'+
                                            '<h4>'+obj.cantidad+'</h4>'+
                                        '</div>';
                        html = html + '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right">'+
                                            '<h4>'+obj.valor+'</h4>'+
                                        '</div>';
                        html = html + '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-right">'+
                                        '<h4>'+obj.total+'</h4>'+
                                        '</div>';
                        html = html + '<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1 text-center">';
                        if(obj.abierta==1){
                            if(obj.tipo == 6){
                                html = html + '<button class="btn btn-warning" onclick="getFormTarifas('+obj.id+')"><span class="glyphicon glyphicon-transfer"></span></button>';
                            }else if(obj.tipo==1){
                                html = html + '<button class="btn btn-danger" onclick="getFromDeleteProducto('+obj.id+','+obj.id_cuenta+','+obj.id_producto+','+id+')"><span class="glyphicon glyphicon-trash"></span></button>';
                            }else if(obj.tipo == 5){
                                html = html + '<button class="btn btn-danger" onclick="getFromDeleteAdicional('+obj.id+')"><span class="glyphicon glyphicon-trash"></span></button>';
                            }else if(obj.tipo == 2){
                                html = html + '<button class="btn btn-danger" onclick="getFromDeleteHabitacion('+obj.id+')"><span class="glyphicon glyphicon-trash"></span></button>';
                            }else if(obj.tipo == 4){
                                html = html + '<button class="btn btn-danger" onclick="getFromDeleteDescuento('+obj.id+')"><span class="glyphicon glyphicon-trash"></span></button>';
                            }else if(obj.tipo == 3){
                                html = html + '<button class="btn btn-danger" onclick="getFromDeletePago('+obj.id+')"><span class="glyphicon glyphicon-remove"></span></button>';
                            }else{
                                html = html + '<button class="btn"><span class="glyphicon glyphicon-trash"></span></button>';
                            }
                        }
                        html = html +'</div>';
                    html = html + '</div>';
                });
                html = html + '<div class="panel-footer" id="cuenta_footer"><i class="fa fa-refresh fa-spin fa-5x"></i></div>';  
                html = html + '</div>';
                                    
                $('#factura').html(html); 
                getCuentaEmpleadoFooter(id,cuenta);
            }else{
               $.growl(data.m, {
                    type: "info"
            }); 
            }
       },
       complete: function(){
           $.growl("Se actualizaron el Tiempo transcurido", {
                    type: "info"
            });
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}

function getCuentaEmpleadoFooter(id,cuenta){
    $.ajax({
       url : getHomeUrl()+'Dashboard/getCuentaEmpleadoFooter/id/'+id+'/cuenta/'+cuenta,
       type: "POST",
       dataType:'json',
       beforeSend:function(){
       },
       success: function(data, textStatus, jqXHR)
       {
           $('#cuenta_footer').html(data.totales);
            
       },
       complete: function(){
           $.growl("Se cargo el Total del Factura ", {
                    type: "info"
            });
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
       }
    });
}

function getTotalEmpleado(id){
    var btn = $('.btnGuardar');
    $.ajax(
    {
        url : getHomeUrl()+'dashboard/getTotalEmpleado/id/'+id,
        type: "POST",
        dataType:'json',
//        data : postData,
        beforesend:function(data, textStatus, jqXHR){
            btn.button('loading');
            $('.pagos').attr('disabled',true);
        },
        success:function(data, textStatus, jqXHR) 
        {
//            $('#Pago_id_cuenta').val(data.id_cuenta);
            $('.id_cuenta').val(data.id_cuenta);
            $('#Pago_valor').val(data.valor);
            $('#Pago_valor').attr('max',data.valor);
            $('.ValorMax').html(data.valor_formato);
            $('.valor_maximo').val(data.valor);
            $('.numero').val(data.numero);
            $('.titulo').html('<h1> Cuenta Nro.: '+data.id_cuenta+'<small> Empleado: '+data.numero+'</small></h1>');
        },
        complete: function(){
             btn.button('reset');
             $('.pagos').attr('disabled',false);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
}

function closeCuentaEmpleado(id){

    $.ajax(
    {
        url : getHomeUrl()+'producto/cuentaEmpleado/closeCuentaEmpleado/id/'+id,
        type: "POST",
        dataType:'json',
        beforesend:function(data, textStatus, jqXHR){
            
        },
        success:function(data, textStatus, jqXHR) 
        {
            if(data.r==1){
                $.growl(data.m, {
                    type: "success"
                }); 
                getEmpleadoSaldos();
            }else{
                $.growl(data.m, {
                    type: "danger"
                }); 
            }
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
}