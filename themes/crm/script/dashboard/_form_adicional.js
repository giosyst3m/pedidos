function getFormAdicional(id){
    $('#modal-adicional').modal('show');
    $('.id_habitacion').val(id);
    
}
function validAdicional(){
    $.growl(false, {
        z_index:1080
    });
    var valor = parseInt($('#CueAdicional_valor').val());
    var cantidad = parseInt($('#CueAdicional_cantidad').val());
    if(valor<=0 || cantidad <= 0){
        $.growl("El valor debe ser mayor a 0", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if(!$.isNumeric(valor) || !$.isNumeric(cantidad)){
         $.growl("Debe colocar un valor y pude ser mayor al indicado", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if($('#CueAdicional_descripcion').val()==""){
         $.growl("Debe escribir uan descripción del Adicional", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else{
        $('.form-group').removeClass('has-error');
        $('.form-group').addClass('has-success');
        $('#modal-adicional').modal('hide');
        return true;
    }
}
$(document).on("submit","#cue-adicional-form",function(e) {

    var postData = $(this).serializeArray();
    $.ajax(
    {
        url : getHomeUrl()+'Dashboard/saveAdicional',
        type: "POST",
        dataType:'json',
        data : postData,
        beforesend:function(data, textStatus, jqXHR){
            
        },
        success:function(data, textStatus, jqXHR) 
        {
            if(data.r == 0){
                $('#modal-adicional').modal('show');
                $.growl(data.m, {
                    type: "danger"
                });
            }else if(data.r == 2){
                $.growl(data.m, {
                    type: "danger"
                });
            }else{
                $.growl(data.m, {
                    type: "success"
                });
                
            }
        },
        complete: function(){
            getTotales();
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
    e.stopPropagation();
    e.preventDefault();
});