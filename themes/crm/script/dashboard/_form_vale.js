function getFormVale(id){
    $('#modal-vale').modal('show');
    $('#Vale_concepto_form').val('Taxi Habitación Nro.: '+$('.numero').val());
    $('.id_habitacion').val(id);
    
    
}
function validVale(){
    $.growl(false, {
        z_index:1080
    });
    var valor = parseInt($('#Vale_valor_form').val());
    if(valor<=0){
        $.growl("El valor debe ser mayor a 0", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if(!$.isNumeric(valor)){
         $.growl("Sólo se permiten nùmeros mayores a 0 ", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if($('#Vale_concepto_form').val()==""){
         $.growl("Debe escribir concepto para generar el vale", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else{
        $('.form-group').removeClass('has-error');
        $('.form-group').addClass('has-success');
        $('#modal-vale').modal('hide');
        return true;
    }
}
$(document).on("submit","#vale-form",function(e) {
    var postData = $(this).serializeArray();
    $.ajax(
    {
        url : getHomeUrl()+'/producto/vale/saveVale/',
        type: "POST",
        dataType:'json',
        data : postData,
        beforesend:function(data, textStatus, jqXHR){
            
        },
        success:function(data, textStatus, jqXHR) 
        {
            if(data.r == 0 || data.r == 2){
//                $('#modal-vale').modal('show');
                $.growl(data.m, {
                    type: "danger"
                });
            }else{
//                $('#taxi-'+$('.id_habitacion').val()).addClass('active');
                $('#taxi-'+$('.id_habitacion').val()).removeClass('hide');
                $.growl(data.m, {
                    type: "success"
                });
                
            }
        },
        complete: function(){
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
    e.stopPropagation();
    e.preventDefault();
});