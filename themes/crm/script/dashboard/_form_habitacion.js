function getFormHabitacion(id){
    $('#modal-habitacion').modal('show');
    $('.id_habitacion').val(id);
    
}
function validHabitacion(){
    $.growl(false, {
        z_index:1080
    });
    
    var cantidad = parseInt($('#CuentaTarifarioHabitacion_cantidad').val());
    if(cantidad <= 0){
        $.growl("El valor debe ser mayor a 0", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if(!$.isNumeric(cantidad)){
         $.growl("Debe colocar un valor y pude ser mayor al indicado", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else if(!$.isNumeric($('#CuentaTarifarioHabitacion_id_tarifario_habitacion').val())){
         $.growl("Debe Selecionar una tarifa", {
            type: "danger"
        });
        $('.form-group').addClass('has-error');
        return false;
    }else{
        $('.form-group').removeClass('has-error');
        $('.form-group').addClass('has-success');
        $('#modal-habitacion').modal('hide');
        return true;
    }
}
$(document).on("submit","#cuenta-tarifario-habitacion-form",function(e) {

    var postData = $(this).serializeArray();
    $.ajax(
    {
        url : getHomeUrl()+'Dashboard/saveHabitacion',
        type: "POST",
        dataType:'json',
        data : postData,
        beforesend:function(data, textStatus, jqXHR){
            
        },
        success:function(data, textStatus, jqXHR) 
        {
            if(data.r == 0){
                $('#modal-habitacion').modal('show');
                $.growl(data.m, {
                    type: "danger"
                });
            }else if(data.r == 2){
                $.growl(data.m, {
                    type: "danger"
                });
            }else{
                $.growl(data.m, {
                    type: "success"
                });
                
            }
        },
        complete: function(){
            getTotales();
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
    e.stopPropagation();
    e.preventDefault();
});

function getHabitacionTarifas(id,btn){
    $.ajax(
    {
        url : getHomeUrl()+'Dashboard/getHabitacionTarifas/idHabitacion/'+id,
        type: "POST",
        dataType:'json',
        beforesend:function(data, textStatus, jqXHR){
            
        },
        success:function(data, textStatus, jqXHR) 
        {
            $('#CuentaTarifarioHabitacion_id_tarifario_habitacion')
            .find('option')
            .remove()
            .end();
            $.each(data, function(i, obj) {
                $('#CuentaTarifarioHabitacion_id_tarifario_habitacion').append($('<option>').text(obj.nombre).attr('value', obj.id));
            });
            $('#CuentaTarifarioHabitacion_id_tarifario_habitacion').selectpicker('refresh');
        },
        complete: function(){
            btn.button('reset');
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            $.growl("Se ha presentado problemas interno en el sistema Error: "+errorThrown, {
                type: "danger"
            }); 
        }
    });
}