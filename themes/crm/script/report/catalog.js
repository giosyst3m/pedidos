$('#report_pdf_no_pict').on('click', function () {
    console.log($('#ProGroup').selectpicker('val'));
    if ($('#ProGroup').selectpicker('val') == null) {
        $.growl("Debe seleccionar una Línea", {
            type: "warning"
        });
    } else {
        window.open(getHomeUrl() + 'product/ListPDF/progroup/' + $('#ProGroup').selectpicker('val'), '_blank');
    }
});
$('#report_pdf_pict').on('click', function () {
    console.log($('#ProGroup').selectpicker('val'));
    if ($('#ProGroup').selectpicker('val') == null) {
        $.growl("Debe seleccionar una Línea", {
            type: "warning"
        });
    } else {
        window.open(getHomeUrl()+'product/CatalogPrint/progroup/'+$('#ProGroup').selectpicker('val')+'/type/1','_blank');
    }
});
$('#report_excel').on('click', function () {
    console.log($('#ProGroup').selectpicker('val'));
    if ($('#ProGroup').selectpicker('val') == null) {
        $.growl("Debe seleccionar una Línea", {
            type: "warning"
        });
    } else {
        window.open(getHomeUrl() + 'download/catalog/progroup/' + $('#ProGroup').selectpicker('val'), '_blank');
    }
});

