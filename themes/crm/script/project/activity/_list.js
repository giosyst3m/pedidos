function doneTodo(obj) {
    $.ajax({
        url: getHomeUrl() + 'project/todo/done',
        type: "post",
        dataType: "json",
        data: {
            id: obj.id,
            done: done = $('#' + obj.id).attr('data-done'),
        },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                $('#' + obj.id).attr('data-done', data.done)
                if (data.done === 1) {
                    $('.done-' + obj.id).attr('disabled', true);
                } else {
                    $('.done-' + obj.id).attr('disabled', false);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}