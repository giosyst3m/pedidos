$(document).ready(function () {
    var step = $('#step').val();
    $('#wizard').smartWizard();

    $('#wizard_verticle').smartWizard({
        transitionEffect: 'slide',
    });
    if (step > 1) {
        $('#wizard_verticle').smartWizard('goToStep', step);
    }
    $('.buttonNext').addClass('btn btn-success');
    $('.buttonPrevious').addClass('btn btn-primary');
    $('.buttonFinish').addClass('btn btn-default');
});

