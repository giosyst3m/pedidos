function ApprovedRequest(id, prod) {
    $.ajax({
        url: getHomeUrl() + 'ordDetail/ApprovedRequest/',
        type: "post",
        dataType: "json",
        data: {
            id: id,
            qty: $('#qty-' + id).val(),
            req: $('#req-' + id).val(),
        },
        beforeSend: function () {
            $('.botones-' + prod).button('loading');
        },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                $('#qty-' + id).val(data.qty);
                $('#' + data.id).text(data.exi);
                var sum = 0;
                $(".items-" + data.id).each(function () {
                    sum += parseFloat($(this).val());
                });
                $('.sum-' + data.id).html(sum);
                if (parseFloat(data.exi) === 0) {
                    $('#product-' + data.id).removeClass('btn-success');
                    $('#product-' + data.id).removeClass('btn-danger');
                    $('#product-' + data.id).addClass('btn-warning');
                } else if (parseFloat(data.exi) < 0) {
                    $('#product-' + data.id).removeClass('btn-success');
                    $('#product-' + data.id).removeClass('btn-warning');
                    $('#product-' + data.id).addClass('btn-danger');
                } else {
                    $('#product-' + data.id).removeClass('btn-warning');
                    $('#product-' + data.id).removeClass('btn-danger');
                    $('#product-' + data.id).addClass('btn-success');
                }
                if (data.req > 0) {
                    $('#btn-' + id).removeClass('btn-primary');
                    $('#btn-' + id).addClass('btn-success');

                } else {
                    $('#btn-' + id).removeClass('btn-success');
                    $('#btn-' + id).addClass('btn-primary');
                }
                if (parseFloat(data.exi) < sum) {
                    $('#total-' + data.id).removeClass('btn-primary');
                    $('#total-' + data.id).addClass('btn-danger');
                } else {
                    $('#total-' + data.id).removeClass('btn-danger');
                    $('#total-' + data.id).addClass('btn-primary');
                }
            }
        },
        complete: function () {
            setTimeout(reset(prod), 3000);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}

function reset(id) {
    $('.botones-' + id).button('reset');
}