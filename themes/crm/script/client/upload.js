function processExcelFile(result) {

    if (result.success) {
        $.ajax(
                {
                    url: getHomeUrl() + 'upload/processFileExcelClient/',
                    type: "POST",
                    dataType: 'html',
                    data: {
                        file: result.filename,
                        zone: $('#Zone').val(),
                    },
                    beforeSend: function (data, textStatus, jqXHR) {
                        $('#procesando').removeClass('hidden');
                        $('.result').html('<i class="fa fa-spinner fa-pulse"></i>');
                    },
                    success: function (data, textStatus, jqXHR)
                    {
                        $('.result').html(data);
                    },
                    complete: function () {
                       $('#procesando').addClass('hidden'); 

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $.growl("Se ha presentado problemas interno en el sistema Error: " + errorThrown, {
                            type: "danger"
                        });
                    }
                });
    }
}

