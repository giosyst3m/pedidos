$(document).ready(function () {
    if($('#id_status').val()==4){
        getDocument($('#id_order').val(),2);
    }
});
function updateDetailQuantity(id) {
    $.ajax({
        url: getHomeUrl() + 'ordDetail/updateDetailQuantity/',
        type: "post",
        dataType: "json",
        data: {
            id: id,
            qty: $('#' + id).val(),
        },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            $('#vale-grid').yiiGridView('update');
            refreshSumary($('#id_order').val());
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}
function ApprovedRequest(id) {
    $.ajax({
        url: getHomeUrl() + 'ordDetail/ApprovedRequest/',
        type: "post",
        dataType: "json",
        data: {
            id: id,
            qty: $('#qty-' + id).val(),
            req: $('#req-' + id).val(),
        },
        beforeSend:function(){
            $('#btn-' + id).button('loading');
        },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if(data.code === 200){
                if(data.req > 0){
                    $('#btn-' + id).removeClass('btn-primary');
                    $('#btn-' + id).addClass('btn-success');
                    
                }else{
                    $('#btn-' + id).removeClass('btn-success');
                    $('#btn-' + id).addClass('btn-primary');
                }
            }
            $('#vale-grid').yiiGridView('update');
            refreshSumary($('#id_order').val());
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}

function refreshSumary(id) {

    $.ajax({
        url: getHomeUrl() + 'ordDetail/refreshSumary/',
        type: "post",
        dataType: "html",
        data: {
            id: id,
        },
        success: function (data, textStatus, jqXHR)
        {
            $('.sumary').html(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}

$('#nextOrder').on('click', function (e) {
    e.preventDefault();
    var id = $('#nextOrder').attr('data-value');
    $.ajax({
        url: getHomeUrl() + 'Order/Step2/',
        type: "post",
        dataType: "json",
        data: {
            id: id,
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                location.reload();
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
});
$('#ReactiveOrder').on('click', function (e) {
    e.preventDefault();
    $.ajax({
        url: getHomeUrl() + 'Order/Step9/',
        type: "post",
        dataType: "json",
        data: {
            id: $('#ReactiveOrder').attr('data-value'),
            reason:$('#reactive_note').val() ,
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                location.reload();
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
});
$('#approvedOrder').on('click', function (e) {
    e.preventDefault();
    var id = $('#approvedOrder').attr('data-value');
    $.ajax({
        url: getHomeUrl() + 'Order/Step3/',
        type: "post",
        dataType: "json",
        data: {
            id: id,
            note: $('#rejectedNote').val(),
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                location.reload();
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
});
$('#preDelivery').on('click', function (e) {
    e.preventDefault();
    $.ajax({
        url: getHomeUrl() + 'Order/Step4/',
        type: "post",
        dataType: "json",
        data: {
            id: $('#preDelivery').attr('data-value'),
            note: $('#preDelivery_note').val(),
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                location.reload();
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
});
$('#DeliveryOrder').on('click', function (e) {
    e.preventDefault();
    var id = $('#DeliveryOrder').attr('data-value');
    var guide = $('#guide').val();
    var guide_fecha = $('#guide_fecha').val();
    $.ajax({
        url: getHomeUrl() + 'Order/addDelivery/',
        type: "post",
        dataType: "json",
        data: {
            id: id,
            guide: guide,
            date: guide_fecha,
            note: $('#guide_note').val(),
            delivery: $('#delivery').val(),
            price: $('#price').val(),
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if(data.code === 200){
                $('.fDelivery').val('');
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
            refreshSumary($('#id_order').val());
            window.setTimeout(myFucntion, 3000);
            function myFucntion(){
                $('#discount-grid').yiiGridView('update');
            }
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
});
$('#IvoiceOrder').on('click', function (e) {
    e.preventDefault();
    $.ajax({
        url: getHomeUrl() + 'Order/SaveDocument/',
        type: "post",
        dataType: "json",
        data: {
            id: $('#IvoiceOrder').attr('data-value'),
            numero: $('#invoice').val(),
            date: $('#invoice_date').val(),
            total: $('#invoice_total').val(),
            note: $('#guide_note').val(),
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                getDocument($('#id_order').val(),2);
                $('.bills').val('');
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
});

function getDocument(id, type){
        $.ajax({
        url: getHomeUrl() + 'Order/getDocument/',
        type: "post",
        dataType: "json",
        data: {
            id: id,
            type: type,
        },
        beforeSend:function(){
            $('#bills').html('<i class="fa fa-spin fa-2x"></i>');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            $('#bills').html(data.datos);
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}
$('#IvoiceOrderContinue').on('click', function (e) {
    e.preventDefault();
    var id = $('#IvoiceOrderContinue').attr('data-value');
    $.ajax({
        url: getHomeUrl() + 'Order/Step5/',
        type: "post",
        dataType: "json",
        data: {
            id: id,
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                location.reload();
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
});
$('#CloseOrder').on('click', function (e) {
    e.preventDefault();
    $.ajax({
        url: getHomeUrl() + 'Order/Step6/',
        type: "post",
        dataType: "json",
        data: {
            id: $('#CloseOrder').attr('data-value'),
            note: $('#close_note').val(),
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                location.reload();
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
});
$('#deleteOrder').on('click', function (e) {
    e.preventDefault();
    $("#modal-delete-order").modal("show");
});
$('#deleteOrder2').on('click', function (e) {
    e.preventDefault();
    $("#modal-delete-inv-order").modal("show");
});

function deleteInvOrder(){
    $('.botones').button('loading');
    $("#modal-delete-inv-order").modal().hide();
    $(function () {
        $(".btn-inv").each(function () {
            $.each(this.attributes, function (i, a) {
                if (a.name == "id") {
                    var producto = a.value.split('btn-');
                    var id = producto[1];
                    $.ajax({
                        url: getHomeUrl() + 'ordDetail/ApprovedRequest/',
                        type: "post",
                        dataType: "json",
                        async: false,
                        data: {
                            id: id,
                            qty: $('#qty-' + id).val(),
                            req: 0,
                        },
                        beforeSend: function () {
                            $('#btn-' + id).button('loading');
                            $.growl('Actualizando Inventario con el Producto: '+ id + ' retornando: ' + $('#qty-' + id).val(), {
                                type: 'warning'
                            });
                        },
                        success: function (data, textStatus, jqXHR)
                        {
                            $.growl(data.menssage, {
                                type: data.type
                            });
                            if (data.code === 200) {
                                if (data.req > 0) {
                                    $('#btn-' + id).removeClass('btn-primary');
                                    $('#btn-' + id).addClass('btn-success');

                                } else {
                                    $('#btn-' + id).removeClass('btn-success');
                                    $('#btn-' + id).addClass('btn-primary');
                                }
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            $.growl("App Error " + errorThrown, {
                                type: "danger"
                            });
                        }
                    });
                }
            })
        })

    })
    refreshSumary($('#id_order').val());
    console.log('inicia:delete pedido');
    $.ajax({
        url: getHomeUrl() + 'Order/Step7/',
        type: "post",
        dataType: "json",
        data: {
            id: $('#deleteOrder2').attr('data-value'),
            reason: $('#deleteInvOrderText').val(),
        },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                location.reload();
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}
function deleteOrder(id){
    $.ajax({
        url: getHomeUrl() + 'Order/Step7/',
        type: "post",
        dataType: "json",
        data: {
            id: $('#deleteOrder').attr('data-value'),
            reason: $('#deleteOrderText').val(),
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                location.reload();
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}

$('#rejectedOrder').on('click', function (e) {
    $.ajax({
        url: getHomeUrl() + 'Order/Step8/',
        type: "post",
        dataType: "json",
        data: {
            id: $('#rejectedOrder').attr('data-value'),
            reason: $('#rejectedNote').val(),
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                location.reload();
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
});

function deleteOrdDetatil(id){
    $("#modal-delete-product").modal("show");
    $('#id_ordDetatil').val(id);
}
function deleteProduct(){
    $.ajax({
        url: getHomeUrl() + 'OrdDetail/delete/',
        type: "post",
        dataType: "json",
        data: {
            id:  $('#id_ordDetatil').val(),
            reason: $('#deleteOrderTextProduct').val(),
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                $('#vale-grid').yiiGridView('update');
                refreshSumary($('#id_order').val());
                $("#modal-delete-product").modal('hide');
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}

function saveDiscount(){
    $.ajax({
        url: getHomeUrl() + 'Order/saveDiscount/',
        type: "post",
        dataType: "json",
        data: {
            id:  $('#id_order').val(),
            id_discount: $('#discount').val(),
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                $('#vale-grid').yiiGridView('update');
                refreshSumary($('#id_order').val());
                $("#modal-delete-product").modal('hide');
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}

function deleteOrdDocument(id){
     $.ajax({
        url: getHomeUrl() + 'Order/deleteOrdDocument/',
        type: "post",
        dataType: "json",
        data: {
            id:  id,
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            if (data.code === 200) {
                getDocument($('#id_order').val(),2);
            }
        },
        complete: function (data)
        {
            $('.botones').button('reset');
            $('#discount-grid').yiiGridView('update');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}
function deleteDelivery(id){
     $.ajax({
        url: getHomeUrl() + 'Order/deleteDelivery/',
        type: "post",
        dataType: "json",
        data: {
            id:  id,
        },
        beforeSend:function(){
            $('.botones').button('loading');
       },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
        },
        complete: function (data)
        {
            $('.botones').button('reset');            
            refreshSumary($('#id_order').val());
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}