    $(window).on('load', function () {
     fancybox();

    });
    
    function fancybox(){
          $(".fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
                closeBtn		: false,
                helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			},
                        buttons	: {}
		}
	});
    }