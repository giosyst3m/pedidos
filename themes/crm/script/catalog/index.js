$('#reseat').on('click', function () {
    $('.form-control').selectpicker('deselectAll');
    $('#conten').val('');
});

$('.btn-cart').on('click', function () {
    addCart();
});

function addCart(id) {
    var id_product = id;
    var id_client = $('#id_client').val();
    $.ajax({
        url: getHomeUrl() + 'order/createOrder/',
        type: "post",
        dataType: "json",
        data: {
            id_product: id_product,
            id_client: id_client,
            quantity: $('#quantity-' + id_product).val(),
        },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            udpdateSumary($('#id_client').val(), 0);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}

$('#lookup').on('click', function () {
    lookup();
});
function lookup(){
  $('#products').html('');
    $('#result').html('');
    $('#page').val(0);
    scrollInfinity($('#infinity').val());  
}
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            scrollInfinity($('#infinity').val());
        }
    });
    if ($('#id_order').val() > 0) {
        udpdateSumary($('#id_client').val(), $('#id_order').val());
    }
    lookup();
});

function scrollInfinity(infinity) {
    if (infinity == 0) {
        if (parseInt($('#pages').val()) > parseInt($('#page').val())) {
            $.ajax({
                type: 'post',
                dataType: "json",
                url: getHomeUrl() + 'Catalog/filterProduct',
                data: {
                    progroup: $('#ProGroup').selectpicker('val'),
                    brand: $('#Brand').selectpicker('val'),
                    condition: $('#Condition').selectpicker('val'),
                    category: $('#Category').selectpicker('val'),
                    content: $('#conten').val(),
                    last: $(".productView:last").attr('id'),
                    page: $('#page').val(),
                    pages: $('#pages').val(),
                },
                beforeSend: function (data, textStatus, jqXHR) {
                    $('#loadmoreProducts').show();
                    $('#infinity').val(1);
                    $('#fin').addClass('hidden');
                },
                success: function (data) {
                    $.growl(data.menssage, {
                        type: 'success'
                    });
                    if (data.html) {
                        $('#result').append(data.html);
                        $('#pages').val(data.pages);
                        fancybox();
                    }
                }, complete: function () {
                    $('#infinity').val(0);
                    $('#page').val(parseInt($('#page').val())+1);
                    
                }
            });
        } else {
            $('#loadmoreProducts').hide();
            $('#fin').removeClass('hidden');
        }
    } else {
        $.growl('procesando.....', {
            type: 'info'
        });
    }
}