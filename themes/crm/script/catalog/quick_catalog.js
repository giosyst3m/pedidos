$(document).ready(function () {
    if($('#id_order').val()>0){
        udpdateSumary($('#id_client').val(),$('#id_order').val());
    }
});
$('#Add').on('click', function () {
    getOrder(9);
});

$('#Add2').on('click', function () {
    getOrder(13);
});
function getOrderDatail(id) {
    $.ajax({
        url: getHomeUrl() + 'Catalog/QuickCatalogView/',
        type: "post",
        dataType: "html",
        data: {
            id: id,
        },
        success: function (data, textStatus, jqXHR)
        {
            $('#data').html(data);
//            $.growl(data.menssage, {
//                type: data.type
//            });
//            udpdateSumary();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
//            $.growl("App Error " + errorThrown, {
//                type: "danger"
//            });
        }
    });
}
$('#search').on('click', function () {
    $('#producto').val('');
    $('#data').html();
    $.ajax({
        url: getHomeUrl() + 'Client/getIdFromFullName/',
        type: "post",
        dataType: "json",
        data: {
            name: $('#client').val(),
        },
        success: function (data, textStatus, jqXHR)
        {
//            $('#data').html(data);
            $.growl(data.menssage, {
                type: data.type
            });
            $('#id_client').val(data.id);
            if (data.id > 0) {
                udpdateSumary(data.id,0);
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
});
function getOrder(origin) {
    if ($('#barcode').val() != '') {
        var barcode = $('#barcode').val();
    } else {
        var barcode = 0;
    }
    $.ajax({
        url: getHomeUrl() + 'order/createOrder/',
        type: "post",
        dataType: "json",
        data: {id_product: $('#producto').val(),
            id_client: $('#id_client').val(),
            quantity: $('#quantity').val(),
            getId: true,
            origin: origin,
            barcode: barcode
        },
        beforeSend: function () {
            $('#Add').button('loading');
            $('#Add2').button('loading');
        },
        success: function (data, textStatus, jqXHR)
        {
            $.growl(data.menssage, {
                type: data.type
            });
            udpdateSumary(data.clientId,0);
        },
        complete: function (data)
        {
            $('#Add').button('reset');
            $('#Add2').button('reset');
            $('#producto').val('');
            $('#quantity').val('');
            $('#barcode').val('');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $.growl("App Error " + errorThrown, {
                type: "danger"
            });
        }
    });
}

