$(document).on('change', '#SisUsuario_id_auth_item', function (e) {
    e.preventDefault();
    if ($('#SisUsuario_id_auth_item').val() === 'Clientes') {
        $('#clients').removeClass('hidden');
    } else {
        $('#clients').addClass('hidden');
    }

});

function username() {
    var nombre = $('#SisUsuario_nombre').val().toLowerCase().charAt(0);
    var apellido = $('#SisUsuario_apellido').val().toLowerCase();
    $('#SisUsuario_username').val(nombre+apellido);
}

$(document).on('blur', '#SisUsuario_apellido', username);
$(document).on('blur', '#SisUsuario_nombre', username);

