<?php /* @var $this Controller */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php if(ENV == 'liv'){?>
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-WX87Z9');</script>
<!-- End Google Tag Manager -->
        <?php }else{ ?>
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5TXSL5');</script>
            <!-- End Google Tag Manager -->
        <?php }?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="language" content="en" />
        <?php
        $cs = Yii::app()->clientScript;
        $themePath = Yii::app()->theme->baseUrl;

        /**
         * StyleSHeets
         */
        $cs
                ->registerCssFile($themePath . '/css/jquery-ui-1.8.19.custom.css')
//        ->registerCssFile($themePath.'/css/bootstrap.css')
                ->registerCssFile($themePath . '/css/bootstrap-custom.min.css')
                ->registerCssFile($themePath . '/css/dashboard/animate.css')
//        ->registerCssFile($themePath.'/css/bootstrap-theme.css')
                ->registerCssFile($themePath . '/css/bootstrap-select.css')
                ->registerCssFile($themePath . '/css/bootstrap-timepicker.css')
                ->registerCssFile($themePath . '/css/bootstrap-datetimepicker.css')
                ->registerCssFile($themePath . '/css/ui.jqgrid.css')
                ->registerCssFile($themePath . '/js/jquery.fancybox/jquery.fancybox.css?v=2.1.5')
                ->registerCssFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5')
                ->registerCssFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7')
                ->registerCssFile($themePath . '/css/colors.css')
                ->registerCssFile($themePath . '/css/program.css')
                ->registerCssFile($themePath . '/css/core.css');
// ->registerCssFile($themePath.'/css/font-awesome.min.css');
//        ->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css');

        /**
         * JavaScripts
         */
        $cs
                ->registerCoreScript('jquery', CClientScript::POS_END)
                ->registerCoreScript('jquery.ui', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/dashboard/bootstrap-growl.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/moment.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-select.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-timepicker.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-datetimepicker.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/grid.locale-es.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.jqGrid.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/script/core.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/script/google.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.mousewheel-3.0.6.pack.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/jquery.fancybox.js?v=2.1.5', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-media.js?v=1.0.6', CClientScript::POS_END)
                ->registerScript('tooltip', "$('[data-toggle=\"tooltip\"]').tooltip();
            $('[data-toggle=\"popover\"]').tooltip()"
                        , CClientScript::POS_READY);
        ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5shiv.js"></script>
            <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
        <![endif]-->
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script src="//cdn.ckeditor.com/4.5.10/full/ckeditor.js"></script>
    </head>

    <body>
        <?php if(ENV == 'liv'){?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WX87Z9"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        <?php }else{ ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5TXSL5"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        <?php } ?>
        <div class="modal-backdrop fade in hidden" id="procesando">
            <span>Procesando</span>
            <?php echo BsHtml::imageResponsive($themePath . '/images/ajax-loader.gif', '', array('class' => 'imgProcesando')); ?>
        </div>
        <input type="hidden" value="<?php echo Yii::app()->homeUrl ?>/" id="homeUrl">   
        <div class="container">
            <div class="clear">&nbsp;</div>
            <div class="clear">&nbsp;</div>
            <div class="clear">&nbsp;</div>
            <div class="clear">&nbsp;</div>
            <div class="clear">&nbsp;</div>
            <div id="header">
                <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
            </div><!-- header -->

            <!--Menu -->
            <?php
            if (Yii::app()->params['SISTEMA_MANTENIMIENTO']) {
                echo BsHtml::alert(BsHtml::ALERT_COLOR_INFO, 'SISTEMA EN MANTENIMIENTO FAVOR INTENTAR MAS TARDE');
                echo BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('msg', 'SOPORTE'));
            } else {
                $time = isset(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_MENU_TIME) ? (int) Yii::app()->user->getState('CACHE')->CACHE_REFRESH_MENU_TIME : Yii::app()->params['CACHE_TIME'];
                $criteria = new CDbCriteria();
                if (Yii::app()->user->isGuest) {
                    $criteria->addColumnCondition(array(
                        'padre' => 0,
                        'id_programa' => 1,
                        'sis_menu_r_d_s' => 1,
                    ));
                } else {
                    $criteria->addColumnCondition(array(
                        'padre' => 0,
                        'id_programa' => 2,
                        'r_d_s' => 1,
                        'name_auth_item' => Yii::app()->user->perfil
                    ));
                }
                $criteria->order = 'orden ASC';
                $Menu = VSisMenuAuthItem::model()->cache($time)->findAll($criteria);
                ?>
                <div class="container">
                    <nav class="navbar navbar-inverse navbar-fixed-top">
                        <div class="navbar-header">
                            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">	<span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>	
                            <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('site'); ?> "><?php echo BsHtml::icon(BsHtml::GLYPHICON_HOME); ?></a>
                        </div>
                        <div class="collapse navbar-collapse js-navbar-collapse ">
                            <?php foreach ($Menu as $data) { ?>
                                <ul class="nav navbar-nav">
                                    <li class="dropdown mega-dropdown">	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="<?php echo $data->icono; ?>"></span>&nbsp;<?php echo $data->nombre ?> <span class="caret pull-right"></span></a>
                                        <ul class="dropdown-menu mega-dropdown-menu row navbar-inverse">
                                            <?php
                                            $criteria = new CDbCriteria();
                                            if (Yii::app()->user->isGuest) {
                                                $criteria->addColumnCondition(array(
                                                    'padre' => $data->id,
                                                    'sis_menu_r_d_s' => 1,
                                                    'r_d_s' => 1,
                                                ));
                                            } else {
                                                $criteria->addColumnCondition(array(
                                                    'r_d_s' => 1,
                                                    'sis_menu_r_d_s' => 1,
                                                    'padre' => $data->id,
                                                    'name_auth_item' => Yii::app()->user->perfil
                                                ));
                                            }
                                            $criteria->order = 'orden ASC';
                                            $SubMenu = VSisMenuAuthItem::model()->cache($time)->findAll($criteria);
                                            ?>
                                            <?php foreach ($SubMenu as $key) { ?>
                                                <li class="col-sm-3">
                                                    <ul>
                                                        <li class="dropdown-header" ><span class="<?php echo $key->icono; ?>"></span>&nbsp;<?php echo $key->nombre; ?></li>
                                                        <?php
                                                        $criteria = new CDbCriteria();
                                                        if (Yii::app()->user->isGuest) {
                                                            $criteria->addColumnCondition(array(
                                                                'padre' => $key->id,
                                                                'sis_menu_r_d_s' => 1,
                                                                'r_d_s' => 1,
                                                            ));
                                                        } else {
                                                            $criteria->addColumnCondition(array(
                                                                'r_d_s' => 1,
                                                                'sis_menu_r_d_s' => 1,
                                                                'padre' => $key->id,
                                                                'name_auth_item' => Yii::app()->user->perfil
                                                            ));
                                                        }
                                                        $criteria->order = 'orden ASC';
                                                        $SubMenu2 = VSisMenuAuthItem::model()->cache($time)->findAll($criteria);
                                                        ?>
                                                        <?php foreach ($SubMenu2 as $value) { ?>
                                                            <li><a href="<?php echo Yii::app()->createUrl($value->link); ?>"><span class="<?php echo $value->icono; ?>"></span>&nbsp;<?php echo $value->nombre ?></a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                </ul>
                            <?php } ?>
                            <?php //} ?>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><?php //echo BsHtml::imageResponsive('/files/client/logo/company_logo.png')  ; ?></li>
                                    <?php if (!Yii::app()->user->isGuest) { ?>
                                        <?php
                                        $criteria = new CDbCriteria();
                                        $criteria->addColumnCondition([
                                            'r_d_s' => 1,
                                            'shortcut' => 1,
                                            'sis_menu_r_d_s' => 1,
                                            'name_auth_item' => Yii::app()->user->perfil
                                        ]);
                                        $criteria->order = 'orden ASC';
                                        $shortcut = VSisMenuAuthItem::model()->cache($time)->findAll($criteria);
                                        ?>
                                        <?php foreach ($shortcut as $value) { ?>
                                            <li><a href="<?php echo Yii::app()->createUrl($value->link); ?>"><span class="<?php echo $value->icono; ?>" target="<?php echo trim($value->target); ?>" data-toggle="tooltip" title="<?php echo $value->nombre ?>" data-placement="bottom"></span></a></li>
                                        <?php } ?>
                                        <li><a class="cart-check" href="#"><span class="fa fa-eye"  data-toggle="tooltip" title="<?php echo Yii::t('app', 'View Order'); ?>" data-placement="bottom"></span></a></li>
                                        <li role="presentation" class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" title="<?php echo Yii::t('app', 'Shopping cart'); ?> " data-placement="bottom">
                                                <span class="fa fa-shopping-cart"></span><span class="badge badge-info" id="cart-total"></span> <span class="caret"></span>
                                            </a>
                                            <ul   class="dropdown-menu list-group dropdown-menu-shop">   
                                            </ul>
                                        </li>
                                        <li role="presentation" class="dropdown">
                                            <?php
                                            if (empty(Yii::app()->user->getState('id_company'))) {
                                                $this->changeCompany(CompanySisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['id_sis_usuario' => Yii::app()->user->id, 'r_d_s' => 1])->id_company, false);
                                            }

                                            echo BsHtml::dropDownList('Company', Yii::app()->user->getState('id_company'), $this->getCompanySisUser(), array(
                                                'class' => 'selectpicker show-tick nemnu_select_company',
                                                'data-live-search' => false,
                                                'id' => 'changeCompany',
                                                'empty' => Yii::t('app', '.::Select::.'),
                                                'data-style' => "btn-info",
                                                'onChange' => 'chageCompany(this)'
                                            ));
                                            ?>
                                        </li>
                                        <li><?php echo BsHtml::link(BsHtml::imageCircle(Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . Yii::app()->user->foto), Yii::app()->createUrl('/sistema/usuario/index'), array("data-toggle" => "tooltip", "title" => Yii::t('app', 'Perfil'), "data-placement" => "bottom")) ?></li>
                                        <li><?php echo BsHtml::link(BsHtml::icon(BsHtml::GLYPHICON_REMOVE_CIRCLE) . ' ' . Yii::app()->user->name, Yii::app()->createUrl('/site/logout'), array("data-toggle" => "tooltip", "title" => Yii::t('app', 'Logout'), "data-placement" => "bottom", 'class' => 'text-red')) ?></li>
                                    <?php } ?>
                                    <?php if (Yii::app()->user->isGuest) { ?>
                                        <li><?php echo BsHtml::link(BsHtml::icon(BsHtml::GLYPHICON_OFF) . ' Entrar', Yii::app()->createUrl('/site/login')); ?></li>
                                    <?php } ?>

                                    <li>&nbsp;</li>
                                    <li>&nbsp;</li>
                                    <li>&nbsp;</li>
                                </ul>
                            </div> 
                        </div>
                    </nav>
                </div>
                <?php // $this->MenuBar();  ?>
                <!-- Menu FIN -->
                <?php if (isset($this->breadcrumbs)): ?>

                    <?php
                    $this->widget('zii.widgets.CBreadcrumbs', array(
                        'links' => $this->breadcrumbs,
                    ));
                    ?><!-- breadcrumbs -->
                <?php endif ?>
                <?php
                $flashMessages = Yii::app()->user->getFlashes();
                if ($flashMessages) {
                    foreach ($flashMessages as $key => $message) {
                        echo BsHtml::tag('div', array('class' => 'info'), BsHtml::alert('alert alert-' . $key, $message), true);
                    }
                }
                ?>
                <?php
                echo $content;
            }
            ?>
            <span id="top-link-block" class="hidden">
                <a href="#top" class="btn btn-orange"  onclick="$('html,body').animate({scrollTop: 0}, 'slow');
                        return false;">
                    <i class="glyphicon glyphicon-chevron-up"></i> Ir al inicio
                </a>
            </span>
            <div class="clear"></div>

            <div class="row-fluid">
                <span class="col-lg-12 text-center">
                    <small >Todos derechos reservados <?php echo Yii::app()->name; ?></small>
                    <hr>
                    <small style="font-size:  x-small"><?php echo Yii::app()->params['PowerBy']; ?> - <span class="badge badge-info"><?php echo ENV; ?></span> - <span class="badge badge-info">V <?php echo Yii::app()->params['APP_VERSION']; ?></span></small>
                </span>
            </div>
        </div><!-- page -->
    </body>
</html>
<?php
// Efecto para el div de Mensajes Flash
Yii::app()->clientScript->registerScript(
        'myHideEffect', '$(".info").animate({opacity: 1.0}, 1000000).slideUp("slow");', CClientScript::POS_READY
);
?>
<?php
$this->widget('bootstrap.widgets.BsModal', array(
    'id' => 'MainModal',
    'header' => Yii::t('app', 'Valid'),
    'footer' => array(
        BsHtml::button(
                Yii::t('app', 'Cerrar'), array(
            'data-dismiss' => 'modal',
            'icon' => BsHtml::GLYPHICON_REMOVE,
            'color' => BsHtml::BUTTON_COLOR_PRIMARY
                )
        )
    )
        )
);
?>
