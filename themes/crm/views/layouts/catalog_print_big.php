<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Catalogo de Productos</title>
        <link href="<?php echo Yii::app()->theme->baseUrl ?>/pdf/catalog/css/estilos_big.css" rel="stylesheet" type="text/css"  />
        <?php $cs = Yii::app()->clientScript; $themePath = Yii::app()->theme->baseUrl;
        $cs->registerCssFile($themePath . '/css/bootstrap-custom.min.css');?>

    </head>
    
    <body>
        <?php echo $content; ?>
    </body>
</html>

