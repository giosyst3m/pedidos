<!DOCTYPE html>
<html lang="en">
    <head>
        <?php if (ENV == 'liv') { ?>
            <!-- Google Tag Manager -->
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start':
                                new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-WX87Z9');</script>
            <!-- End Google Tag Manager -->
        <?php } else { ?>
            <!-- Google Tag Manager -->
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start':
                                new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-5TXSL5');</script>
            <!-- End Google Tag Manager -->
        <?php } ?>
        <meta charset="utf-8">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <?php
        $cs = Yii::app()->clientScript;
        $themePath = Yii::app()->theme->baseUrl;
        $cs
                ->registerCoreScript('jquery', CClientScript::POS_END)
                ->registerCoreScript('jquery.ui', CClientScript::POS_END)
        ?>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <!--
        
        Template 2075 Digital Team
        
        http://www.tooplate.com/view/2075-digital-team
        
        -->
        <link rel="stylesheet" href="<?php echo $themePath; ?>/landing/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $themePath; ?>/landing/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $themePath; ?>/landing/css/animate.min.css">
        <link rel="stylesheet" href="<?php echo $themePath; ?>/landing/css/et-line-font.css">
        <link rel="stylesheet" href="<?php echo $themePath; ?>/landing/css/nivo-lightbox.css">
        <link rel="stylesheet" href="<?php echo $themePath; ?>/landing/css/nivo_themes/default/default.css">
        <link rel="stylesheet" href="<?php echo $themePath; ?>/landing/css/style.css">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    </head>
    <body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
        <?php if (ENV == 'liv') { ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WX87Z9"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        <?php } else { ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5TXSL5"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        <?php } ?>
        <!-- preloader section -->
        <div class="preloader">
            <div class="sk-spinner sk-spinner-circle">
                <div class="sk-circle1 sk-circle"></div>
                <div class="sk-circle2 sk-circle"></div>
                <div class="sk-circle3 sk-circle"></div>
                <div class="sk-circle4 sk-circle"></div>
                <div class="sk-circle5 sk-circle"></div>
                <div class="sk-circle6 sk-circle"></div>
                <div class="sk-circle7 sk-circle"></div>
                <div class="sk-circle8 sk-circle"></div>
                <div class="sk-circle9 sk-circle"></div>
                <div class="sk-circle10 sk-circle"></div>
                <div class="sk-circle11 sk-circle"></div>
                <div class="sk-circle12 sk-circle"></div>
            </div>
        </div>
        <input type="hidden" value="<?php echo $themePath ?>/" id="homeUrl">  
        <!-- navigation section -->
        <section class="navbar navbar-fixed-top custom-navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon icon-bar"></span>
                        <span class="icon icon-bar"></span>
                        <span class="icon icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand">Digital Conecta</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#home" class="smoothScroll">HOME</a></li>
                        <li><a href="#work" class="smoothScroll">EXPERIENCIA</a></li>
                        <li><a href="#about" class="smoothScroll">ACERCA DE</a></li>
                        <!--<li><a href="#team" class="smoothScroll">EQUIPO</a></li>-->
                        <li><a href="#portfolio" class="smoothScroll">PORTAFOLIO</a></li>
                        <li><a href="#pricing" class="smoothScroll">PRECIOS</a></li>
                        <li><a href="#contact" class="smoothScroll">CONTACTANOS</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/login'); ?>" class="smoothScroll">LOGIN</a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- home section -->
        <section id="home">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <h3>WEB DESIGN / WEB DEVELOPMENT / DOMINIOS + HOSTING + SERVER</h3>
                        <h1>lo <span class="color">DIGITAL</span> nos <span class="color"> CONECTA</span></h1>
                        <hr>
                        <a href="#work" class="smoothScroll btn btn-default">Experiencia</a>
                        <a href="#contact" class="smoothScroll btn btn-default">Contactar</a>
                        <a href="<?php echo Yii::app()->createUrl('site/login'); ?>" class="smoothScroll btn btn-danger">Clientes Entrar al Sistema</a>
                    </div>
                </div>
            </div>		
        </section>
        <?php if (isset($this->breadcrumbs)): ?>

            <?php
            $this->widget('zii.widgets.CBreadcrumbs', array(
                'links' => $this->breadcrumbs,
            ));
            ?><!-- breadcrumbs -->
        <?php endif ?>
        <?php
        $flashMessages = Yii::app()->user->getFlashes();
        if ($flashMessages) {
            foreach ($flashMessages as $key => $message) {
                echo BsHtml::tag('div', array('class' => 'info'), BsHtml::alert('alert alert-' . $key, $message), true);
            }
        }
        if (!Yii::app()->user->isGuest) {
            if (Yii::app()->user->getState('SYSTEM')->SYSTEM_MAINTENANCE_ACTIVE == 'true') {
                echo BsHtml::tag('div', ['class' => 'alert alert-dismissible ' . Yii::app()->user->getState('SYSTEM')->SYSTEM_MAINTENANCE_MESSAGE_COLOR . ' in fade', 'style' => '    border: 1px solid !important;'], '<a href="#" class="close" data-dismiss="alert" type="button">×</a>' . Yii::app()->user->getState('SYSTEM')->SYSTEM_MAINTENANCE_MESSAGE);
            }
        }
        ?>
        <?php echo $content; ?>
        <!-- footer section -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="">
                            <small style="font-size:  x-small"><?php echo Yii::app()->params['PowerBy']; ?> - <span class="badge badge-info"><?php echo ENV; ?></span> - <span class="badge badge-info">V <?php echo Yii::app()->params['APP_VERSION']; ?></span></small>
                        </div>
                        <hr>
                        <small style="font-size:  x-small">HTML5 Template | Design: <a href="http://www.tooplate.com" target="_parent">Tooplate</a></small>
                        <!--                        <ul class="social-icon">
                                                    <li><a href="#" class="fa fa-facebook wow fadeIn" data-wow-delay="0.3s"></a></li>
                                                    <li><a href="#" class="fa fa-twitter wow fadeIn" data-wow-delay="0.6s"></a></li>
                                                    <li><a href="#" class="fa fa-dribbble wow fadeIn" data-wow-delay="0.9s"></a></li>
                                                    <li><a href="#" class="fa fa-behance wow fadeIn" data-wow-delay="0.9s"></a></li>
                                                    <li><a href="#" class="fa fa-tumblr wow fadeIn" data-wow-delay="0.9s"></a></li>
                                                </ul>-->
                    </div>
                </div>
            </div>
        </footer>


        <!--<script src="<?php echo $themePath; ?>/landing/js/jquery.js"></script>-->
        <script src="<?php echo $themePath; ?>/landing/js/bootstrap.min.js"></script>
        <script src="<?php echo $themePath; ?>/landing/js/smoothscroll.js"></script>
        <script src="<?php echo $themePath; ?>/landing/js/isotope.js"></script>
        <script src="<?php echo $themePath; ?>/landing/js/imagesloaded.min.js"></script>
        <script src="<?php echo $themePath; ?>/landing/js/nivo-lightbox.min.js"></script>
        <script src="<?php echo $themePath; ?>/landing/js/jquery.backstretch.min.js"></script>
        <script src="<?php echo $themePath; ?>/landing/js/wow.min.js"></script>
        <script src="<?php echo $themePath; ?>/landing/js/custom.js"></script>

    </body>
</html>