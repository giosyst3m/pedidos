<!DOCTYPE html>
<html lang="en">
    <head>
        <?php if (ENV == 'liv') { ?>
            <!-- Google Tag Manager -->
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start':
                                new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-WX87Z9');</script>
            <!-- End Google Tag Manager -->
<?php } else { ?>
            <!-- Google Tag Manager -->
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start':
                                new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-5TXSL5');</script>
            <!-- End Google Tag Manager -->
        <?php } ?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <?php
        $cs = Yii::app()->clientScript;
        $themePath = Yii::app()->theme->baseUrl;

        /**
         * StyleSHeets
         */
        $cs
                ->registerCssFile($themePath . '/css/jquery-ui-1.8.19.custom.css')
                ->registerCssFile($themePath . '/css/dashboard/animate.css')
                ->registerCssFile($themePath . '/css/bootstrap-select.css')
                ->registerCssFile($themePath . '/css/bootstrap-timepicker.css')
                ->registerCssFile($themePath . '/css/bootstrap-datetimepicker.css')
                ->registerCssFile($themePath . '/css/ui.jqgrid.css')
                ->registerCssFile($themePath . '/js/jquery.fancybox/jquery.fancybox.css?v=2.1.5')
                ->registerCssFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5')
                ->registerCssFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7')
                ->registerCssFile($themePath . '/css/colors.css')
                ->registerCssFile($themePath . '/css/program.css')
                ->registerCssFile($themePath . '/css/core.css');
        /**
         * JavaScripts
         */
        $cs
                ->registerCoreScript('jquery', CClientScript::POS_END)
                ->registerCoreScript('jquery.ui', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/dashboard/bootstrap-growl.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/moment.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-select.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-timepicker.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-datetimepicker.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/grid.locale-es.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.jqGrid.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/vendors/jquery.hotkeys/jquery.hotkeys.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/vendors/google-code-prettify/src/prettify.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.mousewheel-3.0.6.pack.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/jquery.fancybox.js?v=2.1.5', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-media.js?v=1.0.6', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/script/core.js', CClientScript::POS_END)
                ->registerScript('tooltip', "$('[data-toggle=\"tooltip\"]').tooltip();
            $('[data-toggle=\"popover\"]').tooltip()"
                        , CClientScript::POS_READY);
        $detect = Yii::app()->mobileDetect;
        ?>
        <!-- Bootstrap -->
        <link href="<?php echo $themePath; ?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo $themePath; ?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="<?php echo $themePath; ?>/vendors/nprogress/nprogress.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="<?php echo $themePath; ?>/build/css/custom.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <!-- Bootstrap Colorpicker -->
    <link href="<?php echo $themePath; ?>/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    </head>

    <body class="<?php echo ($detect->isMobile() || $detect->isTablet()  ?'nav-md':'nav-sm'); ?>">
<?php if (ENV == 'liv') { ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WX87Z9"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
<?php } else { ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5TXSL5"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
<?php } ?>
        <input type="hidden" value="<?php echo Yii::app()->homeUrl ?>/" id="homeUrl">  
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col menu_fixed">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="<?php echo Yii::app()->createUrl('site'); ?>" class="site_title"><i class="fa fa-home"></i> <span>Digital Conecta</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                <img src="<?php echo (Yii::app()->user->isGuest) ? '' : Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . Yii::app()->user->foto; ?>" alt="..." class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span><?php echo Yii::t('app', 'Welcome'); ?></span>
                                <h2><?php echo (Yii::app()->user->isGuest) ? '' : Yii::app()->user->nombre; ?></h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br />
                        <?php
                        $time = isset(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_MENU_TIME) ? (int) Yii::app()->user->getState('CACHE')->CACHE_REFRESH_MENU_TIME : Yii::app()->params['CACHE_TIME'];
                        $criteria = new CDbCriteria();
                        if (Yii::app()->user->isGuest) {
                            $criteria->addColumnCondition(array(
                                'padre' => 0,
                                'id_programa' => 1,
                                'sis_menu_r_d_s' => 1,
                            ));
                        } else {
                            $criteria->addColumnCondition(array(
                                'padre' => 0,
                                'id_programa' => 2,
                                'r_d_s' => 1,
                                'name_auth_item' => Yii::app()->user->perfil
                            ));
                        }
                        $criteria->order = 'orden ASC';
                        $Menu = VSisMenuAuthItem::model()->cache($time)->findAll($criteria);
                        ?>
                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3><?php echo (Yii::app()->user->isGuest) ? '' : Yii::app()->user->perfil; ?></h3>
                                <ul class="nav side-menu">
                                            <?php foreach ($Menu as $data) { ?>
                                        <li><a><i class="<?php echo $data->icono; ?>"></i> <?php echo $data->nombre ?><span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <?php
                                                $criteria = new CDbCriteria();
                                                if (Yii::app()->user->isGuest) {
                                                    $criteria->addColumnCondition(array(
                                                        'padre' => $data->id,
                                                        'sis_menu_r_d_s' => 1,
                                                        'r_d_s' => 1,
                                                    ));
                                                } else {
                                                    $criteria->addColumnCondition(array(
                                                        'r_d_s' => 1,
                                                        'sis_menu_r_d_s' => 1,
                                                        'padre' => $data->id,
                                                        'name_auth_item' => Yii::app()->user->perfil
                                                    ));
                                                }
                                                $criteria->order = 'orden ASC';
                                                $SubMenu = VSisMenuAuthItem::model()->cache($time)->findAll($criteria);
                                                ?>
                                                        <?php foreach ($SubMenu as $key) { ?>
                                                    <li><a><?php echo $key->nombre; ?><span class="fa fa-chevron-down"></span></a>
                                                        <ul class="nav child_menu">
                                                            <?php
                                                            $criteria = new CDbCriteria();
                                                            if (Yii::app()->user->isGuest) {
                                                                $criteria->addColumnCondition(array(
                                                                    'padre' => $key->id,
                                                                    'sis_menu_r_d_s' => 1,
                                                                    'r_d_s' => 1,
                                                                ));
                                                            } else {
                                                                $criteria->addColumnCondition(array(
                                                                    'r_d_s' => 1,
                                                                    'sis_menu_r_d_s' => 1,
                                                                    'padre' => $key->id,
                                                                    'name_auth_item' => Yii::app()->user->perfil
                                                                ));
                                                            }
                                                            $criteria->order = 'orden ASC';
                                                            $SubMenu2 = VSisMenuAuthItem::model()->cache($time)->findAll($criteria);
                                                            ?>
        <?php foreach ($SubMenu2 as $value) { ?>
                                                                <li class="sub_menu"><a href="<?php echo Yii::app()->createUrl($value->link); ?>"><?php echo $value->nombre ?></a></li>
        <?php } ?>
                                                        </ul>
                                                    </li>
    <?php } ?>
                                            </ul>
                                        </li> 
<?php } ?>
                                </ul>
                            </div>
                        </div>
                        <!-- /sidebar menu -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav navbar-fixed-top">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo (Yii::app()->user->isGuest) ? '' : Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . Yii::app()->user->foto; ?>"  alt=""><?php echo (Yii::app()->user->isGuest) ? '' : BsHtml::tag('span', ['class' => 'hidden-xs'], Yii::app()->user->nombre); ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="<?php echo Yii::app()->createUrl('/sistema/usuario/index'); ?>"><i class="fa fa-cog pull-right"></i> <?php echo Yii::t('app', 'Profile'); ?> </a></li>
                                        <li><a href="<?php echo Yii::app()->createUrl('/sistema/help/version/n/1.0.0'); ?>"><i class="fa fa-question pull-right"></i>Ayuda</a></li>
                                        <li><a href="<?php echo Yii::app()->createUrl('/program/legal/info/terminos-condiciones'); ?>"><i class="fa fa-info pull-right"></i>Términos</a></li>
                                        <li><a href="<?php echo Yii::app()->createUrl('/site/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> <?php echo Yii::t('app', 'Logout'); ?></a></li>
                                    </ul>
                                </li>
                                <?php if (!Yii::app()->user->isGuest) { ?>
                                    <?php
                                    $criteria = new CDbCriteria();
                                    $criteria->addColumnCondition([
                                        'r_d_s' => 1,
                                        'shortcut' => 1,
                                        'sis_menu_r_d_s' => 1,
                                        'name_auth_item' => Yii::app()->user->perfil
                                    ]);
                                    $criteria->order = 'orden ASC';
                                    $shortcut = VSisMenuAuthItem::model()->cache($time)->findAll($criteria);
                                    ?>
                                    <?php foreach ($shortcut as $value) { ?>
                                        <li><a href="<?php echo Yii::app()->createUrl($value->link); ?>"><i class="<?php echo $value->icono; ?> <?php echo (Yii::app()->mobileDetect->isMobile()) ? '' : 'fa-2x'; ?>" target="<?php echo trim($value->target); ?>" data-toggle="tooltip" title="<?php echo $value->nombre ?>" data-placement="bottom"></i></a></li>
                                        <?php } ?>
                                    <li><a class="cart-check" href="#"><i class="fa fa-eye <?php echo (Yii::app()->mobileDetect->isMobile()) ? '' : 'fa-2x'; ?>"  data-toggle="tooltip" title="<?php echo Yii::t('app', 'View Order'); ?>" data-placement="bottom"></i></a></li>
                                    <li role="presentation" class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" title="<?php echo Yii::t('app', 'Shopping cart'); ?> " data-placement="bottom">
                                            <i class="fa fa-shopping-cart <?php echo (Yii::app()->mobileDetect->isMobile()) ? '' : 'fa-2x'; ?>"></i><i class="badge badge-info" id="cart-total"></i> <i class="caret"></i>
                                        </a>
                                        <ul   class="dropdown-menu list-group dropdown-menu-shop">   
                                        </ul>
                                    </li>
                                        <?php if (!Yii::app()->mobileDetect->isMobile()) { ?>
                                        <li style="margin-top:10px;">
                                            <?php
                                            if (empty(Yii::app()->user->getState('id_company'))) {
                                                $this->changeCompany(CompanySisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['id_sis_usuario' => Yii::app()->user->id, 'r_d_s' => 1])->id_company, false);
                                            }

                                            echo BsHtml::dropDownList('Company', Yii::app()->user->getState('id_company'), $this->getCompanySisUser(), array(
                                                'class' => 'selectpicker show-tick nemnu_select_company ',
                                                'data-live-search' => false,
                                                'id' => 'changeCompany',
                                                'empty' => Yii::t('app', '.::Select::.'),
                                                'data-style' => "btn-blue-gray",
                                                'onChange' => 'chageCompany(this)'
                                            ));
                                            ?>
                                        </li>
    <?php } ?>
<?php } ?>
<?php if (Yii::app()->user->isGuest) { ?>
                                    <li><?php echo BsHtml::link(BsHtml::icon(BsHtml::GLYPHICON_OFF) . ' Entrar', Yii::app()->createUrl('/site/login')); ?></li>
<?php } ?>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->
                <br>
                <br>
                <br>
                <br>
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php if (isset($this->breadcrumbs)): ?>

                                <?php
                                $this->widget('zii.widgets.CBreadcrumbs', array(
                                    'links' => $this->breadcrumbs,
                                ));
                                ?><!-- breadcrumbs -->
                            <?php endif ?>
                            <?php
                            $flashMessages = Yii::app()->user->getFlashes();
                            if ($flashMessages) {
                                foreach ($flashMessages as $key => $message) {
                                    echo BsHtml::tag('div', array('class' => 'info'), BsHtml::alert('alert alert-' . $key, $message), true);
                                }
                            }
                            if (!Yii::app()->user->isGuest) {
                                if (Yii::app()->user->getState('SYSTEM')->SYSTEM_MAINTENANCE_ACTIVE == 'true') {
                                    echo BsHtml::tag('div', ['class' => 'alert alert-dismissible ' . Yii::app()->user->getState('SYSTEM')->SYSTEM_MAINTENANCE_MESSAGE_COLOR . ' in fade','style'=>'    border: 1px solid !important;'], '<a href="#" class="close" data-dismiss="alert" type="button">×</a>' . Yii::app()->user->getState('SYSTEM')->SYSTEM_MAINTENANCE_MESSAGE);
                                }
                            }
                            ?>
                            <?php echo $content; ?>
                        </div>
                    </div>
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <span id="top-link-block" class="hidden">
                        <a href="#top" class="btn btn-orange"  onclick="$('html,body').animate({scrollTop: 0}, 'slow');
                return false;">
                            <i class="glyphicon glyphicon-chevron-up"></i> Ir al inicio
                        </a>
                    </span>
                    <div class="pull-right">
                        Lo <b>Digital</b> Nos <b>Conecta</b> Usando <a href="https://digitalconecta.com"><b>Sistemas Integrados</b></a>
                        <small style="font-size:  x-small"><?php echo Yii::app()->params['PowerBy']; ?> - <span class="badge badge-info"><?php echo ENV; ?></span> - <span class="badge badge-info">V <?php echo Yii::app()->params['APP_VERSION']; ?></span></small>
                        <small style="font-size:  x-small">Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a></small>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <!-- FastClick -->
        <script src="<?php echo $themePath; ?>/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="<?php echo $themePath; ?>/vendors/nprogress/nprogress.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="<?php echo $themePath; ?>/build/js/custom.min.js"></script>
        <!-- ECharts -->
        <script src="<?php echo $themePath; ?>/vendors/echarts/dist/echarts.min.js"></script>
        <script src="<?php echo $themePath; ?>/vendors/echarts/map/js/world.js"></script>
        <!-- jQuery Smart Wizard -->
    <script src="<?php echo $themePath; ?>/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="<?php echo $themePath; ?>/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    </body>
</html>
