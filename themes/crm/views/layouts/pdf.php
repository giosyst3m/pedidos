<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="language" content="en" />
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5shiv.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
    <![endif]-->
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
    <input type="hidden" value="<?php echo Yii::app()->homeUrl ?>/" id="homeUrl">
<div class="container">

    <?php echo $content; ?>

	<div class="clear"></div>

    <div class="row-fluid">
                <span class="col-lg-12 text-center">
                    <small >Todos derechos reservados <?php echo Yii::app()->name; ?></small>
                    <hr>
                    <small style="font-size:  x-small"><?php echo Yii::app()->params['PowerBy']; ?> - <span class="badge badge-info"><?php echo ENV; ?></span> - <span class="badge badge-info">V <?php echo Yii::app()->params['APP_VERSION']; ?></span></small>
                </span>
            </div>
</div><!-- page -->

</body>
</html>
