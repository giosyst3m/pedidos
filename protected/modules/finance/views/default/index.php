<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
$this->breadcrumbs = array(
    $this->module->id,
);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Account') ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php $this->widget('bootstrap.widgets.BsListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_account',
)); ?>
    </div>
</div>