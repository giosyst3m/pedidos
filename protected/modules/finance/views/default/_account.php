<div class="row">
    <div class="col-xs-12">
        <i class="text-primary <?php echo $data->icon ?>"></i>&nbsp;<b class="text-primary"><?php echo $data->name; ?></b>
        <?php foreach (VFinAccountTotal::model()->findAllByAttributes(['parent' => $data->id]) as $row) { ?>  
            <div class="row">
                <div class="col-xs-6">
                    <a href="<?php echo $this->createUrl('account/index', ['id' => $row->id]); ?>" class="btn btn-dark btn-xs"><i class="fa fa-plus"></i></a>
                    <?php echo $row->name; ?>
                </div>
                <div class="col-xs-6 text-right <?php echo ($row->total < 0) ? 'text-red' : '' ?>">
                    <?php
                    $html = $row->acronym . $row->symbol . ' ';
                    if ($row->total > 0) {
                        $html .= '<span class="text-success"><i class="fa fa-plus"></i>' . number_format((empty($row->total) ? 0 : $row->total), Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS) . '</span>';
                    } else {
                        $html .= '<span class="text-danger"><i class="fa fa-minus"></i>' . number_format((empty($row->total * -1) ? 0 : $row->total), Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS) . '</span>';
                    }
                    echo $html;
                    ?><div class="col-xs-3">
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


