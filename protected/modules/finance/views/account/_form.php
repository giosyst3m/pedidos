<?php
/* @var $this AccountController */
/* @var $model Account */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'account-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListControlGroup($model,'parent',$Account,array('data-style'=>'btn-info','class'=>'selectpicker show-tick','empty'=>'Select one','data-live-search'=>true,)); ?>
    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'icon',array('maxlength'=>10)); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_money',$Money,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','empty'=>'Select one','data-live-search'=>true,)); ?>
    <?php 
    if(Yii::app()->user->checkAccess('AccountCreateStatusChange') || Yii::app()->user->checkAccess('AccountUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('AccountCreateButtonSave') || Yii::app()->user->checkAccess('AccountUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('AccountCreateButtonNew') || Yii::app()->user->checkAccess('AccountUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Account/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
