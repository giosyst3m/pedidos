<?php

/* @var $this AccountController */
/* @var $model Account */


$this->breadcrumbs = array(
    'Accounts' => array('index'),
    'Manage',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-list', 'label' => 'List Account', 'url' => array('index')),
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create Account', 'url' => array('create')),
);
?>


<?php

$this->widget('bootstrap.widgets.BsGridView', array(
    'id' => 'account-grid',
    'dataProvider' => $model2->search(),
    'filter' => $model2,
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        [
            'name' => 'category',
            'type' => 'html',
            'value' => function($data) {
                return Category::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findByPk($data->father)->name . ' / ' . $data->category . '<br>' .
                        BsHtml::tag('small', ['style' => 'font-size: xx-small'], ($data->id_client > 0 ? $data->client : false), true) . '<br/>' .
                        BsHtml::tag('b', ['style' => 'font-size: xx-small'], '<i>' . $data->note . '</i>', true);
            }
                ],
                [
                    'name' => 'total',
                    'type' => 'html',
                    'value' => function($data) {
                        if ($data->total > 0) {
                            return '<span class="text-success">' . BsHtml::tag('i', ['class' => 'fa fa-plus'], false, true) . ' ' . number_format($data->total, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS) . '</span>';
                        } else {
                            return '<span class="text-danger">' . BsHtml::tag('i', ['class' => 'fa fa-minus'], false, true) . ' ' . number_format($data->total * -1, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS) . '</span>';
                        }
                    },
                            'htmlOptions' => [
                                'class' => 'text-right'
                            ]
                        ],
                        array(
                            'template' => '{delete}',
                            'class' => 'bootstrap.widgets.BsButtonColumn',
                            'buttons' => array(
                                'delete' => array(
                                    'url' => function($data) {
                                        return Yii::app()->createUrl("ordDetail/delete/", array("id" => $data->id, 'reason' => Yii::t('JSON', 1016)));
                                    },
                                            'options' => [
                                                'class' => 'btn btn-danger btn-xs',
                                            ]
                                        ),
                                    ),
                                ),
                            ),
                        ));
                        ?>





