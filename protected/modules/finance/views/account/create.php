<?php
/* @var $this AccountController */
/* @var $model Account */
?>

<?php
$this->breadcrumbs = array(
    'Accounts' => array('create'),
    'Create',
);
?>


<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Create') ?><b> <?php echo Yii::t('app', 'Account'); ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />

        <?php
        if (Yii::app()->user->checkAccess('AccountCreateFormView')) {
            $this->renderPartial('_form', array(
                'model' => $model,
                'Money' => $Money,
                'Account' => $Account
                    )
            );
        }
        ?>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('AccountCreateAdminView')) {
    $this->renderPartial('admin', array(
        'model2' => $model2,
        'Money' => $Money,
        'Account' => $Account
            )
    );
}
?>