<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $form BSActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'order-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => Yii::app()->mobileDetect->isMobile()?BsHtml::FORM_LAYOUT_INLINE:BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    )
        ));
?>
<p class="help-block"> <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required.') ?></p>
<?php echo $form->errorSummary(array($model, $model1)); ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php echo $form->dropDownListControlGroup($model, 'id_client', $VClient, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'selectd' => 1, 'data-live-search' => true,)); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php echo $form->textFieldControlGroup($model1, 'date',['class'=> 'date','value'=>  date('Y-m-d')]); ?>
    </div>
</div>
<section>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <?php
            echo BsHtml::dropDownList('category', null, $Category, ['data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Categoria', 'data-live-search' => true,
                'ajax' => [
                    'type' => 'POST',
                    'url' => CController::createUrl('/Category/SubCategory'), //or $this->createUrl('loadcities') if '$this' extends CController
                    'update' => '#OrdDetail_id_category', //or 'success' => 'function(data){...handle the data in the way you want...}',
                    'data' => array('id' => 'js:this.value'),
                ]
            ]);
            ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <?php echo $form->dropDownListControlGroup($model1, 'id_category', [], ['data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Sub-Categoria', 'data-live-search' => true,]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <?php echo $form->dropDownListControlGroup($model1, 'id_money', $Money, ['data-style' => 'btn-info', 'class' => 'selectpicker show-tick', 'data-live-search' => true,]); ?>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <div class="material-switch">
                <input id="OrdDetail_type" name="OrdDetail[type]" type="checkbox"/>
                <label for="OrdDetail_type" class="label-danger"></label>
            </div>
            <i class="fa fa-plus text-success">&nbsp;</i><i class="fa fa-minus text-red"></i>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <?php echo $form->numberFieldControlGroup($model1, 'price', ['class' => 'text-right']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php echo $form->textAreaControlGroup($model1, 'note', ['rows' => 2, 'placeholder' => 'Nota']); ?>
        </div>
    </div>
</section>

<?php
if (Yii::app()->user->checkAccess('OrderCreateButtonSave') || Yii::app()->user->checkAccess('OrderUpdateButtonSave')) {
    echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY));
}
?>    
<?php $this->endWidget(); ?>
