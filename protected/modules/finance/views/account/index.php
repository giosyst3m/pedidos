<?php
/* @var $this AccountController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
    'Accounts',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create Account', 'url' => array('create')),
    array('icon' => 'glyphicon glyphicon-tasks', 'label' => 'Manage Account', 'url' => array('admin')),
);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Registro') ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        $this->renderPartial('_order', array(
            'model' => $model,
            'model1' => $model1,
            'VClient' => $VClient,
            'Category' => $Category,
            'Money' => $Money,
                )
        );
        ?>
    </div>
</div>
<hr>
<?php
$this->renderPartial('_account_admin', array(
    'model2' => $model2,
        )
);
?>
<style>
    .total_footer{
        bottom: 0px;
        clear: both;
        display: block;
        padding: 5px;
        position: fixed;
        width: auto;
        background: #2A3F54;
        color: #FFF;
        font-size: large;
    }
</style>
<div class="row s total_footer">
    <div class="xs-col-12">
        <?php
        if($total > 0){
            $html = '<i class="fa fa-plus text-success"></i> ';
        }else{
            $html = '<i class="fa fa-minus text-danger"></i> ';
        }
        echo 'Total: '.$html.  number_format(($total < 0)?$total*-1:$total, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS);
        ?>
    </div>
</div>