<?php

class DefaultController extends Controller {

    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Account');
        $dataProvider->countCriteria->addColumnCondition(['r_d_s'=>1,'parent'=>null]);
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

}
