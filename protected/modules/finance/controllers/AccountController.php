<?php

class AccountController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('AccountActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('AccountActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('AccountActionCreate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('AccountActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('AccountActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('AccountActionDelete'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Account;
        $model->id_company = Yii::app()->user->getState('id_company');
        $model2 = new Account('search');
        $model2->unsetAttributes();  // clear any default values
        $model2->id_company = $model->id_company;
        if (isset($_GET['Account']))
            $model2->attributes = $_GET['Account'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Account'])) {
            try {
                $model->attributes = $_POST['Account'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Account'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Account'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model,
            'model2' => $model2,
            'Money' => BsHtml::listData(Money::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAll('r_d_s=1'), 'id', 'acronym'),
            'Account' => BsHtml::listData(Account::model()->findAll('r_d_s=1'), 'id', 'name')
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model->id_company = Yii::app()->user->getState('id_company');
        $model2 = new Account('search');
        $model2->unsetAttributes();  // clear any default values
        $model2->id_company = $model->id_company;
        if (isset($_GET['Account']))
            $model2->attributes = $_GET['Account'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Account'])) {
            try {
                $model->attributes = $_POST['Account'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'Account'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'Account'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model, 'model2' => $model2
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('AccountDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Account'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex($id) {
        $Order = new Order('finance');
        $Order->id_status = 12; //Factura Registrada
        $Order->id_discount = 1; // 0% desdescuento
        $Order->id_company = Yii::app()->user->getState('id_company');
        $Order->id_document = 3; // Facturas de Compra
        $Order->id_account = $id;

        $OrdDetail = new OrdDetail('finance');
        $OrdDetail->id_product = 1; // Producto Generico
        $OrdDetail->quantity = 1; // Todo es uno
        $OrdDetail->id_discount = 1; // 0% descuento
        $OrdDetail->id_inv_origin = 14; // formulario de finanzas

        $this->performAjaxValidation([$Order, $OrdDetail]);
        if (isset($_POST['Order'], $_POST['OrdDetail'])) {
            // populate input data to $a and $b
            $Order->attributes = $_POST['Order'];
            if(empty($Order->id_client)){
                $Order->id_client = 1;
            }
            $OrdDetail->attributes = $_POST['OrdDetail'];
            $OrdDetail->id_order = 1;
            if ($OrdDetail->type == 'on') {
                $OrdDetail->type = 1;
                $OrdDetail->price = $OrdDetail->price * -1;
            } else {
                $OrdDetail->type = 0;
            }
            if ($OrdDetail->validate()) {
                $Order->number = $this->getNumber(3);
            }
            // validate BOTH $a and $b
            $valid = $Order->validate();
            $valid = $OrdDetail->validate() && $valid;

            if ($valid) {
                // use false parameter to disable validation
                if ($Order->save(false)) {
                    $OrdDetail->id_order = $Order->id;
                    $OrdDetail->save(false);
                }
            }
            $this->redirect(array('index', 'id' => $id));
        }


        $model2 = new VFinAccount('search');
        $model2->unsetAttributes();  // clear any default values
        $model2->id_company = Yii::app()->user->getState('id_company');
        $model2->id_account = $id;

        if (isset($_GET['VFinAccount']))
            $model2->attributes = $_GET['VFinAccount'];

        $this->render('index', array(
            'model' => $Order,
            'model1' => $OrdDetail,
            'model2' => $model2,
            'total' => VFinAccountTotal::model()->findByAttributes(['id'=>$id])->total,
            'Category' => BsHtml::listData(Category::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAllByAttributes(['r_d_s' => 1, 'father' => null, 'id_cat_group' => 2]), 'id', 'name'),
            'Document' => Document::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAllByAttributes(['r_d_s' => 1]),
            'VClient' => BsHtml::listData(VClient::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAllByAttributes(['r_d_s' => 1]), 'id', 'full_name'),
            'Money' => BsHtml::listData(Money::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAllByAttributes(['r_d_s' => 1]), 'id', 'acronym'),
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Account('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Account']))
            $model->attributes = $_GET['Account'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Account the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Account::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Account $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'account-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'order-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
