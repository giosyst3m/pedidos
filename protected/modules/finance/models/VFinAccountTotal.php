<?php

/**
 * This is the model class for table "v_fin_account_total".
 *
 * The followings are the available columns in table 'v_fin_account_total':
 * @property integer $id
 * @property string $name
 * @property string $icon
 * @property integer $parent
 * @property string $total
 * @property string $acronym
 * @property string $symbol
 */
class VFinAccountTotal extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_fin_account_total';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, acronym, symbol', 'required'),
			array('id, parent', 'numerical', 'integerOnly'=>true),
			array('name, icon, symbol', 'length', 'max'=>255),
			array('total', 'length', 'max'=>43),
			array('acronym', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, icon, parent, total, acronym, symbol', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'name' => Yii::t('app','Name'),
			'icon' => Yii::t('app','Icon'),
			'parent' => Yii::t('app','Parent'),
			'total' => Yii::t('app','Total'),
			'acronym' => Yii::t('app','Acronym'),
			'symbol' => Yii::t('app','Symbol'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('acronym',$this->acronym,true);
		$criteria->compare('symbol',$this->symbol,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VFinAccountTotal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
