<?php

/**
 * This is the model class for table "v_fin_account".
 *
 * The followings are the available columns in table 'v_fin_account':
 * @property string $full_name
 * @property string $category
 * @property integer $father
 * @property string $acronym
 * @property string $total
 * @property integer $id_client
 * @property integer $id_company
 * @property integer $id_account
 * @property integer $r_c_u
 * @property string $r_d_r
 * @property integer $type
 * @property string $note
 * @property integer $id
 * @property string $client
 */
class VFinAccount extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_fin_account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category, acronym, id_client, id_company, client', 'required'),
			array('father, id_client, id_company, id_account, r_c_u, type, id', 'numerical', 'integerOnly'=>true),
			array('category, r_d_r, client', 'length', 'max'=>255),
			array('acronym', 'length', 'max'=>3),
			array('total', 'length', 'max'=>21),
			array('full_name, note', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('full_name, category, father, acronym, total, id_client, id_company, id_account, r_c_u, r_d_r, type, note, id, client', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'full_name' => Yii::t('app','Full Name'),
			'category' => Yii::t('app','Category'),
			'father' => Yii::t('app','Father'),
			'acronym' => Yii::t('app','Acronym'),
			'total' => Yii::t('app','Total'),
			'id_client' => Yii::t('app','Id Client'),
			'id_company' => Yii::t('app','Id Company'),
			'id_account' => Yii::t('app','Id Account'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_d_r' => Yii::t('app','R D R'),
			'type' => Yii::t('app','Type'),
			'note' => Yii::t('app','Note'),
			'id' => Yii::t('app','ID'),
			'client' => Yii::t('app','Client'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('father',$this->father);
		$criteria->compare('acronym',$this->acronym,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('id_client',$this->id_client);
		$criteria->compare('id_company',$this->id_company);
		$criteria->compare('id_account',$this->id_account);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_d_r',$this->r_d_r,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('client',$this->client,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VFinAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
