<?php

class LegalController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('LegalActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('LegalActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('LegalActionCreate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('LegalActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('LegalActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('LegalActionDelete'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Legal;
        $model2 = new Legal('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Legal']))
            $model2->attributes = $_GET['Legal'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Legal'])) {
            try {
                $model->attributes = $_POST['Legal'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Legal'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Legal'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model, 
            'model2' => $model2,
            'LegType'=> BsHtml::listData(LegType::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Program'=> BsHtml::listData(Program::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Country'=> BsHtml::listData(Country::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name')
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model2 = new Legal('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Legal']))
            $model2->attributes = $_GET['Legal'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Legal'])) {
            try {
                $model->attributes = $_POST['Legal'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'Legal'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'Legal'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model, 'model2' => $model2,
            'LegType'=> BsHtml::listData(LegType::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Program'=> BsHtml::listData(Program::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Country'=> BsHtml::listData(Country::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name')
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('LegalDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Legal'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Legal');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Legal('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Legal']))
            $model->attributes = $_GET['Legal'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Legal the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Legal::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Legal $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'legal-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
