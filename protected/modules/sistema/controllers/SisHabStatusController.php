<?php

class SisHabStatusController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column1';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
            array('allow',  // allow all users to perform 'index'
				'actions'=>array('index'),
				'roles'=>array('SisHabStaActionIndex'),
			),
            array('allow',  // allow all users to perform 'view'
				'actions'=>array('view'),
				'roles'=>array('SisHabStaActionView'),
			),
            array('allow',  // allow all users to perform 'create'
				'actions'=>array('create'),
				'roles'=>array('SisHabStaActionCreate'),
			),
            array('allow',  // allow all users to perform 'update'
				'actions'=>array('update'),
				'roles'=>array('SisHabStaActionUpdate'),
			),
            array('allow',  // allow all users to perform 'admin'
				'actions'=>array('admin'),
				'roles'=>array('SisHabStaActionAdmin'),
			),
            array('allow',  // allow all users to perform 'delete'
				'actions'=>array('delete'),
				'roles'=>array('SisHabStaActionDelete'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new SisHabStatus;
        $model2=new SisHabStatus('search');
		$model2->unsetAttributes();  // clear any default values
		if(isset($_GET['SisHabStatus']))
			$model2->attributes=$_GET['SisHabStatus'];
            
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SisHabStatus']))
		{
            try{
                $model->attributes=$_POST['SisHabStatus'];
                if($model->save()){
                    Yii::app()->user->setFlash('success',  Yii::t('app', 'SUCCESS_CREATE_OK'));
                    $this->redirect(array('create','id'=>$model->id));
                }else{
                        Yii::app()->user->setFlash('danger',  Yii::t('app', 'SUCCESS_CREATE_ERROR'));
                    }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger',$e->getCode().' '.$e->getMessage());
            }
		}

		$this->render('create',array(
		'model'=>$model,'model2'=>$model2
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $model2=new SisHabStatus('search');
		$model2->unsetAttributes();  // clear any default values
		if(isset($_GET['SisHabStatus']))
			$model2->attributes=$_GET['SisHabStatus'];
            
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SisHabStatus']))
		{
            try{
                $model->attributes=$_POST['SisHabStatus'];
                if($model->save()){
                    Yii::app()->user->setFlash('success',  Yii::t('app', 'SUCCESS_UPDATE_OK'));
                    $this->redirect(array('create','id'=>$model->id));
                }else{
                    Yii::app()->user->setFlash('danger',  Yii::t('app', 'SUCCESS_UPDATE_ERROR'));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger',$e->getCode().' '.$e->getMessage());
            }
		}

		$this->render('update',array(
			'model'=>$model,'model2'=>$model2
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->user->checkAccess('SisHabStaDeleteButtonDelete')){
            if(Yii::app()->request->isPostRequest){
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500,  Yii::t('msg', 'DELETE_ERROR',array('{info}'=>'Puede tener Productos Asociados')));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else{
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }
        }else{
            throw new CHttpException(400,  Yii::t('auth', 'DENEY',array('{accion}'=>'Eliminar')));
        }
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SisHabStatus');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new SisHabStatus('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SisHabStatus']))
			$model->attributes=$_GET['SisHabStatus'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return SisHabStatus the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=SisHabStatus::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param SisHabStatus $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sis-hab-status-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}