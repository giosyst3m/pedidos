<?php

class UsuarioController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'getForm', 'recoveryPassword'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('update', 'chagePassword', 'sendEmail'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete'),
                'roles' => array('SisUsuActionDelete'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('create'),
                'roles' => array('SisUsuActionCreate'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin'),
                'roles' => array('SisUsuActionAdmin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new SisUsuario('registre');
        $model2 = new SisUsuario('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['SisUsuario']))
            $model2->attributes = $_GET['SisUsuario'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['SisUsuario'])) {
            try {
                $model->attributes = $_POST['SisUsuario'];
                if (!SisUsuario::model()->findByAttributes(array('email' => $model->email))) {
                    
                    $Image = CUploadedFile::getInstance($model, 'foto');
                    if (!empty($Image)) {
                        $model->foto = "usu_" . strtolower(str_replace(' ', '_', $model->nombre)) . '_' . strtolower(str_replace(' ', '_', $model->apellido)) . '.' . $Image->getExtensionName();
                        $Image->saveAs(Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_USUARIO_IMAGEN'] . $model->foto);
                    }
                    if ($model->save()) {
                        SisUsuario::model()->updateByPk($model->getPrimaryKey(), array('clave' => sha1($model->clave), 'clave2' => null));
                        Yii::app()->authManager->assign($model->id_auth_item, $model->getPrimaryKey());
                        if ($model->acceso == 1) {
                            $this->sendEmail($model->email, 'user_create', 'EMAIL_USER_CREATE', 'EMAIL_USER_CREATE_OK', 'EMAIL_USER_CREATE_ERROR', array('email' => $model->email, 'nombre' => $model->nombre, 'apellido' => $model->apellido, 'pws' => $model->clave, 'username' => $model->username));
                        }
                        Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => 'Registro de usuarios')));
                        Yii::log('USUARIOS se ha creado un usuario nuevo en el sistema idUsuario: ' . $model->id . ' Nombre: ' . $model->nombre . ' Apellido: ' . $model->apellido . ' Perfil: ' . $model->id_auth_item . ' Email: ' . $model->email . ' Acceso' . $model->acceso . ' User: ' . Yii::app()->user->id, 'SisUsuario', __METHOD__ . ":" . __LINE__);
                        $this->redirect(array('update', 'id' => $model->id));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR'));
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR_EMAMIL', array('{email}' => $model->email)));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }
        if (Yii::app()->user->checkAccess('SisUsuAdminView')) {
            $rol = CHtml::listData(VAuthRole::model()->findAll(), 'name', 'name');
        } else {
            $rol = CHtml::listData(VAuthRole::model()->findAll("name<>'admin.*'"), 'name', 'name');
        }
        $this->render('create', array(
            'model' => $model,
            'model2' => $model2,
            'Company' => BsHtml::listData(Company::model()->findAll('r_d_s=1'), 'id', 'name'),
            'Companies' => array(),
            'VClient'=> BsHtml::listData(VClient::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'full_name'),
            'role' => $rol,
            'tipo' => CHtml::listData(SisUsuTipo::model()->findAll('r_d_s=1'), 'id', 'nombre'),
            'cargo' => CHtml::listData(SisUsuCargo::model()->findAll('r_d_s=1'), 'id', 'nombre'),
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model7 = new Order('search');
        $model7->unsetAttributes();  // clear any default values
        $model7->r_c_u = $id;
        if (isset($_GET['Order']))
            $model7->attributes = $_GET['Order'];
        
        $model5 = new ZoneSisUsuario();
        $model6 = new ZoneSisUsuario('search');
        $model6->unsetAttributes();  // clear any default values
        $model6->id_sis_usuario = $id;
        if (isset($_GET['ZoneSisUsuario']))
            $model6->attributes = $_GET['ZoneSisUsuario'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model5);

        if (isset($_POST['ZoneSisUsuario'])) {
            try {
                $model5->attributes = $_POST['ZoneSisUsuario'];
                $model5->id_sis_usuario = $id;
                if ($model5->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'ZoneSisUsuario'))));
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'ZoneSisUsuario'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 5007));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $model3 = new CompanySisUsuario;
        $model4 = new CompanySisUsuario('search');
        $model4->unsetAttributes();  // clear any default values
        $model4->id_sis_usuario = $id;
        if (isset($_GET['CompanySisUsuario']))
            $model4->attributes = $_GET['CompanySisUsuario'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model3);

        if (isset($_POST['CompanySisUsuario'])) {
            try {
                $model3->attributes = $_POST['CompanySisUsuario'];
                $model3->id_sis_usuario = $id;
                if ($model3->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'CompanySisUsuario'))));
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'CompanySisUsuario'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 5007));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $model = $this->loadModel($id);
        $model->scenario = 'registre';
        $model2 = new SisUsuario('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['SisUsuario']))
            $model2->attributes = $_GET['SisUsuario'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['SisUsuario'])) {
            try {
                $model->attributes = $_POST['SisUsuario'];
                $Image = CUploadedFile::getInstance($model, 'foto');
                if (!empty($Image)) {
                    $model->foto = "usu_" . strtolower(str_replace(' ', '_', $model->nombre)) . '_' . strtolower(str_replace(' ', '_', $model->apellido)) . '.' . $Image->getExtensionName();
                    $Image->saveAs(Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_USUARIO_IMAGEN'] . $model->foto);
                }
                if ($model->save()) {
                    SisUsuario::model()->updateByPk($id, array('clave' => sha1($model->clave), 'clave2' => null));
                    Yii::log('USUARIOS se ha actualizado en el sistema idUsuario: ' . $model->id . ' Nombre: ' . $model->nombre . ' Apellido: ' . $model->apellido . ' Perfil: ' . $model->id_auth_item . ' Email: ' . $model->email . ' Acceso' . $model->acceso . ' User: ' . Yii::app()->user->id, 'SisUsuario', __METHOD__ . ":" . __LINE__);
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK'));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR'));
                }
                $this->redirect(array('update', 'id' => $model->id));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }
        if (Yii::app()->user->checkAccess('SisUsuAdminView')) {
            $rol = CHtml::listData(VAuthRole::model()->findAll(), 'name', 'name');
        } else {
            $rol = CHtml::listData(VAuthRole::model()->findAll("name<>'admin.*'"), 'name', 'name');
        }
        $this->render('update', array(
            'model' => $model,
            'model2' => $model2,
            'model3' => $model3,
            'model4' => $model4,
            'model5' => $model5,
            'model6' => $model6,
            'model7' => $model7,
            'VClient'=> BsHtml::listData(VClient::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'full_name'),
            'FormDefault2'=>true,//Show Zone
            'Company' => BsHtml::listData(Company::model()->findAll('r_d_s=1'), 'id', 'name'),
            'Companies' => BsHtml::listData(VCompanySisUsuario::model()->findAllByAttributes(array('id_sis_usuario' => $id)), 'id_company', 'company'),
            'role' => $rol,
            'tipo' => CHtml::listData(SisUsuTipo::model()->findAll('r_d_s=1'), 'id', 'nombre'),
            'cargo' => CHtml::listData(SisUsuCargo::model()->findAll('r_d_s=1'), 'id', 'nombre'),
            'VSisUsuario' => CHtml::listData(VSisUsuario::model()->findAll('r_d_s=1'), 'id', 'complete_name'),
            'Zone' => CHtml::listData(Zone::model()->findAll('r_d_s=1'), 'id', 'name'),
            'Client' => CHtml::listData(Client::model()->findAll('r_d_s=1'), 'id', 'name'),
            'Status' => CHtml::listData(Status::model()->findAll('r_d_s=1'), 'id', 'name'),
            'FormDefault' => true, //Show Company
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('SisUsuDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => 'Puede tener Productos Asociados')));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = $this->loadModel(Yii::app()->user->id);
        $model->scenario = 'perfil';
        $model2 = $this->loadModel(Yii::app()->user->id);
        $model2->scenario = 'chagePassword';
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['SisUsuario'])) {
            try {
                $model->attributes = $_POST['SisUsuario'];
                //if(!SisUsuario::model()->findByAttributes(array('email'=>$model->email))){
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK'));
                    //$this->redirect(array('create','id'=>$model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR'));
                }
//                }else{
//                    Yii::app()->user->setFlash('danger',  Yii::t('app', 'SUCCESS_CREATE_ERROR_EMAMIL',array('{email}'=>$model->email)));
//                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('perfil', array(
            'model' => $model,
            'model2' => $model2,
            'tipo' => CHtml::listData(SisUsuTipo::model()->findAll('r_d_s=1'), 'id', 'nombre'),
            'cargo' => CHtml::listData(SisUsuCargo::model()->findAll('r_d_s=1'), 'id', 'nombre'),
        ));
    }

    public function actionchagePassword() {
        $model = $this->loadModel(Yii::app()->user->id);
        $model->scenario = 'perfil';
        $model2 = $this->loadModel(Yii::app()->user->id);
        $model2->scenario = 'chagePassword';
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model2);

        if (isset($_POST['SisUsuario'])) {
            try {
                $model2->attributes = $_POST['SisUsuario'];

                if ($model2->save()) {
                    SisUsuario::model()->updateByPk($model->getPrimaryKey(), array('clave' => sha1($_POST['SisUsuario']['clave']), 'clave2' => null, 'username' => $model->username));
                    $this->sendEmail($model->email, 'user_password_change', 'EMAIL_USER_PASSWORD_CHANGE', 'EMAIL_USER_PASSWORD_CHANGE_OK', 'EMAIL_USER_PASSWORD_CHANGE_ERROR');
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK'));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR'));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }
        $this->render('perfil', array(
            'model' => $model,
            'model2' => $model2,
            'tipo' => CHtml::listData(SisUsuTipo::model()->findAll('r_d_s=1'), 'id', 'nombre'),
            'cargo' => CHtml::listData(SisUsuCargo::model()->findAll('r_d_s=1'), 'id', 'nombre'),
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new SisUsuario('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['SisUsuario']))
            $model->attributes = $_GET['SisUsuario'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SisUsuario the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = SisUsuario::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param SisUsuario $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && ($_POST['ajax'] === 'sis-usuario-form' || $_POST['ajax'] === 'sis-usuario-form-2')) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'company-sis-usuario-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'zone-sis-usuario-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionrecoveryPassword() {
        $email = $_POST['email'];
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $model = SisUsuario::model()->findByAttributes(array('email' => $email));
            if ($model) {
                $pws = $this->getPasswordRandom();
                $model->clave = sha1($pws);
                SisUsuario::model()->updateByPk($model->id, ['clave'=>$model->clave]);
                $this->sendEmail($model->email, 'user_password_recovery', 'EMAIL_USER_PASSWORD_RECOVERY', 'EMAIL_USER_PASSWORD_RECOVERY_OK', 'EMAIL_USER_PASSWORD_RECOVERY_ERROR', array('nombre' => $model->nombre, 'apellido' => $model->apellido, 'pws' => $pws, 'email' => $model->email));
                echo BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, Yii::t('msg', 'EMAIL_USER_PASSWORD_RECOVERY_OK'));
            } else {
                echo BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('msg', 'EMAIL_NO_EXISTE'));
            }
        } else {
            echo BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('msg', 'EMAIL_INVALID'));
        }
    }

    public function actiongetForm() {
        $this->renderPartial('_form_password');
    }

}
