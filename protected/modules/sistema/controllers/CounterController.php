<?php

class CounterController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column1';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' 
				'actions'=>array('index'),
				'roles'=>array('SysCounterActionIndex'),
			),
			array('allow',  // allow all users to perform 'view' 
				'actions'=>array('view'),
				'roles'=>array('SysCounterActionView'),
			),
			array('allow',  // allow all users to perform 'create' 
				'actions'=>array('create'),
				'roles'=>array('SysCounterActionCreate'),
			),
			array('allow',  // allow all users to perform 'update' 
				'actions'=>array('update'),
				'roles'=>array('SysCounterActionUpdate'),
			),
			array('allow',  // allow all users to perform 'admin' 
				'actions'=>array('admin'),
				'roles'=>array('SysCounterActionAdmin'),
			),
			array('allow',  // allow all users to perform 'delete' 
				'actions'=>array('delete'),
				'roles'=>array('SysCounterActionDelete'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new SysCounter;
        $model2=new SysCounter('search');
		$model2->unsetAttributes();  // clear any default values
		if(isset($_GET['SysCounter']))
			$model2->attributes=$_GET['SysCounter'];
            
		// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

		if(isset($_POST['SysCounter']))
		{
            try{
                $model->attributes=$_POST['SysCounter'];
                if($model->save()){
                    Yii::app()->user->setFlash('success',  Yii::t('app', 'SUCCESS_CREATE_OK',array('{info}'=>Yii::t('app','SysCounter'))));
                    $this->redirect(array('create','id'=>$model->id));
                }else{
                        Yii::app()->user->setFlash('danger',  Yii::t('app', 'SUCCESS_CREATE_ERROR',array('{info}'=>Yii::t('app','SysCounter'))));
                    }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger',$e->getCode().' '.$e->getMessage());
            }
		}

		$this->render('create',array(
                    'model'=>$model,
                    'model2'=>$model2,
                    'Company'=>  BsHtml::listData(Company::model()->findAll('r_d_s=1'), 'id', 'name'),
                    'Document'=>  BsHtml::listData(Document::model()->findAll('r_d_s=1'), 'id', 'name')
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $model2=new SysCounter('search');
		$model2->unsetAttributes();  // clear any default values
		if(isset($_GET['SysCounter']))
			$model2->attributes=$_GET['SysCounter'];
            
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['SysCounter']))
		{
            try{
                $model->attributes=$_POST['SysCounter'];
                if($model->save()){
                    Yii::app()->user->setFlash('success',  Yii::t('app', 'SUCCESS_UPDATE_OK',array('{info}'=>Yii::t('app','SysCounter'))));
                    $this->redirect(array('create','id'=>$model->id));
                }else{
                    Yii::app()->user->setFlash('danger',  Yii::t('app', 'SUCCESS_UPDATE_ERROR',array('{info}'=>Yii::t('app','SysCounter'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger',$e->getCode().' '.$e->getMessage());
            }
		}

		$this->render('update',array(
                    'model'=>$model,
                    'model2'=>$model2,
                    'Company'=>  BsHtml::listData(Company::model()->findAll('r_d_s=1'), 'id', 'name'),
                    'Document'=>  BsHtml::listData(Document::model()->findAll('r_d_s=1'), 'id', 'name')
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->user->checkAccess('SysCounterDeleteButtonDelete')){
            if(Yii::app()->request->isPostRequest){
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500,  Yii::t('msg', 'DELETE_ERROR',array('{info}'=>Yii::t('app','SysCounter'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else{
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }
        }else{
            throw new CHttpException(400,  Yii::t('auth', 'DENEY',array('{accion}'=>'Eliminar')));
        }
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SysCounter');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new SysCounter('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SysCounter']))
			$model->attributes=$_GET['SysCounter'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return SysCounter the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=SysCounter::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param SysCounter $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sys-counter-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}