<?php

class AuthController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column1';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new SisAuthItem;
        $model2=new SisAuthItem('search');
		$model2->unsetAttributes();  // clear any default values
		if(isset($_GET['SisAuthItem']))
			$model2->attributes=$_GET['SisAuthItem'];
            
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SisAuthItem']))
		{
            try{
                $model->attributes=$_POST['SisAuthItem'];
                if($model->save()){
                    Yii::app()->user->setFlash('success',  Yii::t('app', 'SUCCESS_CREATE_OK'));
                    $this->redirect(array('create','id'=>$model->name));
                }else{
                        Yii::app()->user->setFlash('danger',  Yii::t('app', 'SUCCESS_CREATE_ERROR'));
                    }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger',$e->getCode().' '.$e->getMessage());
            }
		}

		$this->render('create',array(
		'model'=>$model,'model2'=>$model2
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $model2=new SisAuthItem('search');
		$model2->unsetAttributes();  // clear any default values
		if(isset($_GET['SisAuthItem']))
			$model2->attributes=$_GET['SisAuthItem'];
            
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SisAuthItem']))
		{
            try{
                $model->attributes=$_POST['SisAuthItem'];
                if($model->save()){
                    Yii::app()->user->setFlash('success',  Yii::t('app', 'SUCCESS_UPDATE_OK'));
                    $this->redirect(array('create','id'=>$model->name));
                }else{
                    Yii::app()->user->setFlash('danger',  Yii::t('app', 'SUCCESS_UPDATE_ERROR'));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger',$e->getCode().' '.$e->getMessage());
            }
		}

		$this->render('update',array(
			'model'=>$model,'model2'=>$model2
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
//		$dataProvider=new CActiveDataProvider('SisAuthItem');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
        
        $auth=Yii::app()->authManager;
        echo "Creando Operaciones<br>";
        $auth->createOperation('save','Guardar registros');
        $auth->createOperation('new','Crear nuevo registro');
        $auth->createOperation('search','Buscar registros');
        $auth->createOperation('delete','Eliminar registro');
        $auth->createOperation('status','Cambiar registro');
        $auth->createOperation('create','Entrar CREAR');
        echo "creado BizRule<br>";
        $bizRule='return Yii::app()->user->id==$params["post"]->authID;';
        echo "creado Tarea<br>";
        $task=$auth->createTask('habCreate','Puede entrar a Crear habitaciones');
        $task->addChild('save');
        $task->addChild('new');
        
        echo "Creando Roles<br>";
        $role=$auth->createRole('gerente','Gerente');
        $role->addChild('habCreate');
        
        $role=$auth->createRole('admin','Administrador');
        $role->addChild('gerente');
        $role->addChild('search');
        $role->addChild('delete');
        
        echo "Asignando roeles</b>";
        $auth->assign('admin',1);
        $auth->assign('gerente',2);
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new SisAuthItem('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SisAuthItem']))
			$model->attributes=$_GET['SisAuthItem'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return SisAuthItem the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=SisAuthItem::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param SisAuthItem $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sis-auth-item-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}