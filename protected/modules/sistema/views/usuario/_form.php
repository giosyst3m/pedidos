<?php
/* @var $this UsuarioController */
/* @var $model SisUsuario */
/* @var $form BSActiveForm */
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/sistema/usuario/create.js', CClientScript::POS_HEAD);
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'sis-usuario-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
?>

<p class="help-block"> <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required.') ?></p>

<?php echo $form->errorSummary($model); ?>

        <?php
        echo $form->dropDownListControlGroup($model, 'acceso', array(1 => 'Si', 0 => 'No'), array(
            'data-style' => "btn-success",
            'class' => 'selectpicker show-tick',
            'empty' => 'Seleccionar',
        ));
        ?>
        <?php echo $form->textFieldControlGroup($model, 'nombre', array('maxlength' => 50)); ?>
        <?php echo $form->textFieldControlGroup($model, 'apellido', array('maxlength' => 50)); ?>
        <?php echo $form->textFieldControlGroup($model, 'username', array('maxlength' => 50, 'help' => 'Solo Letras, Numeros,  guiones - y línea abajo _ ')); ?>
        <?php echo $form->textFieldControlGroup($model, 'email', array('maxlength' => 50)); ?>
        <?php echo $form->textFieldControlGroup($model, 'clave', array('maxlength' => 12,'help'=>'Debe colocar minimo 8 caracteres entre letras y Numeros   ')); ?>
        <?php echo $form->dropDownListControlGroup($model, 'id_auth_item', $role, array('empty' => '.::Seleccionar::.', 'data-style' => "btn-primary", 'class' => 'selectpicker show-tick')); ?>
        <div id="clients" class="hidden">
            <?php echo $form->dropDownListControlGroup($model, 'id_client', $VClient, array('empty' => '.::Seleccionar::.', 'data-style' => "btn-warning", 'class' => 'selectpicker show-tick','data-live-search'=>true)); ?>
        </div>
        <?php echo $form->dropDownListControlGroup($model, 'id_sis_usu_tipo', $tipo, array('empty' => '.::Seleccionar::.', 'data-style' => "btn-primary", 'class' => 'selectpicker show-tick')); ?>
        <?php echo $form->dropDownListControlGroup($model, 'id_sis_cargo', $cargo, array('empty' => '.::Seleccionar::.', 'data-style' => "btn-primary", 'class' => 'selectpicker show-tick')); ?>
        <?php
        echo $form->fileFieldControlGroup($model, 'foto');
        if (!empty($model->foto)) {
            echo '<div class="center-block text-center">';
            echo $model->ShowImagen($model->foto, Yii::app()->params['URL_USUARIO_IMAGEN']);
            echo '</div><div class="clearfix">&nbsp;</div>';
        }
        ?>

<?php
if (Yii::app()->user->checkAccess('SisUsuCreateStatusChange') || Yii::app()->user->checkAccess('SisUsuUpdateStatusChange')) {
    echo $form->dropDownListControlGroup($model, 'r_d_s', array(1 => 'Activo', 0 => 'Inactivo'), array('data-style' => "btn-info", 'class' => 'selectpicker show-tick'));
}
?>
<?php
if (Yii::app()->user->checkAccess('SisUsuCreateButtonSave') || Yii::app()->user->checkAccess('SisUsuUpdateButtonSave')) {
    echo BsHtml::submitButton(Yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY));
}
?>
<?php
if (Yii::app()->user->checkAccess('SisUsuCreateButtonNew') || Yii::app()->user->checkAccess('SisUsuUpdateButtonNew')) {
    echo BsHtml::link(Yii::t('app', 'New'), Yii::app()->createAbsoluteUrl('sistema/usuario/create'), array('class' => 'btn btn-primary'));
}
?>

<?php $this->endWidget(); ?>
