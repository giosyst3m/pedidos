<?php
/* @var $this UsuarioController */
/* @var $model SisUsuario */
?>

<?php
$this->breadcrumbs = array(
    'Usuarios' => array('create'),
    'Create',
);

/* $this->menu=array(
  array('icon' => 'glyphicon glyphicon-list','label'=>'List SisUsuario', 'url'=>array('index')),
  array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisUsuario', 'url'=>array('admin')),
  ); */
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Create') ?><b> <?php echo Yii::t('app', 'Usuario'); ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        if (Yii::app()->user->checkAccess('SisUsuCreateFormView')) {
            $this->renderPartial('_form', array(
                'model' => $model,
                'role' => $role,
                'tipo' => $tipo,
                'cargo' => $cargo,
                'Company' => $Company,
                'Companies' => $Companies,
                'VClient' => $VClient
            ));
        }
        ?>
    </div>
</div>
<hr/>
<?php
if (Yii::app()->user->checkAccess('SisUsuCreateViewAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2));
}
?>
