<?php
/* @var $this UsuarioController */
/* @var $model SisUsuario */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nombre',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'apellido',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'clave',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'clave2',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'r_c_u'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_c_d'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_u_u'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_u_d'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_u'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_d'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_s'); ?>
    <?php echo $form->textFieldControlGroup($model,'id_auth_item',array('maxlength'=>64)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
