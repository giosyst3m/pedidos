<?php
/* @var $this UsuarioController */
/* @var $model SisUsuario */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'sis-usuario-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'nombre',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'apellido',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'id_auth_item',array('readonly'=>true)); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_sis_usu_tipo',$tipo,array(
        'empty'=>'.::Seleccionar::.',
        'data-style'=>"btn-primary",
        'class'=>'selectpicker show-tick',
        'disabled'=>true
    )); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_sis_cargo',$cargo,array(
        'empty'=>'.::Seleccionar::.',
        'data-style'=>"btn-primary",
        'class'=>'selectpicker show-tick',
        'disabled'=>true
    )); ?>

    <?php echo BsHtml::submitButton(Yii::t('app','Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
<?php $this->endWidget(); ?>
