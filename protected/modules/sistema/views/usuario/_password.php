<?php
/* @var $this UsuarioController */
/* @var $model SisUsuario */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'sis-usuario-form-2',
    'action'=>$this->createUrl('usuario/chagePassword'),
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model2); ?>

    <?php echo $form->passwordFieldControlGroup($model2,'clave',array('maxlength'=>15)); ?>
    <?php echo $form->passwordFieldControlGroup($model2,'clave2',array('maxlength'=>15)); ?>

    <?php echo BsHtml::submitButton(Yii::t('app','Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
<?php $this->endWidget(); ?>
