<?php
/* @var $this UsuarioController */
/* @var $model SisUsuario */
?>

<?php
$this->breadcrumbs = array(
    'Sis Usuarios' => array('create'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);
?>


<div class="x_panel">
    <div class="x_title">
        <h2><i class="fa fa-bars"></i> <?php echo Yii::t('app', 'Update');?> <small><?php echo $model->username ;?></small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">


        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Datos</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Contraseña</a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                    <?php
                    $this->renderPartial('_perfil', array(
                        'model' => $model,
                        'tipo' => $tipo,
                        'cargo' => $cargo,
                    ));
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                    <?php $this->renderPartial('_password', array('model2' => $model2)); ?>
                </div>
            </div>
        </div>

    </div>
</div>