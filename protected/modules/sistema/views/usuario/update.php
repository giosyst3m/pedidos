<?php
/* @var $this UsuarioController */
/* @var $model SisUsuario */
?>

<?php
$this->breadcrumbs = array(
    'Sis Usuarios' => array('create'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><i class="fa fa-bars"></i> <?php echo Yii::t('app', 'Usuario'); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="user-tab" role="tab" data-toggle="tab" aria-expanded="true"><?php echo Yii::t('app', 'User'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="company-tab" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Company'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="zone-tab2" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Zone'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="order-tab2" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Order'); ?></a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="user-tab">
        <?php
        if (Yii::app()->user->checkAccess('SisUsuUpdateFormView')) {
            $this->renderPartial('_form', array(
                'model' => $model,
                'role' => $role,
                'tipo' => $tipo,
                'cargo' => $cargo,
                'Company' => $Company,
                'Companies' => $Companies,
                'VClient'=>$VClient
                    )
            );
        }
        ?>
        <hr/>
        <?php
        if (Yii::app()->user->checkAccess('SisUsuUpdateAdminView')) {
            $this->renderPartial('admin', array(
                'model2' => $model2,
                    )
            );
        }
        ?>
    </div>
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content2" aria-labelledby="company-tab">
        <?php
        if (Yii::app()->user->checkAccess('CompanySisUsuarioCreateFormView')) {
            $this->renderPartial('../../../../views/companySisUsuario/_form', array(
                'model' => $model3,
                'VSisUsuario' => $VSisUsuario,
                'FormDefault' => $FormDefault,
                'Company' => $Company
                    )
            );
        }
        ?>
        <hr>
        <?php
        if (Yii::app()->user->checkAccess('CompanySisUsuarioCreateAdminView')) {
            $this->renderPartial('../../../../views/companySisUsuario/admin', array(
                'model2' => $model4,
                'VSisUsuario' => $VSisUsuario,
                'FormDefault' => $FormDefault,
                'Company' => $Company
                    )
            );
        }
        ?>
    </div>
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content3" aria-labelledby="zone-tab">

        <?php
        if (Yii::app()->user->checkAccess('ZoneSisUsuarioCreateFormView')) {
            $this->renderPartial('../../../../views/zoneSisUsuario/_form', array(
                'model' => $model5,
                'FormDefault' => $FormDefault2,
                'Zone' => $Zone,
                'VSisuario' => $VSisUsuario,
                    )
            );
        }
        ?>
        <hr>
        <?php
        if (Yii::app()->user->checkAccess('ZoneSisUsuarioCreateAdminView')) {
            $this->renderPartial('../../../../views/zoneSisUsuario/admin', array(
                'model2' => $model6,
                'FormDefault' => $FormDefault2,
                'Zone' => $Zone,
                'VSisuario' => $VSisUsuario,
                    )
            );
        }
        ?>
    </div>
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content4" aria-labelledby="order-tab">
        <?php
        if (Yii::app()->user->checkAccess('StockCompanyCreateAdminView')) {
            $this->renderPartial('../../../../views/order/admin', array(
                'model2' => $model7,
                'VSisUsuario' => $VSisUsuario,
                'client' => $Client,
                'status' => $Status,
                'FormDefault' => $FormDefault,
                'FormDefault2' => false,
                'Company' => $Company,
            ));
        }
        ?>
    </div>
    <div class="tab-pane fade" id="doner">

    </div>
    <div class="clearfix"></div>
</div>
