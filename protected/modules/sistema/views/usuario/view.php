<?php
/* @var $this UsuarioController */
/* @var $model SisUsuario */
?>

<?php
$this->breadcrumbs=array(
	'Sis Usuarios'=>array('index'),
	$model->id,
);

?>
<?php 
$reg = array();
if(Yii::app()->user->checkAccess('SisUsuViewAuthView')){
    $reg = array(	
		'nombre',
		'apellido',
		'email',
        'id_auth_item',
		'clave',
		'clave2',
		'r_c_u',
		'r_c_d',
		'r_u_u',
		'r_u_d',
		'r_d_u',
		'r_d_d',
		'r_d_s',
    );
}else{
    $reg = array(	
		'id',
		'nombre',
		'apellido',
		'email',
        'id_auth_item',
        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=>$model->RegistroEstado($model->r_d_s)
        ),
    );
}
?>
<?php echo BsHtml::pageHeader('View','SisUsuario '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>$reg,
)); ?>

<?php echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => 'history.go(-1)',
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('route/to/create'),array('class'=>  'btn btn-primary')); ?>