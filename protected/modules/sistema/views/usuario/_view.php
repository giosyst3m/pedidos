<?php
/* @var $this UsuarioController */
/* @var $data SisUsuario */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido')); ?>:</b>
	<?php echo CHtml::encode($data->apellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />
    <?php if(Yii::app()->user->checkAccess('SisUsuViewAuthView')){?>
        <b><?php echo CHtml::encode($data->getAttributeLabel('clave')); ?>:</b>
        <?php echo CHtml::encode($data->clave); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('clave2')); ?>:</b>
        <?php echo CHtml::encode($data->clave2); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_c_u')); ?>:</b>
        <?php echo CHtml::encode($data->r_c_u); ?>
        <br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('r_c_d')); ?>:</b>
        <?php echo CHtml::encode($data->r_c_d); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_u_u')); ?>:</b>
        <?php echo CHtml::encode($data->r_u_u); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_u_d')); ?>:</b>
        <?php echo CHtml::encode($data->r_u_d); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_d_u')); ?>:</b>
        <?php echo CHtml::encode($data->r_d_u); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_d_d')); ?>:</b>
        <?php echo CHtml::encode($data->r_d_d); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
        <?php echo CHtml::encode($data->r_d_s); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('id_auth_item')); ?>:</b>
        <?php echo CHtml::encode($data->id_auth_item); ?>
        <br />

    <?php }?>

</div>