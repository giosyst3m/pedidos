<?php
/* @var $this ContactoController */
/* @var $model SisContacto */
/* @var $form BSActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'sis-contacto-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => BsHtml::FORM_LAYOUT_INLINE,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => [
        'class' => 'wow fadeInUp',
        'data-wow-delay' => "0.6s",
        ]
        ));
?>
<style>
    .form-inline .form-group{
        display: block;
    } 
    input, textArea{
        width:100% !important;
    }
</style>
<p class=""> <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required.') ?></p>

<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <?php echo $form->textFieldControlGroup($model, 'nombre', array('maxlength' => 50, 'value' => !empty(Yii::app()->user->nombre) ? Yii::app()->user->nombre : '')); ?>
    </div>
    <div class="col-md-6 col-sm-6">
        <?php echo $form->textFieldControlGroup($model, 'email', array('maxlength' => 50, 'value' => !empty(Yii::app()->user->email) ? Yii::app()->user->email : '')); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <?php echo $form->textFieldControlGroup($model, 'asunto', array('maxlength' => 50)); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <?php echo $form->textAreaControlGroup($model, 'detalle', array('rows' => 6, 'placeholder' => 'Favor dar la mayor información posible que hace referencia')); ?>
    </div>
</div>
<div class="row">
    <?php if (CCaptcha::checkRequirements()): ?>
        <div class="col-md-6 col-sm-6">
            <?php $this->widget('CCaptcha'); ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <?php echo $form->textFieldControlGroup($model, 'verificacion', array('placeholder' => 'Código de verificación')); ?>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <?php echo BsHtml::submitButton(Yii::t('app', 'Enviar'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'icon' => 'icon-send' . BsHtml::GLYPHICON_SEND, 'class'=>'btn-block')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
