<?php
/* @var $this ContactoController */
/* @var $model SisContacto */
?>

<?php
$this->breadcrumbs=array(
	'Sis Contactos'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisContacto', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create SisContacto', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update SisContacto', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete SisContacto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisContacto', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','SisContacto '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		'email',
		'asunto',
		'detalle',
		'r_c_u',
		'r_c_d',
		'r_u_u',
		'r_u_d',
		'r_d_u',
		'r_d_d',
		'r_d_s',
		'id_sis_con_status',
	),
)); ?>

<?php echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => 'history.go(-1)',
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('route/to/create'),array('class'=>  'btn btn-primary')); ?>