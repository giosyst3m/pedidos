<?php
/* @var $this ContactoController */
/* @var $model SisContacto */
?>

<?php
$this->breadcrumbs=array(
	'Sis Contactos'=>array('create'),
	'Create',
);

/*$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisContacto', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisContacto', 'url'=>array('admin')),
);*/
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Contacto')) ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?><hr/>