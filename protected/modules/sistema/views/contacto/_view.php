<?php
/* @var $this ContactoController */
/* @var $data SisContacto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('asunto')); ?>:</b>
	<?php echo CHtml::encode($data->asunto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('detalle')); ?>:</b>
	<?php echo CHtml::encode($data->detalle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_u')); ?>:</b>
	<?php echo CHtml::encode($data->r_c_u); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_d')); ?>:</b>
	<?php echo CHtml::encode($data->r_c_d); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_u')); ?>:</b>
	<?php echo CHtml::encode($data->r_u_u); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_d')); ?>:</b>
	<?php echo CHtml::encode($data->r_u_d); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_u')); ?>:</b>
	<?php echo CHtml::encode($data->r_d_u); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_d')); ?>:</b>
	<?php echo CHtml::encode($data->r_d_d); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
	<?php echo CHtml::encode($data->r_d_s); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_sis_con_status')); ?>:</b>
	<?php echo CHtml::encode($data->id_sis_con_status); ?>
	<br />

	*/ ?>

</div>