<?php
/* @var $this ContactoController */
/* @var $model SisContacto */
?>

<?php
$this->breadcrumbs=array(
	'Sis Contactos'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

/*$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisContacto', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create SisContacto', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View SisContacto', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisContacto', 'url'=>array('admin')),
);*/
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','SisContacto').' Nro: '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?><hr/>
<?php $this->renderPartial('admin', array('model2'=>$model2)); ?>