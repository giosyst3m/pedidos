<?php
/* @var $this SisHabStatusController */
/* @var $model SisHabStatus */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'sis-hab-status-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'nombre',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'color',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'alert',array('maxlength'=>50)); ?>

    <?php echo BsHtml::submitButton(Yii::t('app','Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
    <?php echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('ruta/to/create'),array('class'=>  'btn btn-primary'));?>
<?php $this->endWidget(); ?>
