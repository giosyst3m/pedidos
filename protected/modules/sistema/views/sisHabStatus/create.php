<?php
/* @var $this SisHabStatusController */
/* @var $model SisHabStatus */
?>

<?php
$this->breadcrumbs=array(
	'Sis Hab Statuses'=>array('create'),
	'Create',
);

/*$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisHabStatus', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisHabStatus', 'url'=>array('admin')),
);*/
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','SisHabStatus')) ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?><hr/>
<?php $this->renderPartial('admin', array('model2'=>$model2)); ?>