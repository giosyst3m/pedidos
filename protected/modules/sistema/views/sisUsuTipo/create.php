<?php
/* @var $this SisUsuTipoController */
/* @var $model SisUsuTipo */
?>

<?php
$this->breadcrumbs = array(
    'Sis Usu Tipos' => array('create'),
    'Create',
);

/* $this->menu=array(
  array('icon' => 'glyphicon glyphicon-list','label'=>'List SisUsuTipo', 'url'=>array('index')),
  array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisUsuTipo', 'url'=>array('admin')),
  ); */
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Create') ?><b> <?php echo Yii::t('app', 'SisUsuTipo'); ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        if (Yii::app()->user->checkAccess('SisUsuTipoCreateFormView')) {
            $this->renderPartial('_form', array('model' => $model));
        }
        ?>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('SisUsuTipoCreateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2));
}
?>