<?php
/* @var $this SisPedStatusController */
/* @var $model SisPedStatus */
?>

<?php
$this->breadcrumbs=array(
	'Sis Ped Statuses'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

/*$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisPedStatus', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create SisPedStatus', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View SisPedStatus', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisPedStatus', 'url'=>array('admin')),
);*/
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','SisPedStatus').' Nro: '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?><hr/>
<?php $this->renderPartial('admin', array('model2'=>$model2)); ?>