<?php
/* @var $this SisPedStatusController */
/* @var $model SisPedStatus */
?>

<?php
$this->breadcrumbs=array(
	'Sis Ped Statuses'=>array('create'),
	'Create',
);

/*$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisPedStatus', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisPedStatus', 'url'=>array('admin')),
);*/
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','SisPedStatus')) ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?><hr/>
<?php $this->renderPartial('admin', array('model2'=>$model2)); ?>