<?php
/* @var $this MenuController */
/* @var $model SisMenu */
?>

<?php
$this->breadcrumbs=array(
	'Sis Menus'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Create') ?><b> <?php echo Yii::t('app', 'SisMenu').' '.$model->nombre; ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
<?php 
if(Yii::app()->user->checkAccess('SisMenuUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model,'Program'=>$Program,'SisMenu'=>$SisMenu)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('SisMenuUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2,'Program'=>$Program,'SisMenu'=>$SisMenu)); 
}
?>