<?php
/* @var $this MenuController */
/* @var $model SisMenu */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'padre'); ?>
    <?php echo $form->textFieldControlGroup($model,'nombre',array('maxlength'=>25)); ?>
    <?php echo $form->textFieldControlGroup($model,'link',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'icono',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'orden',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'id_programa'); ?>
    <?php echo $form->textFieldControlGroup($model,'shortcut'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
