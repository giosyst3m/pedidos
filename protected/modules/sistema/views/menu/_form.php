<?php
/* @var $this MenuController */
/* @var $model SisMenu */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'sis-menu-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListControlGroup($model, 'padre', $SisMenu, array('empty' => '.::Seleccionar::.', 'data-style' => "btn-success", 'class' => 'selectpicker show-tick', 'data-live-search' => true)); ?>
    <?php echo $form->textFieldControlGroup($model,'nombre',array('maxlength'=>25)); ?>
    <?php echo $form->textFieldControlGroup($model,'link',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'icono',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'orden',array('maxlength'=>50)); ?>
    <?php echo $form->dropDownListControlGroup($model, 'id_programa', $Program, array('empty' => '.::Seleccionar::.', 'data-style' => "btn-primary", 'class' => 'selectpicker show-tick')); ?>
    <?php echo $form->dropDownListControlGroup($model, 'shortcut', array(1=>'SI',0=>'No'),array('data-style'=>'btn-primary','class'=>'selectpicker show-tick')); ?>
    <?php echo $form->dropDownListControlGroup($model, 'target', [
        '_blank'=>'_blank: Opens the linked document in a new window or tab',
        '_self'=>'_self: Opens the linked document in the same frame as it was clicked (this is default)',
        '_parent'=>'_parent: pens the linked document in the parent frame',
        '_top'=>'_top: Opens the linked document in the full body of the window'
        ],array('data-style'=>'btn-info','class'=>'selectpicker show-tick','empty'=>'Select')); ?>

    
    <?php 
    if(Yii::app()->user->checkAccess('SisMenuCreateStatusChange') || Yii::app()->user->checkAccess('SisMenuUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('SisMenuCreateButtonSave') || Yii::app()->user->checkAccess('SisMenuUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('SisMenuCreateButtonNew') || Yii::app()->user->checkAccess('SisMenuUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('SisMenu/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
