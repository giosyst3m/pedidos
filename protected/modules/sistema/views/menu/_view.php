<?php
/* @var $this MenuController */
/* @var $data SisMenu */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('padre')); ?>:</b>
		<?php echo CHtml::encode($data->padre); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
		<?php echo CHtml::encode($data->nombre); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('link')); ?>:</b>
		<?php echo CHtml::encode($data->link); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('icono')); ?>:</b>
		<?php echo CHtml::encode($data->icono); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('orden')); ?>:</b>
		<?php echo CHtml::encode($data->orden); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_programa')); ?>:</b>
		<?php echo CHtml::encode($data->id_programa); ?>
		<br />

	
		<b><?php echo CHtml::encode($data->getAttributeLabel('shortcut')); ?>:</b>
		<?php echo CHtml::encode($data->shortcut); ?>
		<br />
                <?php if(Yii::app()->user->checkAccess('SisMenuViewAuthView')){?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_s); ?>
		<br />

	<?php }?>

</div>