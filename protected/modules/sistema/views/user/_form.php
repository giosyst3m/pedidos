<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'user-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'first_name',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'last_name',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'username',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'pasword',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'clave2',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'id_auth_item',array('maxlength'=>64)); ?>
    <?php echo $form->textFieldControlGroup($model,'acceso'); ?>
    <?php echo $form->textFieldControlGroup($model,'avatar',array('maxlength'=>255)); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('UserCreateStatusChange') || Yii::app()->user->checkAccess('UserUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('UserCreateButtonSave') || Yii::app()->user->checkAccess('UserUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('UserCreateButtonNew') || Yii::app()->user->checkAccess('UserUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('User/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
