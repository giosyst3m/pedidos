<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b>
		<?php echo CHtml::encode($data->first_name); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('last_name')); ?>:</b>
		<?php echo CHtml::encode($data->last_name); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
		<?php echo CHtml::encode($data->email); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
		<?php echo CHtml::encode($data->username); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('pasword')); ?>:</b>
		<?php echo CHtml::encode($data->pasword); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('clave2')); ?>:</b>
		<?php echo CHtml::encode($data->clave2); ?>
		<br />

	<?php if(Yii::app()->user->checkAccess('UserViewAuthView')){?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('id_auth_item')); ?>:</b>
		<?php echo CHtml::encode($data->id_auth_item); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('acceso')); ?>:</b>
		<?php echo CHtml::encode($data->acceso); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('avatar')); ?>:</b>
		<?php echo CHtml::encode($data->avatar); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_s); ?>
		<br />

	<?php }?>

</div>