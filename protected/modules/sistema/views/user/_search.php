<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'first_name',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'last_name',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'username',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'pasword',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'clave2',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'id_auth_item',array('maxlength'=>64)); ?>
    <?php echo $form->textFieldControlGroup($model,'acceso'); ?>
    <?php echo $form->textFieldControlGroup($model,'avatar',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'r_c_u'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_c_d'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_c_i',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'r_u_u'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_u_d'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_u_i',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_u'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_d'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_i',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_s'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
