<?php
/* @var $this SisMenuAuthItemController */
/* @var $model SisMenuAuthItem */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'sis-menu-auth-item-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListControlGroup($model, 'id_menu', $SisMenu, array('empty' => '.::Seleccionar::.', 'data-style' => "btn-primary", 'class' => 'selectpicker show-tick', 'data-live-search' => true)); ?>
    <?php echo $form->dropDownListControlGroup($model, 'name_auth_item', $VAuthRole, array('empty' => '.::Seleccionar::.', 'data-style' => "btn-primary", 'class' => 'selectpicker show-tick', 'data-live-search' => true)); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('SisMenuAuthItemCreateStatusChange') || Yii::app()->user->checkAccess('SisMenuAuthItemUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('SisMenuAuthItemCreateButtonSave') || Yii::app()->user->checkAccess('SisMenuAuthItemUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('SisMenuAuthItemCreateButtonNew') || Yii::app()->user->checkAccess('SisMenuAuthItemUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('/sistema/SisMenuAuthItem/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
