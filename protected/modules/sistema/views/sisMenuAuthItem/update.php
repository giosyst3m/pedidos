<?php
/* @var $this SisMenuAuthItemController */
/* @var $model SisMenuAuthItem */
?>

<?php
$this->breadcrumbs = array(
    'Sis Menu Auth Items' => array('create'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);
?>


<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Create') ?><b> <?php echo Yii::t('app', 'SisMenu') . ' ' . $model->id; ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        if (Yii::app()->user->checkAccess('SisMenuAuthItemUpdateFormView')) {
            $this->renderPartial('_form', array('model' => $model, 'SisMenu' => $SisMenu, 'VAuthRole' => $VAuthRole));
        }
        ?>
        <hr>
    </div>
</div>
<?php
if (Yii::app()->user->checkAccess('SisMenuAuthItemUpdateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2, 'SisMenu' => $SisMenu, 'VAuthRole' => $VAuthRole));
}
?>