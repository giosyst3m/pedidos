<?php
/* @var $this SisMenuAuthItemController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Sis Menu Auth Items',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create SisMenuAuthItem', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisMenuAuthItem', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Sis Menu Auth Items') ?>
<?php $this->widget('bootstrap.widgets.BsListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>