<?php
/* @var $this AuthController */
/* @var $model SisAuthItem */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>64)); ?>
    <?php echo $form->textFieldControlGroup($model,'type'); ?>
    <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6)); ?>
    <?php echo $form->textAreaControlGroup($model,'bizrule',array('rows'=>6)); ?>
    <?php echo $form->textAreaControlGroup($model,'data',array('rows'=>6)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
