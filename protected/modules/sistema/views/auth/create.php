<?php
/* @var $this AuthController */
/* @var $model SisAuthItem */
?>

<?php
$this->breadcrumbs=array(
	'Sis Auth Items'=>array('create'),
	'Create',
);

/*$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisAuthItem', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisAuthItem', 'url'=>array('admin')),
);*/
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','SisAuthItem')) ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?><hr/>
<?php $this->renderPartial('admin', array('model2'=>$model2)); ?>