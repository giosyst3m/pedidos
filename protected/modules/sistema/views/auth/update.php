<?php
/* @var $this AuthController */
/* @var $model SisAuthItem */
?>

<?php
$this->breadcrumbs=array(
	'Sis Auth Items'=>array('create'),
	$model->name=>array('view','id'=>$model->name),
	'Update',
);

/*$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisAuthItem', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create SisAuthItem', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View SisAuthItem', 'url'=>array('view', 'id'=>$model->name)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisAuthItem', 'url'=>array('admin')),
);*/
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','SisAuthItem').' Nro: '.$model->name) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?><hr/>
<?php $this->renderPartial('admin', array('model2'=>$model2)); ?>