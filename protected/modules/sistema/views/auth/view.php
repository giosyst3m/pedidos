<?php
/* @var $this AuthController */
/* @var $model SisAuthItem */
?>

<?php
$this->breadcrumbs=array(
	'Sis Auth Items'=>array('index'),
	$model->name,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisAuthItem', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create SisAuthItem', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update SisAuthItem', 'url'=>array('update', 'id'=>$model->name)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete SisAuthItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->name),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisAuthItem', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','SisAuthItem '.$model->name) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'name',
		'type',
		'description',
		'bizrule',
		'data',
	),
)); ?>

<?php echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => 'history.go(-1)',
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('route/to/create'),array('class'=>  'btn btn-primary')); ?>