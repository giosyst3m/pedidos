<?php
/* @var $this LegalController */
/* @var $model Legal */
?>

<?php
$this->breadcrumbs = array(
    'Legals' => array('create'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);
?>

<?php echo BsHtml::pageHeader(Yii::t('app', 'Update'), Yii::t('app', 'Legal') . ' Nro: ' . $model->id) ?>

<?php
if (Yii::app()->user->checkAccess('LegalUpdateFormView')) {
    $this->renderPartial('_form', array('model' => $model,
        'LegType' => $LegType,
        'Program' => $Program,
        'Country' => $Country
    ));
}
?>
<hr>
<?php
if (Yii::app()->user->checkAccess('LegalUpdateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2,
        'LegType' => $LegType,
        'Program' => $Program,
        'Country' => $Country
    ));
}
?>