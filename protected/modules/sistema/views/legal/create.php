<?php
/* @var $this LegalController */
/* @var $model Legal */
?>

<?php
$this->breadcrumbs=array(
	'Legals'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Legal')) ?>

<?php 
if(Yii::app()->user->checkAccess('LegalCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
            'LegType'=>$LegType,
            'Program'=>$Program,
            'Country'=>$Country,
            )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('LegalCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2,
        'LegType' => $LegType,
        'Program' => $Program,
        'Country' => $Country
    )); 
}
?>