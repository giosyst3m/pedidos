<?php
/* @var $this LegalController */
/* @var $model Legal */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'legal-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'icon',array('maxlength'=>255)); ?>
    <?php if(!empty($model->icon)){
        echo BsHtml::tag('i',['class'=>$model->icon.' fa-5x'],'',true);
    }?>
        <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6)); ?>
    
    <?php echo $form->dropDownListControlGroup($model, 'id_leg_type', $LegType, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Select one', 'onchange' => 'getAttr(this);', 'data-live-search' => true,)); ?>
    <?php echo $form->dropDownListControlGroup($model, 'id_program', $Program, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Select one', 'onchange' => 'getAttr(this);', 'data-live-search' => true,)); ?>
    <?php echo $form->dropDownListControlGroup($model, 'id_country', $Country, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Select one', 'onchange' => 'getAttr(this);', 'data-live-search' => true,)); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('LegalCreateStatusChange') || Yii::app()->user->checkAccess('LegalUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('LegalCreateButtonSave') || Yii::app()->user->checkAccess('LegalUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('LegalCreateButtonNew') || Yii::app()->user->checkAccess('LegalUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('sistema/Legal/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
