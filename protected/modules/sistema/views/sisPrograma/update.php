<?php
/* @var $this SisProgramaController */
/* @var $model SisPrograma */
?>

<?php
$this->breadcrumbs=array(
	'Sis Programas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisPrograma', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create SisPrograma', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View SisPrograma', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisPrograma', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','SisPrograma '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>