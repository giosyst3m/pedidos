<?php
/* @var $this SisProgramaController */
/* @var $model SisPrograma */
?>

<?php
$this->breadcrumbs=array(
	'Sis Programas'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisPrograma', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisPrograma', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','SisPrograma') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>