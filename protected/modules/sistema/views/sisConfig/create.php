<?php
/* @var $this SisConfigController */
/* @var $model SisConfig */
?>

<?php
$this->breadcrumbs=array(
	'Sis Configs'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','SisConfig')) ?>

<?php 
if(Yii::app()->user->checkAccess('SisConfigCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
        'SisConGroup'=>$SisConGroup,
        'fieType'=>$fieType
        )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('SisConfigCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>