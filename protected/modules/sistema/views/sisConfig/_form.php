<?php
/* @var $this SisConfigController */
/* @var $model SisConfig */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'sis-config-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'default',array('maxlength'=>255)); ?>
     <?php echo $form->dropDownListControlGroup($model,'id_sis_con_group',$SisConGroup,array('empty'=>'.::Seleccionar::.','data-style'=>"btn-primary",'class'=>'selectpicker show-tick')); ?>
    <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6)); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_fie_type',$fieType,array('empty'=>'.::Seleccionar::.','data-style'=>"btn-primary",'class'=>'selectpicker show-tick')); ?>
    <?php echo $form->textFieldControlGroup($model,'value',array('maxlength'=>255,'help'=>Yii::t('app','Add , between list'))); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('SisConfigCreateStatusChange') || Yii::app()->user->checkAccess('SisConfigUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('SisConfigCreateButtonSave') || Yii::app()->user->checkAccess('SisConfigUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('SisConfigCreateButtonNew') || Yii::app()->user->checkAccess('SisConfigUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('SisConfig/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
