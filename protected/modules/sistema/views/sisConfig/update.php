<?php
/* @var $this SisConfigController */
/* @var $model SisConfig */
?>

<?php
$this->breadcrumbs=array(
	'Sis Configs'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','SisConfig').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('SisConfigUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
        'SisConGroup'=>$SisConGroup,
        'fieType'=>$fieType
        )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('SisConfigUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>