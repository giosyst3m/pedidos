<?php
/* @var $this SisConfigController */
/* @var $data SisConfig */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
		<?php echo CHtml::encode($data->name); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('default')); ?>:</b>
		<?php echo CHtml::encode($data->default); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_sis_con_group')); ?>:</b>
		<?php echo CHtml::encode($data->id_sis_con_group); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
		<?php echo CHtml::encode($data->description); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_fie_type')); ?>:</b>
		<?php echo CHtml::encode($data->id_fie_type); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('value')); ?>:</b>
		<?php echo CHtml::encode($data->value); ?>
		<br />

	<?php if(Yii::app()->user->checkAccess('SisConfigViewAuthView')){?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_s); ?>
		<br />

	<?php }?>

</div>