<?php
/* @var $this SisHabLogStatusController */
/* @var $model SisHabLogStatus */
?>

<?php
$this->breadcrumbs=array(
	'Sis Hab Log Statuses'=>array('create'),
	'Create',
);

/*$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisHabLogStatus', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisHabLogStatus', 'url'=>array('admin')),
);*/
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','SisHabLogStatus')) ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?><hr/>
<?php $this->renderPartial('admin', array('model2'=>$model2)); ?>