<?php
/* @var $this SisHabLogStatusController */
/* @var $model SisHabLogStatus */
?>

<?php
$this->breadcrumbs=array(
	'Sis Hab Log Statuses'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SisHabLogStatus', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create SisHabLogStatus', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update SisHabLogStatus', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete SisHabLogStatus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SisHabLogStatus', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','SisHabLogStatus '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		'r_c_u',
		'r_c_d',
		'r_u_u',
		'r_u_d',
		'r_d_u',
		'r_d_d',
		'r_d_s',
	),
)); ?>

<?php echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => 'history.go(-1)',
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('route/to/create'),array('class'=>  'btn btn-primary')); ?>