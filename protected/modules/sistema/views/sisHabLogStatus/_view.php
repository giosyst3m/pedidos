<?php
/* @var $this SisHabLogStatusController */
/* @var $data SisHabLogStatus */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_u')); ?>:</b>
	<?php echo CHtml::encode($data->r_c_u); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_d')); ?>:</b>
	<?php echo CHtml::encode($data->r_c_d); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_u')); ?>:</b>
	<?php echo CHtml::encode($data->r_u_u); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_d')); ?>:</b>
	<?php echo CHtml::encode($data->r_u_d); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_u')); ?>:</b>
	<?php echo CHtml::encode($data->r_d_u); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_d')); ?>:</b>
	<?php echo CHtml::encode($data->r_d_d); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
	<?php echo CHtml::encode($data->r_d_s); ?>
	<br />

	*/ ?>

</div>