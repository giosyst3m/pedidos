<?php
/* @var $this CounterController */
/* @var $model SysCounter */
?>

<?php
$this->breadcrumbs = array(
    'Sys Counters' => array('create'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);
?>
<div class="row">
    <div class="col-lg-8">
        <?php echo BsHtml::pageHeader(Yii::t('app', 'Update'), Yii::t('app', 'SysCounter') . ' Nro: ' . $model->id) ?>
    </div>
    <div class="col-lg-4">
        <a href="#" class="btn btn-sq-lg btn-success">
            <i class="fa fa-cubes fa-5x"></i><br/>
            <h4><?php echo Yii::t('app', 'Last Number'); ?></h4>
            <h2><?php echo $model->number ?></h2>
        </a>        
    </div>
</div>        

<?php
if (Yii::app()->user->checkAccess('SysCounterUpdateFormView')) {
    $this->renderPartial('_form', array('model' => $model,
        'Company' => $Company,
        'Document' => $Document
    ));
}
?>
<hr>

<?php
if (Yii::app()->user->checkAccess('SysCounterUpdateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2,
        'Company' => $Company,
        'Document' => $Document
        ));
}
?>