<?php
/* @var $this CounterController */
/* @var $model SysCounter */
?>

<?php
$this->breadcrumbs=array(
	'Sys Counters'=>array('index'),
	$model->id,
);

?>

<?php echo BsHtml::pageHeader('View','SysCounter '.$model->id) ?>
<?php 
 $reg = array();
if(Yii::app()->user->checkAccess('SysCounterViewAuthView')){
    $reg = array(	
						'id',
		'numer',
		'id_document',
		'id_company',
		'r_c_u',
		'r_c_d',
		'r_c_i',
		'r_u_u',
		'r_u_d',
		'r_u_i',
		'r_d_u',
		'r_d_i',
		'r_d_s',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
        
    );
}else{
    $reg = array(	
				'id',
		'numer',
		'id_document',
		'id_company',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
    );
}
?><?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>$reg
	,
)); ?>

<?php echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => 'history.go(-1)',
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('SysCounter/create'),array('class'=>  'btn btn-primary')); ?>