<?php
/* @var $this CounterController */
/* @var $model SysCounter */
?>

<?php
$this->breadcrumbs=array(
	'Sys Counters'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','SysCounter')) ?>

<?php 
if(Yii::app()->user->checkAccess('SysCounterCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
            'Company'=>$Company,
        'Document'=>$Document
            )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('SysCounterCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2,
        'Company' => $Company,
        'Document' => $Document
        )); 
}
?>