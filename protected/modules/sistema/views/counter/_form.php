<?php
/* @var $this CounterController */
/* @var $model SysCounter */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'sys-counter-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListControlGroup($model,'id_company',$Company,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','empty'=>'Select one','onchange'=>'getAttr(this);','data-live-search'=>true,)); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_document',$Document,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','empty'=>'Select one','onchange'=>'getAttr(this);','data-live-search'=>true,)); ?>
        <?php 
        if(Yii::app()->user->checkAccess('SysCounterNumberChange')){
            echo $form->textFieldControlGroup($model,'number',array('class'=>'text-right','max'=>$model->number)); 
        }
        ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('SysCounterCreateStatusChange') || Yii::app()->user->checkAccess('SysCounterUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('SysCounterCreateButtonSave') || Yii::app()->user->checkAccess('SysCounterUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('SysCounterCreateButtonNew') || Yii::app()->user->checkAccess('SysCounterUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('SysCounter/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
