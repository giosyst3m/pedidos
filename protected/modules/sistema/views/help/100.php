<div class="container">
    <div class="bs-example" data-example-id="simple-jumbotron">
        <div class="jumbotron">
            <h1>Versión 1.0.0 Nuevo diseño</h1>
            <p>Con este nuevo diseño, se tiene una mejor <b>experiencia en el sistema</b>, garantizando su acceso desde diferentes dispositivos <b>Desktop, Tables y Móvil;</b> 
                con un mejor desempeño, Colores agradables en sincronía dando así un ambiente ideal para el trabajo y su uso diario, 
                mejorando la usabilidad, manteniendo las funciones y lograr una satisfacción a sus usuarios</p>
        </div>
    </div>
    <div class="x_panel">
        <div class="x_title">
            <h2><i class="fa fa-align-left"></i> Nuevo Dashboard<small> Responsive</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                    </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <!-- start accordion -->
            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel">
                    <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <h4 class="panel-title">Desktop</h4>
                    </a>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <center>

                                <?php echo BsHtml::imageResponsive(Yii::app()->theme->baseUrl . '/images/help/dashboard-desktop-version-1.0.0.jpg'); ?>
                            </center>
                        </div>
                    </div>
                    <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <h4 class="panel-title">Tablet</h4>
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <center>
                                    <?php echo BsHtml::imageResponsive(Yii::app()->theme->baseUrl . '/images/help/dashboard-tablet-version.1.0.0.png'); ?>
                                </center>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <h4 class="panel-title">Móvil</h4>
                        </a>
                        <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <center>
                                    <?php echo BsHtml::imageResponsive(Yii::app()->theme->baseUrl . '/images/help/dashboard-movil-version-1.0.0.png'); ?>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of accordion -->


            </div>
        </div>
    </div>