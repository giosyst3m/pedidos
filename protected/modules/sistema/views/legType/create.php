<?php
/* @var $this LegTypeController */
/* @var $model LegType */
?>

<?php
$this->breadcrumbs=array(
	'Leg Types'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','LegType')) ?>

<?php 
if(Yii::app()->user->checkAccess('LegTypeCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('LegTypeCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>