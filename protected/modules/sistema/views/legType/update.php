<?php
/* @var $this LegTypeController */
/* @var $model LegType */
?>

<?php
$this->breadcrumbs=array(
	'Leg Types'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','LegType').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('LegTypeUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('LegTypeUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>