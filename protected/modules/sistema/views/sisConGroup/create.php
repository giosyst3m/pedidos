<?php
/* @var $this SisConGroupController */
/* @var $model SisConGroup */
?>

<?php
$this->breadcrumbs=array(
	'Sis Con Groups'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','SisConGroup')) ?>

<?php 
if(Yii::app()->user->checkAccess('SisConGroupCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('SisConGroupCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>