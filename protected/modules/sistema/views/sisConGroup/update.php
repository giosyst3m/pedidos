<?php
/* @var $this SisConGroupController */
/* @var $model SisConGroup */
?>

<?php
$this->breadcrumbs=array(
	'Sis Con Groups'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','SisConGroup').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('SisConGroupUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('SisConGroupUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>