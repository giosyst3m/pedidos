<?php
/* @var $this HabitacionController */
/* @var $model Habitacion */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'role-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
)); ?>

    <p class="help-block"><?php echo Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.');?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name'); ?>
    <?php echo $form->textAreaControlGroup($model,'description'); ?>
    <?php echo $form->textAreaControlGroup($model,'bizrule'); ?>
    <?php echo $form->textAreaControlGroup($model,'data',array('readonly'=>true)); ?>
    
    <?php echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
    
    <?php echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('auth/Operation/index'),array('class'=>  'btn btn-primary'));?>

<?php $this->endWidget(); ?>

