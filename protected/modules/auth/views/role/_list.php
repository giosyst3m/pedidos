
<?php 
echo BsHtml::label(Yii::t('auth','Operation').' & '.Yii::t('auth', 'Task'), 'items');
echo BsHtml::dropDownList('items', CHtml::listData($item,'child','child'), $items, array(
   'multiple'=>'multiple',
    'class'=>'selectpicker show-tick',
    'data-live-search'=>true,
    'name'=>'categoria[]',
    'title'=>Yii::t('app','.::Select::.'),
    'data-style'=>"btn-primary"
));
?>
<hr>
<div class="row">
    <div class="col-xs-12 col-med-12" style="height: 350px; overflow: scroll">
        <?php 
        echo '<ul class="list-group">';
        foreach ($item as $data) {
            echo CHtml::tag('li',array('class'=>'list-group-item '),
                CHtml::tag('span',array('data-toggle'=>'tooltip','data-placement'=>'right','title'=>$data->description),$data->child) .' '.CHtml::tag('span',array('class'=>'label pull-right '.$data->color),Yii::t('auth',$data->tipo))
            );
        }
        echo '</ul>';
        ?>        
    </div>
</div>

