<?php
/* @var $this HabitacionController */
/* @var $model Habitacion */
/* @var $form BSActiveForm */
?>


<p class="help-block"><?php echo Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.');?></p>

<?php echo $form->errorSummary($model); ?>
<?php echo$form->dropDownListControlGroup ($model, 'userid', $user,array( 
    'class'=>'selectpicker show-tick',
    'data-live-search'=>true,
    'title'=>Yii::t('app','.::Select::.'),
    'data-style'=>"btn-primary"));
?>
<?php echo$form->dropDownListControlGroup ($model, 'itemname', $items,array( 
    'class'=>'selectpicker show-tick',
    'data-live-search'=>true,
    'title'=>Yii::t('app','.::Select::.'),
    'data-style'=>"btn-primary"));
?>
<?php echo $form->textAreaControlGroup($model,'bizrule'); ?>
<?php echo $form->textAreaControlGroup($model,'data',array('readonly'=>true)); ?>

<?php echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>

<?php echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('auth/assignment/index'),array('class'=>  'btn btn-primary'));?>



