<?php
/* @var $this AlmacenController */
/* @var $model Almacen */
?>

<?php
$this->breadcrumbs=array(
	'AuthItem'=>array('index'),
	$model->itemname,
);

?>

<?php echo BsHtml::pageHeader('View','AuthItem '.$model->itemname) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'itemname',
		array(
            'label'=>'userid',
            'type'=>'raw',
            'value'=>$model->sisuser->email,
        ),
		'bizrule',
		'data',
	),
)); 

 echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => "history.go(-1)",
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('auth/Assignment/index'),array('class'=>  'btn btn-primary')); 

?>