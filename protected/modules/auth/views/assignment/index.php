<?php
/* @var $this AssignmentController */

$this->breadcrumbs = array(
    'Assignment',
);
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Create') ?><b> <?php echo Yii::t('app', 'Assignment'); ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
            'id' => 'role-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
        ));
        ?>
        <?php $this->renderPartial('_form', array('model' => $model, 'form' => $form, 'items' => $items, 'user' => $user)); ?>

<?php $this->endWidget(); ?>
    </div>
</div>
<hr>
<?php $this->renderPartial('admin', array('model2' => $model2)); ?>
