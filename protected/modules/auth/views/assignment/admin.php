<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#almacen-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button('Advanced search',array('class' =>'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH,'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
                &lt;&gt;</b>
            or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
        </p>

        <div class="search-form" style="display:none">
            <?php 
            $this->renderPartial('_search',array(
                'model'=>$model2,
            ));
            ?>
        </div>
        <!-- search-form -->
        <?php
        $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'almacen-grid',
			'dataProvider'=>$model2->search(2),
			'filter'=>$model2,
			'columns'=>array(
                array(
                    'name'=>'userid',
                    'header'=>'Nombre',
                    'value'=> 'SisUsuario::model()->findByPk($data->userid)->nombre',
                ),
                array(
                    'name'=>'userid',
                    'header'=>'Apellido',
                    'value'=> 'SisUsuario::model()->findByPk($data->userid)->apellido',
                ),
                array(
                    'name'=>'userid',
                    'header'=>'UserName',
                    'value'=> 'SisUsuario::model()->findByPk($data->userid)->username',
                ),
                array(
                    'name'=>'userid',
                    'header'=>'email',
                    'value'=> 'SisUsuario::model()->findByPk($data->userid)->email',
                ),
                'itemname',
				array(
					'class'=>'bootstrap.widgets.BsButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'buttons'=>array(
                        'update' => array(
                            'url'=>'Yii::app()->createUrl("auth/Assignment/update/", array("id"=>$data->itemname, "id2"=>$data->userid))'
                            ),
                        'delete'=>array(
                            'url'=>'Yii::app()->createUrl("auth/Assignment/Delete/", array("id"=>$data->itemname, "id2"=>$data->userid))'
                        ),
                        'view'=>array(
                            'url'=>'Yii::app()->createUrl("auth/Assignment/View/", array("id"=>$data->itemname, "id2"=>$data->userid))'
                        )
                    ),
                ),
			),
        )); 
        ?>
    </div>
</div>




