<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AuthRoleForm extends CFormModel {

    public $name;
    public $type = 2;//Role
    public $description;
    public $bizrule;
    public $data;
    
    public function rules() {
        return array(
            array('name, type','required'),
            array('description, bizrule, data','safe'),
        );
    }
    
    /**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name' => Yii::t('auth','name'),
			'description' => Yii::t('auth','description'),
			'bizrule' => Yii::t('auth','bizrule'),
			'data' => Yii::t('auth','data'),
		);
	}
    
}

