<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AuthAssignmentForm extends CFormModel {

    public $itemname;
    public $userid;
    public $bizrule;
    public $data;
    
    public function rules() {
        return array(
            array('itemname, userid','required'),
            array('description, bizrule, data','safe'),
        );
    }
    
    /**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'itemname' => Yii::t('auth','Role'),
			'userid' => Yii::t('auth','User'),
			'bizrule' => Yii::t('auth','bizrule'),
			'data' => Yii::t('auth','data'),
		);
	}
    
}

