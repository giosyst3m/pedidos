<?php

class AssignmentController extends Controller
{
    /**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column1';

    private $data = array();
    /**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
                'roles'=>array('AuthAsActionIndex'),
                
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view'),
				'roles'=>array('AuthAsActionView'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('delete'),
				'roles'=>array('AuthAsActionDelete'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('update'),
				'roles'=>array('AuthAsActionUpdate'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
	public function actionIndex($id=0,$id2=0)
	{
        $model = new AuthAssignmentForm();
        $model2 = new AuthAssignment('search');
        $model2->unsetAttributes();
        if(isset($_GET['AuthAssignment']))
                    $model2->attributes=$_GET['AuthAssignment'];
        
        if(isset($_POST['AuthAssignmentForm'])){
            $model->attributes = $_POST['AuthAssignmentForm'];
            if($model->validate()){
                if(!AuthAssignment::model()->findByAttributes(array('itemname'=>$model->itemname,'userid'=>$model->userid))){
                    Yii::app()->authManager->assign($model->itemname,$model->userid);
                    Yii::app()->user->setFlash('success',  Yii::t('auth', 'SAVE',array('{dato}'=>$model->itemname,'{type}'=>Yii::t('auth','Assignment'))));
                    $this->redirect(array('index'));
                }else{
                    Yii::app()->user->setFlash('danger',  Yii::t('auth', 'DUPLICATE',array('{dato}'=>$model->itemname,'{type}'=>Yii::t('auth','Assignment'))));
                }
            }
        }
        
        
		$this->render('index',array(
            'model'=>$model,
            'model2'=>$model2,
            'items'=>  CHtml::listData(VAuthItem::model()->findAll('type in(2)'), 'name', 'name'),
            'user'=> CHtml::listData(SisUsuario::model()->findAll('r_d_s=1'), 'id', 'username'),
            'item'=>  array(),
            'id'=>  $id
        ));
        
	}
    

    /**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id=0,$id2=0)
	{
		$model=$this->loadModel($id,$id2);
        $model2=new AuthAssignment('search');
		$model2->unsetAttributes();  // clear any default values
		if(isset($_GET['AuthAssignment']))
			$model2->attributes=$_GET['AuthAssignment'];
        
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AuthAssignment']))
		{
            try{
                //$this->_print($_POST,true);
                $model->attributes=$_POST['AuthAssignment'];
                if($model->save()){
                    AuthItemChild::model()->deleteAll('parent=:parent', array('parent'=>$model->itemname));
                    if(isset($_POST['items'])){
                        if($this->addChild($model->itemname,$_POST['items'])){
                            Yii::app()->user->setFlash('success',  Yii::t('app', 'SUCCESS_UPDATE_OK'));
                        }else{
                            Yii::app()->user->setFlash('danger',  Yii::t('auth', 'ASOCIADATE_ERROR',array('{dato}'=>$model->itemname,'{type}'=>Yii::t('auth','Role'))));
                        }
                    }
                    $this->redirect(array('index'));
                }else{
                    Yii::app()->user->setFlash('danger',  Yii::t('app', 'SUCCESS_UPDATE_ERROR'));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger',$e->getCode().' '.$e->getMessage());
            }
		}
        $item=array();
		$this->render('update',array(
			'model'=>$model,'model2'=>$model2,
            'items'=>  CHtml::listData(VAuthItem::model()->findAll('type in(2)'), 'name', 'name'),
            'item'=> empty($item)?array():$item,
            'user'=> CHtml::listData(SisUsuario::model()->findAll('r_d_s=1'), 'id', 'email'),
            'id'=>  $id
		));
	}
    
    /**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return Almacen the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id,$id2)
	{
		$model= AuthAssignment::model()->findByAttributes(array('itemname'=>$id,'userid'=>$id2));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
    
    /**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id,$id2)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id,$id2),
		));
	}
    
    /**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id=null,$id2=null)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			//$this->loadModel($id)->delete();
            Yii::app()->authManager->revoke($id,$id2);
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			//if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
}