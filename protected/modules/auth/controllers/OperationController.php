<?php

class OperationController extends Controller
{
    /**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column1';

    private $data = array();
    /**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 
				'actions'=>array('index'),
				'roles'=>array('AuthOpActionIndex'),
			),
			array('allow',  // allow all users to perform 
				'actions'=>array('delete'),
				'roles'=>array('AuthOpActionDelete'),
			),
			array('allow',  // allow all users to perform 
				'actions'=>array('update'),
				'roles'=>array('AuthOpActionUpdate'),
			),
			array('allow',  // allow all users to perform 
				'actions'=>array('view'),
				'roles'=>array('AuthOpActionView'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
	public function actionIndex()
	{
        $model = new AuthOperationForm();
        $model2 = new AuthItem('search');
        $model2->unsetAttributes();
        $model2->type = 0;
        if(isset($_GET['AuthItem']))
			$model2->attributes=$_GET['AuthItem'];
        
        if(isset($_POST['AuthOperationForm'])){
            $model->attributes = $_POST['AuthOperationForm'];
            if($model->validate()){
                if(!AuthItem::model()->findByPk($model->name)){
                    Yii::app()->authManager->createOperation($model->name,$model->description,$model->bizrule,$model->data);
                    Yii::app()->user->setFlash('success',  Yii::t('auth', 'SAVE',array('{dato}'=>$model->name,'{type}'=>Yii::t('auth','Operation'))));
                    $this->redirect(array('index'));
                }else{
                    Yii::app()->user->setFlash('danger',  Yii::t('auth', 'DUPLICATE',array('{dato}'=>$model->name,'{type}'=>Yii::t('auth','Operation'))));
                }
            }
        }
		$this->render('index',array('model'=>$model,'model2'=>$model2));
	}
    
    /**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $model2 = new AuthItem('search');
                $model2->unsetAttributes();
                $model2->type = 0;
                if(isset($_GET['AuthItem']))
			$model2->attributes=$_GET['AuthItem'];
        
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AuthItem']))
		{
            try{
                $model->attributes=$_POST['AuthItem'];
                if($model->save()){
                    Yii::app()->user->setFlash('success',  Yii::t('app', 'SUCCESS_UPDATE_OK'));
                    $this->redirect(array('index'));
                }else{
                    Yii::app()->user->setFlash('danger',  Yii::t('app', 'SUCCESS_UPDATE_ERROR'));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger',$e->getCode().' '.$e->getMessage());
            }
		}

		$this->render('update',array(
			'model'=>$model,'model2'=>$model2
		));
	}
    
    /**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return Almacen the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=  AuthItem::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
    
    /**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
    
    /**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			//$this->loadModel($id)->delete();
            Yii::app()->authManager->removeAuthItem($id);
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
}