<?php
/* @var $this LabelController */
/* @var $model Label */
?>

<?php
$this->breadcrumbs=array(
	'Labels'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Label').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('LabelUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('LabelUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>