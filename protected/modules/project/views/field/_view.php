<?php
/* @var $this FieldController */
/* @var $data Field */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('label')); ?>:</b>
		<?php echo CHtml::encode($data->label); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('value')); ?>:</b>
		<?php echo CHtml::encode($data->value); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('require')); ?>:</b>
		<?php echo CHtml::encode($data->require); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_fie_type')); ?>:</b>
		<?php echo CHtml::encode($data->id_fie_type); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_s); ?>
		<br />


</div>