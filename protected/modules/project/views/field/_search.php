<?php
/* @var $this FieldController */
/* @var $model Field */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'label',array('maxlength'=>255)); ?>
    <?php echo $form->textAreaControlGroup($model,'value',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'require'); ?>
    <?php echo $form->textFieldControlGroup($model,'id_fie_type'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_s'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
