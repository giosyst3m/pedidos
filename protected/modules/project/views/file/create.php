<?php
/* @var $this FileController */
/* @var $model File */
?>

<?php
$this->breadcrumbs=array(
	'Files'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','File')) ?>

<?php 
if(Yii::app()->user->checkAccess('FileCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('FileCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>