<?php
/* @var $this FileController */
/* @var $model File */
?>

<?php
$this->breadcrumbs=array(
	'Files'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','File').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('FileUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('FileUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>