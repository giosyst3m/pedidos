<?php
/* @var $this ImpactController */
/* @var $model Impact */
?>

<?php
$this->breadcrumbs=array(
	'Impacts'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Impact').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ImpactUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ImpactUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>