<?php
/* @var $this ProjectSisUsuarioController */
/* @var $model ProjectSisUsuario */
?>

<?php
$this->breadcrumbs=array(
	'Project Sis Usuarios'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','ProjectSisUsuario')) ?>

<?php 
if(Yii::app()->user->checkAccess('ProjectSisUsuarioCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProjectSisUsuarioCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>