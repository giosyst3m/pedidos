

<?php

$this->widget('bootstrap.widgets.BsGridView', array(
    'id' => 'project-sis-usuario-grid',
    'dataProvider' => $model2->search(),
    'filter' => $model2,
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        [
            'name' => 'id_sis_usuario',
            'type' => 'raw',
            'value' => function($data) {
                $vSisUsuario = VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->model()->findByAttributes(['id' => $data->id_sis_usuario]);
                return $vSisUsuario->complete_name . ' - ' . $vSisUsuario->sis_usu_tipo_nombre;
            },
                ],
                array(
                    'name' => 'r_d_s',
                    'value' => '$data->RegistroEstado($data->r_d_s)',
                    'filter' => array(0 => 'Inactivo', 1 => 'Activo')
                ),
                array(
                    'template' => '{delete}',
                    'class' => 'bootstrap.widgets.BsButtonColumn',
                    'buttons' => array(
                        'delete' => array
                            (
                            'label' => Yii::t('app', 'Change Status Seller in the Company'), //Text label of the button.
                            'url' => 'Yii::app()->createUrl("project/projectSisUsuario/delete", array("id"=>$data->id))', //A PHP expression for generating the URL of the button.
                        )
                    )
                ),
            ),
        ));
        ?>




