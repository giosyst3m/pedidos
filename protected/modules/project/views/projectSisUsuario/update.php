<?php
/* @var $this ProjectSisUsuarioController */
/* @var $model ProjectSisUsuario */
?>

<?php
$this->breadcrumbs=array(
	'Project Sis Usuarios'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','ProjectSisUsuario').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ProjectSisUsuarioUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProjectSisUsuarioUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>