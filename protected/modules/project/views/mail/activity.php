
<h1><?php echo Yii::t('app', 'Ticket', array('{numero}' => $model->id)); ?></h1>
<h2><?php echo Yii::t('app', 'Status') . ': ' . $model->idStatus->name; ?></h2>
<h2><?php echo Yii::t('app', 'Prioridad') . ': ' . $model->idPriority->name; ?></h2>
<h3><?php echo Yii::t('app', 'Type') . ': ' . $model->idType->name; ?></h3>
<h3><?php echo Yii::t('app', 'Frequency') . ': ' . $model->idFrequency->name; ?></h3>
<h3><?php echo Yii::t('app', 'r_c_u') . ': ' . VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['id'=>$model->r_c_u])->complete_name; ?></h3>
<br/>
<p><?php echo $model->name?></p>
<p><?php echo $model->description?></p>
<br/>
<?php
echo BsHtml::link(Yii::t('app', 'Click here') . Yii::t('app', 'View') . ' ' . Yii::t('app', 'Ticket', array('{numero}' => $model->id)), Yii::app()->createAbsoluteUrl('project/activity/index/', array('id' => $model->id)));
?>
<br/>
<br/>
