<?php
/* @var $this TimerController */
/* @var $model Timer */
?>

<?php
$this->breadcrumbs=array(
	'Timers'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Timer')) ?>

<?php 
if(Yii::app()->user->checkAccess('TimerCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('TimerCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>