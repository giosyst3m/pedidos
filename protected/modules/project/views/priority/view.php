<?php
/* @var $this PriorityController */
/* @var $model Priority */
?>

<?php
$this->breadcrumbs=array(
	'Priorities'=>array('index'),
	$model->name,
);

?>

<?php echo BsHtml::pageHeader('View','Priority '.$model->id) ?>
<?php 
 $reg = array();
if(Yii::app()->user->checkAccess('PriorityViewAuthView')){
    $reg = array(	
						'id',
		'name',
		'description',
		'order',
		'color',
		'r_c_u',
		'r_c_d',
		'r_c_i',
		'r_u_u',
		'r_u_d',
		'r_u_i',
		'r_d_u',
		'r_d_d',
		'r_d_i',
		'r_d_s',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
        
    );
}else{
    $reg = array(	
				'id',
		'name',
		'description',
		'order',
		'color',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
    );
}
?><?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>$reg
	,
)); ?>

<?php echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => 'history.go(-1)',
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Priority/create'),array('class'=>  'btn btn-primary')); ?>