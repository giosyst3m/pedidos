<?php
/* @var $this ProjectStatusController */
/* @var $model ProjectStatus */
?>

<?php
$this->breadcrumbs=array(
	'Project Statuses'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','ProjectStatus')) ?>

<?php 
if(Yii::app()->user->checkAccess('ProjectStatusCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProjectStatusCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>