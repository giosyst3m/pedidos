<?php
/* @var $this ProjectStatusController */
/* @var $model ProjectStatus */
?>

<?php
$this->breadcrumbs=array(
	'Project Statuses'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','ProjectStatus').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ProjectStatusUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProjectStatusUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>