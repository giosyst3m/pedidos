<?php
/* @var $this ProjectController */
/* @var $model Project */
?>

<?php
$this->breadcrumbs=array(
	'Projects'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Project').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ProjectUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProjectUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>