<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'project-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>
<span class="section"></span>
    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

     <?php echo $form->dropDownListControlGroup($model,'father',$Project,array('data-style'=>'btn-success',
         'class'=>'selectpicker show-tick',
         'empty'=>'Select one',
         'data-live-search'=>true,
         'help'=>'Si va generar un Sub-Proyecto debe seleccinar el Principal al que perteneces.'
         )); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_priority',$Priority,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','empty'=>'Select one','data-live-search'=>true,)); ?>
    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_type',$Type,array('data-style'=>'btn-info','class'=>'selectpicker show-tick','empty'=>'Select one','data-live-search'=>true,)); ?>
    <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'order'); ?>
    <?php echo $form->textFieldControlGroup($model,'color',array('maxlength'=>9,'value'=>'#2A3F54','class'=>'demo2','before'=> BsHtml::icon(BsHtml::GLYPHICON_HEADER))); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('ProjectCreateStatusChange') || Yii::app()->user->checkAccess('ProjectUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('ProjectCreateButtonSave') || Yii::app()->user->checkAccess('ProjectUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('ProjectCreateButtonNew') || Yii::app()->user->checkAccess('ProjectUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('project/project/index'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
