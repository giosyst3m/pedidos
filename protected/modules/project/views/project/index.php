<?php
/* @var $this ProjectController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
    'Projects',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create Project', 'url' => array('create')),
    array('icon' => 'glyphicon glyphicon-tasks', 'label' => 'Manage Project', 'url' => array('admin')),
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/project/project/index.js', CClientScript::POS_HEAD);
?>

<?php echo BsHtml::pageHeader('Projects') ?>
<input type="hidden" id="step" value="<?php echo $step;?>">
<div class="x_panel">
    <div class="x_title">
        <h2>Asistente para creación de Proyectos</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="well">Descripión del asistente</div>
        <div id="wizard_verticle" class="form_wizard wizard_verticle">
            <ul class="list-unstyled wizard_steps">
                <li>
                    <a href="#step-11">
                        <span class="step_no">1</span>
                    </a>
                </li>
                <li>
                    <a href="#step-22">
                        <span class="step_no">2</span>
                    </a>
                </li>
                <li>
                    <a href="#step-33">
                        <span class="step_no">3</span>
                    </a>
                </li>
                <li>
                    <a href="#step-44">
                        <span class="step_no">4</span>
                    </a>
                </li>
                <li>
                    <a href="#step-55">
                        <span class="step_no">5</span>
                    </a>
                </li>
<!--                <li>
                    <a href="#step-66">
                        <span class="step_no">6</span>
                    </a>
                </li>-->
            </ul>

            <div id="step-11">
                <h2 class="StepTitle">Nombre y Descripción del Proyecto <i style="color:<?php echo $model->color;?>"><?php echo $model->name; ?></i></h2>
                <?php $this->renderPartial('_form', ['model' => $model, 'Priority' => $Priority, 'Project' => $Project, 'Type'=> $Type]); ?>
            </div>
            <div id="step-22">
                <h2 class="StepTitle">Asignar Prioridades <i style="color:<?php echo $model->color;?>"><?php echo $model->name; ?></i></h2>
                <?php $this->renderPartial('../projectPriority/_form', ['model' => $ProjectPriority,'model2'=>$ProjectPriorityAdmin,'Priority' => $Priority,]); ?>
                <?php $this->renderPartial('../projectPriority/admin', ['model2'=>$ProjectPriorityAdmin,'Priority' => $Priority,]); ?>
            </div>
            <div id="step-33">
                <h2 class="StepTitle">Asignar Resoluciones <i style="color:<?php echo $model->color;?>"><?php echo $model->name; ?></i></h2>
                <?php $this->renderPartial('../projectResolution/_form', ['model' => $ProjectResolution,'model2'=>$ProjectResolutionAdmin,'Resolution' => $Resolution,]); ?>
                <?php $this->renderPartial('../projectResolution/admin', ['model2'=>$ProjectResolutionAdmin,'Resolution' => $Resolution,]); ?>
            </div>
            <div id="step-44">
                 <h2 class="StepTitle">Asignar Estatus <i style="color:<?php echo $model->color;?>"><?php echo $model->name; ?></i></h2>
                <?php $this->renderPartial('../projectStatus/_form', ['model' => $ProjectStatus,'model2'=>$ProjectStatusAdmin,'Status' => $Status,]); ?>
                <?php $this->renderPartial('../projectStatus/admin', ['model2'=>$ProjectStatusAdmin,'Status' => $Status,]); ?>
            </div>
            <div id="step-55">
               <h2 class="StepTitle">Asignar Usuarios <i style="color:<?php echo $model->color;?>"><?php echo $model->name; ?></i></h2>
                <?php $this->renderPartial('../projectSisUsuario/_form', ['model' => $ProjectSisUsuario,'model2'=>$ProjectSisUsuarioAdmin,'VSisUsuario' => $VSisUsuario,]); ?>
                <?php $this->renderPartial('../projectSisUsuario/admin', ['model2'=>$ProjectSisUsuarioAdmin,'VSisUsuario' => $VSisUsuario,]); ?>
            </div>
<!--            <div id="step-66">
                <h2 class="StepTitle">Tipo de Proyecto <i style="color:<?php echo $model->color;?>"><?php echo $model->name; ?></i></h2>
                
            </div>-->
        </div>
    </div>
</div>
<!-- End SmartWizard Content -->
<!-- Bootstrap Colorpicker -->
<script>
    $(document).ready(function () {
        $('.demo1').colorpicker();
        $('.demo2').colorpicker();

        $('#demo_forceformat').colorpicker({
            format: 'rgba',
            horizontal: true
        });

        $('#demo_forceformat3').colorpicker({
            format: 'rgba',
        });

        $('.demo-auto').colorpicker();
    });
</script>
<!-- /Bootstrap Colorpicker -->