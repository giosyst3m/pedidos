<div class="timeline">
    <span class="timeline-label">
        <span class="label label-info">Hoy</span>
    </span>
    <?php foreach ($model as $value) {
        $color = $value->idPriority->color;
        $VSisUsuario = VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['id' => $value->r_c_u]);
        ?>
        <div class="timeline-item">
            <div class="timeline-point timeline-point-<?php echo $color; ?>">
                <i class="<?php echo $value->idPriority->icon ?>"></i>
            </div>
            <div class="timeline-event timeline-event-<?php echo $color; ?>">
                <div class="timeline-heading">
                    <h4><?php echo BsHtml::imageCircle(Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . $VSisUsuario->photo).' '.$VSisUsuario->complete_name; ?></h4>
                </div>
                <div class="timeline-body">
                    <p><?php echo $value->messages; ?></p>
                    <br>
                    <p>
                    <div class="btn-group" role="group" aria-label="...">
                            <?php
                            foreach (ComFile::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes(['id_comment' => $value->id]) as $file) {
                                echo $this->getLinktypeFile($file->extension, Yii::app()->user->getState('TICKET')->TICKET_COMMENT_PATH . $file->file, ['target' => '_blank', 'class' => 'btn ' . $value->idPriority->boton_lcolor, 'title' => $file->name]);
                            }
                            ?>
                        </div>
                    </p>
                </div>
                <div class="timeline-footer">
                    <smal><?php echo $value->idPriority->name; ?></smal>
                    <p class="text-right"><?php echo $value->r_c_d; ?></p>
                </div>
            </div>
        </div>
    <?php  } ?>
    <span class="timeline-label">
        <button class="btn btn-primary"><i class="fa fa-star"></i></button>
    </span>
</div>