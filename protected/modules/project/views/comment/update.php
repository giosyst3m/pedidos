<?php
/* @var $this CommentController */
/* @var $model Comment */
?>

<?php
$this->breadcrumbs=array(
	'Comments'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Comment').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('CommentUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('CommentUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>