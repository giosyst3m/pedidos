<?php
/* @var $this CommentController */
/* @var $model Comment */
?>

<?php
$this->breadcrumbs=array(
	'Comments'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Comment')) ?>

<?php 
if(Yii::app()->user->checkAccess('CommentCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('CommentCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>