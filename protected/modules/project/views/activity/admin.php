<?php
/* @var $this ActivityController */
/* @var $model Activity */


$this->breadcrumbs = array(
    'Activities' => array('index'),
    'Manage',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-list', 'label' => 'List Activity', 'url' => array('index')),
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create Activity', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#activity-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button(Yii::t('app', 'Advanced search'), array('class' => 'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH, 'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            <?php echo Yii::t('app', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
                &lt;&gt;</b>
            or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.'); ?>        </p>

        <div class="search-form" style="display:none">
            <?php
            $this->renderPartial('_search', array(
                'model' => $model2,
            ));
            ?>
        </div>
        <!-- search-form -->

        <?php
        $this->widget('bootstrap.widgets.BsGridView', array(
            'id' => 'activity-grid',
            'dataProvider' => $model2->search(),
            'filter' => $model2,
            'type' => BsHtml::GRID_TYPE_RESPONSIVE,
            'columns' => array(
                'id',
                'name',
                [
                    'header' => 'Tipo',
                    'type' => 'raw',
                    'name' => 'id_type',
                    'filter' => $Type,
                    'value' => function($data) {
                        return BsHtml::tag('span', ['class' => $data->idType->color], $data->idType->name);
                    }
                        ],
                        [
                            'header' => 'Estado',
                            'type' => 'raw',
                            'name' => 'id_status',
                            'filter' => $Status,
                            'value' => function($data) {
                                return BsHtml::tag('span', ['class' => $data->idStatus->color], $data->idStatus->name);
                            }
                                ],
                                [
                                    'header' => 'Prioridad',
                                    'type' => 'raw',
                                    'name' => 'id_priority',
                                    'filter' => $Priority,
                                    'value' => function($data) {
                                        return BsHtml::tag('span', ['class' => $data->idPriority->boton_lcolor], BsHtml::tag('span', ['class' => $data->idPriority->icon]) . ' ' . $data->idPriority->name);
                                    }
                                        ],
                                        [
                                            'header' => 'Frecuencia',
                                            'type' => 'raw',
                                            'name' => 'id_frequency',
                                            'filter' => $Frequency,
                                            'value' => function($data) {
                                                return BsHtml::tag('span', ['class' => $data->idFrequency->color], $data->idFrequency->name);
                                            }
                                                ],
                                                [
                                                    'header' => 'Resolución',
                                                    'type' => 'raw',
                                                    'name' => 'id_resolution',
                                                    'filter' => $Resolution,
                                                    'value' => function($data) {
                                                        return BsHtml::tag('span', ['class' => $data->idResolution->color], $data->idResolution->name);
                                                    }
                                                        ],
                                                        [
                                                            'header' => 'Usuario',
                                                            'type' => 'raw',
                                                            'name' => 'r_c_u',
                                                            'filter' => $VSisUsuario,
                                                            'value' => function($data) {
                                                                $VSisUsuario = VSisUsuario::model()->findByAttributes(['id' => $data->r_c_u]);
                                                                return BsHtml::imageCircle(Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . $VSisUsuario->photo).$VSisUsuario->complete_name;
                                                            }
                                                                ],
                                                                [
                                                                    'class' => 'bootstrap.widgets.BsButtonColumn',
                                                                    'template' => '{update}',
                                                                    'buttons' => array(
                                                                        'update' => array(
                                                                            'label' => Yii::t('app', 'Update'),
                                                                            'url' => 'Yii::app()->createUrl("ticket/Activity/index", array("id"=>$data->id))',
                                                                        )
                                                                    )
                                                                ],
                                                            ),
                                                        ));
                                                        ?>
    </div>
</div>




