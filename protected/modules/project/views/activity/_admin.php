<div class="panel panel-default">
    <div class="panel-body">
        <?php
        $this->widget('bootstrap.widgets.BsGridView', array(
            'id' => 'activity-grid',
            'dataProvider' => $model2->search(),
            'filter' => $model2,
            'type' => BsHtml::GRID_TYPE_RESPONSIVE,
            'columns' => array(
                'id',
                'name',
                'type',
                [
                    'header' => 'Estado',
                    'type' => 'raw',
                    'name' => 'id_status',
                    'value' => function($data) {
                        return BsHtml::tag('span', ['class' => $data->status_color], $data->status);
                    }
                ],
                [
                    'header' => 'Prioridad',
                    'type' => 'raw',
                    'name' => 'id_priority',
                    'value' => function($data) {
                        return BsHtml::tag('span', ['class' => $data->priority_boton_lcolor], BsHtml::tag('span', ['class' => $data->priority_icon]) . ' ' . $data->priority);
                    }
                ],
                'frequency',
                'resolution',
                [
                    'header' => 'Usuario',
                    'type' => 'raw',
                    'name' => 'r_c_u',
                    'value' => function($data) {
                        $VSisUsuario = VSisUsuario::model()->findByAttributes(['id' => $data->r_c_u]);
                        return '<div class="row"><div class="col-lg-6">'. BsHtml::image(Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . $VSisUsuario->photo,'',['class'=>'img-circle profile_img']).'</div><div class="col-lg-6"> '.$VSisUsuario->complete_name;
                    }
                ],
                [
                    'class' => 'bootstrap.widgets.BsButtonColumn',
                    'template' => '{update}',
                    'buttons' => array(
                        'update' => array(
                            'label' => Yii::t('app', 'Update'),
                            'url' => 'Yii::app()->createUrl("ticket/Activity/index", array("id"=>$data->id))',
                        )
                    )
                ],
                    ),
                ));
                ?>
    </div>
</div>



