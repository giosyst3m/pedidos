<?php
/* @var $this ActivityController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
    'Activities',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create Activity', 'url' => array('create')),
    array('icon' => 'glyphicon glyphicon-tasks', 'label' => 'Manage Activity', 'url' => array('admin')),
);
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/project/activity/_list.js', CClientScript::POS_HEAD); ?>
<style>
    .form-group{
        display: block !important;
        margin: 5px;
    }
</style>
<div class="row">
    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Registrar</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">
                <?php
                $this->renderPartial('_form_in_line', [
                    'model' => $model,
                    'Impact' => $Impact,
                    'Type' => $Type,
                    'Priority' => $Priority,
                    'Frequency' => $Frequency,
                    'Resolution' => $Resolution,
                    'Status' => $Status,
                    'VSisUsuario'=>$VSisUsuario,
                ])
                ?>
            </div>
        </div>
        <?php if (Yii::app()->user->checkAccess('ActivityIndexShowTimer')) { ?>
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">Reportar</h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                </div>
                <div class="panel-body">
                    <?php
                    $this->renderPartial('../timer/_form', [
                        'model' => $Timer,
                    ]);
                    $this->renderPartial('../timer/admin', [
                        'model2' => $TimerAdmin,
                    ]);
                    ?>
                </div>
            </div>
        <?php } ?>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Incidencias</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">
                <?php
                $this->renderPartial('admin', [
                    'model2' => $model2,
                    'Impact' => $Impact,
                    'Type' => $Type,
                    'Priority' => $Priority,
                    'Frequency' => $Frequency,
                    'Resolution' => $Resolution,
                    'Status' => $Status,
                    'VSisUsuario' => $VSisUsuario,
                ])
                ?>

            </div>
        </div>
    </div>

    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
        <?php if (Yii::app()->user->checkAccess('ActivityIndexShowTodo')) { ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">To do</h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                </div>
                <div class="panel-body">
                    <?php
                    $this->renderPartial('../todo/_form', [
                        'model' => $Todo,
                        'Priority' => $Priority,
                    ]);
                    $this->widget('bootstrap.widgets.BsListView', array(
                        'dataProvider' => $TodoAdmin,
                        'itemView' => '../todo/_list',
                    ));
                    ?>
                </div>
            </div>
        <?php } ?>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Comentarios</h3><small><?php echo $model->name; ?></small>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">
                <?php
                $this->renderPartial('../comment/_form', [
                    'model' => $Comment,
                    'Priority' => $Priority,
                ]);
                $this->renderPartial('../comment/_timeline', [
                    'model' => $Commentmodel2,
                    'Priority' => $Priority,
                ]);
                ?>
            </div>
        </div>
    </div>
</div>