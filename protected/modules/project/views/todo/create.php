<?php
/* @var $this TodoController */
/* @var $model Todo */
?>

<?php
$this->breadcrumbs=array(
	'Todos'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Todo')) ?>

<?php 
if(Yii::app()->user->checkAccess('TodoCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('TodoCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>