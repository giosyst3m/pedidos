<ul class="list-group">
    <li class="list-group-item <?php echo $data->idPriority->boton_lcolor; ?> ">
        <div class="row">
            <div class="col-lg-8">
                <?php echo BsHtml::tag('span', ['class' => $data->idPriority->icon . ' fa-2x '], '') . ' ' . $data->name; ?>
            </div>
            <div class="col-lg-1">
                <div class="material-switch pull-right">
                    <input id="<?php echo $data->id; ?>" name="someSwitchOption001" type="checkbox" data-done="<?php echo $data->done; ?>" <?php echo ($data->done ? 'checked="checked"' : ''); ?> onclick="doneTodo(this);"/>
                    <label for="<?php echo $data->id; ?>" class="label-<?php echo $data->idPriority->color; ?>"></label>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="btn-group" role="group" aria-label="...">
                    <?php echo BsHtml::link(BsHtml::icon(BsHtml::GLYPHICON_EDIT), $this->createUrl('index', ['id' => $data->id_activity, 'idTodo' => $data->id]), ['class' => 'btn btn-' . $data->idPriority->color . ' btn-sm done-' . $data->id, 'disabled' => ($data->done ? 'disabled' : '')]); ?>
                    <?php
                    echo BsHtml::ajaxLink(BsHtml::icon(BsHtml::GLYPHICON_TRASH), $this->createUrl('todo/Delete', ['id' => $data->id, 'idActivity' => $data->id_activity]), [
                        'type' => 'post',
                        'dataType' => 'json',
                        'success' => 'function(data){
                         $.growl(data.menssage, {
                                type: data.type
                            });
                            location.reload();
                        }',
                            ], ['class' => 'btn btn-' . $data->idPriority->color . ' btn-sm done-' . $data->id, 'disabled' => ($data->done ? 'disabled' : '')]);
                    ?>
                </div>
            </div>
        </div>
    </li>
</ul>