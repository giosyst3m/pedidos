<?php
/* @var $this TodoController */
/* @var $model Todo */
/* @var $form BSActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'todo-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => BsHtml::FORM_LAYOUT_INLINE,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    )
        ));
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldControlGroup($model, 'name', array('maxlength' => 255, 'style' => "width:100%; !important",'placeholder'=>'Título de la Tarea')); ?>
<br>
<?php echo $form->dateField($model, 'from'); ?>
<?php echo $form->dateField($model, 'to'); ?>
<div class="row">
    <div class="col-lg-6">
        <?php echo $form->dropDownListControlGroup($model, 'id_priority', $Priority, array('data-style' => 'btn-primary', 'class' => 'tipo_prod selectpicker show-tick', 'empty' => Yii::t('app', 'Priority'))); ?>
    </div>
    <div class="col-lg-6">
        <?php
        if (Yii::app()->user->checkAccess('TodoCreateButtonSave') || Yii::app()->user->checkAccess('TodoUpdateButtonSave')) {
            echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY,'class'=> 'btn-block'));
        }
        ?>       
    </div>
</div>



<?php $this->endWidget(); ?>
