<?php
/* @var $this ProjectPriorityController */
/* @var $model ProjectPriority */
?>

<?php
$this->breadcrumbs=array(
	'Project Priorities'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','ProjectPriority').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ProjectPriorityUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProjectPriorityUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>