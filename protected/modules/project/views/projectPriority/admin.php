

<?php

$this->widget('bootstrap.widgets.BsGridView', array(
    'id' => 'project-priority-grid',
    'dataProvider' => $model2->search(),
    'filter' => $model2,
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        [
            'name' => 'id_priority',
            'type' => 'raw',
            'value' => function($data) {
                return BsHtml::tag('span', ['class' =>  $data->idPriority->boton_lcolor], $data->idPriority->name, true);
            },
                ],
                array(
                    'name' => 'r_d_s',
                    'value' => '$data->RegistroEstado($data->r_d_s)',
                    'filter' => array(0 => 'Inactivo', 1 => 'Activo')
                ),
                array(
                    'template' => '{delete}',
                    'class' => 'bootstrap.widgets.BsButtonColumn',
                    'buttons' => array(
                    'delete' => array
                        (
                        'label' => Yii::t('app', 'Change Status Seller in the Company'), //Text label of the button.
                        'url' => 'Yii::app()->createUrl("project/projectPriority/delete", array("id"=>$data->id))', //A PHP expression for generating the URL of the button.
                        )
                    )
                ),
            ),
        ));
        ?>




