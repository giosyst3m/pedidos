<?php
/* @var $this ProjectPriorityController */
/* @var $model ProjectPriority */
?>

<?php
$this->breadcrumbs=array(
	'Project Priorities'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','ProjectPriority')) ?>

<?php 
if(Yii::app()->user->checkAccess('ProjectPriorityCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProjectPriorityCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>