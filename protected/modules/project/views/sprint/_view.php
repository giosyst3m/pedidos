<?php
/* @var $this SprintController */
/* @var $data Sprint */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
		<?php echo CHtml::encode($data->name); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
		<?php echo CHtml::encode($data->description); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('order')); ?>:</b>
		<?php echo CHtml::encode($data->order); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('color')); ?>:</b>
		<?php echo CHtml::encode($data->color); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('from')); ?>:</b>
		<?php echo CHtml::encode($data->from); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('to')); ?>:</b>
		<?php echo CHtml::encode($data->to); ?>
		<br />

	
		<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
		<?php echo CHtml::encode($data->status); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_project')); ?>:</b>
		<?php echo CHtml::encode($data->id_project); ?>
		<br />
<?php if(Yii::app()->user->checkAccess('SprintViewAuthView')){?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_s); ?>
		<br />

	<?php }?>

</div>