<?php
/* @var $this SprintController */
/* @var $model Sprint */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'sprint-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'order'); ?>
    <?php echo $form->textFieldControlGroup($model,'color',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'from'); ?>
    <?php echo $form->textFieldControlGroup($model,'to'); ?>
    <?php echo $form->textFieldControlGroup($model,'status'); ?>
    <?php echo $form->textFieldControlGroup($model,'id_project'); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('SprintCreateStatusChange') || Yii::app()->user->checkAccess('SprintUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('SprintCreateButtonSave') || Yii::app()->user->checkAccess('SprintUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('SprintCreateButtonNew') || Yii::app()->user->checkAccess('SprintUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Sprint/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
