<?php
/* @var $this SprintController */
/* @var $model Sprint */
?>

<?php
$this->breadcrumbs=array(
	'Sprints'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Sprint')) ?>

<?php 
if(Yii::app()->user->checkAccess('SprintCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('SprintCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>