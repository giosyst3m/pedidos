<?php
/* @var $this ProjectResolutionController */
/* @var $model ProjectResolution */
?>

<?php
$this->breadcrumbs=array(
	'Project Resolutions'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','ProjectResolution').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ProjectResolutionUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProjectResolutionUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>