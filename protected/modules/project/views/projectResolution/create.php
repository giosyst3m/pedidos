<?php
/* @var $this ProjectResolutionController */
/* @var $model ProjectResolution */
?>

<?php
$this->breadcrumbs=array(
	'Project Resolutions'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','ProjectResolution')) ?>

<?php 
if(Yii::app()->user->checkAccess('ProjectResolutionCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProjectResolutionCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>