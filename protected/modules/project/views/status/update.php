<?php
/* @var $this StatusController */
/* @var $model Status */
?>

<?php
$this->breadcrumbs=array(
	'Statuses'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Status').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('StatusUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('StatusUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>