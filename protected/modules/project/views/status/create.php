<?php
/* @var $this StatusController */
/* @var $model Status */
?>

<?php
$this->breadcrumbs=array(
	'Statuses'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Status')) ?>

<?php 
if(Yii::app()->user->checkAccess('StatusCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('StatusCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>