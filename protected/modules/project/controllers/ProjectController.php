<?php

class ProjectController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('ProjectActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('ProjectActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('ProjectActionCreate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('ProjectActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('ProjectActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('ProjectActionDelete'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Project;
        $model2 = new Project('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Project']))
            $model2->attributes = $_GET['Project'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Project'])) {
            try {
                $model->attributes = $_POST['Project'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Project'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Project'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model, 'model2' => $model2
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model2 = new Project('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Project']))
            $model2->attributes = $_GET['Project'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Project'])) {
            try {
                $model->attributes = $_POST['Project'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'Project'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'Project'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model, 'model2' => $model2
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('ProjectDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Project'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex($id = 0, $step = 1) {
        if (empty($id)) {
            $Project = new Project;
        } else {
            $Project = $this->loadModel($id);
        }
        $ProjectAdmin = new Project('search');
        $ProjectAdmin->unsetAttributes();  // clear any default values
        if (isset($_GET['Project']))
            $ProjectAdmin->attributes = $_GET['Project'];
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($Project);

        if (isset($_POST['Project'])) {
            try {
                $Project->attributes = $_POST['Project'];
                if ($Project->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Project'))));
                    $this->redirect(array('index', 'id' => $Project->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Project'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }
        $ProjectPriority = new ProjectPriority;
        $ProjectResolution = new ProjectResolution;
        $ProjectStatus = new ProjectStatus;
        $ProjectSisUsuario = new ProjectSisUsuario;
        if (!empty($id)) {
            $ProjectPriority->id_project = $Project->id;
            $ProjectResolution->id_project = $Project->id;
            $ProjectStatus->id_project = $Project->id;
            $ProjectSisUsuario->id_project = $Project->id;
        }
        $ProjectPriorityAdmin = new ProjectPriority('search');
        $ProjectPriorityAdmin->unsetAttributes();  // clear any default values
        if (isset($_GET['ProjectPriority']))
            $ProjectPriorityAdmin->attributes = $_GET['ProjectPriority'];
        $ProjectPriorityAdmin->id_project = $Project->id;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($ProjectPriority);

        if (isset($_POST['ProjectPriority'])) {
            try {
                $ProjectPriority->attributes = $_POST['ProjectPriority'];
                if ($ProjectPriority->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Project'))));
                    $this->redirect(array('index', 'id' => $Project->id, 'step' => 2));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Project'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 23000, [
                                '{code}' => Yii::app()->request->getPost('Client')['code'],
                                '{correlativo}' => Yii::app()->request->getPost('Client')['correlative']
                                    ]
                    ));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $ProjectResolutionAdmin = new ProjectResolution('search');
        $ProjectResolutionAdmin->unsetAttributes();
        if (isset($_GET['ProjectResolution']))
            $ProjectResolutionAdmin->attributes = $_GET['ProjectResolution'];
        $ProjectResolutionAdmin->id_project = $Project->id;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($ProjectResolution);
        if (isset($_POST['ProjectResolution'])) {
            try {
                $ProjectResolution->attributes = $_POST['ProjectResolution'];
                if ($ProjectResolution->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Project'))));
                    $this->redirect(array('index', 'id' => $Project->id, 'step' => 3));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Project'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 23000, [
                                '{code}' => Yii::app()->request->getPost('Client')['code'],
                                '{correlativo}' => Yii::app()->request->getPost('Client')['correlative']
                                    ]
                    ));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $ProjectStatusAdmin = new $ProjectStatus('search');
        $ProjectStatusAdmin->unsetAttributes();
        if (isset($_GET['ProjectStatus']))
            $ProjectStatusAdmin->attributes = $_GET['ProjectStatus'];
        $ProjectStatusAdmin->id_project = $Project->id;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($ProjectStatus);
        if (isset($_POST['ProjectStatus'])) {
            try {
                $ProjectStatus->attributes = $_POST['ProjectStatus'];
                if ($ProjectStatus->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Project'))));
                    $this->redirect(array('index', 'id' => $Project->id, 'step' => 4));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Project'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 23000, [
                                '{code}' => Yii::app()->request->getPost('Client')['code'],
                                '{correlativo}' => Yii::app()->request->getPost('Client')['correlative']
                                    ]
                    ));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $ProjectSisUsuarioAdmin = new $ProjectSisUsuario('search');
        $ProjectSisUsuarioAdmin->unsetAttributes();
        if (isset($_GET['ProjectSisUsuario']))
            $ProjectStatusAdmin->attributes = $_GET['ProjectSisUsuario'];
        $ProjectSisUsuarioAdmin->id_project = $Project->id;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($ProjectStatus);
        if (isset($_POST['ProjectSisUsuario'])) {
            try {
                $ProjectSisUsuario->attributes = $_POST['ProjectSisUsuario'];
                if ($ProjectSisUsuario->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Project'))));
                    $this->redirect(array('index', 'id' => $Project->id, 'step' => 5));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Project'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 23000, [
                                '{code}' => Yii::app()->request->getPost('Client')['code'],
                                '{correlativo}' => Yii::app()->request->getPost('Client')['correlative']
                                    ]
                    ));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $this->render('index', array(
            'model' => $Project, 'model2' => $ProjectAdmin,
            'ProjectPriority' => $ProjectPriority, 'ProjectPriorityAdmin' => $ProjectPriorityAdmin,
            'ProjectResolution' => $ProjectResolution, 'ProjectResolutionAdmin' => $ProjectResolutionAdmin,
            'ProjectStatus' => $ProjectStatus, 'ProjectStatusAdmin' => $ProjectStatus,
            'ProjectSisUsuario' => $ProjectSisUsuario, 'ProjectSisUsuarioAdmin' => $ProjectSisUsuarioAdmin,
            'step' => $step,
            'Type' => BsHtml::listData(Type::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes(['r_d_s' => 1]), 'id', 'name'),
            'Priority' => BsHtml::listData(Priority::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes(['r_d_s' => 1, 'project' => 1]), 'id', 'name'),
            'Resolution' => BsHtml::listData(Resolution::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes(['r_d_s' => 1]), 'id', 'name'),
            'Status' => BsHtml::listData(Status::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes(['r_d_s' => 1]), 'id', 'name'),
            'Project' => BsHtml::listData(Project::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes(['r_d_s' => 1]), 'id', 'name', 'father'),
            'VSisUsuario' => BsHtml::listData(VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes(['r_d_s' => 1]), 'id', 'complete_name','sis_usu_tipo_nombre'),
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Project('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Project']))
            $model->attributes = $_GET['Project'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Project the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Project::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Project $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'project-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'project-priority-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'project-resolution-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'project-status-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'project-sis-usuario-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
