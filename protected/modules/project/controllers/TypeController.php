<?php

class TypeController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('TypeActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('TypeActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('TypeActionCreate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('TypeActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('TypeActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('TypeActionDelete'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Type;
        $model2 = new Type('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Type']))
            $model2->attributes = $_GET['Type'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Type'])) {
            try {
                $model->attributes = $_POST['Type'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Type'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Type'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model, 'model2' => $model2
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model2 = new Type('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Type']))
            $model2->attributes = $_GET['Type'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        
        if (isset($_POST['Type'])) {
            try {
                //$this->_print($_POST,true);
                $model->attributes = $_POST['Type'];
                if ($model->save()) {
                    TypeField::model()->deleteAll('id_type=:id', array('id' => $model->id));
                    if (!empty($_POST['items']) && isset($_POST['items'])) {
                        foreach ($_POST['items'] as $value) {
                            $_save = new TypeField();
                            $_save->id_field = $value;
                            $_save->id_type = $model->id;
                            if (!$_save->save()) {
                                Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'TypeField'))));
                            }
                        }
                    }
                    $this->redirect(array('index'));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR'));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }
        $item = TypeField::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('id_type=:id', array('id' => $model->id));
        $this->render('update', array(
            'model' => $model, 'model2' => $model2,
            'items' => CHtml::listData(Field::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'label'),
            'item' => empty($item) ? array() : $item,
            'id' => $id
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('TypeDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Type'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex($id = 0) {
        $model = new Type();
        $model2 = new Type('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Type']))
            $model2->attributes = $_GET['Type'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Type'])) {
            try {
                $model->attributes = $_POST['Type'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'ProType'))));
                    if (!empty($_POST['items']) && isset($_POST['items'])) {
                        foreach ($_POST['items'] as $value) {
                            $_save = new TypeField();
                            $_save->id_field = $value;
                            $_save->id_type = $model->id;
                            if (!$_save->save()) {
                                Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'ProTypeField'))));
                            }
                        }
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'ProType'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('index', array(
            'model' => $model,
            'model2' => $model2,
            'items' => CHtml::listData(Field::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'label'),
            'item' => array(),
            'id' => $id
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Type('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Type']))
            $model->attributes = $_GET['Type'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Type the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Type::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Type $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'type-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
