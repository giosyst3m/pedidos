<?php

class ActivityController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('ActivityActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('ActivityActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('ActivityActionCreate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('ActivityActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('ActivityActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('ActivityActionDelete'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Activity;
        $model2 = new Activity('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Activity']))
            $model2->attributes = $_GET['Activity'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Activity'])) {
            try {
                $model->attributes = $_POST['Activity'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Activity'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Activity'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model, 'model2' => $model2
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model2 = new Activity('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Activity']))
            $model2->attributes = $_GET['Activity'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Activity'])) {
            try {
                $model->attributes = $_POST['Activity'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'Activity'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'Activity'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model, 'model2' => $model2
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('ActivityDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Activity'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex($id = 0, $idTodo = 0) {
        if (empty($id)) {
            $model = new Activity;
        } else {
            $model = $this->loadModel($id);
        }

        $model2 = new Activity('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Activity']))
            $model2->attributes = $_GET['Activity'];
        $CDbCriteria = new CDbCriteria();
        $CDbCriteria->order = 'id DESC';
        if (!Yii::app()->user->checkAccess('ActivityShowAllActivity')) {
            $model2->r_c_u = Yii::app()->user->id;
        }
        $model2->setDbCriteria($CDbCriteria);
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Activity'])) {
            try {
                $model->attributes = $_POST['Activity'];
                if ($model->save()) {
                    $this->saveImagen($model, $_FILES['file']);
                    $this->saveLog($model, 'project.activity');
                    if (empty($id)) {
                        $this->saveProjectActivity($model);
                        $subject = Yii::t('email', 'Email-2005', ['{numero}' => $model->id, '{sub_dominio}' => Yii::app()->user->getState('SYSTEM')->SYSTEM_SUB_DOMAIN, '{priority}' => $model->idPriority->name]);
                        $to = 'Soporte <' . trim(Yii::app()->params['emailSupport']) . '>';
                        $ok = Yii::t('email', 'Email-2000');
                        $error = Yii::t('email', 'Email-5004', array('{numero}' => $model->id));
                        $from = Yii::app()->user->nombre . '<' . Yii::app()->user->email . '>';
                        $this->layout = 'mail';
                        $msg = $this->render('../mail/activity', ['model' => $model], true);
                        $this->sendEmail($from, $to, $subject, $msg, $ok, $error);
                    }
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Activity'))));
                    $this->redirect(array('index', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Activity'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $Comment = new Comment;
        $Comment->id_activity = $id;

        if (isset($_POST['Comment'])) {
            try {
                $this->performAjaxValidation($Comment);
                $Comment->attributes = $_POST['Comment'];
                if ($Comment->save()) {
                    $this->saveImagen($Comment, $_FILES['file'], false);
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Activity'))));
                    $this->redirect(array('index', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Activity'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        if (empty($idTodo)) {
            $Todo = new Todo;
            $Todo->id_activity = $id;
        } else {
            $Todo = Todo::model()->findByPk($idTodo);
        }
        if (isset($_POST['Todo'])) {
            try {
                $this->performAjaxValidation($Todo);
                $Todo->attributes = $_POST['Todo'];
                if ($Todo->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Todo'))));
                    $this->redirect(array('index', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Todo'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $TodoAdmin = new CActiveDataProvider('Todo');
        $TodoAdmin->criteria->addColumnCondition(['id_activity' => $id, 'r_d_s' => 1])->order = 'done ASC, id_priority DESC';

        $Timer = new Timer;
        $Timer->id_activity = $id;
        $TimerAdmin = new Timer('search');
        $TimerAdmin->unsetAttributes();  // clear any default values
        $TimerAdmin->r_d_s = 1;
        $TimerAdmin->id_activity = $id;
        if (isset($_GET['Timer']))
            $TimerAdmin->attributes = $_GET['Timer'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($Timer);

        if (isset($_POST['Timer'])) {
            try {
                $Timer->attributes = $_POST['Timer'];
                if ($Timer->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Timer'))));
                    $this->redirect(array('index', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Timer'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }


        $this->render('index', array(
            'model' => $model,
            'model2' => $model2,
            'Comment' => $Comment,
            'Commentmodel2' => Comment::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('id_activity=' . $id . ' order by r_c_d DESC'),
            'Todo' => $Todo,
            'TodoAdmin' => $TodoAdmin,
            'Timer' => $Timer,
            'TimerAdmin' => $TimerAdmin,
            'Impact' => BsHtml::listData(Impact::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Type' => BsHtml::listData(Type::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Priority' => BsHtml::listData(Priority::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Resolution' => BsHtml::listData(Resolution::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Frequency' => BsHtml::listData(Frequency::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Status' => BsHtml::listData(Status::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'VSisUsuario' => BsHtml::listData(VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll(), 'id', 'complete_name'),
        ));
    }

    public function saveProjectActivity($data) {
        try {
            $model = new ProjectActivity;
            $Project = Project::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['sub_domain' => Yii::app()->user->getState('SYSTEM')->SYSTEM_SUB_DOMAIN]);
            $model->id_activity = $data->id;
            $model->id_project = $Project->id;
            $model->id_sprint = Sprint::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['id_project' => $Project->id, 'status' => 1])->id;
            $model->save();
        } catch (Exception $e) {
            Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
        }
    }

    /**
     * Save imagen products
     * @param type $model
     * @param type $files
     */
    public function saveImagen($model, $files, $act = true) {
        for ($index = 0; $index < count($files['name']); $index++) {
            if (!empty($files['name'][$index])) {
                if ($act) {
                    $proImg = new File();
                    $url = Yii::app()->user->getState('TICKET')->TICKET_ACTIVITY_PATH;
                    $proImg->id_activity = $model->id;
                } else {
                    $proImg = new ComFile();
                    $url = Yii::app()->user->getState('TICKET')->TICKET_COMMENT_PATH;
                    $proImg->id_comment = $model->id;
                }

                $proImg->extension = $files['type'][$index];
                $proImg->name = $files['name'][$index];
                $proImg->file = $this->generateRandomString(5) . '-' . strtolower(str_replace(' ', '-', $files['name'][$index]));
                move_uploaded_file($files['tmp_name'][$index], Yii::getPathOfAlias('webroot') . $url . $proImg->file);
                $proImg->save();
            }
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Activity('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Activity']))
            $model->attributes = $_GET['Activity'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Activity the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Activity::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Activity $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'activity-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'comment-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'todo-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'timer-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
