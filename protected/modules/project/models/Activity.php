<?php

/**
 * This is the model class for table "activity".
 *
 * The followings are the available columns in table 'activity':
 * @property integer $id
 * @property integer $father
 * @property string $name
 * @property string $description
 * @property string $estimate
 * @property integer $id_status
 * @property integer $id_impact
 * @property integer $id_type
 * @property integer $id_priority
 * @property integer $id_resolution
 * @property integer $id_frequency
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 *
 * The followings are the available model relations:
 * @property Status $idStatus
 * @property Impact $idImpact
 * @property Type $idType
 * @property Priority $idPriority
 * @property Activity $father0
 * @property Activity[] $activities
 * @property Resolution $idResolution
 * @property Frequency $idFrequency
 * @property ActivityLabel[] $activityLabels
 * @property Comment[] $comments
 * @property File[] $files
 * @property ProjectActivity[] $projectActivities
 * @property Timer[] $timers
 * @property Todo[] $todos
 */
class Activity extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'activity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, id_impact, id_type, id_priority, id_frequency', 'required'),
			array('father, id_status, id_impact, id_type, id_priority, id_resolution, id_frequency, r_c_u, r_u_u, r_d_u, r_d_s', 'numerical', 'integerOnly'=>true),
			array('name, estimate, r_c_i, r_u_i, r_d_i', 'length', 'max'=>255),
			array('r_c_d, r_u_d, r_d_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, father, name, description, estimate, id_status, id_impact, id_type, id_priority, id_resolution, id_frequency, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_d_u, r_d_d, r_d_i, r_d_s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idStatus' => array(self::BELONGS_TO, 'Status', 'id_status'),
			'idImpact' => array(self::BELONGS_TO, 'Impact', 'id_impact'),
			'idType' => array(self::BELONGS_TO, 'Type', 'id_type'),
			'idPriority' => array(self::BELONGS_TO, 'Priority', 'id_priority'),
			'father0' => array(self::BELONGS_TO, 'Activity', 'father'),
			'activities' => array(self::HAS_MANY, 'Activity', 'father'),
			'idResolution' => array(self::BELONGS_TO, 'Resolution', 'id_resolution'),
			'idFrequency' => array(self::BELONGS_TO, 'Frequency', 'id_frequency'),
			'activityLabels' => array(self::HAS_MANY, 'ActivityLabel', 'id_activity'),
			'comments' => array(self::HAS_MANY, 'Comment', 'id_activity'),
			'files' => array(self::HAS_MANY, 'File', 'id_activity'),
			'projectActivities' => array(self::HAS_MANY, 'ProjectActivity', 'id_activity'),
			'timers' => array(self::HAS_MANY, 'Timer', 'id_activity'),
			'todos' => array(self::HAS_MANY, 'Todo', 'id_activity'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'father' => Yii::t('app','Father'),
			'name' => Yii::t('app','Name'),
			'description' => Yii::t('app','Description'),
			'estimate' => Yii::t('app','Estimate'),
			'id_status' => Yii::t('app','Id Status'),
			'id_impact' => Yii::t('app','Id Impact'),
			'id_type' => Yii::t('app','Id Type'),
			'id_priority' => Yii::t('app','Id Priority'),
			'id_resolution' => Yii::t('app','Id Resolution'),
			'id_frequency' => Yii::t('app','Id Frequency'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('father',$this->father);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('estimate',$this->estimate,true);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('id_impact',$this->id_impact);
		$criteria->compare('id_type',$this->id_type);
		$criteria->compare('id_priority',$this->id_priority);
		$criteria->compare('id_resolution',$this->id_resolution);
		$criteria->compare('id_frequency',$this->id_frequency);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db4;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Activity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
