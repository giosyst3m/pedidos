<?php

/**
 * This is the model class for table "field".
 *
 * The followings are the available columns in table 'field':
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property string $value
 * @property integer $require
 * @property integer $id_fie_type
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 *
 * The followings are the available model relations:
 * @property FieType $idFieType
 * @property TypeField[] $typeFields
 */
class Field extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'field';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('label, require, id_fie_type', 'required'),
			array('require, id_fie_type, r_c_u, r_u_u, r_d_u, r_d_s', 'numerical', 'integerOnly'=>true),
			array('label, description, r_c_i, r_u_i, r_d_i', 'length', 'max'=>255),
			array('value, r_c_d, r_u_d, r_d_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, label, description, value, require, id_fie_type, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_d_u, r_d_d, r_d_i, r_d_s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idFieType' => array(self::BELONGS_TO, 'FieType', 'id_fie_type'),
			'typeFields' => array(self::HAS_MANY, 'TypeField', 'id_field'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'label' => Yii::t('app','Label'),
			'description' => Yii::t('app','Description'),
			'value' => Yii::t('app','Value'),
			'require' => Yii::t('app','Require'),
			'id_fie_type' => Yii::t('app','Id Fie Type'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('require',$this->require);
		$criteria->compare('id_fie_type',$this->id_fie_type);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db4;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Field the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
