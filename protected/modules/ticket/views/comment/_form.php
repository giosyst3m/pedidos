<?php
/* @var $this CommentController */
/* @var $model Comment */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'comment-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_INLINE,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textAreaControlGroup($model,'messages',array('rows'=>6,'style'=>'width:100%','placeholder'=>'Comentario')); ?>
    <?php echo $form->dropDownListControlGroup($model, 'id_priority', $Priority, array('data-style' => 'btn-primary', 'class' => 'tipo_prod selectpicker show-tick', 'empty' => 'Prioridad')); ?>
    <br>
    <?php echo $form->fileField($model,'file',['name'=>'file[]', 'multiple' => 'multiple']); ?>
    <br>
    <?php 
    if(Yii::app()->user->checkAccess('CommentCreateButtonSave') || Yii::app()->user->checkAccess('CommentUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('CommentCreateButtonNew') || Yii::app()->user->checkAccess('CommentUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Comment/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
