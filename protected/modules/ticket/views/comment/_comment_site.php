<li>
    <div class="block">
        <div class="block_content">
            <h2 class="title">
                <a><?php echo $data->activity; ?> </a>
            </h2>
            <div class="byline">
                <span><?php echo $data->r_c_d; ?></span> by <a><?php echo VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['id'=>  $data->r_c_u])->complete_name; ?></a>
            </div>
            <p class="excerpt"><?php echo $data->messages; ?></p>
            <a class="btn btn-<?php echo $data->color; ?> btn-xs" href="<?php echo $this->createAbsoluteUrl('ticket/activity/',['id'=>$data->id_activity]); ?>"  ><i class="<?php echo $data->icon; ?>"></i> Ver Ticket # <?php echo $data->id_activity; ?></a>
        </div>
    </div>
</li>