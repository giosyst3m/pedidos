<?php
/* @var $this CommentController */
/* @var $model Comment */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textAreaControlGroup($model,'messages',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'file',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'id_priority'); ?>
    <?php echo $form->textFieldControlGroup($model,'id_activity'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
