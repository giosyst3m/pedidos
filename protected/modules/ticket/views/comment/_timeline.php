<ul class="list-unstyled msg_list">
    <?php
    foreach ($model as $value) {
        $color = $value->idPriority->color;
        $VSisUsuario = VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['id' => $value->r_c_u]);
        ?>

        <li>
            <a>
                <span class="image">
                    <img src="<?php echo Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . $VSisUsuario->photo ?>" alt="img" />
                </span>
                <span>
                    <span><?php echo $VSisUsuario->complete_name; ?></span>
                    <span class="text-mobil time"><?php echo $value->r_c_d; ?></span>
                </span>
                <span class="message">
                    <?php echo BsHtml::tag('label',['class'=>'label label-'.$color],$value->idPriority->name); ?>
                    <?php echo $value->messages; ?>
                </span>
            </a>
        </li>
        <?php
        $files = ComFile::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes(['id_comment' => $value->id]);
        if ($files) {
            ?>
            <li>
                <div class="btn-group" role="group" aria-label="...">
                    <?php
                    foreach ($files as $file) {
                        echo $this->getLinktypeFile($file->extension, Yii::app()->user->getState('TICKET')->TICKET_COMMENT_PATH . $file->file, ['target' => '_blank', 'class' => 'btn ' . $value->idPriority->boton_lcolor, 'title' => $file->name]);
                    }
                    ?>
                </div>
            </li>
        <?php } ?>
    <?php } ?>
</ul>