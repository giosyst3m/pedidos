<?php
/* @var $this LabelController */
/* @var $model Label */
?>

<?php
$this->breadcrumbs=array(
	'Labels'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Label')) ?>

<?php 
if(Yii::app()->user->checkAccess('LabelCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('LabelCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>