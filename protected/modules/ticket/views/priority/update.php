<?php
/* @var $this PriorityController */
/* @var $model Priority */
?>

<?php
$this->breadcrumbs=array(
	'Priorities'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Priority').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('PriorityUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('PriorityUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>