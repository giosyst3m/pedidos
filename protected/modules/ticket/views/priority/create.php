<?php
/* @var $this PriorityController */
/* @var $model Priority */
?>

<?php
$this->breadcrumbs=array(
	'Priorities'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Priority')) ?>

<?php 
if(Yii::app()->user->checkAccess('PriorityCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('PriorityCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>