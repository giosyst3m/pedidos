<?php
/* @var $this TimerController */
/* @var $model Timer */
?>

<?php
$this->breadcrumbs=array(
	'Timers'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Timer').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('TimerUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('TimerUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>