<?php

$this->widget('bootstrap.widgets.BsGridView', array(
    'id' => 'timer-grid',
    'dataProvider' => $model2->search(),
    'filter' => $model2,
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        'quantity',
        'note',
        'r_c_d',
        [
            'class' => 'bootstrap.widgets.BsButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => Yii::t('app', 'Delete'),
                    'url' => 'Yii::app()->createUrl("project/Timer/delete", array("id"=>$data->id))',
                )
            )
        ],
    ),
));
?>
