<?php
/* @var $this TodoController */
/* @var $model Todo */
?>

<?php
$this->breadcrumbs=array(
	'Todos'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Todo').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('TodoUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('TodoUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>