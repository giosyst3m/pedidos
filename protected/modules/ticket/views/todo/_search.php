<?php
/* @var $this TodoController */
/* @var $model Todo */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'from'); ?>
    <?php echo $form->textFieldControlGroup($model,'to'); ?>
    <?php echo $form->textFieldControlGroup($model,'estimate',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'id_priority'); ?>
    <?php echo $form->textFieldControlGroup($model,'id_activity'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
