<?php
/* @var $this SprintController */
/* @var $model Sprint */
?>

<?php
$this->breadcrumbs=array(
	'Sprints'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Sprint').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('SprintUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('SprintUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>