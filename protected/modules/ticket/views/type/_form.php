<?php
/* @var $this TypeController */
/* @var $model Type */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'type-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'order'); ?>
    <?php echo $form->textFieldControlGroup($model,'color',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'r_c_u'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_c_d'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_c_i',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'r_u_u'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_u_d'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_u_i',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_u'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_d'); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_i',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'r_d_s'); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('TypeCreateStatusChange') || Yii::app()->user->checkAccess('TypeUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('TypeCreateButtonSave') || Yii::app()->user->checkAccess('TypeUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('TypeCreateButtonNew') || Yii::app()->user->checkAccess('TypeUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Type/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
