<?php
/* @var $this TypeController */
/* @var $model Type */
?>

<?php
$this->breadcrumbs=array(
	'Types'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Type')) ?>

<?php 
if(Yii::app()->user->checkAccess('TypeCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('TypeCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>