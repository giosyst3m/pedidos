<?php
/* @var $this ProjectController */
/* @var $model Project */
?>

<?php
$this->breadcrumbs=array(
	'Projects'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Project')) ?>

<?php 
if(Yii::app()->user->checkAccess('ProjectCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProjectCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>