<?php
/* @var $this ImpactController */
/* @var $model Impact */
?>

<?php
$this->breadcrumbs=array(
	'Impacts'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Impact')) ?>

<?php 
if(Yii::app()->user->checkAccess('ImpactCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ImpactCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>