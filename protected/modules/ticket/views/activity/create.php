<?php
/* @var $this ActivityController */
/* @var $model Activity */
?>

<?php
$this->breadcrumbs=array(
	'Activities'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Activity')) ?>

<?php 
if(Yii::app()->user->checkAccess('ActivityCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ActivityCreateAdminView')){
//    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>