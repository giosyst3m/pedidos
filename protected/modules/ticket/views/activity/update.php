<?php
/* @var $this ActivityController */
/* @var $model Activity */
?>

<?php
$this->breadcrumbs=array(
	'Activities'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Activity').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ActivityUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ActivityUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>