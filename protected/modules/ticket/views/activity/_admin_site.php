<div class="panel panel-default">
    <div class="panel-body">
        <?php
        $this->widget('bootstrap.widgets.BsGridView', array(
            'id' => 'activity-grid',
            'dataProvider' => $model2->search(),
            'filter' => $model2,
            'type' => BsHtml::GRID_TYPE_RESPONSIVE,
            'columns' => array(
                [
                  'name'=>'id',
                    'type'=>'raw',
                    'value'=>function($data){
                        return BsHtml::link($data->id,Yii::app()->createUrl("ticket/Activity/index", array("id"=>$data->id)),['class'=>'btn btn-primary','icon'=>  BsHtml::GLYPHICON_EDIT]);
                    }
                ],
                [
                    'name'=>'name',
                         'htmlOptions' => [
                        'class' => 'text-mobil',
                    ]
                ],
                [
                    'header' => 'Estado',
                    'type' => 'raw',
                    'name' => 'id_status',
                    'value' => function($data) {
                        return BsHtml::tag('span', ['class' => $data->status_color], $data->status).'<br>'.
                            $data->type.'<br>'.
                                $data->frequency.'<br>'.
                                $data->resolution;
                    },
                            'htmlOptions' => [
                        'class' => 'text-mobil',
                    ]
                ],
                [
                    'header' => 'Prioridad',
                    'type' => 'raw',
                    'name' => 'id_priority',
                    'value' => function($data) {
                        return BsHtml::tag('span', ['class' => $data->priority_boton_lcolor], BsHtml::tag('span', ['class' => $data->priority_icon]) . ' ' . $data->priority);
                    },
                    'filterHtmlOptions' => [
                        'class' => 'hidden-xs hidden-sm',
                    ],
                    'headerHtmlOptions' => [
                        'class' => 'hidden-xs hidden-sm',
                    ],
                    'htmlOptions' => [
                        'class' => 'hidden-xs hidden-sm',
                    ]
                ],
                [
                    'header' => 'Prioridad',
                    'type' => 'raw',
                    'name' => 'id_priority',
                    'value' => function($data) {
                        $VSisUsuario = VSisUsuario::model()->findByAttributes(['id' => $data->r_c_u]);
                        return BsHtml::tag('span', ['class' => $data->priority_boton_lcolor], BsHtml::tag('span', ['class' => $data->priority_icon]) . ' ' . $data->priority).'<div class="row"><div class="col-lg-6 col-sm-12 col-,d-12 col-xs-12">'. BsHtml::image(Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . $VSisUsuario->photo,'',['class'=>'img-circle','style'=>'width:50%']).'</div><div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> '.$VSisUsuario->complete_name;
                    },
                    'htmlOptions' => [
                        'class' => 'text-mobil  hidden-lg  hidden-md',
                    ],
                    'filterHtmlOptions' => [
                        'class' => 'hidden-lg hidden-md',
                    ],
                    'headerHtmlOptions' => [
                        'class' => 'hidden-lg hidden-md',
                    ],
                    'htmlOptions' => [
                        'class' => 'hidden-lg text-mobil hidden-md',
                    ],
                ],
                [
                    'header' => 'Usuario',
                    'type' => 'raw',
                    'name' => 'r_c_u',
                    'value' => function($data) {
                        $VSisUsuario = VSisUsuario::model()->findByAttributes(['id' => $data->r_c_u]);
                        return '<div class="row"><div class="col-lg-6 col-sm-12 col-xs-12">'. BsHtml::imageCircle(Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . $VSisUsuario->photo,'',['class'=>'img-circle profile_img']).'</div><div class="col-lg-6 col-sm-12 col-xs-12"> '.$VSisUsuario->complete_name;
                    },
                    'filterHtmlOptions' => [
                        'class' => 'hidden-xs hidden-sm',
                    ],
                    'headerHtmlOptions' => [
                        'class' => 'hidden-xs hidden-sm',
                    ],
                    'htmlOptions' => [
                        'class' => 'hidden-xs hidden-sm',
                    ]
                ],
                [
                    'class' => 'bootstrap.widgets.BsButtonColumn',
                    'template' => '{update}',
                    'buttons' => array(
                        'update' => array(
                            'label' => Yii::t('app', 'Update'),
                            'url' => 'Yii::app()->createUrl("ticket/Activity/index", array("id"=>$data->id))',
                        'options' => [
                                        'class' => 'hidden-xs hidden-sm hidden-md',
                                    ]),
                        
                    )
                ],
                    ),
                ));
                ?>
    </div>
</div>



