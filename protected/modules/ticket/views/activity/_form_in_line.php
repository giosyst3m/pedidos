<?php
/* @var $this ActivityController */
/* @var $model Activity */
/* @var $form BSActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'activity-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => BsHtml::FORM_LAYOUT_INLINE,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
?>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->textFieldControlGroup($model, 'name', array('maxlength' => 255, 'style' => "width:100%; !important",'placeholder'=>'Titulo del Error','help'=>'Ej: Error al crear un producto')); ?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
        <?php echo $form->dropDownListControlGroup($model, 'id_impact', $Impact, array('data-style' => 'btn-primary', 'class' => 'tipo_prod selectpicker show-tick', 'empty' => 'Impacto')); ?>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
        <?php echo $form->dropDownListControlGroup($model, 'id_type', $Type, array('data-style' => 'btn-primary', 'class' => 'tipo_prod selectpicker show-tick', 'empty' => 'Tipo')); ?>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <?php echo $form->dropDownListControlGroup($model, 'id_priority', $Priority, array('data-style' => 'btn-primary', 'class' => 'tipo_prod selectpicker show-tick', 'empty' => 'Prioridad')); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <?php echo $form->dropDownListControlGroup($model, 'id_frequency', $Frequency, array('data-style' => 'btn-primary', 'class' => 'tipo_prod selectpicker show-tick', 'empty' => 'Frequencia')); ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <?php
        if (Yii::app()->user->checkAccess('ActivityShowResolution')) {
            echo $form->dropDownListControlGroup($model, 'id_resolution', $Resolution, array('data-style' => 'btn-info', 'class' => 'tipo_prod selectpicker show-tick', 'empty' => 'Resolución'));
        } else {
            echo '<br>Resolucion: ';
            echo BsHtml::tag('span', ['class' => $model->idStatus->color], $model->idResolution->name); 
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <?php
        if (Yii::app()->user->checkAccess('ActivityShowStatus')) {
            echo $form->dropDownListControlGroup($model, 'id_status', $Status, array('data-style' => 'btn-info', 'class' => 'tipo_prod selectpicker show-tick', 'empty' => 'Estado'));
        } else {
            echo '<br>Estatus: ';
            echo BsHtml::tag('span', ['class' => $model->idStatus->color], $model->idStatus->name);
        }
        ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <?php
        if (Yii::app()->user->checkAccess('ActivityShowUser')) {
            echo $form->dropDownListControlGroup($model, 'r_c_u', $VSisUsuario, array('data-style' => 'btn-info', 'class' => 'tipo_prod selectpicker show-tick', 'empty' => Yii::t('app','Who Report?'), 'data-live-search' => true));
        } else {
            echo '<br>'.Yii::t('app','Who Report?').' ';
            echo BsHtml::tag('span', ['class' => $model->idStatus->color], VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['id'=>empty($model->r_c_u)?Yii::app()->user->id:$model->r_c_u])->complete_name);
        }
        ?>
    </div>
</div>



<?php echo $form->textAreaControlGroup($model, 'description', array('rows' => 10, 'style' => 'width:100%', 'placeholder' => 'Describir detalladamente, paso a paso, recuerde subir una imagen con el error','help'=>'Describir detalladamente, paso a paso, recurde subir una imagen que pueda ayudar')); ?>
<br>
<?php echo $form->fileField($model, 'file', ['name' => 'file[]', 'multiple' => 'multiple']); ?>
<br>
<div class="btn-group" role="group" aria-label="...">
    <?php
    foreach (File::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes(['id_activity' => $model->id]) as $file) {
        echo $this->getLinktypeFile($file->extension, Yii::app()->user->getState('TICKET')->TICKET_ACTIVITY_PATH . $file->file, ['target' => '_blank', 'class' => 'btn ' . $model->idStatus->boton_lcolor, 'title' => $file->name]);
    }
    ?>
</div>
<br>
<br>
<?php
if (Yii::app()->user->checkAccess('ActivityCreateButtonSave') || Yii::app()->user->checkAccess('ActivityUpdateButtonSave')) {
    echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY));
}
?>    
<?php
if (Yii::app()->user->checkAccess('ActivityCreateButtonNew') || Yii::app()->user->checkAccess('ActivityUpdateButtonNew')) {
    echo BsHtml::link(Yii::t('app', 'New'), Yii::app()->createAbsoluteUrl('ticket/Activity/index'), array('class' => 'btn btn-primary'));
}
?>  
<?php $this->endWidget(); ?>
