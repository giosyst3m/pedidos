<?php
/* @var $this ActivityController */
/* @var $model Activity */


$this->breadcrumbs = array(
    'Activities' => array('index'),
    'Manage',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-list', 'label' => 'List Activity', 'url' => array('index')),
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create Activity', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#activity-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button(Yii::t('app', 'Advanced search'), array('class' => 'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH, 'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            <?php echo Yii::t('app', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
                &lt;&gt;</b>
            or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.'); ?>        </p>

        <div class="search-form" style="display:none">
            <?php
            $this->renderPartial('_search', array(
                'model' => $model2,
            ));
            ?>
        </div>
        <!-- search-form -->

        <?php
        $this->widget('bootstrap.widgets.BsGridView', array(
            'id' => 'activity-grid',
            'dataProvider' => $model2->search(),
            'filter' => $model2,
            'type' => BsHtml::GRID_TYPE_RESPONSIVE,
            'columns' => array(
                [
                    'header' => 'Prioridad',
                    'type' => 'raw',
                    'name' => 'id_priority',
                    'filter' => $Priority,
                    'value' => function($data) {
                        return BsHtml::link($data->id, Yii::app()->createUrl("ticket/Activity/index", array("id" => $data->id)), ['class' => 'btn btn-primary']) . '</b><br> ' . $data->name;
                    },
                    'filterHtmlOptions' => [
                        'class' => '',
                    ],
                    'headerHtmlOptions' => [
                        'class' => 'text-mobil',
                    ],
                    'htmlOptions' => [
                        'class' => 'text-mobil',
                    ],
                        ],
                        [
                            'header' => 'Tipo',
                            'type' => 'raw',
                            'name' => 'id_type',
                            'filter' => $Type,
                            'value' => function($data) {
                                return BsHtml::tag('span', ['class' => $data->idType->color], $data->idType->name);
                            },
                                    'filterHtmlOptions' => [
                                        'class' => 'hidden-xs hidden-sm hidden-md',
                                    ],
                                    'headerHtmlOptions' => [
                                        'class' => 'hidden-xs hidden-sm hidden-md text-mobil',
                                    ],
                                    'htmlOptions' => [
                                        'class' => 'hidden-xs hidden-sm hidden-md text-mobil',
                                        'style' => 'width:10%;'
                                    ],
                                ],
                                [
                                    'header' => 'Estado',
                                    'type' => 'raw',
                                    'name' => 'id_status',
                                    'filter' => $Status,
                                    'value' => function($data) {
                                        return BsHtml::tag('span', ['class' => $data->idStatus->color], $data->idStatus->name) . '<br>' .
                                                BsHtml::tag('span', ['class' => $data->idType->color . ' hidden-lg  hidden-sm'], $data->idType->name);
                                    },
                                    'filterHtmlOptions' => [
                                        'class' => '',
                                    ],
                                    'headerHtmlOptions' => [
                                        'class' => 'text-mobil',
                                    ],
                                    'htmlOptions' => [
                                        'class' => 'text-mobil',
                                    ],
                                        ],
                                        [
                                            'header' => 'Prioridad',
                                            'type' => 'raw',
                                            'name' => 'id_priority',
                                            'filter' => $Priority,
                                            'value' => function($data) {
                                                return BsHtml::tag('span', ['class' => $data->idPriority->boton_lcolor], BsHtml::tag('span', ['class' => $data->idPriority->icon]) . ' ' .
                                                                $data->idPriority->name) . '<br>' .
                                                        BsHtml::tag('span', ['class' => $data->idFrequency->color], $data->idFrequency->name) . '<br>' .
                                                        BsHtml::tag('span', ['class' => $data->idResolution->color . ' hidden-lg hidden-md'], $data->idResolution->name);
                                            },
                                                    'filterHtmlOptions' => [
                                                        'class' => '',
                                                    ],
                                                    'headerHtmlOptions' => [
                                                        'class' => 'text-mobil',
                                                    ],
                                                    'htmlOptions' => [
                                                        'class' => 'text-mobil',
                                                    ],
                                                ],
                                                [
                                                    'header' => 'Resolución',
                                                    'type' => 'raw',
                                                    'name' => 'id_resolution',
                                                    'filter' => $Resolution,
                                                    'value' => function($data) {
                                                        return BsHtml::tag('span', ['class' => $data->idResolution->color], $data->idResolution->name);
                                                    },
                                                            'filterHtmlOptions' => [
                                                                'class' => 'hidden-xs hidden-md',
                                                            ],
                                                            'headerHtmlOptions' => [
                                                                'class' => 'hidden-xs hidden-md text-mobil',
                                                            ],
                                                            'htmlOptions' => [
                                                                'class' => 'hidden-xs hidden-md text-mobil',
                                                                'style' => 'width:10%;'
                                                            ],
                                                        ],
                                                        [
                                                            'header' => 'Usuario',
                                                            'type' => 'raw',
                                                            'name' => 'r_c_u',
                                                            'filter' => $VSisUsuario,
                                                            'value' => function($data) {
                                                                $VSisUsuario = VSisUsuario::model()->findByAttributes(['id' => $data->r_c_u]);
                                                                return BsHtml::image(Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . $VSisUsuario->photo, '', ['class' => 'img-circle','style'=>'width: 25%']) . $VSisUsuario->complete_name;
                                                            },
                                                            'filterHtmlOptions' => [
                                                                'class' => '',
                                                            ],
                                                            'headerHtmlOptions' => [
                                                                'class' => ' text-mobil',
                                                            ],
                                                            'htmlOptions' => [
                                                                'class' => ' text-mobil',
                                                                'style' => 'width:10%;'
                                                            ],
                                                                    
                                                                ],
                                                                [
                                                                    'class' => 'bootstrap.widgets.BsButtonColumn',
                                                                    'template' => '{update}',
                                                                    'buttons' => array(
                                                                        'update' => array(
                                                                            'label' => Yii::t('app', 'Update'),
                                                                            'url' => 'Yii::app()->createUrl("ticket/Activity/index", array("id"=>$data->id))',
                                                                            'options'=>['class'=> 'hidden-xs']
                                                                        )
                                                                    )
                                                                ],
                                                            ),
                                                        ));
                                                        ?>
    </div>
</div>




