<?php
/* @var $this ActivityController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
    'Activities',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create Activity', 'url' => array('create')),
    array('icon' => 'glyphicon glyphicon-tasks', 'label' => 'Manage Activity', 'url' => array('admin')),
);
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/project/activity/_list.js', CClientScript::POS_HEAD); ?>
<style>
    .form-group{
        display: block !important;
        margin: 5px;
    }
</style>
<div class="row">
    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-ticket"></i>&nbsp;<?php echo Yii::t('app', 'Ticket') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $this->renderPartial('_form_in_line', [
                    'model' => $model,
                    'Impact' => $Impact,
                    'Type' => $Type,
                    'Priority' => $Priority,
                    'Frequency' => $Frequency,
                    'Resolution' => $Resolution,
                    'Status' => $Status,
                    'VSisUsuario' => $VSisUsuario,
                ])
                ?>
            </div>
        </div>
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-ticket"></i>&nbsp;<?php echo Yii::t('app', 'Tickets') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $this->renderPartial('admin', [
                    'model2' => $model2,
                    'Impact' => $Impact,
                    'Type' => $Type,
                    'Priority' => $Priority,
                    'Frequency' => $Frequency,
                    'Resolution' => $Resolution,
                    'Status' => $Status,
                    'VSisUsuario' => $VSisUsuario,
                ])
                ?>

            </div>
        </div>
    </div>

    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
        <?php if (Yii::app()->user->checkAccess('ActivityIndexShowTodo')) { ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i>&nbsp;<?php echo Yii::t('app', 'Todo') ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <?php
                    $this->renderPartial('../todo/_form', [
                        'model' => $Todo,
                        'Priority' => $Priority,
                    ]);?>
                    <ul class="list-group">
                    <?php    
                    $this->widget('bootstrap.widgets.BsListView', array(
                        'dataProvider' => $TodoAdmin,
                        'itemView' => '../todo/_list',
                    ));
                    ?>
                        </ul>
                </div>
            </div>
        <?php } ?>


        <?php if (Yii::app()->user->checkAccess('ActivityIndexShowTimer')) { ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-clock-o"></i>&nbsp;<?php echo Yii::t('app', 'Timer') ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <?php
                    $this->renderPartial('../timer/_form', [
                        'model' => $Timer,
                    ]);
                    $this->renderPartial('../timer/admin', [
                        'model2' => $TimerAdmin,
                    ]);
                    ?>
                </div>
            </div>
        <?php } ?>
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-comment"></i>&nbsp;<?php echo Yii::t('app', 'Comment') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                $this->renderPartial('../comment/_form', [
                    'model' => $Comment,
                    'Priority' => $Priority,
                ]);
                $this->renderPartial('../comment/_timeline', [
                    'model' => $Commentmodel2,
                    'Priority' => $Priority,
                ]);
                ?>
            </div>
        </div>
    </div>
</div>