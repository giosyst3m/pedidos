<?php

class DefaultController extends Controller {

    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('AccDocSisUsu');
        
        $dataProvider->countCriteria->addInCondition('id_sis_usuario', [Yii::app()->getUser()->id]);
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

}
