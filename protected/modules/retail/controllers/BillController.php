<?php

class BillController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('BillActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('BillActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('BillActionCreate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('BillActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('BillActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('BillActionDelete'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Registro de Facturas
     * @param int $id ID de la cuenta que se va registrar
     */
    public function actionCreate($id) {
        $Order = new Order;
        $Order->id_status = 12; //Factura Registrada
        $Order->id_company = Yii::app()->user->getState('id_company');
        $Order->id_document = 2; // Facutras de ventas
        $Order->id_account = $id;
        $Order->id_discount = 1; // 0%

        $OrdDetail = new OrdDetail();
        $OrdDetail->id_inv_origin = 15; // formulario de Bills
        $OrdDetail->id_discount = 1; // 0%

        $model2 = new Order('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Order']))
            $model2->attributes = $_GET['Order'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation([$Order, $OrdDetail]);
        if (isset($_POST['Order'], $_POST['OrdDetail'])) {
            try {
                // populate input data to $a and $b
                $Order->attributes = $_POST['Order'];
                if (empty($Order->id_client)) {
                    $Order->id_client = 1;
                }
                $OrdDetail->attributes = $_POST['OrdDetail'];
                $OrdDetail->id_order = 1;
                $OrdDetail->price = ProductRate::model()->findByAttributes(['id_product' => $OrdDetail->id_product, 'id_rate'=>1])->price;// Precio de venta
                if ($OrdDetail->validate()) {
                    $Order->number = $this->getNumber(3);
                }
                // validate BOTH $a and $b
                $valid = $Order->validate();
                $valid = $OrdDetail->validate() && $valid;

                if ($valid) {
                    // use false parameter to disable validation
                    if ($Order->save(false)) {
                        $OrdDetail->id_order = $Order->id;
                        $OrdDetail->save(false);
                    }
                }
                Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Order'))));
                $this->redirect(array('update', 'id' => $Order->id));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $Order,
            'model1' => $OrdDetail,
            'model2' => $model2,
            'Document' => Document::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findByPk(2), // Facturas de ventas
            'Discount' => BsHtml::listData(Discount::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAllByAttributes(['r_d_s' => 1]), 'id', 'name'),
            'Client' => BsHtml::listData(VClient::model()->findAllByAttributes(['r_d_s' => 1, 'id_cli_type' => 1]), 'id', 'full_name'), // clientes
            'Product' => BsHtml::listData(Product::model()->findAllByAttributes(['r_d_s' => 1]), 'id', 'name'),
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $Order = $this->loadModel($id);
        
        $OrdDetail = new OrdDetail();
        $OrdDetail->id_inv_origin = 15; // formulario de Bills
        $OrdDetail->id_discount = 1; // 0%
        $OrdDetail->id_order = $Order->id;
        
        $model2 = new OrdDetail('search');
        $model2->unsetAttributes();  // clear any default values
        $model2->id_order = $Order->id;
        if (isset($_GET['Order']))
            $model2->attributes = $_GET['Order'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation([$Order, $OrdDetail]);
        
        if (isset($_POST['Order'], $_POST['OrdDetail'])) {
            try {
                // populate input data to $a and $b
                $Order->attributes = $_POST['Order'];
                if (empty($Order->id_client)) {
                    $Order->id_client = 1;
                }
                $OrdDetail->attributes = $_POST['OrdDetail'];
                $OrdDetail->price = ProductRate::model()->findByAttributes(['id_product' => $OrdDetail->id_product, 'id_rate'=>1, 'id_money' => $OrdDetail->id_money])->price;// Precio de venta
                
                // validate BOTH $a and $b
                $valid = $Order->validate();
                $valid = $OrdDetail->validate() && $valid;

                if ($valid) {
                    // use false parameter to disable validation
                    if ($Order->save(false)) {
                        $OrdDetail->save(false);
                    }
                }
                Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Order'))));
                
                $this->redirect(array('update', 'id' => $Order->id));
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 5004));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $this->render('update', array(
            'model' => $Order, 
            'model1' => $OrdDetail,
            'model2' => $model2,
            'Document' => Document::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findByPk(2), // Facturas de ventas
            'Discount' => BsHtml::listData(Discount::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAllByAttributes(['r_d_s' => 1]), 'id', 'name'),
            'Product' => BsHtml::listData(Product::model()->findAllByAttributes(['r_d_s' => 1]), 'id', 'name'),
            'Money' => BsHtml::listData(Money::model()->findAllByAttributes(['r_d_s' => 1]), 'id', 'acronym'),
            'Client' => BsHtml::listData(VClient::model()->findAllByAttributes(['r_d_s' => 1, 'id_cli_type' => 1]), 'id', 'full_name'), // clientes
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('BillDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Order'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Order');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Order('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Order']))
            $model->attributes = $_GET['Order'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Order the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Order::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Order $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'order-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'order-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
