<?php
/* @var $this BillController */
/* @var $model Order */
?>

<?php
$this->breadcrumbs = array(
    'Orders' => array('create'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', $Document->name); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <label>Numero #:</label>&nbsp;<?php echo $model->number;?>
        <br />
        <?php
        if (Yii::app()->user->checkAccess('BillUpdateFormView')) {
            $this->renderPartial('_form_order', array(
                'model' => $model,
                'model1' => $model1,
                'Discount' => $Discount,
                'Product' => $Product,
                'Money' => $Money,
                'Client' => $Client,
            ));
        }
        ?>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('BillUpdateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2));
}
?>