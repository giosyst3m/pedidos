<?php

/* @var $this BillController */
/* @var $model Order */
?>

<?php

$this->breadcrumbs = array(
    'Orders' => array('index'),
    $model->id,
);
?>

<?php echo BsHtml::pageHeader('View', 'Bill ' . $model->id) ?>
<?php

$reg = array();
if (Yii::app()->user->checkAccess('BillViewAuthView')) {
    $reg = array(
        'id',
        'id_ralate',
        'number',
        'id_client',
        'id_status',
        'id_discount',
        'id_company',
        'id_document',
        'id_account',
        'note',
        'r_c_u',
        'r_c_d',
        'r_c_i',
        'r_u_u',
        'r_u_d',
        'r_u_i',
        'r_d_u',
        'r_d_d',
        'r_d_i',
        'r_d_s',
        'r_d_r',
        array(
            'name' => 'r_d_s',
            'type' => 'raw',
            'value' => $model->RegistroEstado($model->r_d_s)
        ),
    );
} else {
    $reg = array(
        'id',
        'id_ralate',
        'number',
        'id_client',
        'id_status',
        'id_discount',
        'id_company',
        'id_document',
        'id_account',
        array(
            'name' => 'r_d_s',
            'type' => 'raw',
            'value' => $model->RegistroEstado($model->r_d_s)
        ),
    );
}
?><?php

$this->widget('zii.widgets.CDetailView', array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data' => $model,
    'attributes' => $reg
        ,
));
?>

<?php

echo BsHtml::button(Yii::t('app', 'Back'), array(
    'name' => 'btnBack',
    'class' => 'uibutton loading confirm',
    'color' => BsHtml::BUTTON_COLOR_PRIMARY,
    'onclick' => 'history.go(-1)',
        )
);
echo BsHtml::link(Yii::t('app', 'New'), Yii::app()->createAbsoluteUrl('Order/create'), array('class' => 'btn btn-primary'));
?>