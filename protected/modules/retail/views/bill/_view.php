<?php
/* @var $this BillController */
/* @var $data Order */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_ralate')); ?>:</b>
    <?php echo CHtml::encode($data->id_ralate); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('number')); ?>:</b>
    <?php echo CHtml::encode($data->number); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_client')); ?>:</b>
    <?php echo CHtml::encode($data->id_client); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_status')); ?>:</b>
    <?php echo CHtml::encode($data->id_status); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_discount')); ?>:</b>
    <?php echo CHtml::encode($data->id_discount); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_company')); ?>:</b>
    <?php echo CHtml::encode($data->id_company); ?>
    <br />

    <?php if (Yii::app()->user->checkAccess('BillViewAuthView')) { ?>
        <b><?php echo CHtml::encode($data->getAttributeLabel('id_document')); ?>:</b>
        <?php echo CHtml::encode($data->id_document); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('id_account')); ?>:</b>
        <?php echo CHtml::encode($data->id_account); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('note')); ?>:</b>
        <?php echo CHtml::encode($data->note); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_c_u')); ?>:</b>
        <?php echo CHtml::encode($data->r_c_u); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_c_d')); ?>:</b>
        <?php echo CHtml::encode($data->r_c_d); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_c_i')); ?>:</b>
        <?php echo CHtml::encode($data->r_c_i); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_u_u')); ?>:</b>
        <?php echo CHtml::encode($data->r_u_u); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_u_d')); ?>:</b>
        <?php echo CHtml::encode($data->r_u_d); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_u_i')); ?>:</b>
        <?php echo CHtml::encode($data->r_u_i); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_d_u')); ?>:</b>
        <?php echo CHtml::encode($data->r_d_u); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_d_d')); ?>:</b>
        <?php echo CHtml::encode($data->r_d_d); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_d_i')); ?>:</b>
        <?php echo CHtml::encode($data->r_d_i); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
        <?php echo CHtml::encode($data->r_d_s); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('r_d_r')); ?>:</b>
        <?php echo CHtml::encode($data->r_d_r); ?>
        <br />

    <?php } ?>

</div>