

<?php

$this->widget('bootstrap.widgets.BsGridView', array(
    'id' => 'order-grid',
    'dataProvider' => $model2->search(),
    'filter' => $model2,
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        [
            'name' => 'id',
            'value' => function($data) {
                return $data->idProduct->name;
            }
        ],
        [
            'name'  =>  'quantity',
            'value' =>  function($data){
                return number_format($data->quantity,  Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS);
            },
            'htmlOptions' => [
                'class' =>  'text-right'
            ],
        ],
        [
            'name'  =>  'price',
            'value' =>  function($data){
                return number_format($data->price,  Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS);
            },
            'htmlOptions' => [
                'class' =>  'text-right'
            ],
        ],
        [
            'name'  =>  'price',
            'value' =>  function($data){
                return number_format($data->price * $data->quantity,  Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS);
            },
            'htmlOptions' => [
                'class' =>  'text-right'
            ],
        ],
        array(
            'class' => 'bootstrap.widgets.BsButtonColumn',
        ),
    ),
));
?>




