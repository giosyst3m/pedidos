<?php
/* @var $this BillController */
/* @var $model Order */
/* @var $form BSActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'order-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => Yii::app()->mobileDetect->isMobile() ? BsHtml::FORM_LAYOUT_INLINE : BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    )
        ));
?>

<p class="help-block"> <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required.') ?></p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->dropDownListControlGroup($model, 'id_client', $Client, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' =>'Clientes', 'data-live-search' => true,)); ?>
<?php
if (Yii::app()->user->checkAccess('BillApplyDiscount')) {
    echo $form->dropDownListControlGroup($model, 'id_discount', $Discount, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Descuentos', 'data-live-search' => true,));
}
?>


<?php echo $form->dropDownListControlGroup($model1, 'id_product', $Product, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Productos', 'data-live-search' => true,)); ?>

<?php echo $form->numberFieldControlGroup($model1, 'quantity', ['class' => 'text-right', 'min' => 0]); ?>

<?php
if (Yii::app()->user->checkAccess('BillApplyDiscount')) {
    echo $form->dropDownListControlGroup($model1, 'id_discount', $Discount, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Descuentos', 'data-live-search' => true,));
}
?>
<?php echo $form->textAreaControlGroup($model1, 'note', ['rows' => 2, 'placehorder'=> 'Notas']) ?>
<?php
if (Yii::app()->user->checkAccess('BillCreateButtonSave') || Yii::app()->user->checkAccess('BillUpdateButtonSave')) {
    echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY));
}
?>      
<?php
if (Yii::app()->user->checkAccess('BillCreateButtonNew') || Yii::app()->user->checkAccess('BillUpdateButtonNew')) {
    echo BsHtml::link(Yii::t('app', 'New'), Yii::app()->createAbsoluteUrl('Order/create'), array('class' => 'btn btn-primary'));
}
?>  
<?php $this->endWidget(); ?>
