<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        $SisUsuario = SisUsuario::model()->find('LOWER(username)=:username AND acceso=1', array(':username' => $this->username));

        if (!$SisUsuario) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif ($SisUsuario->clave !== sha1($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $SisUsuario->id;
            $this->setState('email', $SisUsuario->email);
            $this->setState('username', $SisUsuario->username);
            $this->setState('nombre', $SisUsuario->nombre . ' ' . $SisUsuario->apellido);
            $this->setState('perfil', $SisUsuario->id_auth_item);
            $this->setState('id_client', $SisUsuario->id_client);
            $this->setState('foto', $SisUsuario->foto);
            $this->errorCode = self::ERROR_NONE;
            foreach (SisConGroup::model()->findAll('r_d_s=1') as $value) {
                $this->setState($value->name, (object) CHtml::listData(SisConfig::model()->findAllByAttributes(array('r_d_s' => 1, 'id_sis_con_group' => $value->id)), 'name', 'default'));
            }
        }
        return !$this->errorCode;
    }

    function getId() {
        return $this->_id;
    }

}
