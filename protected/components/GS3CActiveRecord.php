<?php

/**
 * Extender Active Record para los Modelos
 */
class GS3CActiveRecord extends CActiveRecord {

    /**
     * Guarda información en los campos de Registro Auth
     * Tomado de Videos, se relaizaron adaptaciones del sistema, manetneindo la esencia
     * @author Gustavo Salgado
     * @link https://www.youtube.com/watch?v=3jwHGe0jEnE&index=34&list=PLfF1XiMdn-eD5g56zjmx4KhCGlr1pIRDr Información como usarlo
     */
    public function behaviors() {
        return array(
            'GS3Behavior' => array(
                'class' => 'application.components.GS3Behavior',
            ),
        );
    }

    /**
     * Buscar el Padre de la Categoria
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-06-17
     * @version 1.0.0
     * @param int $padre
     * @return string
     */
    public function Categoria($padre = 0) {
        //SI es padre
        if ($padre == 0) {
            //Retorna dos guionescuando no tiene padre
            return "--";
        } else {
            //Retorna el nombre del padre segun el ID recibido
            return ProCategoria::model()->find('id=:padre', array(':padre' => $padre))->nombre;
        }
    }

    /**
     * Retorna el EStado del Registro
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-06-17
     * @version 1.0.0
     * @param int $id
     * @return string
     */
    public function RegistroEstado($id = 0) {
        //Si es el id es 0
        if ($id == 0) {
            //Retornar la traducción del registro Inactivo
            return Yii::t('app', 'Inactive');
        } else if ($id == 1) {
            //Retorna la traducción del Registro Activo
            return Yii::t('app', 'Active');
        }
    }

    /**
     * Obtener el Tipo de entrada o salida
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-06-22
     * @version 1.0.0
     * @param int $id
     * @return string
     */
    public function getInventarioTipo($id) {
        //Si es el id es 0
        if ($id == 2) {
            //Retornar la traducción del registro Inactivo
            return Yii::t('app', 'OUT');
        } else if ($id == 1) {
            //Retorna la traducción del Registro Activo
            return Yii::t('app', 'IN');
        }
    }

    /**
     * Despleiga la imagenes
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-06-22
     * @version 1.0.0
     * @param string $nombre nombre del archivo
     * @param string $url Ruta donde se almaceno
     * @param int $ancho Ancho para la imagen predeterminado 100px
     * @param int $alto Alto para la imagen predeterminado 100px
     * @param boolean $tumbenails Si desea que este en Tumbenails
     * @return html <img> imagen
     */
    public function ShowImagen($nombre = "", $url = "", $ancho = 100, $alto = 100, $tumbenails = true) {
        if (!empty($nombre)) {
            if ($tumbenails) {
                return BsHtml::link(BsHtml::imageThumbnail(Yii::app()->getBaseUrl(FALSE) . $url . $nombre, $nombre,["style" => "width:".$ancho."px;height:".$alto."px;"]), Yii::app()->getBaseUrl(FALSE) . $url . $nombre, array('class'=>'fancybox','rel'=>'gallery1'));
            } else {
                return BsHtml::image(Yii::app()->getBaseUrl(FALSE) . Yii::app()->params[$url] . $nombre, $nombre, array("style" => "width:100px;height:100px;"));
            }
        } else {
            return BsHtml::imageThumbnail(Yii::app()->theme->baseUrl . '/images/no_disponible.png', 'no_disponible.png', array("style" => "width:100px;height:100px;"));
        }
    }

    /**
     * Retorma el valor tota de un sumatoria
     * @param array $records
     * @return string Total sumado
     */
    public function getSum($records = array()) {
        $total = 0;
        foreach ($records as $record)
            if ($record->r_d_s == 1)
                $total+=$record->total;
        return $total;
    }
    /**
     * Retorma el valor tota de un sumatoria
     * @param array $records
     * @return string Total sumado
     */
    public function getTotal($records = array()) {
        $total = 0;
        foreach ($records as $record)
            if ($record->r_d_s == 1)
                $total+=$record->quantity;
        return $total;
    }

    /**
     * Retorma el valor tota de un sumatoria multiplicado por cantidad
     * @param array $records
     * @return string Total sumado
     */
    public function getTotalPorCantidad($records = array()) {
        $total = 0;
        foreach ($records as $record)
            if ($record->r_d_s == 1)
//                $toal += ($record->price - ($record->price * ($record->idDiscount /100))* $record->quantity); 
                $total+= $record->price * $record->quantity;
        return $total;
    }

    public function test($param) {
        var_dump($param);
    }

    public function formatoFechaHora($fechahora, $formato = 'd-m-Y h:i:s') {
        return date($formato, strtotime($fechahora));
    }

    /* bracode */

    public static function getItemBarcode($valueArray) {
        $elementId = $valueArray['itemId'] . "_bcode";
        $value = $valueArray['barocde'];
        if (!empty($value)) {
            $type = 'code128'; /* you can set the type dynamically if you want valueArray eg - $valueArray['type'] */
            $optionsArray = array(
                'elementId' => $valueArray['itemId'] . "_bcode",
                'value' => $valueArray['barocde'],
                'type' => Yii::app()->user->getState('BARCODE')->BARCODE_TYPE,
                'settings' => array(
                    'output' => Yii::app()->user->getState('BARCODE')->BARCODE_OUTPUT /* css, bmp, canvas note- bmp and canvas incompatible wtih IE */,
                    /* if the output setting canvas */
                    'posX' => Yii::app()->user->getState('BARCODE')->BARCODE_POSX,
                    'posY' => Yii::app()->user->getState('BARCODE')->BARCODE_POSY,
                    /* */
                    'bgColor' => Yii::app()->user->getState('BARCODE')->BARCODE_BG_COLOR, /* background color */
                    'color' => Yii::app()->user->getState('BARCODE')->BARCODE_COLOR, /* "1" Bars color */
                    'barWidth' => Yii::app()->user->getState('BARCODE')->BARCODE_BAR_WIDTH,
                    'barHeight' => Yii::app()->user->getState('BARCODE')->BARCODE_BAR_HEIGHT,
                    /* -----------below settings only for datamatrix-------------------- */
                    'moduleSize' => Yii::app()->user->getState('BARCODE')->BARCODE_MODULE_SIZE,
                    'addQuietZone' => Yii::app()->user->getState('BARCODE')->BARCODE_ADD_QUIET_ZONE, /* Quiet Zone Modules */
                    'showHRI'=>(boolean)Yii::app()->user->getState('BARCODE')->BARCODE_SHOW_HRI,
                ),
                'rectangular' => (boolean)Yii::app()->user->getState('BARCODE')->BARCODE_RECTANGULAR /* true or false */
                    /* */
            );
            self::getBarcode($optionsArray);
        }
        return CHtml::tag('div', array('id' => $elementId));
    }

    /**
     * This function returns the item barcode
     */
    public static function getBarcode($optionsArray) {

        return Yii::app()->getController()->widget('ext.barcode.Barcode', $optionsArray);
    }

}
