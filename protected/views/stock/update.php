<?php
/* @var $this StockController */
/* @var $model Stock */
?>

<?php
$this->breadcrumbs=array(
	'Stocks'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Stock').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('StockUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
        'stoType'=>$stoType,
        'stoLocation'=>$stoLocation,
        )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('StockUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>