<?php
/* @var $this StockController */
/* @var $model Stock */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'stock-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->dropDownListControlGroup($model, 'id_sto_location', $stoLocation,array('data-style'=>'btn-info','class'=>'selectpicker show-tick','empty'=>Yii::t('app','Select')));?>
    <?php echo $form->dropDownListControlGroup($model, 'id_sto_type', $stoType,array('data-style'=>'btn-info','class'=>'selectpicker show-tick','empty'=>Yii::t('app','Select')));?>
    <?php 
    if(Yii::app()->user->checkAccess('StockCreateStatusChange') || Yii::app()->user->checkAccess('StockUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('StockCreateButtonSave') || Yii::app()->user->checkAccess('StockUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('StockCreateButtonNew') || Yii::app()->user->checkAccess('StockUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Stock/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
