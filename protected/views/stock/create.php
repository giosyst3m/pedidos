<?php
/* @var $this StockController */
/* @var $model Stock */
?>

<?php
$this->breadcrumbs=array(
	'Stocks'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Stock')) ?>

<?php 
if(Yii::app()->user->checkAccess('StockCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
        'stoType'=>$stoType,
        'stoLocation'=>$stoLocation,
        )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('StockCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>