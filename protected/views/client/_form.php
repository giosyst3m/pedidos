<?php
/* @var $this ClientController */
/* @var $model Client */
/* @var $form BSActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'client-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data','onsubmit'=>'return validateForm();',)
        ));
?>

<p class="help-block"> <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required.') ?></p>
<div class="row">
    <div class="col-lg-6 col-med-6 col-xs-12 col-sm-12">
        <?php echo $form->errorSummary($model); ?>
        <?php echo $form->textFieldControlGroup($model, 'code', array('maxlength' => 15)); ?>
        <?php echo $form->textFieldControlGroup($model, 'correlative', array('maxlength' => 15,'help'=> 'Colcar 0 cuando sea la principal<br>Colocar mayor 1 cuando se una sucursal')); ?>
        <?php echo $form->textFieldControlGroup($model, 'name', array('maxlength' => 255)); ?>
        <?php echo $form->textFieldControlGroup($model, 'phone', array('maxlength' => 15)); ?>
        <?php echo $form->textFieldControlGroup($model, 'mobil', array('maxlength' => 255)); ?>
        <?php echo $form->textAreaControlGroup($model, 'address', array('rows' => 6)); ?>
        <?php echo $form->dropDownListControlGroup($model,'id_city',$City,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','empty'=>'Select one','data-live-search'=>true,)); ?>
        <?php echo $form->textFieldControlGroup($model, 'conctact', array('maxlength' => 255)); ?>
        <?php echo $form->textFieldControlGroup($model, 'email', array('maxlength' => 255)); ?>
        <?php echo $form->fileFieldControlGroup($model, 'logo'); ?>

        <?php
        if (!empty($model->logo)) {
            echo '<div class="center-block text-center">';
            echo $model->ShowImagen($model->logo, Yii::app()->params['URL_CLIENT_LOGO']);
            echo '</div><div class="clearfix">&nbsp;</div>';
        }
        ?>
        <?php
        if (Yii::app()->user->checkAccess('ClientCreateStatusChange') || Yii::app()->user->checkAccess('ClientUpdateStatusChange')) {
            echo $form->dropDownListControlGroup($model, 'r_d_s', array(1 => 'Activo', 0 => 'Inactivo'), array('data-style' => 'btn-info', 'class' => 'selectpicker show-tick'));
        }
        ?>  
    </div>
    <div id="proAttr" class="col-lg-6 col-med-6 col-xs-12 col-sm-12">
        <h3><?php echo Yii::t('app','Caracteristic');?></h3>
        <?php echo $attr; ?>
    </div>
      <?php
        if (Yii::app()->user->checkAccess('ClientCreateButtonSave') || Yii::app()->user->checkAccess('ClientUpdateButtonSave')) {
            echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY));
        }
        ?>    
        <?php
        if (Yii::app()->user->checkAccess('ClientCreateButtonNew') || Yii::app()->user->checkAccess('ClientUpdateButtonNew')) {
            echo BsHtml::link(Yii::t('app', 'New'), Yii::app()->createAbsoluteUrl('Client/create'), array('class' => 'btn btn-primary'));
        }
        ?>  
</div>
<?php $this->endWidget(); ?>
