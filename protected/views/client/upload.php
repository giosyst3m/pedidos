<?php
$this->breadcrumbs=array(
	'Products'=>array('create'),
	'Create',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/script/client/upload.js', CClientScript::POS_HEAD);
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Import'),Yii::t('app','Client from Excel File')) ?>

<?php 
if(Yii::app()->user->checkAccess('ClientUploadFiles')){
    echo Yii::t('app', 'Follow next Step');
    
    $this->renderPartial('_upload' ); 
}

?>
<br>