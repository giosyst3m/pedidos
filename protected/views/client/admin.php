<?php
/* @var $this ClientController */
/* @var $model Client */


$this->breadcrumbs = array(
    'Clients' => array('index'),
    'Manage',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-list', 'label' => 'List Client', 'url' => array('index')),
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create Client', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#client-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button(Yii::t('app', 'Advanced search'), array('class' => 'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH, 'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            <?php echo Yii::t('app', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
                &lt;&gt;</b>
            or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.'); ?>        </p>

        <div class="search-form" style="display:none">
            <?php
//            $this->renderPartial('_search', array(
//                'model' => $model2,
//            ));
            ?>
        </div>
        <!-- search-form -->

        <?php
        $this->widget('bootstrap.widgets.BsGridView', array(
            'id' => 'client-grid',
            'dataProvider' => $model2->search(),
            'filter' => $model2,
            'type' => BsHtml::GRID_TYPE_RESPONSIVE,
            'columns' => array(
                'code',
                'correlative',
                'name',
                'city',
                'zone',
                array(
                    'name' => 'client_r_d_s',
                    'value' => '$data->RegistroEstado($data->client_r_d_s)',
                    'filter' => array(0 => 'Inactivo', 1 => 'Activo')
                ),
                array(
                    'class' => 'bootstrap.widgets.BsButtonColumn',
                    'template'=>'{delete}{update}',
                    'buttons'=>[
                        'update'=>[
                            'url'=>'Yii::app()->createUrl("client/update", array("id"=>$data->id))'
                        ],
                        'delete'=>[
                            'url'=>'Yii::app()->createUrl("client/delete", array("id"=>$data->id,"ajax"=>"client-grid" ))',
                        ]
                    ]
                ),
            ),
        ));
        ?>
    </div>
</div>




