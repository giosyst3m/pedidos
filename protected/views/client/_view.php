<?php
/* @var $this ClientController */
/* @var $data Client */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
		<?php echo CHtml::encode($data->name); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
		<?php echo CHtml::encode($data->phone); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('mobil')); ?>:</b>
		<?php echo CHtml::encode($data->mobil); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
		<?php echo CHtml::encode($data->address); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('conctact')); ?>:</b>
		<?php echo CHtml::encode($data->conctact); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
		<?php echo CHtml::encode($data->email); ?>
		<br />

	<?php if(Yii::app()->user->checkAccess('ClientViewAuthView')){?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('logo')); ?>:</b>
		<?php echo CHtml::encode($data->logo); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_u_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_u_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_u')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_u); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_d); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_i')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_i); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_s); ?>
		<br />

	<?php }?>

</div>