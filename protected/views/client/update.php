<?php
/* @var $this ClientController */
/* @var $model Client */
?>

<?php
$this->breadcrumbs = array(
    'Clients' => array('create'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><i class="fa fa-bars"></i> <?php echo Yii::t('app','Cliente');?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="client-tab" role="tab" data-toggle="tab" aria-expanded="true"><?php echo Yii::t('app', 'Client'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="zona-tab" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Zone'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="order-tab2" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Order'); ?></a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="client-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('ClientUpdateFormView')) {
                        $this->renderPartial('_form', array('model' => $model, 'attr' => $attr, 'City' => $City));
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="zona-tab">

                    <?php
                    if (Yii::app()->user->checkAccess('ZoneClientCreateFormView')) {
                        $this->renderPartial('../zoneClient/_form', array(
                            'model' => $model3,
                            'FormDefault' => $FormDefault2,
                            'Zone' => $Zone
                                )
                        );
                    }
                    ?>
                    <hr>
                    <?php
                    if (Yii::app()->user->checkAccess('ZoneClientCreateAdminView')) {
                        $this->renderPartial('../zoneClient/admin', array(
                            'model2' => $model4,
                            'FormDefault' => $FormDefault,
                            'Zone' => $Zone
                                )
                        );
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="order-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('StockCompanyCreateAdminView')) {
                        $this->renderPartial('../order/admin', array(
                            'model2' => $model7,
                            'VSisUsuario' => $VSisUsuario,
                            'Company' => $Company,
                            'client' => $Client,
                            'status' => $Status,
                            'FormDefault' => $FormDefault,
                            'FormDefault2' => $FormDefault2,
                        ));
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('ClientUpdateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2));
}
?>