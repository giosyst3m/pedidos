<h1><?php echo $Program->name ?></h1>
<p><?php echo $Program->description;?></p>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-offset-4 col-sm-8">
            <div class="row">
                <?php
                $index = 0;
                foreach ($Plan as $value) {
                    ?>
                    <div class="col-xs-3 my_planHeader my_plan<?php echo $index; ?>">
                        <div class="my_planTitle"><h2><i class="<?php echo $value->icon;?>"></i> <?php echo $value->name; ?> </h2></div>
                        <div class="my_planPrice"><?php echo number_format($value->price, 0); ?></div>
                        <div class="my_planDuration"><?php echo $value->idFrequency->name; ?></div>
                        <div class="my_planDuration"><?php echo $value->description; ?></div>
                        <a href="http://www.defconfs.com" type="button" class="btn btn-default">Sign Up</a>
                    </div>

                    <?php
                    $index++;
                }
                ?>
            </div>
        </div>
    </div>
    <?php
    foreach ($PlanConfig as $value) {
        ?>
        <div class="row my_featureRow">
            <div class="col-xs-12 col-sm-4 my_feature">
                <?php echo Yii::t('lic',$value->name); ?>&nbsp;<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="<?php echo $value->description;?>"></i>
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="row">
                    <?php
                    $PlanPlaConfig = PlanPlaConfig::model()->cache(Yii::app()->params['CACHE_TIME'])->findAllByAttributes([
                        'id_pla_config' => $value->id
                    ]);
                    $index = 0;
                    foreach ($PlanPlaConfig as $data) {
                        ?>
                        <div class="col-xs-3 col-sm-3 my_planFeature my_plan<?php echo $index; ?>">
                            <?php echo $data->default; ?>
                        </div>
                    <?php $index++; } ?>
                </div>

            </div>
        </div>
    <?php } ?>
    
</div>

<h4 class="text-center"><?php echo BsHtml::link(Yii::t('app','Service and Agreement'),$this->createAbsoluteUrl('legal/ServicesAgreement'),['target'=>'_blank']); ?></h4>