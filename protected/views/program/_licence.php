<?php

$this->widget('bootstrap.widgets.BsGridView', array(
    'id' => 'manufacturer-grid',
    'dataProvider' => $model,
    'pager' => array('pageSize' => 1),
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        array(
            'name' => 'description',
            'type' => 'raw',
            'value' => function ($data) {
                return  Yii::t('lic',$data->name).'&nbsp;'.
                BsHtml::tag('i',[
                    'class'=>"fa fa-info-circle",
                    'data-toggle'=>"tooltip",
                    'data-placement'=>"right",
                    'title'=>$data->description
                    ],'',true);
            }
        ),
        array(
            'name' => 'default',
            'header' => 'Valor',
            'type'=>'raw'
        ),
//        'name',
        array(
            'name' => 'name',
            'header' => 'Estado',
            'type' => 'raw',
            'htmlOptions' => array('class' => 'text-center'),
            'value' => function ($data) {
        switch ($data->name) {
            case 'LICENCE_USER':
                return $data->default - SisUsuario::model()->count('id_auth_item NOT IN ("Admin.*","Vendedor")');
            case 'LICENCE_CLIENT':
                return number_format($data->default - Client::model()->count(), 0);
            case 'LICENCE_SELLER':
                return $data->default - SisUsuario::model()->count('id_auth_item IN ("Vendedor")');
            case 'LICENCE_ORDER':
                return Order::model()->count();
            case 'LICENCE_CLIENT_ACCESS':
                return 0;
            case 'LICENCE_FROM':
                if (date($data->default) <= date("Y-m-d")) {
                    return BsHtml::label(BsHtml::icon(BsHtml::GLYPHICON_OK), null, array('class' => 'label label-primary'));
                } else {
                    return BsHtml::label(BsHtml::icon(BsHtml::GLYPHICON_REMOVE), null, array('class' => 'label label-danger'));
                }
            case 'LICENCE_TO':
                if (date($data->default) >= date("Y-m-d")) {
                    return BsHtml::label(BsHtml::icon(BsHtml::GLYPHICON_OK), null, array('class' => 'label label-primary'));
                } else {
                    return BsHtml::label(BsHtml::icon(BsHtml::GLYPHICON_REMOVE), null, array('class' => 'label label-danger'));
                }
            case 'LICENCE_STATUS':
                if ($data->default === 'SI') {
                    return BsHtml::label(BsHtml::icon(BsHtml::GLYPHICON_OK), null, array('class' => 'label label-primary'));
                } else {
                    return BsHtml::label(BsHtml::icon(BsHtml::GLYPHICON_REMOVE), null, array('class' => 'label label-danger'));
                }
            default:
                return BsHtml::label(BsHtml::icon(BsHtml::GLYPHICON_OK), null, array('class' => 'label label-primary'));
                break;
        }
    }
        ),
    ),
));
?>
