
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel-group" id="accordion">
            <?php
            $index = 0;
            foreach ($VSysUserPlan as $value) {
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $index; ?>"><span class="<?php echo $value->icon; ?> fa-2x">
                                </span> Plan: <?php echo $value->plan.' Vigencia Desde '.$value->licen_from.' Hasta '.$value->licence_to ?> </a>
                        </h4>
                    </div>
                    <div id="collapse-<?php echo $index; ?>" class="panel-collapse collapse <?php echo ($index == 0 ? 'in' : ''); ?>">
                        <div class="panel-body">
                            <?php
                            $model = new VPlanPlaConfig('search');
                            $criteria = new CDbCriteria();
                            $criteria->addInCondition('id_plan', [$value->id_plan]);
                            $dataProvider = new CActiveDataProvider($model, array(
                                'pagination' => array(
                                    'pageSize' => 50,
                                ),
                                'criteria' => $criteria,
                            ));
                            $this->renderPartial('_licence', array(
                                'model' => $dataProvider,
                            ));
                            ?>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label class=" text-blue">Empresa &nbsp; <?php echo $value->company; ?></label><br>
                                    <label class=" text-blue">Usuario &nbsp; <?php echo $value->name.'&nbsp;'.$value->last_name; ?></label><br>
                                    <label class=" text-blue">Sub-Dominio &nbsp; <?php echo $value->sub_domain; ?></label><br>
                                </div>
                                <div class="col-lg-6">
                                    <label class=" text-blue">Precio &nbsp; <?php echo number_format($value->price,0); ?></label><br>
                                    <label class=" text-blue">Fechas &nbsp; <?php echo $value->licen_from.' - '.$value->licence_to; ?></label><br>
                                    <label class=" text-blue">Licencia &nbsp; <?php echo $value->licence_numbre; ?></label><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $index++;
        }
        ?>
    </div>
</div>