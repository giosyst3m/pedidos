<?php
/* @var $this LicenceController */

$this->breadcrumbs = array(
    'Licence',
);
echo BsHtml::pageHeader('Términos y Condiciones');
//Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/menu-toggle.js', CClientScript::POS_HEAD);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><i class="fa fa-align-left"></i> Collapsible / Accordion <small>Sessions</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <!-- start accordion -->
        <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
            <?php
            $index = 0;
            foreach ($Legal as $value) {
                ?>
                <div class="panel">
                    <a class="panel-heading collapsed" role="tab" id="heading<?php echo $index ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $index ?>" aria-expanded="false" aria-controls="collapse<?php echo $index ?>">
                        <h4 class="panel-title"><i class="<?php echo $value->icon;?>"></i>&nbsp;<?php echo $value->name; ?></h4>
                    </a>
                    <div id="collapse<?php echo $index ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $index ?>">
                        <div class="panel-body">
                            <?php echo $value->description; ?>
                        </div>
                    </div>
                </div>

                <?php
                $index++;
            }
            ?>
        </div>
        <!-- end of accordion -->
    </div>
</div>
