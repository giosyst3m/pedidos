<?php
/* @var $this DiscountController */
/* @var $model Discount */
?>

<?php
$this->breadcrumbs=array(
	'Discounts'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Discount')) ?>

<?php 
if(Yii::app()->user->checkAccess('DiscountCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('DiscountCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>