<?php
/* @var $this DiscountController */
/* @var $model Discount */
?>

<?php
$this->breadcrumbs=array(
	'Discounts'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Discount').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('DiscountUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('DiscountUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>