<?php

/* @var $this CatGroupController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php

$this->breadcrumbs = array(
    'Cat Groups',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create CatGroup', 'url' => array('create')),
    array('icon' => 'glyphicon glyphicon-tasks', 'label' => 'Manage CatGroup', 'url' => array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Cat Groups') ?>
<?php

$this->widget('bootstrap.widgets.BsListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
));
?>