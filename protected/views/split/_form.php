<?php
/* @var $this SplitController */
/* @var $model Split */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'split-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListControlGroup($model, 'parent', $Split, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Select one', 'onchange' => 'getAttr(this);', 'data-live-search' => true,)); ?>
    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->dropDownListControlGroup($model, 'id_stock', $Stock, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Select one', 'onchange' => 'getAttr(this);', 'data-live-search' => true,)); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('SplitCreateStatusChange') || Yii::app()->user->checkAccess('SplitUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('SplitCreateButtonSave') || Yii::app()->user->checkAccess('SplitUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('SplitCreateButtonNew') || Yii::app()->user->checkAccess('SplitUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Split/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
