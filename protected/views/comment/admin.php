<?php

$this->widget('bootstrap.widgets.BsGridView', array(
    'id' => 'comment-grid',
    'dataProvider' => $model2->search(),
    'filter' => $model2,
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        [
            'name' => 'photo',
            'type' => 'raw',
            'value' => function($data) {
                return '<div class="avatar pull-left">' . BsHtml::imageCircle(Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . $data->photo) . '' .
                        '<h4>' . $data->complete_name . '</h4></div>'.
                        BsHtml::tag('h2',['class'=>'hodden-lg hidden-md hidden-sm'],'<label class="label label-' . $data->color . '">' . $data->priority . '</label>');
            },
            'filterHtmlOptions' => [
                'class' => 'text-mobil  ',
            ],
            'headerHtmlOptions' => [
                'class' => 'text-mobil ',
            ],
            'htmlOptions' => [
                'class' => 'text-mobil ',
            ],
        ],
        [
            'name' => 'priority',
            'type' => 'raw',
            'value' => function($data) {
                $html = '<h2><label class="label label-' . $data->color . '">' . $data->priority . '</label></h2>';
                return $html;
            },
            'filterHtmlOptions' => [
                'class' => 'hidden-xs text-mobil ',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-xs text-mobil ',
            ],
            'htmlOptions' => [
                'class' => 'hidden-xs text-mobil ',
            ],
                ],
                [
                    'name' => 'message',
                    'type' => 'raw',
                    'value' => function($data) {
                        return $data->number.'<br>'.$data->title.'<br>'.$data->message;
                    },
                    'filterHtmlOptions' => [
                        'class' => 'text-mobil  ',
                    ],
                    'headerHtmlOptions' => [
                        'class' => 'text-mobil ',
                    ],
                    'htmlOptions' => [
                        'class' => 'text-mobil ',
                    ],
                ],
                [
                    'name' => 'file',
                    'type' => 'raw',
                    'value' => function($data) {
                        if (!empty($data->file)) {
                            return BsHtml::link(BsHtml::icon(BsHtml::GLYPHICON_DOWNLOAD) . ' Download', Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('COMMENT')->COMMENT_ORDER_FILE_PATH . $data->file, array('target' => '_blank', 'class' => 'btn btn-' . $data->color));
                        }
                    },
                    'filterHtmlOptions' => [
                        'class' => 'text-mobil  ',
                    ],
                    'headerHtmlOptions' => [
                        'class' => 'text-mobil ',
                    ],
                    'htmlOptions' => [
                        'class' => 'text-mobil ',
                    ],
                ],
                        [
                        'name'  =>  'r_c_d',
                        'filterHtmlOptions' => [
                            'class' => 'text-mobil  ',
                        ],
                        'headerHtmlOptions' => [
                            'class' => 'text-mobil ',
                        ],
                        'htmlOptions' => [
                            'class' => 'text-mobil ',
                        ],
                    ]
                    ),
                ));
                ?>




