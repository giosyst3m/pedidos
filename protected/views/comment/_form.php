<?php
/* @var $this CommentController */
/* @var $model Comment */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'comment-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data',)
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php //echo $form->textFieldControlGroup($model,'title',array('maxlenght'=>200)); ?>
    <?php echo $form->textAreaControlGroup($model,'message',array('rows'=>6)); ?>
    <?php echo $form->fileFieldControlGroup($model, 'file'); ?>   
    <?php echo $form->dropDownListControlGroup($model,'id_com_priority',$ComPriority,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','empty'=>'Select one','data-live-search'=>true,)); ?>
    <?php 
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    ?>      
<?php $this->endWidget(); ?>
