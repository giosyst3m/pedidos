<?php
/* @var $this DocumentController */
/* @var $model Document */
?>

<?php
$this->breadcrumbs = array(
    'Documents' => array('create'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Update') ?> <?php echo Yii::t('app', 'Document') . ': ' . BsHtml::tag('b', [], $model->name, true); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>

            </li>

        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        if (Yii::app()->user->checkAccess('DocumentUpdateFormView')) {
            $this->renderPartial('_form', array('model' => $model, 'DocType' => $DocType));
        }
        ?>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('DocumentUpdateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2, 'DocType' => $DocType));
}
?>