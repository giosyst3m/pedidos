<table>
    <thead>
        <tr>
            <th><?php echo Yii::t('app', 'Barcode'); ?></th>
            <th><?php echo Yii::t('app', 'SKU'); ?></th>
            <th><?php echo Yii::t('app', 'Name'); ?></th>
            <th><?php echo Yii::t('app', 'Brand'); ?></th>
            <th><?php echo Yii::t('app', 'Pro Group'); ?></th>
            <th><?php echo Yii::t('app', 'Category'); ?></th>
            <th class="text-right"><?php echo Yii::t('app', 'Price'); ?></th>
        </tr>
    </thead>
    <?php foreach ($VProduct as $value) { ?>
        <tr>
            <td><?php echo $value->barcode; ?></td>
            <td><?php echo $value->sku; ?></td>
            <td><?php echo $value->name; ?></td>
            <td><?php echo $value->brand; ?></td>
            <td><?php echo $value->pro_group; ?></td>
            <td><?php echo $value->category_name; ?></td>
            <td><?php echo number_format($value->price,  Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></td>
        </tr>
    <?php } ?>
</table>

