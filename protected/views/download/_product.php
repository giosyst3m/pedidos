<?php

$html = BsHtml::tag('table');
$html .= BsHtml::tag('tr');
$html .= BsHtml::tag('th', array(), Yii::t('app', 'SKU'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'Barcode'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'Name'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'Description'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'Type'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'Brand'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'Conditional'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'Group'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'Category'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'Quantity'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'Price'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'r_d_s'), true);
$html .= BsHtml::tag('th', array(), Yii::t('app', 'Photo'), true);
foreach ($model as $value) {
    $html .= BsHtml::tag('tr');
    $html .= BsHtml::tag('td', array(), $value->sku, true);
    $html .= BsHtml::tag('td', array(), $value->barcode, true);
    $html .= BsHtml::tag('td', array(), $value->name, true);
    $html .= BsHtml::tag('td', array(), $value->description, true);
    $html .= BsHtml::tag('td', array(), $value->type, true);
    $html .= BsHtml::tag('td', array(), $value->brand, true);
    $html .= BsHtml::tag('td', array(), $value->conditiona, true);
    $html .= BsHtml::tag('td', array(), $value->pro_group, true);
    $html .= BsHtml::tag('td', array(), $value->category_name, true);
    $html .= BsHtml::tag('td', array(), $value->quantity, true);
    $html .= BsHtml::tag('td', array(), $value->price, true);
    $html .= BsHtml::tag('td', array(), ($value->r_d_s == 1 ? Yii::t('app', 'Active') : Yii::t('app', 'Inactive')), true);
    $html .= BsHtml::tag('td', array(), $value->photo, true);
    $html .= BsHtml::closeTag('tr');
}
$html .= BsHtml::closeTag('table');
echo $html;

