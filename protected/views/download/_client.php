<?php
$html = BsHtml::tag('table');
$html .= BsHtml::tag('tr');
$html .= BsHtml::tag('th',array(),Yii::t('app','Code'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','Correlative'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','Name'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','Phone'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','Mobil'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','Address'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','Contact'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','Email'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','City'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','Departamento'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','Country'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','Zone'),true);
$html .= BsHtml::tag('th',array(),Yii::t('app','r_d_s'),true);
foreach ($model as $value) {
    $html .= BsHtml::tag('tr');
    $html .= BsHtml::tag('td', array(), $value->code,true);
    $html .= BsHtml::tag('td', array(), $value->correlative,true);
    $html .= BsHtml::tag('td', array(), $value->name,true);
    $html .= BsHtml::tag('td', array(), $value->phone,true);
    $html .= BsHtml::tag('td', array(), $value->mobil,true);
    $html .= BsHtml::tag('td', array(), $value->address,true);
    $html .= BsHtml::tag('td', array(), $value->conctact,true);
    $html .= BsHtml::tag('td', array(), $value->email,true);
    $html .= BsHtml::tag('td', array(), $value->city,true);
    $html .= BsHtml::tag('td', array(), $value->stade,true);
    $html .= BsHtml::tag('td', array(), $value->country,true);
    $html .= BsHtml::tag('td', array(), $value->zone,true);
    $html .= BsHtml::tag('td', array(), ($value->r_d_s==1?Yii::t('app','Active'):Yii::t('app','Inactive')),true);
    $html .= BsHtml::closeTag('tr');
}
$html .= BsHtml::closeTag('table');
echo $html;

