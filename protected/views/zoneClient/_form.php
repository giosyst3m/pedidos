<?php
/* @var $this ZoneClientController */
/* @var $model ZoneClient */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'zone-client-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php 
    
    if($FormDefault){
        echo $form->dropDownListControlGroup($model, 'id_zone', $Zone, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Select one', 'onchange' => 'getAttr(this);', 'data-live-search' => true,)); 
    }else{
        echo $form->dropDownListControlGroup($model, 'id_client', $VClient, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Select one', 'onchange' => 'getAttr(this);', 'data-live-search' => true,)); 
    }
    if(Yii::app()->user->checkAccess('ZoneClientCreateButtonSave') || Yii::app()->user->checkAccess('ZoneClientUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }
 $this->endWidget(); 
