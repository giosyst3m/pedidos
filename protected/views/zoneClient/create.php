<?php
/* @var $this ZoneClientController */
/* @var $model ZoneClient */
?>

<?php
$this->breadcrumbs=array(
	'Zone Clients'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','ZoneClient')) ?>

<?php 
if(Yii::app()->user->checkAccess('ZoneClientCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ZoneClientCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>