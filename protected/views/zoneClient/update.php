<?php
/* @var $this ZoneClientController */
/* @var $model ZoneClient */
?>

<?php
$this->breadcrumbs=array(
	'Zone Clients'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','ZoneClient').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ZoneClientUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ZoneClientUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>