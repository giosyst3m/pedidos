<?php
/* @var $this InventoryController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
    'Pack Off',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create Inventory', 'url' => array('create')),
    array('icon' => 'glyphicon glyphicon-tasks', 'label' => 'Manage Inventory', 'url' => array('admin')),
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inventory-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/catalog/_index.css');
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/inventory/pack_off.js', CClientScript::POS_END); ?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Pack Off') ?><b> <?php echo Yii::t('app', 'Order'); ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        $this->renderPartial('_search', array(
            'model' => $dataProvider,
        ));
        ?>
    </div>
</div>
<hr>
<?php
$this->widget('bootstrap.widgets.BsListView', array(
    'dataProvider' => $dataProvider->search(false),
    'id' => 'pack_off_list',
    'itemView' => '_pack_off',
));
?>