<?php
/* @var $this OrderController */
/* @var $data Order */
if ($data->quantity == 0) {
    $class = 'warning';
    $class2 = 'btn-lred';
} else if ($data->quantity < 0) {
    $class = 'danger';
    $class2 = 'btn_lorgange';
} else if ($data->quantity > 0) {
    $class = 'success';
    $class2 = 'btn-lgreen';
}
?>
<div class="row">
    <div class="col-lg-1 col-md-1 col-sm-1">
        <?php echo $data->ShowImagen($data->photo, Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH, 100, 50); ?>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2">
        <a href="#" id="product-<?php echo $data->id_product; ?>" class="btn btn-sq-md btn-<?php echo $class; ?>">
            <i class="fa fa-car fa-2x"></i><br/>
            <span><?php echo Yii::t('app', 'Existencia'); ?></span>
            <span id="<?php echo $data->id_product; ?>"><?php echo $data->quantity ?></span>
        </a>   
    </div>
    <div class="col-lg-9 col-md-9 col-sm-9">
        <div class="row">
            <span class="btn-lgreen"><?php echo Yii::t('app', 'Product'); ?>: <smal><?php echo CHtml::encode($data->name); ?></smal></span>
            <br>
            <span><?php echo Yii::t('app', 'SKU'); ?>: <smal><?php echo CHtml::encode($data->sku); ?></smal></span>
            &nbsp;
            <span><?php echo Yii::t('app', 'Barcode'); ?>: <smal><?php echo CHtml::encode($data->barcode); ?></smal></span>
            &nbsp;
            <span><?php echo Yii::t('app', 'Type'); ?>: <smal><?php echo CHtml::encode($data->type); ?></smal></span>
            &nbsp;
            <span><?php echo Yii::t('app', 'Category'); ?>: <smal><?php echo CHtml::encode($data->category_name); ?></smal></span>
            &nbsp;
            <span><?php echo Yii::t('app', 'Brand'); ?>: <smal><?php echo CHtml::encode($data->brand); ?></smal></span>
            &nbsp;
            <span><?php echo Yii::t('app', 'Línea'); ?>: <smal><?php echo CHtml::encode($data->pro_group); ?></smal></span>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-10">
        <div class="row btn-primary">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <?php echo Yii::t('app', 'Order Number'); ?>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5">
                <?php echo Yii::t('app', 'Client'); ?>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 text-right">
                <?php echo Yii::t('app', 'Solicitado'); ?>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 text-right">
                <?php echo Yii::t('app', 'Despachar'); ?>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1">
                <?php echo Yii::t('app', 'R C U'); ?>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1">
                <?php echo Yii::t('app', 'R C D'); ?>
            </div>
        </div>
        &nbsp;
        <?php
        $index = 0;
        $sum = 0;
        $CDbCriteria = new CDbCriteria();
        $CDbCriteria->addInCondition('id_status', explode(',', Yii::app()->user->getState('ORDER')->ORDER_STATUS_TO_PACK_OFF));
        $CDbCriteria->addColumnCondition(['id_product' => $data->id_product]);
        foreach (VOrderDetail::model()->findAll($CDbCriteria) as $value) {
            $index++;
            $sum = $sum + $value->ord_detail_quantity;
            ?>
            <div class="row">
                <div class="col-lg-2 col-sm-2 col-md-2">
                    <?php echo BsHtml::link($value->number, $this->createUrl('ordDetail/view', ['id' => $value->id]), ['target' => '_blank']); ?>
                    <span class="<?php echo $value->color; ?>"><?php echo $value->status; ?></span>
                </div>
                <div class="col-lg-5 col-sm-5 col-md-5">
                    <?php echo $value->code; ?>
                    -&nbsp;
                    <?php echo $value->correlative; ?>
                    &nbsp;
                    <?php echo $value->client_name; ?>
                    <br>
                    <?php echo $value->city_name; ?>
                    &nbsp;
                    <?php echo $value->address; ?>
                </div>
                <div class="col-lg-1 col-sm-1 col-md-1 text-right">
                    <input type="number" disabled="disabled" class="text-right" style="width:50px; border: none" id="qty-<?php echo $value->ord_detail_id; ?>" value="<?php echo $value->ord_detail_quantity; ?>">
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 text-right">
                    <?php if ($data->quantity > 0) { ?>
                        <input type="number" class="text-right product-quantity items-<?php echo $value->id_product; ?>" style="width:50px" id="req-<?php echo $value->ord_detail_id; ?>" value="<?php echo $value->ord_detail_quantity; ?>" min="0">
                        <button id="btn-<?php echo $value->id_ord_detail; ?>"  data-loading-text="..." data-value="" class="btn btn-<?php echo $value->inv_in > 0 ? 'success' : 'primary' ?> botones-<?php echo $value->id_product; ?>" onclick="ApprovedRequest(<?php echo $value->ord_detail_id ?>,<?php echo $value->id_product ?>)"><i class="fa fa-save"></i></button>
                    <?php } else { ?>
                        <input type="number" class="text-right items-<?php echo $value->id_product; ?>" style="width:50px" disabled="disabled" value="0">    
                        <span class="fa fa-close btn btn-danger"></span>
                    <?php } ?>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1">
                    <?php echo $value->user_complete_name ?>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1">
                    <?php echo $value->r_c_d ?>
                </div>
            </div>
        <?php } 
        if($sum > $data->quantity){
            $class3 = 'btn-danger';
        }else{
            $class3 = 'btn-primary';
        }
        ?>
        <div class="row <?php echo $class3;?>" id="total-<?php echo $value->id_product; ?>">
            <div class="col-lg-2 col-sm-2 col-md-2">
                <?php echo Yii::t('app', 'Total'); ?>
            </div>
            <div class="col-lg-5 col-sm-5 col-md-5">
                <?php echo number_format($index, 0); ?>
            </div>
            <div class="col-lg-1 col-sm-1 col-md-1 text-right sum-<?php echo $value->id_product; ?>">
                <?php echo number_format($sum, 0); ?>
            </div>
            <div class="col-lg-2 col-sm-2 col-md-2 text-right">
                <span id="" class="sum-<?php echo $value->id_product; ?>"><?php echo number_format($sum, 0); ?></span>                
                <span class="fa fa-check btn btn-primary"></span>
            </div>
            <div class="col-lg-1 col-sm-1 col-md-1">

            </div>
            <div class="col-lg-1 col-sm-1 col-md-1">

            </div>
        </div>
    </div>
</div>
<hr>
