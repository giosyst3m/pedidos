<?php
/* @var $this InventoryController */
/* @var $model Inventory */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_INLINE,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'client_name'); ?>
    <?php echo $form->textFieldControlGroup($model,'barcode'); ?>
    <?php echo $form->textFieldControlGroup($model,'sku'); ?>
    <?php echo $form->textFieldControlGroup($model,'status'); ?>
    <?php echo $form->textFieldControlGroup($model,'brand'); ?>
    <?php echo $form->textFieldControlGroup($model,'pro_group'); ?>
    <?php echo $form->textFieldControlGroup($model,'category_name'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
