<?php
/* @var $this InventoryController */
/* @var $model Inventory */
?>

<?php
$this->breadcrumbs = array(
    'Inventories' => array('create'),
    'Create',
);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Create') ?><b> <?php echo Yii::t('app', 'Inventory'); ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        if (Yii::app()->user->checkAccess('InventoryCreateFormView')) {
            $this->renderPartial('_form', array(
                'model' => $model,
                'product' => $product,
                'stock' => $stock,
            ));
        }
        ?>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('InventoryCreateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2));
}
?>