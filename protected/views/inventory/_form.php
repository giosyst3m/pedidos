<?php
/* @var $this InventoryController */
/* @var $model Inventory */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'inventory-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListControlGroup($model, 'id_product', $product,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','data-live-search'=>true,'empty'=>  Yii::t('app','Select'))); ?>
    <?php echo $form->numberFieldControlGroup($model,'quantity',array('class'=>'text-right')); ?>
    <?php echo $form->dropDownListControlGroup($model, 'id_stock', $stock,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','data-live-search'=>true,'empty'=>  Yii::t('app','Select'))); ?>
    <?php echo $form->dropDownListControlGroup($model, 'type', array(1=>Yii::t('app', 'IN'),0=>Yii::t('app','Out')),array('data-style'=>'btn-info','class'=>'selectpicker show-tick','empty'=>Yii::t('app','Select'))) ?>
    <?php echo $form->textAreaControlGroup($model,'comment',array('rows'=>6)); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('InventoryCreateStatusChange') || Yii::app()->user->checkAccess('InventoryUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('InventoryCreateButtonSave') || Yii::app()->user->checkAccess('InventoryUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('InventoryCreateButtonNew') || Yii::app()->user->checkAccess('InventoryUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Inventory/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
