<?php
/* @var $this InventoryController */
/* @var $model Inventory */


$this->breadcrumbs=array(
	'Inventories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-list','label'=>'List Inventory', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Inventory', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inventory-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button(Yii::t('app','Advanced search'),array('class' =>'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH,'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            <?php echo Yii::t('app','You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
                &lt;&gt;</b>
            or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.'); ?>        </p>

        <div class="search-form" style="display:none">
            <?php
//            $this->renderPartial('_search',array(
//                'model'=>$model2,
//            ));
            ?>
        </div>
        <!-- search-form -->

        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'inventory-grid',
			'dataProvider'=>$model2->search(),
			'filter'=>$model2,
            'type' => BsHtml::GRID_TYPE_RESPONSIVE,
			'columns'=>array(
        				'id',
                            array(
                                'name'=>'type',
                                'value'=>'$data->getInventarioTipo($data->type)',
                                'filter'=>  array(1=>  Yii::t('app', 'IN'),0=>Yii::t('app', 'OUT'))
                            ),
                            array(
                                'name'=>'id_product',
                                'value'=>'$data->idProduct->name',
                                'filter'=>  CHtml::listData(VProduct::model()->findAll('r_d_s=1'), 'id', 'complete_name')

                            ),
                            
				'quantity',
                            array(
                                'name'=>'id_stock',
                                'value'=>'$data->idStock->name',
                                'filter'=>  CHtml::listData(Stock::model()->findAll('r_d_s=1'), 'id', 'name')

                            ),
                            array(
                                'name'=>'id_inv_origin',
                                'value'=>'$data->idInvOrigin->name',
                                'filter'=>  CHtml::listData(InvOrigin::model()->findAll('r_d_s=1'), 'id', 'name')

                            ),
		/*
				'comment',
				'r_c_u',
				'r_c_d',
				'r_c_i',
				'r_u_u',
				'r_u_d',
				'r_u_i',
				'r_d_u',
				'r_d_d',
				'r_d_i',
				'r_d_s',
			*/
                array(
                    'name'=>'r_d_s',
                    'value'=>'$data->RegistroEstado($data->r_d_s)',
                    'filter'=>  array(0=>'Inactivo',1=>'Activo')
                ),
                array(
                    'class'=>'bootstrap.widgets.BsButtonColumn',
                ),
			),
        )); ?>
    </div>
</div>




