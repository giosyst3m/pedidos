<?php
/* @var $this DelRateController */
/* @var $model DelRate */
?>

<?php
$this->breadcrumbs=array(
	'Del Rates'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','DelRate')) ?>

<?php 
if(Yii::app()->user->checkAccess('DelRateCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model,'delivery'=>$delivery)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('DelRateCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>