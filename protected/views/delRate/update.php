<?php
/* @var $this DelRateController */
/* @var $model DelRate */
?>

<?php
$this->breadcrumbs=array(
	'Del Rates'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','DelRate').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('DelRateUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model,'delivery'=>$delivery)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('DelRateUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>