<?php
/* @var $this ClientFieldController */
/* @var $model ClientField */
?>

<?php
$this->breadcrumbs = array(
    'Client Fields' => array('create'),
    'Create',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/product/create.js', CClientScript::POS_HEAD);
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Create') ?><b> <?php echo Yii::t('app', 'ClientField'); ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        if (Yii::app()->user->checkAccess('ClientFieldCreateFormView')) {
            $this->renderPartial('_form', array('model' => $model,
                'fieType' => $fieType
            ));
        }
        ?>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('ClientFieldCreateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2));
}
?>