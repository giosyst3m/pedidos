<?php
/* @var $this ManufacturerController */
/* @var $model Manufacturer */
?>

<?php
$this->breadcrumbs=array(
	'Manufacturers'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Manufacturer').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ManufacturerUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ManufacturerUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>