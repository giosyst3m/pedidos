<?php
/* @var $this ManufacturerController */
/* @var $model Manufacturer */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'manufacturer-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->fileFieldControlGroup($model,'logo',array('maxlength'=>255)); ?>
    <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'meta_title',array('maxlength'=>70)); ?>
    <?php echo $form->textFieldControlGroup($model,'meta_description',array('maxlength'=>160)); ?>
    <?php echo $form->textFieldControlGroup($model,'friendly_url',array('maxlength'=>255)); ?>
    <?php
        if (!empty($model->logo)) {
            echo '<div class="center-block text-center">';
            echo $model->ShowImagen($model->logo, Yii::app()->user->getState('PRODUCT')->MANUAFACTURER_LOGO);
            echo '</div><div class="clearfix">&nbsp;</div>';
        }
        ?>
    <?php 
    if(Yii::app()->user->checkAccess('ManufacturerCreateStatusChange') || Yii::app()->user->checkAccess('ManufacturerUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('ManufacturerCreateButtonSave') || Yii::app()->user->checkAccess('ManufacturerUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('ManufacturerCreateButtonNew') || Yii::app()->user->checkAccess('ManufacturerUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Manufacturer/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
