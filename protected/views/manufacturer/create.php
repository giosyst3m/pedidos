<?php
/* @var $this ManufacturerController */
/* @var $model Manufacturer */
?>

<?php
$this->breadcrumbs=array(
	'Manufacturers'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Manufacturer')) ?>

<?php 
if(Yii::app()->user->checkAccess('ManufacturerCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ManufacturerCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>