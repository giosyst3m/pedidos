<?php
$this->breadcrumbs = array(
    'Updaload' => array('Updload'),
    'facturas',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/upload/bill.js', CClientScript::POS_HEAD);
?>

<?php echo BsHtml::pageHeader(Yii::t('app', 'Import'), Yii::t('app', 'Facturas desde un archivo de excel')) ?>

<?php
if (Yii::app()->user->checkAccess('ClientUploadFiles')) {
    echo Yii::t('app', 'Follow next Step');
    ?>
    <ul class="list-group">
        <li class="list-group-item">Debe seleccionar El sistema </li>
        <li class="list-group-item">Al subir el archivo tomara un tiempo para su procesamiento</li>
        <li class="list-group-item">Sí la factura esta <b>registrada</b> pasara a un estado <b>Pagado</b> con un <i class="fa fa-check"></i></li>
        <li class="list-group-item list-group-item-danger">Sí la factura ya estaba <b>Pagada</b> con un <i class="fa fa-ban"></i></li>
    </ul>
    <?php
    $this->renderPartial('_bill', array(
        'System' => $System
    ));
}
?>
<br>