<?php

$this->widget('bootstrap.widgets.BsGridView', array(
	'dataProvider' => $arrayDataProvider,
	'columns' => array(
            'id',
		array(
			'name' => 'sku',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["sku"])'
		),
		array(
			'name' => 'name',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["name"])'
		),
		array(
			'name' => 'barcode',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["barcode"])'
		),
		array(
			'name' => 'imagen',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["imagen"])'
		),
		array(
			'name' => 'save',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["save"])'
		),
		array(
			'name' => 'Resultado',
			'type' => 'raw',
			'value' => '$data["result"]'
		),	
),
           'type' => BsHtml::GRID_TYPE_CONDENSED 
    . ' ' . BsHtml
    ::GRID_TYPE_BORDERED . ' ' . BsHtml
    ::GRID_TYPE_STRIPED  
));
