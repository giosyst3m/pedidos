<?php
/* @var $this ProductController */
/* @var $model Product */
?>

<?php
$this->breadcrumbs = array(
    'Products' => array('create'),
    'Create',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/upload/upload.js', CClientScript::POS_HEAD);
echo BsHtml::hiddenField('type',$_GET['type']);
?>

<?php echo BsHtml::pageHeader(Yii::t('app', 'Import'), $title) ?>

<?php
if (Yii::app()->user->checkAccess('ProductUploadFiles')) {
    switch ($type) {
        case 1015:
            ?>
            <ul class="list-group">
                <li class="list-group-item">Cartera</li>
            </ul>
            <?php
            $this->renderPartial('_file');
            break;
        default:
            echo BsHtml::alert(BsHtml::LABEL_COLOR_DANGER, Yii::t('app', 'Tray again, you cannot import any files.'));
            break;
    }
}

?>
<br>