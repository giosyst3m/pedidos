<?php
$this->breadcrumbs = array(
    'Products' => array('create'),
    'Create',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/client/upload.js', CClientScript::POS_HEAD);
?>

<?php echo BsHtml::pageHeader(Yii::t('app', 'Import'), Yii::t('app', 'Client from Excel File')) ?>

<?php
if (Yii::app()->user->checkAccess('ClientUploadFiles')) {
    echo Yii::t('app', 'Follow next Step');
    ?>
    <ul class="list-group">
        <li class="list-group-item">Debe seleccionar un Zona</li>
        <li class="list-group-item">Al subir un archivo los clientes pasaran a desactivos y se activaran si estan presente en el archivo</li>
        <li class="list-group-item">Si el cliente no existe se registra en como nuevo y se asociasa a la zona</li>
        <li class="list-group-item">Un cliente puede pertenecer a diferentes zonas.</li>
        <li class="list-group-item list-group-item-danger">Importante hacer un backup o comunicarse con Soporte si no esta seguro para hacer dicho proceso</li>
        <li class="list-group-item list-group-item-warning">Se recomienda hacer este proceso al inicio o al final del día</li>
    </ul>
    <?php
    $this->renderPartial('_client', array(
        'Zone' => $Zone
    ));
}
?>
<br>