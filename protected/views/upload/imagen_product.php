<?php
/* @var $this ProductController */
/* @var $model Product */
?>

<?php
$this->breadcrumbs=array(
	'Products'=>array('create'),
	'Create',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/script/product/uploadImagen.js', CClientScript::POS_HEAD);
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Import'),Yii::t('app',"Products' Imagen from ZIP File")) ?>

<?php 
if(Yii::app()->user->checkAccess('ProductUploadFiles')){
    echo Yii::t('app', 'Follow next Step');
    ?>
    <ul class="list-group">
        <li class="list-group-item">Subir un archivo ZIP con todas las imagenes de los productos </li>
        <li class="list-group-item">el nombre de las imagenes deben tener Código de Barra del producto</li>
        <li class="list-group-item">Si tiene mas de una imagen para un mismo producto debe agregar un Guio y un correlativo, Ej: 1888833-1 , 1888833-2 , 1888833-X </li>
        <li class="list-group-item">Las imagenes permitidas son .jpeg .png .jpg</li>
        <li class="list-group-item list-group-item-danger">Importante hacer un backup o comunicarse con Soporte si no esta seguro para hacer dicho proceso</li>
        <li class="list-group-item list-group-item-warning">Se recomienda hacer este proceso al inicio o al final del día</li>
    </ul>
    <?php
    $this->renderPartial('_imagen_product' ); 
}

?>
<br>