<?php
/* @var $this ProductController */
/* @var $model Product */
?>

<?php
$this->breadcrumbs = array(
    'Products' => array('create'),
    'Create',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/product/upload.js', CClientScript::POS_HEAD);
?>

<?php echo BsHtml::pageHeader(Yii::t('app', 'Import'), $title) ?>

<?php
if (Yii::app()->user->checkAccess('ProductUploadFiles')) {
    if (!empty($data)) {
        $this->renderPartial('_product_valid', ['model'=>$model, 'Status' => $Status]);
    } else {
        switch ($type) {
            case 1005:
                ?>
                <ul class="list-group">
                    <li class="list-group-item">Todos los productos que existe de desactivaran y se activaran todos aquellos que esten llegando en el archivo</li>
                    <li class="list-group-item">Todos los productos existentes, su unidades pasaran a las cargadas en el archivo</li>
                    <li class="list-group-item">Si el producto no existe de creará como nuevo</li>
                    <li class="list-group-item">Los productos nuevos se asignarán las uanidades recibidas en el archivo.</li>
                    <li class="list-group-item list-group-item-danger">Importante hacer un backup o comunicarse con Soporte si no esta seguro para hacer dicho proceso</li>
                    <li class="list-group-item list-group-item-warning">Se recomienda hacer este proceso al inicio o al final del día</li>
                </ul>
                <?php
                $this->renderPartial('_product');
                break;
            case 1006:
            case 1007:
                ?>
                <ul class="list-group">
                    <li class="list-group-item">Todos los productos que existe de desactivaran y se activaran todos aquellos que esten llegando en el archivo</li>
                    <li class="list-group-item">Todos los productos existentes, se agregará la diferencia de undiades</li>
                    <li class="list-group-item">ejemplo: si producto tiene 80 unidades y el archivo dice que tiene 100 se agregaran <b>20 como diferencia</b></li>
                    <li class="list-group-item">Si el producto no existe de creará como nuevo</li>
                    <li class="list-group-item">Los productos nuevos se asignarán las uanidades recibidas en el archivo.</li>
                    <li class="list-group-item list-group-item-danger">Importante hacer un backup o comunicarse con Soporte si no esta seguro para hacer dicho proceso</li>
                    <li class="list-group-item list-group-item-warning">Se recomienda hacer este proceso al inicio o al final del día</li>
                </ul>
                <?php
                $this->renderPartial('_product');
                break;
            default:
                echo BsHtml::alert(BsHtml::LABEL_COLOR_DANGER, Yii::t('app', 'Tray again, you cannot import any files.'));
                break;
        }
    }
}
?>
<br>