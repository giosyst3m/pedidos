<hr>
<h2>Procesos finalizado favor revisar, cuando halla terminado clic en cotinuar para hacer la revisión de las ordenes, para cambiar estado pagado.</h2>
<a class="btn btn-primary btn-lamber" href="<?php echo $this->createUrl('Order/bill') ?>" >Clic para continuar</a>

<?php

$this->widget('bootstrap.widgets.BsGridView', array(
	'dataProvider' => $arrayDataProvider,
	'columns' => array(
            'id',
		array(
			'name' => 'D',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["D"])'
		),
                array(
			'name' => 'Resultado',
			'type' => 'raw',
			'value' => '$data["result"]'
		),
		
),
           'type' => BsHtml::GRID_TYPE_CONDENSED 
    . ' ' . BsHtml
    ::GRID_TYPE_BORDERED . ' ' . BsHtml
    ::GRID_TYPE_STRIPED  
));
