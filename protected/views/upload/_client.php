<form>
    <?php
        echo BsHtml::dropDownListControlGroup('Zone', array(), $Zone, array(
            'class' => 'selectpicker show-tick',
            'data-live-search' => true,
            'label' => 'Category',
            'name' => 'Zone',
            'title' => Yii::t('app', '.::Select::.'),
            'data-style' => "btn-primary",
            'empty'=>  Yii::t('app','Select'),
            'help'=>  Yii::t('app','Select any zone to asociede all client from file to Zone')
        ));

    $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
        'id' => 'uploadFile',
        'config' => array(
            'action' => Yii::app()->createUrl('upload/saveUploadClient'),
            'allowedExtensions' => array("xls", 'xlsx'), //array("jpg","jpeg","gif","exe","mov" and etc...
            'sizeLimit' => Yii::app()->user->getState('FILE')->FILES_UPLOAD_MAX, // maximum file size in bytes
//               'minSizeLimit'=>10*1024*1024,// minimum file size in bytes
            'onComplete' => "js:function(id, fileName, responseJSON){ processExcelFile(responseJSON); }",
            'messages' => array(
                'typeError' => "{file} has invalid extension. Only {extensions} are allowed.",
                'sizeError' => "{file} is too large, maximum file size is {sizeLimit}.",
                'minSizeError' => "{file} is too small, minimum file size is {minSizeLimit}.",
                'emptyError' => "{file} is empty, please select files again without it.",
                'onLeave' => "The files are being uploaded, if you leave now the upload will be cancelled."
            ),
            'showMessage' => "js:function(message){ alert(message); }"
        )
    ));
    ?>

</form>
<div class="result"></div>
