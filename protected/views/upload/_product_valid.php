<div class="x_panel">
<div class="x_title">
    <h2>Listado de Pedido </h2>
    <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
    </ul>
    <div class="clearfix"></div>
</div>
<div class="x_content">
    Los siguientes pedidos con los estados:
    <ul class="list-group">
        <?php 
        foreach ($Status as $value) { 
            echo BsHtml::tag('li', ['class' => 'list-group-item'],  BsHtml::tag('i',['class' => $value->icon.' '.$value->boton_lcolor],'').' '.$value->name);
        }            
        ?>
        <li class="list-group-item list-group-item-warning">Debe ser cambiado de estado para que se pueda subir el inventario.</li>
    </ul>
    <br />
    <?php
    $this->widget('bootstrap.widgets.BsGridView', array(
        'id' => 'tag-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'type' => BsHtml::GRID_TYPE_RESPONSIVE,
        'columns' => array(
            'orderNumber',
            'client',
            [
                'name'  =>  'status',
                'type'  =>  'raw',
                'value' =>  function($data){
                    return BsHtml::tag('li',['class'=>$data->icon.' btn '.$data->boton_color,'']).' '.$data->status;
                }
            ],
            [
                'name'  =>  'total',
                'value'  =>  function($data){
                    return number_format($data->total, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS);
                },
                'htmlOptions' => [
                        'class' => 'text-right',
                    ],
            ],
            array(
                'template' => '{view}',
                'class' => 'bootstrap.widgets.BsButtonColumn',
                'buttons' => array(
                    'view' => array(
                        'url' => function($data) {
                            return Yii::app()->createUrl("ordDetail/view/", array("id" => $data->orderNumber));
                        },
                        'options' => [
                            'class'     =>  'hidden-xs hidden-sm hidden-md btn btn-primary',
                            'target'    =>  '_blank'
                        ]
                            ),

                        ),
                    ),
        ),
    ));
    ?>
</div>
</div>
<hr>