<?php
/* @var $this TagController */
/* @var $model Tag */
?>

<?php
$this->breadcrumbs=array(
	'Tags'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Tag').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('TagUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('TagUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>