<?php
/* @var $this TagController */
/* @var $model Tag */
?>

<?php
$this->breadcrumbs=array(
	'Tags'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Tag')) ?>

<?php 
if(Yii::app()->user->checkAccess('TagCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('TagCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>