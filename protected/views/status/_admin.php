<?php

if ($options) {

    $this->widget('bootstrap.widgets.BsGridView', array(
        'id' => 'status-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'type' => BsHtml::GRID_TYPE_RESPONSIVE,
        'columns' => array(
            'id',
            'name',
            'description',
            'next_step',
            'next_description',
            'id_sta_group',
            'orden',
            /* 		'r_c_u',
              'r_c_d',
              'r_c_i',
              'r_u_u',
              'r_u_d',
              'r_u_i',
              'r_d_u',
              'r_d_d',
              'r_d_i',
              'r_d_s',
             */
            array(
                'name' => 'r_d_s',
                'value' => '$data->RegistroEstado($data->r_d_s)',
                'filter' => array(0 => 'Inactivo', 1 => 'Activo')
            ),
            array(
                'class' => 'bootstrap.widgets.BsButtonColumn',
            ),
        ),
    ));
} else {
    $this->widget('bootstrap.widgets.BsGridView', array(
        'id' => 'status-grid',
        'dataProvider' => $model->search(),
        'enableSorting' => false,
        'type' => BsHtml::GRID_TYPE_RESPONSIVE,
        'columns' => array(
            [
                'name'  =>  'id',
                'filterHtmlOptions' => [
                    'class' => 'hidden-xs text-mobil  ',
                ],
                'headerHtmlOptions' => [
                    'class' => 'hidden-xs text-mobil ',
                ],
                'htmlOptions' => [
                    'class' => 'hidden-xs text-mobil ',
                ],
            ],
            [
                'name'  =>  'name',
                'type'  =>  'raw',
                'value' =>  function($data){
                    return '<span class="'.$data->color.'">'.BsHtml::tag('i',['class'=>$data->icon]).' '.$data->name.'</span>';
                },
                'filterHtmlOptions' => [
                    'class' => 'text-mobil  ',
                ],
                'headerHtmlOptions' => [
                    'class' => 'text-mobil ',
                ],
                'htmlOptions' => [
                    'class' => 'text-mobil ',
                ],
            ],
            [
                'name'  =>  'description',
                'filterHtmlOptions' => [
                    'class' => 'text-mobil  ',
                ],
                'headerHtmlOptions' => [
                    'class' => 'text-mobil ',
                ],
                'htmlOptions' => [
                    'class' => 'text-mobil ',
                ],
            ],
            [
                'name'  =>  'next_step',
                'filterHtmlOptions' => [
                    'class' => 'text-mobil  ',
                ],
                'headerHtmlOptions' => [
                    'class' => 'text-mobil ',
                ],
                'htmlOptions' => [
                    'class' => 'text-mobil ',
                ],
            ],
            [
                'name'  =>  'next_description',
                'filterHtmlOptions' => [
                    'class' => 'hidden-xs text-mobil  ',
                ],
                'headerHtmlOptions' => [
                    'class' => 'hidden-xs text-mobil ',
                ],
                'htmlOptions' => [
                    'class' => 'hidden-xs text-mobil ',
                ],
            ],
        ),
    ));
}
?>