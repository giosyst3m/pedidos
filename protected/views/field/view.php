<?php
/* @var $this FieldController */
/* @var $model Field */
?>

<?php
$this->breadcrumbs=array(
	'Fields'=>array('index'),
	$model->id,
);

?>

<?php echo BsHtml::pageHeader('View','Field '.$model->id) ?>
<?php 
 $reg = array();
if(Yii::app()->user->checkAccess('FieldViewAuthView')){
    $reg = array(	
						'id',
		'label',
		'value',
		'require',
		'id_fie_type',
		'r_d_s',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
        
    );
}else{
    $reg = array(	
				'id',
		'label',
		'value',
		'require',
		'id_fie_type',
		'r_d_s',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
    );
}
?><?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>$reg
	,
)); ?>

<?php echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => 'history.go(-1)',
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Field/create'),array('class'=>  'btn btn-primary')); ?>