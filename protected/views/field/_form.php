<?php
/* @var $this FieldController */
/* @var $model Field */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'field-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'label',array('maxlength'=>255)); ?>
    <?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6)); ?>
    <?php echo $form->textAreaControlGroup($model,'value',array('rows'=>6)); ?>
    <?php echo $form->dropDownListControlGroup($model,'require',array(1=>'YES',0=>'NO'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick')); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_fie_type',$fieType,array('empty'=>'.::Seleccionar::.','data-style'=>"btn-primary",'class'=>'selectpicker show-tick')); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('FieldCreateStatusChange') || Yii::app()->user->checkAccess('FieldUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('FieldCreateButtonSave') || Yii::app()->user->checkAccess('FieldUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('FieldCreateButtonNew') || Yii::app()->user->checkAccess('FieldUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Field/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
