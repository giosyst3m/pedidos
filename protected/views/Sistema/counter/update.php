<?php
/* @var $this CounterController */
/* @var $model SysCounter */
?>

<?php
$this->breadcrumbs=array(
	'Sys Counters'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','SysCounter').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('SysCounterUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('SysCounterUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>