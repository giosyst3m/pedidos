<?php
/* @var $this ProImagenController */
/* @var $model ProImagen */
?>

<?php
$this->breadcrumbs=array(
	'Pro Imagens'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','ProImagen')) ?>

<?php 
if(Yii::app()->user->checkAccess('ProImagenCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
        'Product'=>$Product
        )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProImagenCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>