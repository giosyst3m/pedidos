<?php
/* @var $this ProImagenController */
/* @var $model ProImagen */
?>

<?php
$this->breadcrumbs=array(
	'Pro Imagens'=>array('create'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','ProImagen').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ProImagenUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
            'Product'=>$Product
            )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProImagenUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>