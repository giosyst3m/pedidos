<?php $index = 1;?>
<?php foreach ($OrdDetatil as $data) {?> 
<tr>
    <td><?php echo $index;?>
    <td><?php echo $this->showImagen($data->idProduct->photo, 'URL_PRODUCTO_IMAGEN', true,30,30); ?></td>
    <td>
        <h6><?php echo BsHtml::bold($data->idProduct->name); ?></h6>
        <h6><?php echo BsHtml::small(Yii::t('app', 'SKU').': '.$data->idProduct->sku);?></h6>
        <h6><?php echo BsHtml::small(Yii::t('app', 'Barcode').': '.$data->idProduct->barcode);?></h6>
    </td>
    <td><?php echo $data->idProduct->idBrand->name;?></td>
    <td class="text-right"><?php echo number_format($data->price,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS);?></td>
    <td class="text-center"><?php echo number_format($data->quantity,0);?></td>
    <td class="text-right"><?php echo number_format($data->price*$data->quantity,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS);?></td>
</tr>
<?php $index++;?>
<?php } ?>

	
