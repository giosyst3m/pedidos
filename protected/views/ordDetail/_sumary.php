<ul class="list-group">
    <li class="list-group-item list-group-item-info">
        <span class="fa fa-usd"></span>
        <?php echo Yii::t('app', 'Sub-Total'); ?>
        <span id="total" class="badge total"><?php echo number_format($orderSumary->sub_total_productos, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></span>
    </li>
    <li class="list-group-item list-group-item-danger">
        <div class="row">
            <div class="col-lg-4">
                <span class="fa fa-minus"></span>
                <?php echo Yii::t('app', 'Discount') . ': ' . $orderSumary->discount_name; ?>
            </div>
            <div class="col-lg-4">
                <?php
                if (Yii::app()->user->getState('ORDER')->ORDER_DISCOUNT_BY_PRODUCTO === 'false' && Yii::app()->user->checkAccess('OrdDetailViewDiscount') && ($orderSumary->id_status == 4 or $orderSumary->id_status == 1) && empty(Yii::app()->user->getState('id_client'))) {
                    echo BsHtml::dropDownList('discount', $orderSumary->id_discount, $Discount, array(
                        'data-style' => "btn-danger",
                        'class' => 'selectpicker show-tick',
                        'onchange' => 'saveDiscount();'
                    ));
                }
                ?>
            </div>
            <div class="col-lg-4 text-right"><span id="discount" class="badge discount"><?php echo number_format($orderSumary->discount, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></span></div>
        </div>
    </li>
    <?php if (Yii::app()->user->getState('ORDER')->ORDER_SHOW_SUB_TOTAL_AFTER_DISCOUNT == 'true') { ?>
        <li class="list-group-item list-group-item-info">
            <span class="fa fa-usd"></span>
            <?php echo Yii::t('app', 'Sub-Total'); ?>
            <span id="discount" class="badge discount"><?php echo number_format($orderSumary->sub_total_menos_descuento, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></span>
        </li>
    <?php } ?>
    <?php
    $delivery = 0;
    foreach ($OrderDelRate as $value) {
        if (Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == 'true') {
            ?>
            <li class="list-group-item list-group-item-success">
                <span class="fa fa-plus"></span>
                <?php echo $value->idDelRate->name . ': ' . $value->idDelRate->idDelivery->name.CHtml::tag('span', array('class' => 'fa fa-times btn btn-danger', 'onclick' => 'deleteDelivery(' . $value->id . ')')); ?>
                <span id="discount" class="badge delivery"><?php echo number_format($value->idDelRate->price, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS) ?></span>
            </li>
        <?php } else { ?>
            <li class="list-group-item list-group-item-success">
                <span class="fa fa-plus"></span>
                <?php echo $value->idDelivery->name.($orderSumary->id_status ==4? ' '.CHtml::tag('span',array('class' => 'fa fa-trash btn btn-danger', 'onclick' => 'deleteDelivery(' . $value->id . ')'),'',true):''); ?>
                <span id="discount" class="badge delivery"><?php echo number_format($value->rate, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS) ?></span>

            </li>  
        <?php
        }
    }
    ?>
        <?php if (Yii::app()->user->getState('ORDER')->ORDER_SHOW_SUB_TOTAL_AFTER_DELIVERY == 'true') { ?>
        <li class="list-group-item list-group-item-info">
            <span class="fa fa-usd"></span>
        <?php echo Yii::t('app', 'Sub-Total'); ?>
            <span id="discount" class="badge discount"><?php echo number_format($orderSumary->sub_total_mas_delivery, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></span>
        </li>
    <?php } ?>
    <?php
    $taxValue = 0;

    foreach ($OrderTax as $value) {
        ?>
        <li class="list-group-item list-group-item-success">
            <span class="fa fa-plus"></span>
            <?php echo Yii::t('app', $value->idTax->name) . ' ' . $value->idTax->tax . '%'; ?>
            <span id="discount" class="badge tax"><?php echo number_format($orderSumary->sub_total_mas_delivery * ($value->idTax->tax / 100), Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS) ?></span>

        </li>
        <?php } ?>
    <li class="list-group-item active">
        <span class="fa fa-usd"></span>
<?php echo Yii::t('app', 'Total'); ?>
        <span id="total_grant" class="badge total_grant"><?php echo number_format($orderSumary->total, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></span>
    </li>
</ul>  


