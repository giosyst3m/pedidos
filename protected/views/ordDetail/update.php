<?php
/* @var $this OrdDetailController */
/* @var $model OrdDetail */
?>

<?php
$this->breadcrumbs=array(
	'Ord Details'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','OrdDetail').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('OrdDetailUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
        'order'=>$order,
        'product'=>$product
            )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('OrdDetailUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>