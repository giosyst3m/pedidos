
<?php echo BsHtml::pageHeader(Yii::t('app', 'Order', array('{numero}' => $Order->number)), BsHtml::bold(Yii::t('app', 'Status') . ' ' . $Order->idStatus->name)); ?>
<table style="width: 100%">
    <tr>
        <th>
            <?php echo Yii::t('app', 'Client') . ': '; ?>
        </th>
        <td>
            <?php echo $Order->idClient->name; ?>
        </td>
        <th>
            <?php echo Yii::t('app', 'City') . ': '; ?>
        </th>
        <td>
            <?php echo $Order->idClient->idCity->name; ?>
        </td>
    </tr>
    <tr>
        <th>
            <?php echo Yii::t('app', 'Address') . ': '; ?>
        </th>
        <td>
            <?php echo $Order->idClient->address; ?>
        </td>
        <th>
            <?php echo Yii::t('app', 'Seller') . ': '; ?>
        </th>
        <td>
            <?php echo $Order->rcu->nombre . ' ' . $Order->rcu->apellido; ?>
        </td>
    </tr>
    <tr>
        <th>
            <?php echo Yii::t('app', 'Phone') . ': '; ?>
        </th>
        <td>
            <?php echo $Order->idClient->phone . ' ' . $Order->idClient->mobil; ?>
        </td>
        <th>
            <?php echo Yii::t('app', 'Code') . ': '; ?>
        </th>
        <td>
            <?php echo $Order->idClient->code ?>
        </td>
        <th>
            <?php echo Yii::t('app', 'Date') . ': '; ?>
        </th>
        <td>
            <?php echo $Order->r_c_d ?>
        </td>
    </tr>
</table>
<br>
<div class="table-responsive table-striped">
    <table class='table' >
        <thead>
            <tr>
                <th>#</th>
                <th><?php echo Yii::t('app', 'Item'); ?></th>
                <th><?php echo Yii::t('app', 'Product'); ?></th>
                <th><?php echo Yii::t('app', 'Brand'); ?></th>
                <th class="text-center"><?php echo Yii::t('app', 'Price'); ?></th>
                <th class="text-center"><?php echo Yii::t('app', 'Quantity'); ?></th>
                <th class="text-right"><?php echo Yii::t('app', 'Total'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            echo $this->renderPartial('_order_pdf', array(
                'OrdDetatil' => $OrdDetatil
            ));
            ?>
            <?php
            echo $this->renderPartial('_sumary_pdf', array(
                'orderSumary' => $orderSumary,
                'OrderTax' => $OrderTax,
                'OrderDelRate' => $OrderDelRate
                    )
            );
            ?> 
        </tbody>
    </table>
</div>                   
