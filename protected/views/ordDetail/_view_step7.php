<?php

if (Yii::app()->user->getState('ORDER')->ORDER_DISCOUNT_BY_PRODUCTO === 'true') {
    $this->widget('ext.LiveGridView.RefreshGridView', array(
        'id' => 'vale-grid',
        'dataProvider' => $model->search(Yii::app()->user->getState('ORDER')->ORDER_NUMBER_RAW_IN_ORDER),
        'updatingTime' => Yii::app()->params['DASHBOARD_ACTUALIZACION_TIEMPOS_VALES'],
        'filter' => $model,
        'beforeAjaxUpdate' => 'js:function(){'
        . ' var btn = $("#refresh-button-vale");'
        . 'btn.button("loading");'
        . '}',
        'afterAjaxUpdate' => 'js:function(){'
        . 'var btn = $("#refresh-button-vale");'
        . 'btn.button("reset");'
        . 'refreshSumary("' . $model->id_order . '");'
        . '}',
        'type' => BsHtml::GRID_TYPE_RESPONSIVE,
        'columns' => array(
            array(
                'header' => Yii::t('app', 'Qty'),
                'value' => '$row+1',
            ),
            array(
                'header' => Yii::t('app', 'Photo'),
                'type' => 'html',
                'value' => function($data) {
                    return $data->ShowImagen($data->idProduct->photo, Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH);
                },
                'headerHtmlOptions' => array('style' => 'width:10%;'),
            ),
            array(
                'header' => Yii::t('app', 'Code'),
                'value' => '$data->idProduct->barcode',
            ),
            array(
                'header' => Yii::t('app', 'SKU'),
                'type' => 'raw',
                'value' => function ($data) {
                    return BsHtml::link($data->idProduct->sku, $this->createUrl('Product/view', array('id' => $data->idProduct->id)), array('target' => '_blank'));
                }
                    ),
                    array(
                        'header' => Yii::t('app', 'Descripcion'),
                        'type' => 'html',
                        'value' => '"<b>".$data->idProduct->name."</b><br/>".$data->idProduct->description',
                        'headerHtmlOptions' => array('style' => 'width:30%;'),
                    ),
                    array(
                        'header' => Yii::t('app', 'Brand'),
                        'value' => '$data->idProduct->idBrand->name',
                    ),
                    array(
//                     'name'=>'price',
                        'header' => Yii::t('app', 'Price'),
                        'value' => 'number_format($data->price,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)',
                        'htmlOptions' => array(
                            'class' => 'text-right'
                        ),
                    ),
                    array(
                        'header' => Yii::t('app', 'Discount'),
                        'value' => '$data->idDiscount->name',
                        'htmlOptions' => array(
                            'class' => 'text-right'
                        ),
                    ),
                    array(
                        'header' => Yii::t('app', 'Quantity'),
                        'type' => 'raw',
                        'value' => function($data) {
                            return $data->quantity;
                        },
                    ),
                    array(
                        'header' => Yii::t('app', 'Request'),
                        'type' => 'raw',
                        'value' => function($data) {
                            return $data->request;
                        },
                    ),
                    array(
                        'header' => Yii::t('app', 'Total'),
                        'value' => 'number_format(($data->price -($data->price * ($data->idDiscount->discount)/100))*$data->quantity,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)',
                        'htmlOptions' => array(
                            'class' => 'text-right'
                        ),
                    ),
                ),
            ));
        } else {
            $this->widget('ext.LiveGridView.RefreshGridView', array(
                'id' => 'vale-grid',
                'dataProvider' => $model->search(Yii::app()->user->getState('ORDER')->ORDER_NUMBER_RAW_IN_ORDER),
                'updatingTime' => Yii::app()->params['DASHBOARD_ACTUALIZACION_TIEMPOS_VALES'],
                'filter' => $model,
                'beforeAjaxUpdate' => 'js:function(){'
                . ' var btn = $("#refresh-button-vale");'
                . 'btn.button("loading");'
                . '}',
                'afterAjaxUpdate' => 'js:function(){'
                . 'var btn = $("#refresh-button-vale");'
                . 'btn.button("reset");'
                . 'refreshSumary("' . $model->id . '");'
                . '}',
                'type' => BsHtml::GRID_TYPE_RESPONSIVE,
                'columns' => array(
                    [
                        'header' => Yii::t('app', 'Photo'),
                        'type' => 'html',
                        'value' => function($data) {
                            return $data->ShowImagen($data->photo, Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH,50,50).
                                $data->barcode.'<br>'.
                                $data->sku.'<br>'.
                                $data->brand.'<br>'.
                                (!empty($data->code_alternative_2)?'<span class="label label-success">'.$data->code_alternative_2.'</span>':'').'<br>'.
                                (!empty($data->code_alternative_3)?'<span class="label label-danger">'.$data->code_alternative_3.'</span>':'');
                        },
                        'filterHtmlOptions' => [
                            'class' => 'hidden-lg hidden-md hidden-sm',
                        ],
                        'headerHtmlOptions' => [
                            'class' => 'hidden-lg hidden-md hidden-sm text-mobil',
                        ],
                        'htmlOptions' => [
                            'class' => 'hidden-lg hidden-md hidden-sm text-mobil',
                            'style' => 'width:10%;'
                        ],
                    ],
                    [
                        'header' => Yii::t('app', 'Nombre'),
                        'type' => 'html',
                        'value' => function($data) {
                            return 
                                $data->name;
                        },
                        'filterHtmlOptions' => [
                            'class' => 'hidden-lg hidden-md hidden-sm',
                        ],
                        'headerHtmlOptions' => [
                            'class' => 'hidden-lg hidden-md hidden-sm text-mobil',
                        ],
                        'htmlOptions' => [
                            'class' => 'hidden-lg hidden-md hidden-sm text-mobil',
                            'style' => 'width:10%;'
                        ],
                    ],
                    array(
                        'header' => Yii::t('app', 'Qty'),
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                        'filterHtmlOptions' => [
                            'class' => 'hidden-xs  ',
                        ],
                        'headerHtmlOptions' => [
                            'class' => 'hidden-xs  ',
                        ],
                        'htmlOptions' => [
                            'class' => 'hidden-xs  ',
                        ],
                    ),
                    array(
                        'header' => Yii::t('app', 'Photo'),
                        'type' => 'html',
                        'value' => function($data) {
                            return $data->ShowImagen($data->photo, Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH);
                        },
                        'filterHtmlOptions' => [
                            'class' => 'hidden-xs  ',
                        ],
                        'headerHtmlOptions' => [
                            'class' => 'hidden-xs  ',
                        ],
                        'htmlOptions' => [
                            'class' => 'hidden-xs  ',
                            'style' => 'width:10%;'
                        ],
                    ),
                    array(
                        'header' => Yii::t('app', 'Code'),
                        'type' => 'raw',
                        'value' => function($data){
                            return $data->barcode.'<br>'.
                                    (!empty($data->code_alternative_2)?'<span class="label label-success">'.$data->code_alternative_2.'</span>':'').'<br>'.
                                    (!empty($data->code_alternative_3)?'<span class="label label-danger">'.$data->code_alternative_3.'</span>':'');
                        },
                        'filterHtmlOptions' => [
                            'class' => 'hidden-xs  ',
                        ],
                        'headerHtmlOptions' => [
                            'class' => 'hidden-xs  ',
                        ],
                        'htmlOptions' => [
                            'class' => 'hidden-xs  ',
                        ],
                    ),
                    array(
                        'header' => Yii::t('app', 'SKU'),
                        'type' => 'raw',
                        'value' => function ($data) {
                            return BsHtml::link($data->sku, $this->createUrl('Product/view', array('id' => $data->id_product)), array('target' => '_blank'));
                        },
                        'filterHtmlOptions' => [
                            'class' => 'hidden-xs  ',
                        ],
                        'headerHtmlOptions' => [
                            'class' => 'hidden-xs  ',
                        ],
                        'htmlOptions' => [
                            'class' => 'hidden-xs  ',
                        ],
                            ),
                            array(
                                'header' => Yii::t('app', 'Descripcion'),
                                'type' => 'html',
                                'value' => '"<b>".$data->name."</b><br/>".$data->description',
                                'filterHtmlOptions' => [
                                    'class' => 'hidden-xs  ',
                                ],
                                'headerHtmlOptions' => [
                                    'class' => 'hidden-xs  ',
                                ],
                                'htmlOptions' => [
                                    'class' => 'hidden-xs  ',
                                    'style' => 'width:30%;'
                                ],
                            ),
                            array(
                                'header' => Yii::t('app', 'Brand'),
                                'value' => '$data->brand',
                                'filterHtmlOptions' => [
                                    'class' => 'hidden-xs  ',
                                ],
                                'headerHtmlOptions' => [
                                    'class' => 'hidden-xs  ',
                                ],
                                'htmlOptions' => [
                                    'class' => 'hidden-xs  ',
                                ],
                            ),
                            array(
                                'header' => Yii::t('app', 'Price'),
                                'value' => 'number_format($data->ord_detail_price,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)',
                                'filterHtmlOptions' => [
                                    'class' => 'hidden-xs  ',
                                ],
                                'headerHtmlOptions' => [
                                    'class' => 'hidden-xs  ',
                                ],
                                'htmlOptions' => [
                                    'class' => 'hidden-xs  text-right',
                                ],
                            ),
                            array(
                                'header' => Yii::t('app', 'Quantity'),
                                'type' => 'raw',
                                'value' => function($data) {
                                    return $data->ord_detail_quantity;
                                },
                                'filterHtmlOptions' => [
                                    'class' => '  ',
                                ],
                                'headerHtmlOptions' => [
                                    'class' => ' text-mobil ',
                                ],
                                'htmlOptions' => [
                                    'class' => ' text-right',
                                ],
                            ),
                            array(
                                'header' => Yii::t('app', 'Total'),
                                'value' => 'number_format(($data->ord_detail_price -($data->ord_detail_price * ($data->discount)/100))*$data->ord_detail_quantity,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)',
                                 'filterHtmlOptions' => [
                                    'class' => '  ',
                                ],
                                'headerHtmlOptions' => [
                                    'class' => ' text-mobil ',
                                ],
                                'htmlOptions' => [
                                    'class' => ' text-right',
                                ],
                            ),
                        ),
                    ));
                }
?>