<?php
/* @var $this OrdDetailController */
/* @var $model OrdDetail */
?>

<?php
$this->breadcrumbs=array(
	'Ord Details'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','OrdDetail')) ?>

<?php 
if(Yii::app()->user->checkAccess('OrdDetailCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
        'order'=>$order,
        'product'=>$product
        )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('OrdDetailCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>