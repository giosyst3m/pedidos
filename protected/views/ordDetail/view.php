<?php
/* @var $this OrdDetailController */
/* @var $model OrdDetail */
?>

<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/ordDetail/view.css');
$this->breadcrumbs = array(
    'Ord Details' => array('index'),
    $orderSumary->number,
);

$param = array('post' => $orderSumary);
if (Yii::app()->user->checkAccess('OrdDetailViewAll') OR Yii::app()->user->checkAccess('OrdDetailViewOwn', $param)) {
    $status = $orderSumary->status;
    $statusID = $orderSumary->id_status;
    //$next_step = $model->idOrder->idStatus->next_step;
    //$next_description = $model->idOrder->idStatus->next_description;
    echo CHtml::hiddenField('id_order', $orderSumary->id);
    echo CHtml::hiddenField('id_status', $statusID);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/ordDetail/view.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/catalog/index.js', CClientScript::POS_END);
    ?>

    <div class="x_panel">
        <div class="x_title">
            <h2><?php echo $orderSumary->client; ?></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    <?php echo BsHtml::tag('h2',['class'=>'hidden-lg hidden-md'],$orderSumary->client); ?>
                    <?php echo BsHtml::tag('h4', [], Yii::t('app', 'NIT') . ' ' . $VClient->code . '| <span class="badge badge-important">' . $VClient->correlative . '</span>'); ?>
                    <?php echo BsHtml::tag('h5', [], $VClient->address); ?>
                    <h4><?php echo BsHtml::label($VClient->city . ', ' . $VClient->stade . ', ' . $VClient->country, $VClient->city, ['class' => 'label label-primary']); ?></h4>
                    <h4><?php echo BsHtml::label($VClient->zone, $VClient->zone, ['class' => 'label label-warning']); ?></h4>
                    <h5><?php echo Yii::t('app', 'Phone') . '  ' . $VClient->phone . ' ' . $VClient->mobil ?></h5>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <?php
                    echo BsHtml::tag('h2', [], Yii::t('app', 'Order Number') . ' ' .
                            $orderSumary->number . ' ' . Yii::t('app', 'Status') .
                            ' <span class="' . $orderSumary->color . '">' .
                            $status . '</span>', true);
                    ?>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                            
                                    <?php echo BsHtml::image(Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . $VSisUsuario->photo,'..',['class'=>'img-circle profile_img']); ?>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                            <?php echo $VSisUsuario->complete_name . '<br>' . BsHtml::tag('br', [], $orderSumary->r_c_d); ?>  
                        </div>
                    </div>          
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <?php if ($statusID < Yii::app()->user->getState('ORDER')->ORDER_ADD_MORE_PRODUCT_STATUS_MINOR) { ?>
                        <a class="btn btn-info btn-block btn-xs text-mobil" href="<?php echo $this->createUrl('Catalog/index', ['id' => $orderSumary->clientId, 'order' => $model->id]); ?>"><i class="fa fa-shopping-cart"></i> <?php echo Yii::t('app', 'Add more product from Stantdar Catalog'); ?></a>
                        <a class="btn btn-info btn-block btn-xs text-mobil" href="<?php echo $this->createUrl('Catalog/QuickCatalog', ['id' => $orderSumary->clientId, 'order' => $model->id]); ?>"><i class="fa fa-shopping-cart"></i> <?php echo Yii::t('app', 'Add more product from Basic Catalog'); ?></a>
                        <a class="btn btn-info btn-block btn-xs text-mobil" href="<?php echo $this->createUrl('Catalog/Simple', ['id' => $orderSumary->clientId, 'order' => $model->id]); ?>"><i class="fa fa-shopping-cart"></i> <?php echo Yii::t('app', 'Add more product from Codebare Catalog'); ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="wizard">
        <div class="wizard-inner">
            <ul class="nav nav-tabs" role="tablist">

                <li role="presentation" class="active">
                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="<?php echo Yii::t('app', 'Order'); ?>">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-shopping-cart"></i>
                        </span>
                    </a>
                </li>

                <li role="presentation">
                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="<?php echo Yii::t('app', 'Documents'); ?>">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-tasks"></i>
                        </span>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="<?php echo Yii::t('app', 'Comments'); ?>">
                        <span class="round-tab">
                            <i class="glyphicon glyphicon-comment"></i>
                        </span>
                    </a>
                </li>

                <li role="presentation">
                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="<?php echo Yii::t('app', 'Info'); ?>">
                        <span class="round-tab">
                            <i class="fa fa-info-circle"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </div>


        <div class="tab-content">
            <div class="tab-pane active" role="tabpanel" id="step1">
                <?php
                $this->renderPartial('_order', array(
                    'status' => $status,
                    'statusID' => $statusID,
                    'model' => $model,
                    'orderSumary' => $orderSumary,
                    'OrderTax' => $OrderTax,
                    'OrderDelRate' => $OrderDelRate,
                    'Discount' => $Discount,
                    'Delivery' => $Delivery
                ))
                ?>
            </div>
            <div class="tab-pane" role="tabpanel" id="step2">
                <?php $this->renderPartial('../order/_document', array('model' => $model2)); ?>
            </div>
            <div class="tab-pane" role="tabpanel" id="step3">
                <?php
                if ($statusID <= 5) {
                    $this->renderPartial('../comment/_form', array('model' => $model3, 'ComPriority' => $ComPriority));
                }
                ?>
                <?php $this->renderPartial('../comment/admin', array('model2' => $VCommentOrder)); ?>
            </div>
            <div class="tab-pane" role="tabpanel" id="complete">
                <?php $this->renderPartial('../status/_admin', array('model' => $model4, 'options' => false)); ?>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>
    <?php
} else {
    echo BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('json', 5011, array('{numero}' => $model->id)));
}
?>
