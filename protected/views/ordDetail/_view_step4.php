<?php

if (Yii::app()->user->getState('ORDER')->ORDER_DISCOUNT_BY_PRODUCTO === 'true') {
    $this->widget('ext.LiveGridView.RefreshGridView', array(
        'id' => 'vale-grid',
        'dataProvider' => $model->search(Yii::app()->user->getState('ORDER')->ORDER_NUMBER_RAW_IN_ORDER),
        'updatingTime' => Yii::app()->params['DASHBOARD_ACTUALIZACION_TIEMPOS_VALES'],
        'filter' => $model,
        'beforeAjaxUpdate' => 'js:function(){'
        . ' var btn = $("#refresh-button-vale");'
        . 'btn.button("loading");'
        . '}',
        'afterAjaxUpdate' => 'js:function(){'
        . 'var btn = $("#refresh-button-vale");'
        . 'btn.button("reset");'
        . 'refreshSumary("' . $model->id_order . '");'
        . '}',
        'type' => BsHtml::GRID_TYPE_RESPONSIVE,
        'columns' => array(
            array(
                'header' => Yii::t('app', 'Qty'),
                'value' => '$row+1',
            //'headerHtmlOptions' => array('style' => 'width:40px;'),
            ),
            array(
                'header' => Yii::t('app', 'Photo'),
                'type' => 'html',
                'value' => function($data) {
                    return $data->ShowImagen($data->idProduct->photo, Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH);
                },
                'headerHtmlOptions' => array('style' => 'width:10%;'),
            ),
            array(
                'header' => Yii::t('app', 'Code'),
                'type' => 'raw',
                'value' => function($data) {
                    if (Yii::app()->user->getState('ORDER')->ORDER_CODE_ALTERNATIVE_1_SHOW_GRID == 'true') {
                        return $data->idProduct->barcode . '<br>' . BsHtml::tag('b', array(), Yii::t('app', 'code_alternative_1'), true) . '<br>' . $data->idProduct->code_alternative_1;
                    } else {
                        return $data->idProduct->barcode;
                    }
                },
                    ),
                    array(
                        'header' => Yii::t('app', 'SKU'),
                        'type' => 'raw',
                        'value' => function ($data) {
                            return BsHtml::link($data->idProduct->sku, $this->createUrl('Product/view', array('id' => $data->idProduct->id)), array('target' => '_blank'));
                        }
                            ),
                            array(
                                'header' => Yii::t('app', 'Descripcion'),
                                'type' => 'html',
                                'value' => '"<b>".$data->idProduct->name."</b><br/>".$data->idProduct->description',
                                'headerHtmlOptions' => array('style' => 'width:30%;'),
                            ),
                            array(
                                'header' => Yii::t('app', 'Brand'),
                                'value' => '$data->idProduct->idBrand->name',
                            ),
                            array(
//                     'name'=>'price',
                                'header' => Yii::t('app', 'Price'),
                                'value' => 'number_format($data->price,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)',
                                'htmlOptions' => array(
                                    'class' => 'text-right'
                                ),
                            ),
                            array(
                                'header' => Yii::t('app', 'Discount'),
                                'value' => '$data->idDiscount->name',
                                'htmlOptions' => array(
                                    'class' => 'text-right'
                                ),
                            ),
                            array(
                                'header' => Yii::t('app', 'Quantity'),
                                'type' => 'raw',
                                'value' => function($data) {
                                    return $data->quantity;
                                },
                            ),
                            array(
                                'header' => Yii::t('app', 'Approved'),
                                'type' => 'raw',
                                'value' => function($data) {
                                    return CHtml::numberField($data->id, $data->quantity, array('class' => 'text-right', 'style' => 'width:50px', 'min' => 1)) .
                                            CHtml::tag('span', array('class' => 'fa fa-save btn btn-primary', 'onclick' => 'ApprovedRequest(' . $data->id . ')'));
                                },
                                    ),
                                    array(
                                        'header' => Yii::t('app', 'Total'),
                                        'value' => 'number_format(($data->price -($data->price * ($data->idDiscount->discount)/100))*$data->quantity,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)',
                                        'htmlOptions' => array(
                                            'class' => 'text-right'
                                        ),
                                    ),
                                ),
                            ));
                        } else {
                            $this->widget('ext.LiveGridView.RefreshGridView', array(
                                'id' => 'vale-grid',
                                'dataProvider' => $model->search(Yii::app()->user->getState('ORDER')->ORDER_NUMBER_RAW_IN_ORDER),
                                'updatingTime' => Yii::app()->params['DASHBOARD_ACTUALIZACION_TIEMPOS_VALES'],
                                'filter' => $model,
                                'beforeAjaxUpdate' => 'js:function(){'
                                . ' var btn = $("#refresh-button-vale");'
                                . 'btn.button("loading");'
                                . '}',
                                'afterAjaxUpdate' => 'js:function(){'
                                . 'var btn = $("#refresh-button-vale");'
                                . 'btn.button("reset");'
                                . 'refreshSumary("' . $model->id . '");'
                                . '}',
                                'type' => BsHtml::GRID_TYPE_RESPONSIVE,
                                'columns' => array(
                                    [
                                        'header' => Yii::t('app', 'Photo'),
                                        'type' => 'html',
                                        'value' => function($data) {
                                            return $data->ShowImagen($data->photo, Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH,50,50).
                                                $data->barcode.'<br>'.
                                                $data->sku.'<br>'.
                                                $data->brand.'<br>'.
                                                (!empty($data->code_alternative_2)?'<span class="label label-success">'.$data->code_alternative_2.'</span>':'').'<br>'.
                                                (!empty($data->code_alternative_3)?'<span class="label label-danger">'.$data->code_alternative_3.'</span>':'');
                                        },
                                        'filterHtmlOptions' => [
                                            'class' => 'hidden-lg hidden-md hidden-sm',
                                        ],
                                        'headerHtmlOptions' => [
                                            'class' => 'hidden-lg hidden-md hidden-sm text-mobil',
                                        ],
                                        'htmlOptions' => [
                                            'class' => 'hidden-lg hidden-md hidden-sm text-mobil',
                                            'style' => 'width:10%;'
                                        ],
                                    ],
                                    [
                                        'header' => Yii::t('app', 'Nombre'),
                                        'type' => 'html',
                                        'value' => function($data) {
                                            return 
                                                $data->name;
                                        },
                                        'filterHtmlOptions' => [
                                            'class' => 'hidden-lg hidden-md hidden-sm',
                                        ],
                                        'headerHtmlOptions' => [
                                            'class' => 'hidden-lg hidden-md hidden-sm text-mobil',
                                        ],
                                        'htmlOptions' => [
                                            'class' => 'hidden-lg hidden-md hidden-sm text-mobil',
                                            'style' => 'width:10%;'
                                        ],
                                    ],
                                    array(
                                        'header' => Yii::t('app', 'Qty'),
                                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                                        'filterHtmlOptions' => [
                                            'class' => 'hidden-xs  ',
                                        ],
                                        'headerHtmlOptions' => [
                                            'class' => 'hidden-xs  ',
                                        ],
                                        'htmlOptions' => [
                                            'class' => 'hidden-xs  ',
                                        ],
                                    ),
                                    array(
                                        'header' => Yii::t('app', 'Photo'),
                                        'type' => 'html',
                                        'value' => function($data) {
                                            return $data->ShowImagen($data->photo, Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH);
                                        },
                                        'filterHtmlOptions' => [
                                            'class' => 'hidden-xs  ',
                                        ],
                                        'headerHtmlOptions' => [
                                            'class' => 'hidden-xs  ',
                                        ],
                                        'htmlOptions' => [
                                            'class' => 'hidden-xs  ',
                                            'style' => 'width:10%;'
                                        ],
                                    ),
                                    array(
                                        'header' => Yii::t('app', 'Code'),
                                        'type' => 'raw',
                                        'value' => function($data) {
                                            if (Yii::app()->user->getState('ORDER')->ORDER_CODE_ALTERNATIVE_1_SHOW_GRID == 'true') {
                                                return $data->barcode . '<br>' . BsHtml::tag('b', array(), Yii::t('app', 'code_alternative_1'), true) . '<br>' . $data->code_alternative_1.'<br>'.
                                                        (!empty($data->code_alternative_2)?'<span class="label label-success">'.$data->code_alternative_2.'</span>':'').'<br>'.
                                                    (!empty($data->code_alternative_3)?'<span class="label label-danger">'.$data->code_alternative_3.'</span>':'');
                                            } else {
                                                return $data->barcode.'<br>'.
                                                    (!empty($data->code_alternative_2)?'<span class="label label-success">'.$data->code_alternative_2.'</span>':'').'<br>'.
                                                    (!empty($data->code_alternative_3)?'<span class="label label-danger">'.$data->code_alternative_3.'</span>':'');
                                            }
                                        },
                                        'filterHtmlOptions' => [
                                            'class' => 'hidden-xs  ',
                                        ],
                                        'headerHtmlOptions' => [
                                            'class' => 'hidden-xs  ',
                                        ],
                                        'htmlOptions' => [
                                            'class' => 'hidden-xs  ',
                                        ],
                                            ),
                                            array(
                                                'header' => Yii::t('app', 'SKU'),
                                                'type' => 'raw',
                                                'value' => function ($data) {
                                                    return BsHtml::link($data->sku, $this->createUrl('Product/view', array('id' => $data->id_product)), array('target' => '_blank'));
                                                },
                                                'filterHtmlOptions' => [
                                                    'class' => 'hidden-xs  ',
                                                ],
                                                'headerHtmlOptions' => [
                                                    'class' => 'hidden-xs  ',
                                                ],
                                                'htmlOptions' => [
                                                    'class' => 'hidden-xs  ',
                                                ],
                                                    ),
                                                    array(
                                                        'header' => Yii::t('app', 'Descripcion'),
                                                        'type' => 'html',
                                                        'value' => '"<b>".$data->name."</b><br/>".$data->description',
                                                        'filterHtmlOptions' => [
                                                            'class' => 'hidden-xs  ',
                                                        ],
                                                        'headerHtmlOptions' => [
                                                            'class' => 'hidden-xs  ',
                                                        ],
                                                        'htmlOptions' => [
                                                            'class' => 'hidden-xs  ',
                                                            'style' => 'width:30%;'
                                                        ],
                                                    ),
                                                    array(
                                                        'header' => Yii::t('app', 'Brand'),
                                                        'value' => '$data->brand',
                                                        'filterHtmlOptions' => [
                                                            'class' => 'hidden-xs  ',
                                                        ],
                                                        'headerHtmlOptions' => [
                                                            'class' => 'hidden-xs  ',
                                                        ],
                                                        'htmlOptions' => [
                                                            'class' => 'hidden-xs  ',
                                                        ],
                                                    ),
                                                    array(
                                                        'header' => Yii::t('app', 'Price'),
                                                        'value' => 'number_format($data->ord_detail_price,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)',
                                                        'filterHtmlOptions' => [
                                                            'class' => 'hidden-xs  ',
                                                        ],
                                                        'headerHtmlOptions' => [
                                                            'class' => 'hidden-xs  ',
                                                        ],
                                                        'htmlOptions' => [
                                                            'class' => 'hidden-xs text-right' ,
                                                        ],
                                                    ),
                                                    array(
                                                        'header' => Yii::t('app', 'Ext'),
                                                        'type' => 'raw',
                                                        'value' => function($data) {
                                                            $html = $data->ord_detail_quantity;
                                                            $html .= BsHtml::tag('br');
                                                            $ext = VProduct::model()->find('id=' . $data->id_product)->quantity;
                                                            if ($ext > 0) {
                                                                $html .=  BsHtml::label(Yii::t('app', 'Ext') . ' ' . $ext, '', ['class' => 'label label-success']);
                                                            } else {
                                                                $html .= BsHtml::label(Yii::t('app', 'Ext') . ' ' . $ext, '', ['class' => 'label label-danger']);
                                                            }
                                                            return $html.'</br></br>'.
                                                                CHtml::tag('span', array('class' => 'fa fa-trash btn btn-danger hidden-lg hidden-md hidden-sm', 'onclick' => 'deleteOrdDetatil(' . $data->ord_detail_id . ')'));
                                                        },
                                                        'filterHtmlOptions' => [
                                                            'class' => '',
                                                        ],
                                                        'headerHtmlOptions' => [
                                                            'class' => 'text-mobil',
                                                        ],
                                                        'htmlOptions' => [
                                                            'class' => 'text-mobil text-right',
                                                        ],
                                                    ),
                                                            array(
                                                                'header' => Yii::t('app', 'Despachar'),
                                                                'type' => 'raw',
                                                                'value' => function($data) {
                                                                    return BsHtml::tag('b',['class'=>'hidden-lg hidden-md hidden-sm'],number_format($data->ord_detail_price,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)).'<br>'.
                                                                            CHtml::numberField($data->ord_detail_id, $data->ord_detail_quantity, array('class' => 'text-right', 'style' => 'width:50px;border: solid 1px','min'=>1,'id'=>'req-'.$data->ord_detail_id)) .
                                                                            CHtml::hiddenField($data->ord_detail_id, $data->ord_detail_quantity, array('id'=>'qty-'.$data->ord_detail_id)) .
                                                                            CHtml::tag('i', array('class' => 'botones fa fa-save btn btn-'.($data->inv_in >0?'success btn-inv':'primary'), 'id'=>'btn-'.$data->ord_detail_id, 'data-loading-text'=>' <i class="fa fa-spinner fa-spin"></i>' , 'onclick' => 'ApprovedRequest(' . $data->ord_detail_id . ')'),'',true).
                                                                            BsHtml::tag('span',['class'=>'hidden-lg hidden-md hidden-sm'],number_format(($data->ord_detail_price -($data->ord_detail_price * ($data->discount)/100))*$data->ord_detail_quantity,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS));
                                                                },
                                                                'filterHtmlOptions' => [
                                                                    'class' => ' ',
                                                                ],
                                                                'headerHtmlOptions' => [
                                                                    'class' => ' text-mobil',
                                                                ],
                                                                'htmlOptions' => [
                                                                    'class' => ' text-mobil',
                                                                ],
                                                            ),
                                                                    array(
                                                                        'header' => Yii::t('app', 'Total'),
                                                                        'value' => 'number_format(($data->ord_detail_price -($data->ord_detail_price * ($data->discount)/100))*$data->ord_detail_quantity,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)',
                                                                         'filterHtmlOptions' => [
                                                                            'class' => 'hidden-xs  ',
                                                                        ],
                                                                        'headerHtmlOptions' => [
                                                                            'class' => 'hidden-xs text-mobil ',
                                                                        ],
                                                                        'htmlOptions' => [
                                                                            'class' => 'hidden-xs text text-right',
                                                                        ],
                                                                    ),
                                                                        array(
                                                                'header' => Yii::t('app', ''),
                                                                'type' => 'raw',
                                                                'value' => function($data) {
                                                                    return CHtml::tag('i', array('class' => 'fa fa-trash btn btn-danger', 'onclick' => 'deleteOrdDetatil(' . $data->ord_detail_id . ')'));
                                                                },
                                                                        'filterHtmlOptions' => [
                                                                    'class' => ' ',
                                                                ],
                                                                'headerHtmlOptions' => [
                                                                    'class' => 'hidden-xs text-mobil',
                                                                ],
                                                                'htmlOptions' => [
                                                                    'class' => 'hidden-xs text-mobil',
                                                                ],
                                                                    ),
                                                                ),
                                                            ));
                                                        }
?>