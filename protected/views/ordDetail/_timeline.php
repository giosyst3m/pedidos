<br>
<?php if (!empty($VCommentOrder)) { ?>
    <?php foreach ($VCommentOrder as $value) { ?>
        <div class="qa-message-list" id="wallmessages">
            <div class="message-item primary" id="m16">
                <div class="message-inner">
                    <div class="message-head clearfix">
                        <div class="avatar pull-left"><?php echo BsHtml::imageCircle(Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . $value->photo); ?></div>
                        <div class="user-detail">    
                            <h2><label class="label label-<?php echo $value->color; ?>"><?php echo $value->complete_name ?></label>&nbsp;<?php echo $value->priority; ?></h2>
                            <div class="post-meta">
                                <div class="asker-meta">
                                    <span class="qa-message-what"></span>
                                    <span class="qa-message-when">  
                                        <h5><span class="qa-message-when-data"><span class="fa fa-calendar-o"></span>&nbsp;<?php echo $value->r_c_d; ?></span></h5>
                                    </span>
                                    <span class="qa-message-who">
                                        <span class="qa-message-who-data"></span>
                                        <?php
                                        if (!empty($value->file)) {
                                            echo BsHtml::link(BsHtml::icon(BsHtml::GLYPHICON_DOWNLOAD) . ' Download', Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('COMMENT')->COMMENT_ORDER_FILE_PATH . $value->file, array('target' => '_blank','class'=>'btn btn-'.$value->color));
                                        }
                                        ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="qa-message-content">
                        <div class="offer offer-<?php echo $value->color; ?>">
                            <div class="shape">
                                <div class="shape-text">
                                    <?php echo $value->number; ?>								
                                </div>
                            </div>
                            <div class="offer-content">
                                <h3 class="lead">
                                    <?php echo $value->title; ?>

                                </h3>
                                <p>
                                    <?php echo $value->message; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>  
    <?php
}?>