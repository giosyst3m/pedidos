<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <a href="<?php echo Yii::app()->request->urlReferrer ?>" class="btn btn-info"><span class="fa fa-arrow-left"></span><?php echo Yii::t('app', 'Back') ?></a>
                <button id="refresh-button-vale" class="btn btn-info" data-loading-text="Actualizando"><i class="fa fa-refresh"></i> <?php echo Yii::t('app', 'Update'); ?></button>
                <?php if ($statusID != 7) { ?>
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('OrdDetail/pdf', array('id' => $model->id)); ?> " target="_blank" class="btn btn-info"><i class="fa fa-file-pdf-o"></i></a>
                <?php } ?>
                <?php if (($statusID == 1 OR $statusID == 9) && Yii::app()->user->checkAccess('OrderStep1')) { ?>
                    <button id="nextOrder" class="btn btn-success botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>" data-value="<?php echo $model->id; ?>"><i class="fa fa-send"></i> <?php echo Yii::t('app', 'Notify and Create New Order'); ?></button>
                    <button id="deleteOrder" class="btn btn-danger botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>"  data-value="<?php echo $model->id; ?>"><i class="fa fa-close"></i> <?php echo Yii::t('app', 'Delete'); ?></button>
                <?php } elseif ($statusID == 2 && Yii::app()->user->checkAccess('OrderStep2')) { ?>
                    <button id="approvedOrder" class="btn btn-success botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>"  data-value="<?php echo $model->id; ?>"><i class="fa fa-check"></i> <?php echo Yii::t('app', 'Approved'); ?></button>
                    <button id="rejectedOrder" class="btn btn-danger botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>"  data-value="<?php echo $model->id; ?>"><i class="fa fa-close"></i> <?php echo Yii::t('app', 'Rejected'); ?></button>
                    <label for="rejectedNote" class="label label-important botones"><?php echo Yii::t('app', 'Note'); ?></label>                        
                    <input type="text" name="" id="rejectedNote" class="text text-primary " maxlength="255" style="width: 50%">
                <?php } elseif ($statusID == 3 && Yii::app()->user->checkAccess('OrderStep3')) { ?>
                            <label for="preDelivery_note" class="label label-important"><?php echo Yii::t('app', 'Note'); ?></label>
                            <input type="text" name="preDelivery_note" id="preDelivery_note" class="text text-primary " maxlength="255">
                            <button id="preDelivery" class="btn btn-success botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>"  data-value="<?php echo $model->id; ?>"><i class="fa fa-check"></i> <?php echo Yii::t('app', 'Next'); ?></button>
                            <button id="deleteOrder2" class="btn btn-danger botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>"  data-value="<?php echo $model->id; ?>"><i class="fa fa-close"></i> <?php echo Yii::t('app', 'Delete'); ?></button>    
                    </div>
                <?php } elseif ($statusID == 4 && Yii::app()->user->checkAccess('OrderStep4')) { ?>
                    <button id="deleteOrder2" class="btn btn-danger botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>"  data-value="<?php echo $model->id; ?>"><i class="fa fa-close"></i> <?php echo Yii::t('app', 'Delete'); ?></button>    
                    <div class="row">
                        <div class="col-lg-2 col-xs-6">
                            <label for="delivery" class="label label-important"><?php echo Yii::t('app', 'Delivery'); ?></label>
                            <?php
                            echo BsHtml::dropDownList('delivery', null, $Delivery, array(
                                'data-style' => "btn-info",
                                'class' => 'selectpicker show-tick fDelivery',
                                'empty' => 'Seleccionar',
                                'data-live-search' => true,
                            ));
                            ?>        
                        </div>
                        <?php if (Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == 'false') { ?>
                            <div class="col-lg-2 col-xs-6">
                                <label for="price" class="label label-important"><?php echo Yii::t('app', 'Price'); ?></label>
                                <input type="number" name="price" id="price" class="text text-primary text-right fDelivery" maxlength="100" style="width: 100%">
                                <br>
                                <br>
                            </div>
                        <?php } ?>
                        <div class="col-lg-2 col-xs-6">
                            <label for="guide" class="label label-important"><?php echo Yii::t('app', 'Guide Nro.'); ?></label>
                            <input type="text" name="guide" id="guide" class="text text-primary fDelivery" maxlength="100" style="width: 100%">
                        </div>
                        <div class="col-lg-2 col-xs-6">
                            <label for="guide_note" class="label label-important"><?php echo Yii::t('app', 'Date'); ?></label>
                            <input type="date" name="guide_fecha" id="guide_fecha" class="text text-primary  fDelivery" style="width: 100%">
                        </div>
                        <div class="col-lg-2 col-xs-12">
                            <label for="guide_note" class="label label-important"><?php echo Yii::t('app', 'Note'); ?></label>
                            <input type="text" name="guide_note" id="guide_note" class="text text-primary fDelivery" maxlength="255" style="width: 100%">
                        </div>
                        <div class="col-lg-2 col-xs-12">
                            <br>
                            <button id="DeliveryOrder" class="btn btn-warning botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>"  data-value="<?php echo $model->id; ?>" style="width: 100%"><i class="fa fa-truck"></i> <?php echo Yii::t('app', 'Delivery'); ?></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-xs-6">
                            <label for="invoice" class="label label-important"><?php echo Yii::t('app', 'Invoice'); ?></label>
                            <input type="text" name="guide" id="invoice" class="text text-primary bills" style="width: 100%">
                        </div>
                        <div class="col-lg-2 col-xs-6">
                            <label for="invoice_total" class="label label-important"><?php echo Yii::t('app', 'Total'); ?></label>
                            <input type="number" name="total" id="invoice_total" class="text text-primary text-right bills" style="width: 100%">
                        </div>
                        <div class="col-lg-2 col-xs-6">
                            <label for="invoice_date" class="label label-important"><?php echo Yii::t('app', 'Date'); ?></label>
                            <input type="date" name="guide_fecha" id="invoice_date" class="text text-primary bills" style="width: 100%">
                        </div>
                        <div class="col-lg-2 col-xs-6">
                            <label for="invoice_note" class="label label-important"><?php echo Yii::t('app', 'Note'); ?></label>
                            <input type="text" name="invoice_note" id="invoice_note" class="text text-primary bills" maxlength="255" style="width: 100%">
                        </div>
                        <div class="col-lg-2 col-xs-6">
                            <br>
                            <button id="IvoiceOrder" class="btn btn-warning botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>" data-value="<?php echo $model->id; ?>" style="width: 100%"><i class="fa fa-plus"></i> <?php echo Yii::t('app', 'Add'); ?></button>
                        </div>
                        <div class="col-lg-2 col-xs-6">
                            <br>
                            <button id="IvoiceOrderContinue" class="btn btn-success botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>" data-value="<?php echo $model->id; ?>" style="width: 100%"><i class="fa fa-check"></i> <?php echo Yii::t('app', 'Continue'); ?></button>
                        </div>
                    </div>
                <?php } else if ($statusID == 5 && Yii::app()->user->checkAccess('OrderStep5')) { ?>
                    <label for="close_note" class="label label-important"><?php echo Yii::t('app', 'Note'); ?></label>                        
                    <input type="text" name="close_note" id="close_note" class="text text-primary " maxlength="255">
                    <button id="CloseOrder" class="btn btn-success botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>"  data-value="<?php echo $model->id; ?>"><i class="fa fa-dollar"></i> <?php echo Yii::t('app', 'Close'); ?></button>
                <?php } elseif ($statusID == 6 && Yii::app()->user->checkAccess('OrderStep6')) { ?>
                    <span></span>
                <?php } elseif ($statusID == 7 && Yii::app()->user->checkAccess('OrderStep7')) { ?>
                    <label for="" class="label label-important"><?php echo Yii::t('app', 'Reason') . ': ' . $model->r_d_r; ?></label>
                    <label for="" class="label label-important"><?php echo Yii::t('app', 'Date') . ': ' . $model->r_d_d; ?></label>  
                    <label for="" class="label label-important"><?php echo Yii::t('app', 'User') . ': ' . $model->complete_name ?></label>  
                <?php } elseif ($statusID == 8 && Yii::app()->user->checkAccess('OrderStep8')) { ?>   
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-2">
                                    <label for="reactive_note" class="label label-important"><?php echo Yii::t('app', 'Reason'); ?></label>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="reactive_note" id="reactive_note" class="text text-primary " maxlength="255" style="width: 100%">  
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <button id="ReactiveOrder" class="btn btn-success botones" data-loading-text="<?php echo Yii::t('app', 'Proccess') ?>"  data-value="<?php echo $model->id; ?>"><i class="fa fa-check"></i> <?php echo Yii::t('app', 'Create a new order base on this'); ?></button>
                        </div>
                    </div>
                <?php }
                ?>
                <?php
                Yii::app()->clientScript->registerScript('initRefresh2', "js:
                        $('#refresh-button-vale').on('click',function(e) {
                            e.preventDefault();
                            $('#vale-grid').yiiGridView('update');
                            refreshSumary(" . $model->id . ");
                        });
                    "
                        , CClientScript::POS_READY);
                ?>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div id="bills"></div>
        <?php
        if (($statusID == 1 OR $statusID == 9)) {
            if (Yii::app()->user->checkAccess('OrderStep1') OR Yii::app()->user->checkAccess('OrderStep9')) {
                $this->renderPartial('_view_step1', array('model' => $model));
            } else {
                $this->renderPartial('_view_step0', array('model' => $model));
            }
        } elseif ($statusID == 2) {
            if (Yii::app()->user->checkAccess('OrderStep2')) {
                $this->renderPartial('_view_step2', array('model' => $model));
            } else {
                $this->renderPartial('_view_step0', array('model' => $model));
            }
        } else if ($statusID == 3) {
            if (Yii::app()->user->checkAccess('OrderStep3')) {
                $this->renderPartial('_view_step3', array('model' => $model));
            } else {
                $this->renderPartial('_view_step0', array('model' => $model));
            }
        } elseif ($statusID == 4) {
            if (Yii::app()->user->checkAccess('OrderStep4')) {
                $this->renderPartial('_view_step4', array('model' => $model));
            } else {
                $this->renderPartial('_view_step0', array('model' => $model));
            }
        } elseif ($statusID == 5) {
            if (Yii::app()->user->checkAccess('OrderStep5')) {
                $this->renderPartial('_view_step5', array('model' => $model));
            } else {
                $this->renderPartial('_view_step0', array('model' => $model));
            }
        } elseif ($statusID == 6) {
            if (Yii::app()->user->checkAccess('OrderStep6')) {
                $this->renderPartial('_view_step6', array('model' => $model));
            } else {
                $this->renderPartial('_view_step0', array('model' => $model));
            }
        } elseif ($statusID == 7) {
            if (Yii::app()->user->checkAccess('OrderStep7')) {
                $this->renderPartial('_view_step7', array('model' => $model));
            } else {
                $this->renderPartial('_view_step0', array('model' => $model));
            }
        } elseif ($statusID == 8) {
            if (Yii::app()->user->checkAccess('OrderStep8')) {
                $this->renderPartial('_view_step8', array('model' => $model));
            } else {
                $this->renderPartial('_view_step0', array('model' => $model));
            }
        } else {
            echo BsHtml::alert(BsHtml::LABEL_COLOR_DANGER, Yii::t('json', 5010));
        }
        ?>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="row">
                    <button id="refresh-button-vale" class="btn btn-primary" data-loading-text="Actualizando"><i class="fa fa-refresh"></i> <?php echo Yii::t('app', 'Update'); ?></button>
                    <?php
                    Yii::app()->clientScript->registerScript('initRefresh2', "js:
                        $('#refresh-button-vale').on('click',function(e) {
                            e.preventDefault();
                            $('#vale-grid').yiiGridView('update');
                        });
                    "
                            , CClientScript::POS_READY);
                    ?>
                </div>
                <br>
                <div class="row">
                    <?php if ($statusID == 7 OR $statusID == 8) { ?>
                        <?php
                        echo BsHtml::alert('danger', BsHtml::tag('h4', array(), Yii::t('app', 'Status') . ':<i> ' . $status . '</i>', true) .
                                BsHtml::tag('h5', array(), Yii::t('app', 'Date') . ':<i> ' . $model->r_d_d . '</i>', true) .
                                BsHtml::tag('h5', array(), Yii::t('app', 'User') . ':<i> ' . $model->complete_name. '</i>', true) .
                                BsHtml::tag('h5', array(), Yii::t('app', 'Reason') . ':<i> ' . $model->r_d_r . '</i>', true)
                        );
                        ?>

                    <?php } ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 sumary">
                <?php
                $this->renderPartial('_sumary', array(
                    'orderSumary' => $orderSumary,
                    'OrderTax' => $OrderTax,
                    'OrderDelRate' => $OrderDelRate,
                    'Discount' => $Discount,
                ));
                ?> 
            </div>    
        </div>
    </div>
    <?php
    $this->widget('bootstrap.widgets.BsModal', array(
        'id' => 'modal-delete-order',
        'header' => Yii::t('app', 'Delete Order'),
        'content' => $this->renderPartial('_form_delete_order', array(), TRUE),
        'footer' => array(
            BsHtml::button(
                    Yii::t('app', 'OK'), array(
                'class' => 'btn btn-success',
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'icon' => BsHtml::GLYPHICON_FLOPPY_DISK,
                'onClick' => 'deleteOrder();',
                    )
            ),
            BsHtml::button(
                    Yii::t('app', 'Cancel'), array(
                'data-dismiss' => 'modal',
                'icon' => BsHtml::GLYPHICON_FLOPPY_REMOVE,
                'onclick' => "$('#modal-cuenta').modal('show');"
                    )
            )
        )
            )
    );
    ?>
    <?php
    $this->widget('bootstrap.widgets.BsModal', array(
        'id' => 'modal-delete-inv-order',
        'header' => Yii::t('app', 'Delete Order'),
        'content' => $this->renderPartial('_form_delete_inv_order', array(), TRUE),
        'footer' => array(
            BsHtml::button(
                    Yii::t('app', 'OK'), array(
                'class' => 'btn btn-success',
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'icon' => BsHtml::GLYPHICON_FLOPPY_DISK,
                'onClick' => 'deleteInvOrder();',
                    )
            ),
            BsHtml::button(
                    Yii::t('app', 'Cancel'), array(
                'data-dismiss' => 'modal',
                'icon' => BsHtml::GLYPHICON_FLOPPY_REMOVE,
                'onclick' => "$('#modal-cuenta').modal('show');"
                    )
            )
        )
            )
    );
    ?>
    <?php
    $this->widget('bootstrap.widgets.BsModal', array(
        'id' => 'modal-delete-product',
        'header' => Yii::t('app', 'Delete Product'),
        'content' => $this->renderPartial('_form_delete_product', array(), TRUE),
        'footer' => array(
            BsHtml::button(
                    Yii::t('app', 'OK'), array(
                'class' => 'btn btn-success',
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'icon' => BsHtml::GLYPHICON_FLOPPY_DISK,
                'onClick' => 'deleteProduct();',
                    )
            ),
            BsHtml::button(
                    Yii::t('app', 'Cancel'), array(
                'data-dismiss' => 'modal',
                'icon' => BsHtml::GLYPHICON_FLOPPY_REMOVE,
                'onclick' => "$('#modal-cuenta').modal('show');"
                    )
            )
        )
            )
    );
    ?>
</div>