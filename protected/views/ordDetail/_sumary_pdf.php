<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="3" ><?php echo Yii::t('app', 'Sub-Total'); ?></td>
    <td class="text-right"><?php echo number_format($orderSumary->sub_total_productos, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="3" ><?php echo Yii::t('app', 'Discount') . ': ' . $orderSumary->discount_name; ?></td>
    <td class="text-right">
        <?php
        echo number_format($orderSumary->discount, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS);
        ?>
    </td>
</tr>
<?php if (Yii::app()->user->getState('ORDER')->ORDER_SHOW_SUB_TOTAL_AFTER_DISCOUNT == 'true') { ?>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="3" ><?php echo Yii::t('app', 'Sub-Total'); ?></td>
        <td class="text-right"><?php echo number_format($orderSumary->sub_total_menos_descuento, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></td>
    </tr>

<?php } ?>
<?php
$delivery = 0;
foreach ($OrderDelRate as $value) {
    if (Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == 'true') {
        ?>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="3" ><?php echo $value->idDelRate->name . ': ' . $value->idDelRate->idDelivery->name . CHtml::tag('span', array('class' => 'fa fa-times btn btn-danger', 'onclick' => 'deleteDelivery(' . $value->id . ')')); ?></td>
            <td class="text-right"><?php echo number_format($value->idDelRate->price, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS) ?></td>
        </tr>
    <?php } else { ?>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="3" ><?php echo $value->idDelivery->name . ($orderSumary->id_status == 4 ? ' ' . CHtml::tag('span', array('class' => 'fa fa-trash btn btn-danger', 'onclick' => 'deleteDelivery(' . $value->id . ')'), '', true) : ''); ?></td>
            <td class="text-right"><?php echo number_format($value->rate, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS) ?></td>
        </tr>
        <?php
    }
}
?>

<?php if (Yii::app()->user->getState('ORDER')->ORDER_SHOW_SUB_TOTAL_AFTER_DELIVERY == 'true') { ?>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="3" ><?php echo Yii::t('app', 'Sub-Total'); ?></td>
        <td class="text-right"><?php echo number_format($orderSumary->sub_total_mas_delivery, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></td>
    </tr>
<?php } ?>
<?php
$taxValue = 0;

foreach ($OrderTax as $value) {
    ?>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="3" ><?php echo Yii::t('app', $value->idTax->name) . ' ' . $value->idTax->tax . '%'; ?></td>
        <td class="text-right"><?php echo number_format($orderSumary->sub_total_mas_delivery * ($value->idTax->tax / 100), Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS) ?></td>
    </tr>
<?php } ?>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="3" style="font-weight: bold" > <?php echo Yii::t('app', 'Total'); ?></td>
    <td class="text-right" style="font-weight: bold"><?php echo number_format($orderSumary->total, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></td>
</tr>



