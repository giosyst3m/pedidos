
<?php
echo BsHtml::alert(BsHtml::ALERT_COLOR_INFO, Yii::t('json', '1001'));
echo BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', '4046'));
echo BsHtml::textArea('deleteInvOrderText','',array(
        'placeholder'=> Yii::t('json','1002'),
        'rows'=>6,
        'maxlength'=>200,
    ));
echo BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('json', '4047'));