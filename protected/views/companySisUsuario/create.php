<?php
/* @var $this CompanySisUsuarioController */
/* @var $model CompanySisUsuario */
?>

<?php
$this->breadcrumbs=array(
	'Company Sis Usuarios'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','CompanySisUsuario')) ?>

<?php 
if(Yii::app()->user->checkAccess('CompanySisUsuarioCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('CompanySisUsuarioCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>