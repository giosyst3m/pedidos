<?php
/* @var $this CompanySisUsuarioController */
/* @var $model CompanySisUsuario */
?>

<?php
$this->breadcrumbs=array(
	'Company Sis Usuarios'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','CompanySisUsuario').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('CompanySisUsuarioUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('CompanySisUsuarioUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>