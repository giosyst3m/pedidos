<?php
/* @var $this CompanySisUsuarioController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Company Sis Usuarios',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create CompanySisUsuario', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage CompanySisUsuario', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Company Sis Usuarios') ?>
<?php $this->widget('bootstrap.widgets.BsListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>