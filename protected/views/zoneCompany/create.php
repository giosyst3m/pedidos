<?php
/* @var $this ZoneCompanyController */
/* @var $model ZoneCompany */
?>

<?php
$this->breadcrumbs=array(
	'Zone Companies'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','ZoneCompany')) ?>

<?php 
if(Yii::app()->user->checkAccess('ZoneCompanyCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ZoneCompanyCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>