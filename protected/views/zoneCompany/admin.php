<?php
if($FormDefault){
    $Column = array(
        'name' => 'id_zone',
        'value' => '$data->idZone->name',
        'filter' => $Zone
    );
}else{
    $Column = array(
        'name' => 'id_company',
        'value' => '$data->idCompany->name',
        'filter' => $Company
    );
}
$this->widget('bootstrap.widgets.BsGridView', array(
    'id' => 'zone-company-grid',
    'dataProvider' => $model2->search(),
    'filter' => $model2,
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
         $Column,
        array(
            'name' => 'r_d_s',
            'value' => '$data->RegistroEstado($data->r_d_s)',
            'filter' => array(0 => 'Inactivo', 1 => 'Activo')
        ),
        array(
            'class' => 'bootstrap.widgets.BsButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array
                    (
                    'label' => Yii::t('app', 'Change Status Seller in the Company'), //Text label of the button.
                    'url' => 'Yii::app()->createUrl("ZoneCompany/delete", array("id"=>$data->id))', //A PHP expression for generating the URL of the button.
                )
            )
        ),
    ),
));
?>




