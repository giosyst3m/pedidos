<?php
/* @var $this ZoneCompanyController */
/* @var $model ZoneCompany */
?>

<?php
$this->breadcrumbs=array(
	'Zone Companies'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','ZoneCompany').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ZoneCompanyUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ZoneCompanyUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>