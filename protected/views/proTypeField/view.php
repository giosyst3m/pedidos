<?php
/* @var $this ProTypeFieldController */
/* @var $model ProTypeField */
?>

<?php
$this->breadcrumbs=array(
	'Pro Type Fields'=>array('index'),
	$model->id,
);

?>

<?php echo BsHtml::pageHeader('View','ProTypeField '.$model->id) ?>
<?php 
 $reg = array();
if(Yii::app()->user->checkAccess('ProTypeFieldViewAuthView')){
    $reg = array(	
						'id',
		'id_field',
		'id_pro_type',
		'r_d_s',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
        
    );
}else{
    $reg = array(	
				'id',
		'id_field',
		'id_pro_type',
		'r_d_s',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
    );
}
?><?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>$reg
	,
)); ?>

<?php echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => 'history.go(-1)',
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('ProTypeField/create'),array('class'=>  'btn btn-primary')); ?>