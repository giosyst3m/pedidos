<?php
/* @var $this ProTypeFieldController */
/* @var $model ProTypeField */
?>

<?php
$this->breadcrumbs=array(
	'Pro Type Fields'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','ProTypeField')) ?>

<?php 
if(Yii::app()->user->checkAccess('ProTypeFieldCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProTypeFieldCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>