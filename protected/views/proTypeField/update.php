<?php
/* @var $this ProTypeFieldController */
/* @var $model ProTypeField */
?>

<?php
$this->breadcrumbs=array(
	'Pro Type Fields'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','ProTypeField').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ProTypeFieldUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProTypeFieldUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>