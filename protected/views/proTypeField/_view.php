<?php
/* @var $this ProTypeFieldController */
/* @var $data ProTypeField */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_field')); ?>:</b>
		<?php echo CHtml::encode($data->id_field); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_pro_type')); ?>:</b>
		<?php echo CHtml::encode($data->id_pro_type); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_s); ?>
		<br />


</div>