<?php
/* @var $this BrandController */
/* @var $model Brand */
?>

<?php
$this->breadcrumbs = array(
    'Brands' => array('create'),
    'Create',
);
?>


<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Create')?><b> <?php echo Yii::t('app', 'Brand');?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        if (Yii::app()->user->checkAccess('BrandCreateFormView')) {
            $this->renderPartial('_form', array('model' => $model));
        }
        ?>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('BrandCreateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2));
}
?>