<?php
/* @var $this ProductController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
    'Products',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create Product', 'url' => array('create')),
    array('icon' => 'glyphicon glyphicon-tasks', 'label' => 'Manage Product', 'url' => array('admin')),
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/product/index.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/product/update.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/product/create.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/menu-toggle.js', CClientScript::POS_HEAD);
if(isset($VProduct)){
if ($VProduct->quantity == 0) {
    $class = 'warning';
} else if ($VProduct->quantity < 0) {
    $class = 'danger';
} else if ($VProduct->quantity > 0) {
    $class = 'success';
}
}
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Update') ?> <?php echo Yii::t('app', 'Producto') . ': ' . BsHtml::tag('b', [], $model->name); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <div class="row">
            <div class="col-lg-3">
                <h4><?php echo Yii::t('app', 'SKU'); ?>:<small> <?php echo $model->sku; ?></small></h4>
                <h4><?php echo Yii::t('app', 'Barcode'); ?>:<small> <?php echo $model->barcode; ?></small></h4>                
                <h4><?php echo Yii::t('app', 'Brand'); ?>:<small> <?php echo isset($model->idBrand)?$model->idBrand->name:''; ?></small></h4>                
                <h4><?php echo Yii::t('app', 'Group'); ?>:<small> <?php echo $model->idProGroup->name; ?></small></h4>                
            </div>
            <div class="col-lg-5">
                <?php
                if (!empty($model->photo)) {
                    echo '<div class="center-block text-center">';
                    echo $model->ShowImagen($model->photo, Yii::app()->params['URL_PRODUCTO_IMAGEN']);
                    echo '</div><div class="clearfix">&nbsp;</div>';
                }
                ?>
            </div>
            <div class="col-lg-4">
                <a href="#" class="btn btn-sq-lg btn-<?php echo isset($class)?$class:'dark'; ?>">
                    <i class="fa fa-cab fa-5x"></i><br/>
                    <h4><?php echo Yii::t('app', 'Quantity'); ?></h4>
                    <h2><?php echo isset($VProduct)?$VProduct->quantity:0; ?></h2>
                </a>        
            </div>
        </div>
    </div>
</div>
<hr>
<div class="x_panel">
    <div class="x_title">
        <h2><i class="fa fa-bars"></i> <?php echo Yii::t('app', 'Cliente'); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <script>
        

    </script>
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="product-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cab"></i>&nbsp;<?php echo Yii::t('app', 'Product'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="rate-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-usd"></i>&nbsp;<?php echo Yii::t('app', 'Rate'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="imagen-tab2" data-toggle="tab" aria-expanded="false"><i class="fa fa-picture-o"></i>&nbsp;<?php echo Yii::t('app', 'Imagen'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content4" role="tab" id="inventory-tab2" data-toggle="tab" aria-expanded="false"><i class="fa fa-random"></i>&nbsp;<?php echo Yii::t('app', 'Inventory'); ?></a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="product-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('ProductCreateFormView')) {
                        $this->renderPartial('_form', array(
                            'model' => $model,
                            'product' => $product,
                            'proType' => $proType,
                            'imagen' => $imagen,
                            'split' => $split,
                            'splits' => $splits,
                            'brand' => $brand,
                            'categories' => $categories,
                            'category' => $category,
                            'Condition' => $Condition,
                            'ProGroup' => $ProGroup
                        ));
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="rate-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('ProductRateUpdateFormView')) {
                        $this->renderPartial('_form_rate', array(
                            'model' => $ProductRate,
                            'Rate' => $Rate,
                            'Discount' => $Discount,
                            'Tax' => $Tax,
                            'id' => $id,
                            'Money' => $Money
                        ));
                    }
                    ?>
                    <hr>
                    <?php
                    if (Yii::app()->user->checkAccess('ProductRateUpdateAdminView')) {
                        $this->renderPartial('admin_rate', array('model2' => $ProductRateAdmin,
                            'Tax' => $Tax,
                            'Discount' => $Discount,
                            'Rate' => $Rate,
                            'id' => $id
                        ));
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="imagen-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('ProImagenUpdateFormView')) {
                        $this->renderPartial('_form_imagen', array('model' => $ProImagen, 'id' => $id));
                    }
                    ?>
                    <hr>
                    <?php
                    if (Yii::app()->user->checkAccess('ProImagenUpdateAdminView')) {
                        $this->renderPartial('admin_imagen', array('model2' => $ProImagenAdmin,
                            'id' => $id
                        ));
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="inventory-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('InventoryUpdateFormView')) {
                        $this->renderPartial('_form_inventory', array(
                            'model' => $Inventory,
                        ));
                    }
                    ?>
                    <?php
                    if (Yii::app()->user->checkAccess('ProductUpdateAdminView')) {
                        $this->renderPartial('admin_inventory', array('model2' => $InventoryAdmin,
                            'Stock' => $Stock,
                            'InvOrigin' => $InvOrigin
                        ));
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('ProductCreateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2));
}
?>