<?php
/* @var $this ProductController */
/* @var $model Product */
?>

<?php
$this->breadcrumbs = array(
    'Products' => array('index'),
    $model->name,
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/product/view.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/product/view.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/catalog/index.js', CClientScript::POS_END);
?>
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Producto</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <div class="product-image">
                            <img src="<?php echo Yii::app()->getBaseUrl(FALSE) . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . $model->photo; ?>" alt="..." />
                        </div>
                        <div class="product_gallery">
                            <?php foreach ($ProImagen as $value) { ?>
                                <a>
                                    <img src="<?php echo Yii::app()->getBaseUrl(FALSE) . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . $value->image; ?>" alt="..." />
                                </a>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="col-md-5 col-sm-5 col-xs-12" style="border:0px solid #e5e5e5;">

                        <h3 class="prod_title"><?php echo $model->name ?></h3>
                        <h4 class=""><?php echo Yii::t('app', 'SKU') . ': <span class="text-success">' . $model->sku.'</span>'; ?></h4>
                        <h4 class=""><?php echo Yii::t('app', 'Barcode') . ': <span class="text-primary">'. $model->barcode.'</span>'; ?></h4>

                        <p><?php echo $model->description; ?></p>
                        <br />

                        <div class="">
                            <h2><?php echo Yii::t('app', 'Categories'); ?> <small></small></h2>
                            <ul class="list-inline prod_size">
                                <?php foreach ($ProductCategory as $value) { ?>
                                    <li>
                                        <button type="button" class="btn btn-default btn-xs"><?php echo $value->idCategory->name; ?></button>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <br />
                        <div class="">
                            <h2><?php echo Yii::t('app', 'Caracteristic'); ?> <small></small></h2>
                            <ul class="list-inline prod_size">
                                <?php foreach ($Caracteristic as $value) { ?>
                                    <li>
                                        <button type="button" class="btn btn-default btn-xs"><?php echo $value->idProTypeField->idField->label; ?></button>
                                        <button type="button" class="btn btn-primary btn-xs"><?php echo $value->value; ?>/button>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <br />
                        <div class="">
                            <?php foreach ($ProductRate as $value) { ?>
                                <?php $price_tax = $value->price + ($value->price * $value->idTax->tax / 100) ?> 
                                <?php if ($value->idDiscount->discount > 0) { ?>
                                    <span class="product-discount hidden-xs"><?php echo $value->idDiscount->discount; ?>%</span>
                                    <span class="product-discount-phone visible-xs"><?php echo $value->idDiscount->discount; ?>%</span>
                                    <span class="product-price-discount"><spna class="fa fa-usd"></spna><?php echo number_format($price_tax - ( $price_tax * ($value->idDiscount->discount / 100)), 2); ?></span><br/>
                                    <span class="product-price"><spna class="fa fa-usd"></spna><?php echo number_format($price_tax, 2); ?></span>
                                <?php } else { ?>

                                    <div class="product_price">
                                        <h1 class="price text-center"><?php echo number_format($value->price, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></h1>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                        <div class="">
                            <button  class="btn btn-block btn-success btn-product btn-cart" id="<?php echo $model->id; ?>"><span class="glyphicon glyphicon-shopping-cart"></span> <?php echo Yii::t('app', 'Buy'); ?></button>
                            <a  class="btn btn-block btn-info" href="<?php echo Yii::app()->request->urlReferrer; ?>"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo Yii::t('app', 'Back'); ?></a>
                            <?php
                            echo CHtml::dropDownList('id_client', array(), $Client, array(
                                'data-style' => 'btn-primary btn-block',
                                'class' => 'selectpicker show-tick',
                                'empty' => Yii::t('app', 'Client'),
                                'data-live-search' => true,
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
