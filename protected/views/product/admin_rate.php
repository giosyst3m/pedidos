<?php
/* @var $this ProductRateController */
/* @var $model ProductRate */


$this->breadcrumbs = array(
    'Product Rates' => array('index'),
    'Manage',
);

$this->menu = array(
    array('icon' => 'glyphicon glyphicon-list', 'label' => 'List ProductRate', 'url' => array('index')),
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create ProductRate', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-rate-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button(Yii::t('app', 'Advanced search'), array('class' => 'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH, 'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            <?php echo Yii::t('app', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
                &lt;&gt;</b>
            or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.'); ?>        </p>

        <div class="search-form" style="display:none">
            <?php
            $this->renderPartial('_search_rate', array(
                'model' => $model2,
            ));
            ?>
        </div>
        <!-- search-form -->

        <?php
        $this->widget('bootstrap.widgets.BsGridView', array(
            'id' => 'product-rate-grid',
            'dataProvider' => $model2->search(),
            'filter' => $model2,
            'type' => BsHtml::GRID_TYPE_RESPONSIVE,
            'columns' => array(
                array(
                    'name' => 'id_rate',
                    'value' => '$data->idRate->name',
                    'filter' => $Rate
                ),
                'price',
                array(
                    'name' => 'id_discount',
                    'value' => '$data->idDiscount->name',
                    'filter' => $Discount
                ),
                array(
                    'name' => 'id_tax',
                    'value' => '$data->idTax->name',
                    'filter' => $Tax
                ),
                /* 		'r_c_u',

                  'r_c_d',
                  'r_c_i',
                  'r_u_u',
                  'r_u_d',
                  'r_u_i',
                  'r_d_u',
                  'r_d_d',
                  'r_d_i',
                  'r_d_s',
                 */
                array(
                    'name' => 'r_d_s',
                    'value' => '$data->RegistroEstado($data->r_d_s)',
                    'filter' => array(0 => 'Inactivo', 1 => 'Activo')
                ),
                array(
                    'class' => 'bootstrap.widgets.BsButtonColumn',
                    'template' => '{update}{delete}',
                    'buttons' => array(
                        'update' => array(
                            'label' => Yii::t('app', 'Update'),
                            'url' => 'Yii::app()->createUrl("Product/update", array("id"=>' . $id . ',"idRate"=>$data->id))',
                        )
                    )
                ),
            ),
        ));
        ?>
    </div>
</div>




