<?php
/* @var $this ProImagenController */
/* @var $model ProImagen */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'image',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'title',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'description',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'id_product'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
