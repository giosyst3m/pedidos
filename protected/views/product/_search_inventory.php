<?php
/* @var $this InventoryController */
/* @var $model Inventory */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'id_product'); ?>
    <?php echo $form->textFieldControlGroup($model,'quantity'); ?>
    <?php echo $form->textFieldControlGroup($model,'id_stock'); ?>
    <?php echo $form->textFieldControlGroup($model,'type'); ?>
    <?php echo $form->textFieldControlGroup($model,'id_inv_origin'); ?>
    <?php echo $form->textFieldControlGroup($model,'comment',array('maxlength'=>255)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
