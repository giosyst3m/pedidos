<style>
    td, th{
        padding: 3px;
        font-size: 13px;
        font-family: 'Arial'
    }
    tbody, td {
        border: 1px solid #ccc;
        font-weight: bold;
    }

</style>
<table border="" style="width:100%">
    <thead>
        <tr>
            <th colspan="5" style="font-size:xx-small;color:#ccc"><small><?php echo Yii::app()->params['PowerBy_PDF']; ?> - <span><?php echo ENV; ?></span> - <span>V <?php echo Yii::app()->params['APP_VERSION']; ?></span></small></th>
        </tr>
        <tr style="border:none">
            <th style="border:none" colspan="2"><?php echo BsHtml::imageResponsive('/files/client/logo/company_logo.png', '', ['style' => 'width:200px']); ?></th>
            <th style="border:none"><center><?php echo BsHtml::imageResponsive('/files/client/logo/lineas-marcas.png', '', ['style' => 'width:300px;']); ?></center></th>
            <th colspan="2" style="border:none">    
                <i class="fa fa-phone"></i>&nbsp;877 36 59<br>
                <i class="fa fa-phone"></i>&nbsp;878 47 76<br>
                <i class="fa fa-envelope"></i>&nbsp;autovarsal@yahoo.es
            </th>
</tr>
<tr>
    <th><?php echo Yii::t('app', 'Código'); ?></th>
    <th><?php echo Yii::t('app', 'SKU'); ?></th>
    <th><?php echo Yii::t('app', 'Name'); ?></th>
    <th><?php echo Yii::t('app', 'Brand'); ?></th>
    <th class="text-right"><?php echo Yii::t('app', 'Price'); ?></th>
</tr>
</thead>
<tbody style="border: 1px solid #ccc">
    <?php
    foreach ($ProGroups as $ProGroup) {

        echo BsHtml::tag('tr', [], BsHtml::tag('td', ['colspan' => 5, 'class' => 'text-center'], '<h3>' . $ProGroup->pro_group . '</h3>', true), true);
        $Categories = VProduct::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll([
            'select' => 't.category_name',
            'distinct' => true,
            'order' => 'category_orden'
        ]);
        foreach ($Categories as $Category) {
            echo BsHtml::tag('tr', [], BsHtml::tag('td', ['colspan' => 5, 'class' => 'text-center'], '<h5><small>' . $ProGroup->pro_group . '</small> - ' . $Category->category_name . '<h5>', true), true);
            $CDbCriteria = new CDbCriteria();
            $CDbCriteria->addInCondition('id_stock', array(Yii::app()->user->getState('id_stock')));
            $CDbCriteria->addColumnCondition([
                'category_name' => $Category->category_name,
                'pro_group' => $ProGroup->pro_group,
                'r_d_s' => 1
            ]);
            $CDbCriteria->addCondition('quantity > 0');
            $CDbCriteria->order = Yii::app()->user->getState('CATALOG')->CATALOG_ORDER_PRODUCT;
            $VProducts = VProduct::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll($CDbCriteria);
            foreach ($VProducts as $VProduct) {
                ?>
                <tr>
                    <td style="width:1px;"><?php echo $VProduct->barcode; ?></td>
                    <td><?php echo $VProduct->sku; ?></td>
                    <td><?php echo $VProduct->name.
                            (empty($VProduct->code_alternative_2)?"":BsHtml::tag('span',['style'=>"background:#8AC249;color:#fff;padding:0px 5px 0px 5px"],$VProduct->code_alternative_2,true))." ".
                            (empty($VProduct->code_alternative_3)?"":BsHtml::tag('span',['style'=>"background:#E81D62;color:#fff;padding:0px 5px 0px 5px"],$VProduct->code_alternative_3,true));
                            ?>
                    </td>
                    <td><?php echo $VProduct->brand; ?></td>
                    <td class="text-right"><?php echo number_format($VProduct->price, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></td>
                </tr>
                <?php
            }
            unset($CDbCriteria);
        }
    }
    ?>
</tbody>
</table>




