<?php
/* @var $this ProductController */
/* @var $model Product */
?>

<?php
$this->breadcrumbs=array(
	'Products'=>array('create'),
	'Create',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/script/product/uploadImagen.js', CClientScript::POS_HEAD);
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Import'),Yii::t('app',"Products' Imagen from ZIP File")) ?>

<?php 
if(Yii::app()->user->checkAccess('ProductUploadFiles')){
    echo Yii::t('app', 'Follow next Step');
    
    $this->renderPartial('_uploadimagen' ); 
}

?>
<br>