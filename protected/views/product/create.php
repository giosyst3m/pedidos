<?php
/* @var $this ProductController */
/* @var $model Product */
?>

<?php
$this->breadcrumbs = array(
    'Products' => array('create'),
    'Create',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/product/create.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/menu-toggle.js', CClientScript::POS_HEAD);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Create') ?><b> <?php echo Yii::t('app', 'Product'); ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
<?php
if (Yii::app()->user->checkAccess('ProductCreateFormView')) {
    $this->renderPartial('_form', array(
        'model' => $model,
        'product' => $product,
        'proType' => $proType,
        'imagen' => $imagen,
        'split' => $split,
        'splits' => $splits,
        'brand' => $brand,
        'categories' => $categories,
        'category' => $category,
        'Condition' => $Condition,
        'ProGroup' => $ProGroup
    ));
}
?>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('ProductCreateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2));
}
?>