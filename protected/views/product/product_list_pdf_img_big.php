<?php 
require_once __DIR__ . '../../../extensions/TextToImage/TextToImage.php';
$img = new TextToImage;
?>
<div class="conten_cat">
    <div class="conten_productos">
        <?php
        $col = 1;
        $index = 1;
        $pro = 1;
        ?>
        <?php foreach ($VProduct as $value) { ?>
            <?php if ($index == 1) { ?>
                <div class="hedaer_print">
                    <table border="0" style="width:100%">
                        <thead>
                            <tr>
                                <th colspan="5" style="font-size:xx-small;color:#ccc; text-align: center;"><small><?php echo Yii::app()->params['PowerBy_PDF']; ?> - <span><?php echo ENV; ?></span> - <span>V <?php echo Yii::app()->params['APP_VERSION']; ?></span></small></th>
                            </tr>
                            <tr style="border:none">
                                <th style="border:none" colspan="2"><?php echo BsHtml::imageResponsive('/files/client/logo/company_logo.png', '', ['style' => 'width:200px']); ?></th>
                                <th style="border:none"><center><?php echo BsHtml::imageResponsive('/files/client/logo/lineas-marcas.png', '', ['style' => 'width:300px;']); ?></center></th>
                        <th colspan="2" style="border:none">    
                            <i class="fa fa-phone"></i>&nbsp;877 36 59<br>
                            <i class="fa fa-phone"></i>&nbsp;878 47 76<br>
                            <i class="fa fa-envelope"></i>&nbsp;autovarsal@yahoo.es
                        </th>
                        </tr>
                        </thead>
                    </table>
                    <div class="linea">
                        <span><?php echo $value->pro_group; ?></span>
                    </div>
                </div>
            <?php } ?>
            <div class="<?php echo 'col_' . $col; ?>">
                <div class="producto">
                    <div class="info_producto">
                        <div class="foto_producto">
                            <?php echo BsHtml::imageResponsive(Yii::app()->getBaseUrl(FALSE) . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . $value->photo, $value->name, [ 'style' => 'max-height:100%', 'class' => 'pro_img']); ?>
                        </div>
                        <div>
                            <span  class="referencia"><?php echo trim($value->sku); ?></span>
                        </div>                      

                        <div class="titulo_producto">
                            <span><?php //echo trim($value->name); ?>  
                            <?php
                            $output_array = array();
                            preg_match("/\((.*?)\)/", $value->name, $output_array);
                            echo preg_replace("/\((.*?)\)/", BsHtml::tag('span',['class'=>'brand'],$output_array[0],true),$value->name );
                            ?></span>
                        </div>
                        <div class="detalle_producto">
                            <br><span class="codigo"><span class="valor"><?php echo str_replace(' ', '', $value->barcode); ?></span></span>
                            <div style="margin: 10px">
                                <span class="precio">$<?php echo number_format($value->price, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></span>
                            </div>
                        </div>
                        <?php 
                        if(!empty($value->code_alternative_3)){
                            $text = $value->code_alternative_3 ;
                            $img->createImage($text,20,140,30);
                            //save image as png format
                            $file = $this->url_slug($value->code_alternative_3);
                            $url = '/files/product/etiquetas/';
                            $img->saveAsPng($file, Yii::getPathOfAlias('webroot').$url);
                            echo BsHtml::image(Yii::app()->getBaseUrl(FALSE).$url. $file.'.png','',['class'=>'etiqueta']);
                        }
                        ?>
                        
                        <?php $url = Yii::app()->theme->baseUrl.'/images' ;?>
                        <?php // echo empty($value->code_alternative_2)?"":  BsHtml::tag('span',['class'=>'product-codigo-2','style'=>"background-image:url('$url/code_alternative_2.png') !important;"],trim($value->code_alternative_2),true) ; ?>
                        <?php // echo empty($value->code_alternative_3)?"":  BsHtml::tag('span',['class'=>'product-codigo-3','style'=>"background-image:url('$url/code_alternative_3.png') !important;"],trim($value->code_alternative_3),true) ; ?>
                    </div>
                </div>
            </div>

            <?php
            $col++;
            $index++;
            if ($index > 18) {
                $index = 1;
            }
            if ($col > 3) {
                $col = 1;
            }
            if ($pro == 18) {
                $pro = 1;
                echo BsHtml::tag('div', ['class' => 'saltopagina'], '', true);
            } else {
                $pro++;
            }
            ?>
        <?php } ?>
    </div>
</div>
