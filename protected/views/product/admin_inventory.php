<?php
/* @var $this InventoryController */
/* @var $model Inventory */


$this->breadcrumbs=array(
	'Inventories'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-list','label'=>'List Inventory', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Inventory', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inventory-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button(Yii::t('app','Advanced search'),array('class' =>'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH,'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            <?php echo Yii::t('app','You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
                &lt;&gt;</b>
            or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.'); ?>        </p>

        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search_inventory',array(
                'model'=>$model2,
            )); ?>
        </div>
        <!-- search-form -->

        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'inventory-grid',
			'dataProvider'=>$model2->search(),
			'filter'=>$model2,
                        'type' => BsHtml::GRID_TYPE_RESPONSIVE,
			'columns'=>array(
                            array(
                                'name'=>'type',
                                'value'=>'$data->getInventarioTipo($data->type)',
                                'filter'=>  array(1=>  Yii::t('app', 'IN'),2=>Yii::t('app', 'OUT'))
                            ),
				'quantity',
                            array(
                                'name'=>'id_stock',
                                'value'=>'$data->idStock->name',
                                'filter'=>  $Stock,

                            ),
                            array(
                                'name'=>'id_inv_origin',
                                'value'=>'$data->idInvOrigin->name',
                                'filter'=>  $InvOrigin

                            ),
                            'id_order',
                            'id_ord_detail',
				'comment',
                            'r_c_d',

                array(
                    'name'=>'r_d_s',
                    'value'=>'$data->RegistroEstado($data->r_d_s)',
                    'filter'=>  array(0=>'Inactivo',1=>'Activo')
                ),
                
			),
        )); ?>
    </div>
</div>




