<div class="conten_cat">
    <div class="conten_productos">
        <?php $col = 1;
        $index = 1
        ?>
        <?php foreach ($VProduct as $value) { ?>
    <?php if ($index == 1) { ?>
                <div>
                    <table border="0" style="width:100%">
                        <thead>
                            <tr>
                                <th colspan="5" style="font-size:xx-small;color:#ccc"><small><?php echo Yii::app()->params['PowerBy_PDF']; ?> - <span><?php echo ENV; ?></span> - <span>V <?php echo Yii::app()->params['APP_VERSION']; ?></span></small></th>
                            </tr>
                            <tr style="border:none">
                                <th style="border:none" colspan="2"><?php echo BsHtml::imageResponsive('/files/client/logo/company_logo.png', '', ['style' => 'width:200px']); ?></th>
                                <th style="border:none"><center><?php echo BsHtml::imageResponsive('/files/client/logo/lineas-marcas.png', '', ['style' => 'width:300px;']); ?></center></th>
                        <th colspan="2" style="border:none">    
                            <i class="fa fa-phone"></i>&nbsp;877 36 59<br>
                            <i class="fa fa-phone"></i>&nbsp;878 47 76<br>
                            <i class="fa fa-envelope"></i>&nbsp;autovarsal@yahoo.es
                        </th>
                        </tr>
                        </thead>
                    </table>
                </div>
    <?php } ?>
            <div class="<?php echo 'col_' . $col; ?>">
                <div class="producto">
                    <div class="info_producto">
                        <div class="foto_producto"><?php
                            if ($value->photo != 'no_disponible.png')
                                echo BsHtml::imageResponsive(Yii::app()->getBaseUrl(FALSE) . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . $value->photo, $value->name, [ 'width' => 80, 'height' => 75]);
                            ?></div>
                        <div class="titulo_producto">
    <?php echo $value->name; ?>
                            <br><span class="categoria"><?php echo $value->category_name; ?></span>
                        </div>
                    </div>
                    <div  class="info_producto_inf">
                        <div class="logo_marca">
    <?php echo BsHtml::imageResponsive(Yii::app()->getBaseUrl(FALSE) . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . 'progroup/' . $value->pro_group_imagen, $value->name, [ 'width' => 40, 'height' => 40]); ?>
                        </div>
                        <div class="datos_inf">
                            <div class="referencia">Ref. <span><?php echo $value->sku; ?></span></div>
                            <div class="codigo">Cod. <span><?php echo $value->barcode; ?></span></div>
                        </div>
                        <div class="precio">$<?php echo number_format($value->price, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></div>
                    </div>
                </div>
            </div>

            <?php
            $col++;
            $index++;
            if ($index > 15) {
                $index = 1;
                
            }
            if ($col > 3) {
                $col = 1;
            }
            ?>
<?php } ?>
    </div>
</div>