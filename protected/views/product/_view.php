<?php
/* @var $this ProductController */
/* @var $data Product */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('sku_father')); ?>:</b>
		<?php echo CHtml::encode($data->sku_father); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('sku')); ?>:</b>
		<?php echo CHtml::encode($data->sku); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
		<?php echo CHtml::encode($data->name); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
		<?php echo CHtml::encode($data->description); ?>
		<br />


		<b><?php echo CHtml::encode($data->getAttributeLabel('id_pro_type')); ?>:</b>
		<?php echo CHtml::encode($data->id_pro_type); ?>
		<br />

	<?php if(Yii::app()->user->checkAccess('ProductViewAuthView')){?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_s); ?>
		<br />

	<?php }?>

</div>