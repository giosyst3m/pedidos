<?php foreach ($model as $value) {?>

        <?php 
        
        $required = ($value->require==1?BsHtml::tag('span',array('class'=>'required'),' *',true):'');
        $requiredClass = ($value->require==1?'required':'');
        switch ($value->id_fie_type) {
            case 1://String
                echo BsHtml::textFieldControlGroup('car['.$value->id.']', isset($car[$value->id])?$car[$value->id]:'', array(
                   'help'=>$value->description,
                   'placeholder'=>$value->description,
                   'label'=>$value->label.$required,
                    'class'=>$requiredClass
                    
                ));
                break;
            case 2://TextArea
                echo BsHtml::textAreaControlGroup('car['.$value->id.']', isset($car[$value->id])?$car[$value->id]:'', array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>  $value->label, 'car['.$value->id.']' .$required,
                    'class'=>$requiredClass
                ));
                break;
            case 3://Number
                echo BsHtml::numberFieldControlGroup('car['.$value->id.']', isset($car[$value->id])?$car[$value->id]:'', array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>$value->label.$required,
                    'class'=>'text-right '.$requiredClass,
                ));
                break;
            case 4://DateTime
                echo BsHtml::dateFieldControlGroup('car['.$value->id.']', isset($car[$value->id])?$car[$value->id]:'', array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>$value->label.$required,
                    'class'=>'datetimepicker '.$requiredClass,
                    'append'=>  BsHtml::icon(BsHtml::GLYPHICON_CALENDAR),
                ));
                break;
            case 5://Date
                echo BsHtml::dateFieldControlGroup('car['.$value->id.']', isset($car[$value->id])?$car[$value->id]:'', array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>$value->label.$required,
                    'class'=>'date '.$requiredClass,
                    'append'=>  BsHtml::icon(BsHtml::GLYPHICON_CALENDAR),
                ));
                break;
            case 6://Time
                echo BsHtml::textFieldControlGroup('car['.$value->id.']', isset($car[$value->id])?$car[$value->id]:'', array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>$value->label.$required,
                    'class'=>'timepicker '.$requiredClass,
                    'append'=>  BsHtml::icon(BsHtml::GLYPHICON_TIME),
                ));
                break;
            case 7://img
                echo BsHtml::fileFieldControlGroup('car['.$value->id.']', '', array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>$value->label.$required,
                    'class'=>$requiredClass
                ));
                if(isset($car[$value->id])){
                    echo '<div class="center-block text-center">';
                    echo $this->ShowImagen($car[$value->id],'URL_PRODUCTO_FILE');
                    echo '</div><div class="clearfix">&nbsp;</div>';
                }
                break;
            case 8://DropDown One Select
                echo BsHtml::dropDownListControlGroup('car['.$value->id.']', isset($car[$value->id])?$car[$value->id]:'',array_combine(explode(',', $value->value),explode(',', $value->value)), array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>$value->label.$required,
                    'data-style'=>"btn-primary",
                    'data-live-search'=>true,
                    'class'=>'selectpicker show-tick '.$requiredClass,
                    'empty'=>Yii::t('app','.::Select::.'),
                ));
                break;
            case 9://DropDown Multiple Select
                echo BsHtml::dropDownListControlGroup('car['.$value->id.']', isset($car[$value->id])?explode(',', $car[$value->id]):array(),array_combine(explode(',', $value->value),explode(',', $value->value)), array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>$value->label.$required,
                    'multiple'=>'multiple',
                'class'=>'selectpicker show-tick '.$requiredClass,
                'data-live-search'=>true,
                'title'=>Yii::t('app','.::Select::.'),
                'data-style'=>"btn-primary"
                ));
                break;
            case 10://Radio
                echo BsHtml::radioButtonListControlGroup('car['.$value->id.']', isset($car[$value->id])?$car[$value->id]:'',array_combine(explode(',', $value->value),explode(',', $value->value)),array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>$value->label.$required,
                    'class'=>$requiredClass
                ));
                break;
            case 11://Check
                echo BsHtml::checkBoxListControlGroup('car['.$value->id.']',  isset($car[$value->id])?explode(',', $car[$value->id]):array(),array_combine(explode(',', $value->value),explode(',', $value->value)),array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>$value->label.$required,
                    'class'=>$requiredClass
                ));
                break;
            case 12://Link
                echo BsHtml::urlFieldControlGroup('car['.$value->id.']', isset($car[$value->id])?$car[$value->id]:'',array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>$value->label.$required,
                    'class'=>$requiredClass
                ));
                if(isset($car[$value->id])){
                    echo BsHtml::link($car[$value->id], $car[$value->id],array(
                        'icon'=>  BsHtml::GLYPHICON_LINK,
                        'target'=>'_blanck'
                    ));
                }
                break;
            case 13://File
                echo BsHtml::fileFieldControlGroup('car['.$value->id.']', '', array(
                   'help'=>$value->description,
                    'placeholder'=>$value->description,
                    'label'=>$value->label.$required,
                    'class'=>$requiredClass
                ));
                if(isset($car[$value->id])){
                    echo BsHtml::link($car[$value->id], Yii::app()->getBaseUrl(true).Yii::app()->params['URL_PRODUCTO_FILE'].$car[$value->id],array(
                        'icon'=>  BsHtml::GLYPHICON_FILE
                    ));
                }
                break;
            default:
                break;
        }
        ?>


<?php }?>

