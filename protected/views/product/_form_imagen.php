<?php
/* @var $this ProImagenController */
/* @var $model ProImagen */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'pro-imagen-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data',)
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>
    <?php
        if (!empty($model->image)) {
            echo '<div class="center-block text-center">';
            echo $model->ShowImagen($model->image, 'URL_PRODUCTO_IMAGEN');
            echo '</div><div class="clearfix">&nbsp;</div>';
        }
    ?>
    <?php echo $form->fileFieldControlGroup($model,'image'); ?>
    <?php echo $form->textFieldControlGroup($model,'title',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'description',array('maxlength'=>255)); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('ProImagenCreateStatusChange') || Yii::app()->user->checkAccess('ProImagenUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('ProImagenCreateButtonSave') || Yii::app()->user->checkAccess('ProImagenUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('ProImagenCreateButtonNew') || Yii::app()->user->checkAccess('ProImagenUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Product/update',array('id'=>$id)),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
