<?php

$this->widget('bootstrap.widgets.BsGridView', array(
	'dataProvider' => $arrayDataProvider,
	'columns' => array(
            'id',
		array(
			'name' => 'A',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["A"])'
		),
		array(
			'name' => 'B',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["B"])'
		),
		array(
			'name' => 'C',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["C"])'
		),
		array(
			'name' => 'D',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["D"])'
		),
		array(
			'name' => 'E',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["E"])'
		),
		array(
			'name' => 'F',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["F"])'
		),
		array(
			'name' => 'G',
			'type' => 'raw',
			'value' => 'CHtml::encode($data["G"])'
		),
		array(
			'name' => 'Resultado',
			'type' => 'raw',
			'value' => '$data["result"]'
		),	
),
           'type' => BsHtml::GRID_TYPE_CONDENSED 
    . ' ' . BsHtml
    ::GRID_TYPE_BORDERED . ' ' . BsHtml
    ::GRID_TYPE_STRIPED  
));
