<form>
    <?php
    $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
        'id' => 'uploadFile',
        'config' => array(
            'action' => Yii::app()->createUrl('product/saveUpload'),
            'allowedExtensions' => array("xls", 'xlsx'), //array("jpg","jpeg","gif","exe","mov" and etc...
            'sizeLimit' => Yii::app()->user->getState('FILE')->FILES_UPLOAD_MAX, // maximum file size in bytes
//               'minSizeLimit'=>10*1024*1024,// minimum file size in bytes
            'onComplete' => "js:function(id, fileName, responseJSON){ processExcelFile(responseJSON); }",
            'messages' => array(
                'typeError' => "{file} has invalid extension. Only {extensions} are allowed. GIO",
                'sizeError' => "{file} is too large, maximum file size is {sizeLimit}.",
                'minSizeError' => "{file} is too small, minimum file size is {minSizeLimit}.",
                'emptyError' => "{file} is empty, please select files again without it.",
                'onLeave' => "The files are being uploaded, if you leave now the upload will be cancelled."
            ),
            'showMessage' => "js:function(message){ alert(message); }"
        )
    ));
    ?>

</form>
<div class="result"></div>
