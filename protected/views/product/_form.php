<?php
/* @var $this ProductController */
/* @var $model Product */
/* @var $form BSActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'product-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data', 'onsubmit' => 'return validateForm();',)
        ));
?>
<p class="help-block"> <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required.') ?></p>
<div class="row">
    <div class="col-lg-6 col-md-6 col-xs-12 col-xs-12">

        <?php echo $form->errorSummary($model); ?>
        <?php echo $form->hiddenField($model, 'id', array('readony' => 255,'class'=>'id_pro')); ?>
        <?php //echo $form->dropDownListControlGroup($model,'sku_father',$product,array('data-style'=>'btn-success','class'=>'selectpicker show-tick','empty'=>'Select one','data-live-search'=>true,)); ?>
        <?php echo $form->textFieldControlGroup($model, 'sku', array('maxlength' => 255)); ?>
        <?php echo $form->textFieldControlGroup($model, 'barcode', array('maxlength' => 255)); ?>
        <?php echo $form->textFieldControlGroup($model, 'name', array('maxlength' => 255)); ?>
        <?php echo $form->dropDownListControlGroup($model, 'id_pro_type', $proType, array('data-style' => 'btn-primary', 'class' => 'tipo_prod selectpicker show-tick', 'empty' => 'Select one', 'onchange' => 'getAttr(this);', 'data-live-search' => true,)); ?>
        <?php echo $form->dropDownListControlGroup($model, 'id_pro_group', $ProGroup, array('data-style' => 'btn-primary', 'class' => 'selectpicker show-tick', 'empty' => 'Select one', 'data-live-search' => true,)); ?>
        <?php echo $form->dropDownListControlGroup($model, 'id_brand', $brand, array('data-style' => 'btn-info', 'class' => 'selectpicker show-tick', 'empty' => Yii::t('app', 'Select'), 'data-live-search' => true,)); ?>
        <?php echo $form->dropDownListControlGroup($model, 'id_condition', $Condition, array('data-style' => 'btn-info', 'class' => 'selectpicker show-tick', 'empty' => Yii::t('app', 'Select'), 'data-live-search' => true,)); ?>
        <?php echo $form->textAreaControlGroup($model, 'description', array('rows' => 6)); ?>
        <?php // echo $form->fileFieldControlGroup($model, 'photo'); ?>
        <?php
        if (!empty($model->photo)) {
            echo '<div class="center-block text-center">';
            echo $model->ShowImagen($model->photo, Yii::app()->params['URL_PRODUCTO_IMAGEN']);
            echo '</div><div class="clearfix">&nbsp;</div>';
        }
        ?>
        <?php
        echo BsHtml::dropDownListControlGroup('Category', $category, $categories, array(
            'multiple' => 'multiple',
            'class' => 'selectpicker show-tick',
            'data-live-search' => true,
            'label' => Yii::t('app', 'Category'),
            'name' => 'Category[]',
            'title' => Yii::t('app', '.::Select::.'),
            'data-style' => "btn-warning",
        ));
        ?>
        <?php
        echo BsHtml::dropDownListControlGroup('Split', $splits, $split, array(
            'multiple' => 'multiple',
            'class' => 'selectpicker show-tick',
            'data-live-search' => true,
            'label' => Yii::t('app', 'Split'),
            'name' => 'split[]',
            'title' => Yii::t('app', '.::Select::.'),
            'data-style' => "btn-danger"
        ));
        ?>



        <?php
        if (Yii::app()->user->checkAccess('ProductCreateStatusChange') || Yii::app()->user->checkAccess('ProductUpdateStatusChange')) {
            echo $form->dropDownListControlGroup($model, 'r_d_s', array(1 => 'Activo', 0 => 'Inactivo'), array('data-style' => 'btn-info', 'class' => 'selectpicker show-tick'));
        }
        ?> 

        <div class="row">
            <div class="col-xs-12 col-lg-12">
                <?php $this->renderPartial('_img', array('model' => $model, 'imagen' => $imagen)); ?>
            </div>
        </div>
        <?php
        if (Yii::app()->user->checkAccess('ProductCreateButtonSave') || Yii::app()->user->checkAccess('ProductUpdateButtonSave')) {
            echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY));
        }
        ?>    
        <?php
        if (Yii::app()->user->checkAccess('ProductCreateButtonNew') || Yii::app()->user->checkAccess('ProductUpdateButtonNew')) {
            echo BsHtml::link(Yii::t('app', 'New'), Yii::app()->createAbsoluteUrl('Product/create'), array('class' => 'btn btn-primary'));
        }
        ?> 

    </div>
    <div class="col-lg-6 col-md-6 col-xs-12 col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $form->textFieldControlGroup($model, 'code_alternative_1', array('maxlength' => 255)); ?>
                <?php echo $form->textFieldControlGroup($model, 'code_alternative_2', array('maxlength' => 255)); ?>
                <?php echo $form->textFieldControlGroup($model, 'code_alternative_3', array('maxlength' => 255)); ?>
            </div>
        </div>
        <div class="row" ></div>
        <div class="col-xs-12" id="proAttr"></div>

    </div>
</div>

<?php $this->endWidget(); ?>
