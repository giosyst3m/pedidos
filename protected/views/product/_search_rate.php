<?php
/* @var $this ProductRateController */
/* @var $model ProductRate */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'id_product'); ?>
    <?php echo $form->textFieldControlGroup($model,'id_rate'); ?>
    <?php echo $form->textFieldControlGroup($model,'price',array('maxlength'=>2)); ?>
    <?php echo $form->textFieldControlGroup($model,'id_discount'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
