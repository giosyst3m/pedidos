<?php
/* @var $this ProImagenController */
/* @var $model ProImagen */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pro-imagen-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BsHtml::button(Yii::t('app', 'Advanced search'), array('class' => 'search-button', 'icon' => BsHtml::GLYPHICON_SEARCH, 'color' => BsHtml::BUTTON_COLOR_PRIMARY), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <p>
            <?php echo Yii::t('app', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
                &lt;&gt;</b>
            or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.'); ?>        </p>

        <div class="search-form" style="display:none">
            <?php
            $this->renderPartial('_search_imagen', array(
                'model' => $model2,
            ));
            ?>
        </div>
        <!-- search-form -->

        <?php
        $this->widget('bootstrap.widgets.BsGridView', array(
            'id' => 'pro-imagen-grid',
            'dataProvider' => $model2->search(),
            'filter' => $model2,
            'type' => BsHtml::GRID_TYPE_RESPONSIVE,
            'columns' => array(
                array(
                    'header' => Yii::t('app', 'image'),
                    'type' => 'html',
                    'value' => '$data->ShowImagen($data->image, "URL_PRODUCTO_IMAGEN")',
                    'headerHtmlOptions' => array('style' => 'width:10%;'),
                ),
                'title',
                'description',
                array(
                    'name' => 'r_d_s',
                    'value' => '$data->RegistroEstado($data->r_d_s)',
                    'filter' => array(0 => 'Inactivo', 1 => 'Activo')
                ),
                array(
                    'class' => 'bootstrap.widgets.BsButtonColumn',
                    'template' => '{update}{delete}',
                    'buttons' => array(
                        'update' => array(
                            'label' => Yii::t('app', 'Update'),
                            'url' => 'Yii::app()->createUrl("Product/update", array("id"=>' . $id . ',"idRate"=>0,"idImage"=>$data->id))',
                        )
                    )
                ),
            ),
        ));
        ?>
    </div>
</div>




