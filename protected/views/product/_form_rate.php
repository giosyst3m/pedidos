<?php
/* @var $this ProductRateController */
/* @var $model ProductRate */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'product-rate-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->hiddenField($model, 'id_product',array('value'=>$id));?>
    <?php echo $form->dropDownListControlGroup($model,'id_rate',$Rate,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','empty'=>'Select one','data-live-search'=>true,)); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_money',$Money,array('data-style'=>'btn-success','class'=>'selectpicker show-tick','empty'=>'Select one','data-live-search'=>true,)); ?>
        <?php echo $form->numberFieldControlGroup($model,'price',array('class'=>'text-right')); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_discount',$Discount,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','empty'=>'Select one','data-live-search'=>true,)); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_tax',$Tax,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','empty'=>'Select one','data-live-search'=>true,)); ?>
    <?php 
    if(Yii::app()->user->checkAccess('ProductRateCreateStatusChange') || Yii::app()->user->checkAccess('ProductRateUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('ProductRateCreateButtonSave') || Yii::app()->user->checkAccess('ProductRateUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('ProductRateCreateButtonNew') || Yii::app()->user->checkAccess('ProductRateUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Product/index',array('id'=>$id)),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
