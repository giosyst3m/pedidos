<?php
$this->breadcrumbs = array(
    'Catalog',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/catalog/quick_catalog.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/ordDetail/view.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/catalog/quick_catalog.css');
?>

<link href='https://fonts.googleapis.com/css?family=Orbitron|Black+Ops+One|Sigmar+One' rel='stylesheet' type='text/css'>
<?php
if (Yii::app()->user->getState('CATALOG')->CATALOG_SHOW_QUANTITY == 'true') {

    $qty = '<span class="label label-info"><span class="">' . Yii::t('app', 'QTY') . ': </span>{{quantity}}</span>';
} else {
    $qty = '';
}
$template = '
    <div class="row">
        <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <div class="">
                <img class="img-responsive" src="' . Yii::app()->getBaseUrl(FALSE) . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . '{{photo}}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
            <div class="product-name">
                <h4>{{name}}</h4>
            </div>
            <span class="product-sku">
                <span class="text-primary">' . Yii::t('app', 'SKU') . ': </span>{{sku}}
            </span> 
            <br>
            <span class="product-sku">
                <span class="text-primary">' . Yii::t('app', 'Barcode') . ': </span>{{barcode}}
            </span> 
            <h3>{{brand}}</h3>
            <span class="product-price-discount"><spna class="fa fa-usd"></spna>{{price}}</span>'
        . $qty . '
        </div>
    </div><hr>';
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Catalogue') ?><b> <?php echo Yii::t('app', 'Barcode'); ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <h2><?php echo Yii::t('app', 'Client'); ?></h2>
                <?php
                $this->widget('ext.typeahead.TbTypeAhead', array(
                    'name' => 'client',
                    'attribute' => 'keyword',
                    'enableHogan' => true,
                    'value' => $Client['name'],
                    'options' => array(
                        array(
                            'limit' => 10,
                            'name' => 'full_name',
                            'valueKey' => 'full_name',
                            'remote' => array(
                                'url' => Yii::app()->createUrl('/client/getClientLists') . '?term=%QUERY',
                            ),
                            'template' => '<p style="font-size:' . Yii::app()->user->getState('ACCESSIBILITY')->ACCESSIBILITY_AUTOCOMPLETE_FONT_SIZE . '"><b>{{code}} | <span class="badge badge-info">{{correlative}}</span> | </b> {{name}} <br><b>{{address}}</span><br><span class="label label-primary">{{city}}, {{stade}}, {{country}}</b> <span class="label label-info">{{zone}}</span></p> ',
                            'engine' => new CJavaScriptExpression('Hogan'),
                        ),
                    ),
                ));
                ?>
                <input type="hidden" id="id_client" value="<?php echo $Client['id']; ?>"/>
                <input type="hidden" id="id_order" value="<?php echo $Client['order']; ?>"/>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <h2><?php echo Yii::t('app', 'Valid'); ?></h2>
                <button class="btn btn-primary btn-block" id="search"><i class="fa fa-check fa-x2"></i></button>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-8">
                <h2><?php echo Yii::t('app', 'Product'); ?></h2>
                <?php
                $this->widget('ext.typeahead.TbTypeAhead', array(
                    'name' => 'producto',
                    'attribute' => 'keyword',
                    'enableHogan' => true,
                    'options' => array(
                        array(
                            'name' => 'complete_name',
                            'valueKey' => 'complete_name',
                            'remote' => array(
                                'url' => Yii::app()->createUrl('/product/getProductLists') . '?term=%QUERY',
                            ),
                            'template' => $template,
                            'engine' => new CJavaScriptExpression('Hogan'),
                        ),
                    ),
                ));
                ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <h2><?php echo Yii::t('app', 'Quianttity'); ?></h2>
                <input type="number" class="text-right" value="" id="quantity" min="1"/>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                <h2><?php echo Yii::t('app', 'Add'); ?></h2>
                <button class="btn btn-success btn-block" id="Add"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
</div>
<hr>
<div id="block">
    <div id="data">NOT FOUND</div>

</div>
<?php
$this->widget('bootstrap.widgets.BsModal', array(
    'id' => 'modal-delete-product',
    'header' => Yii::t('app', 'Delete Product'),
    'content' => $this->renderPartial('_form_delete_product', array(), TRUE),
    'footer' => array(
        BsHtml::button(
                Yii::t('app', 'OK'), array(
            'class' => 'btn btn-success',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'icon' => BsHtml::GLYPHICON_FLOPPY_DISK,
            'onClick' => 'deleteProduct();',
                )
        ),
        BsHtml::button(
                Yii::t('app', 'Cancel'), array(
            'data-dismiss' => 'modal',
            'icon' => BsHtml::GLYPHICON_FLOPPY_REMOVE,
            'onclick' => "$('#modal-cuenta').modal('show');"
                )
        )
    )
        )
);
?>



