<?php

$this->widget('ext.LiveGridView.RefreshGridView', array(
    'id' => 'vale-grid',
    'dataProvider' => $model,
    'enablePagination'=>false,
    'updatingTime' => Yii::app()->params['DASHBOARD_ACTUALIZACION_TIEMPOS_VALES'],
    'filter' => $model,
    'beforeAjaxUpdate' => 'js:function(){'
    . ' var btn = $("#refresh-button-vale");'
    . 'btn.button("loading");'
    . '}',
    'afterAjaxUpdate' => 'js:function(){'
    . 'var btn = $("#refresh-button-vale");'
    . 'btn.button("reset");'
    . '}',
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        array(
            'header' => Yii::t('app', 'Qty'),
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            'filterHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'htmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
        ),
        [
            'header' => Yii::t('app', 'Photo'),
            'type' => 'html',
            'value' => function($data) {
                return $data->ShowImagen($data->idProduct->photo, Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH,50,50).
                    $data->idProduct->barcode.'<br>'.
                    $data->idProduct->sku.'<br>'.
                    $data->idProduct->idBrand->name.'<br>'.
                    (!empty($data->idProduct->code_alternative_2)?'<span class="label label-success">'.$data->idProduct->code_alternative_2.'</span>':'').'<br>'.
                    (!empty($data->idProduct->code_alternative_3)?'<span class="label label-success">'.$data->idProduct->code_alternative_3.'</span>':'');
            },
            'filterHtmlOptions' => [
                'class' => 'hidden-lg hidden-sm hidden-md',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-lg hidden-sm hidden-md text-mobil',
            ],
            'htmlOptions' => [
                'class' => 'hidden-lg hidden-sm hidden-md text-mobil',
                'style' => 'width:10%;'
            ],
        ],
        array(
            'header' => Yii::t('app', 'Photo'),
            'type' => 'html',
            'value' => function($data){
                return $data->ShowImagen($data->idProduct->photo, Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH);
            },
            'filterHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-xs  ',
                'style' => 'width:10%;'
            ],
            'htmlOptions' => [
                'class' => 'hidden-xs  ',
                
            ],
        ),
        array(
            'header' => Yii::t('app', 'Code'),
            'type' => 'raw',
//            'value' => '$data->idProduct->barcode',
            'value' => function($data){
                return $data->idProduct->barcode.'<br>'.
                    (!empty($data->idProduct->code_alternative_2)?'<span class="label label-success">'.$data->idProduct->code_alternative_2.'</span>':'').'<br>'.
                    (!empty($data->idProduct->code_alternative_3)?'<span class="label label-danger">'.$data->idProduct->code_alternative_3.'</span>':'');
            },
            'filterHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'htmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
        ),
        array(
            'header' => Yii::t('app', 'SKU'),
            'type'  => 'raw',
            'value' => function ($data){
                return BsHtml::link($data->idProduct->sku, $this->createUrl('Product/view', array('id' => $data->idProduct->id)),array('target'=>'_blank'));
            },
            'filterHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'htmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            
        ),
        array(
            'header' => Yii::t('app', 'Descripcion'),
            'type' => 'html',
            'value' => '"<b>".$data->idProduct->name."</b><br/>".$data->idProduct->description',
            'filterHtmlOptions' => [
                'class' => '  text-mobil',
            ],
            'headerHtmlOptions' => [
                'class' => '  text-mobil',
                'style' => 'width:30%;',
            ],
            'htmlOptions' => [
                'class' => '  text-mobil',
            ],
        ),
        array(
            'header' => Yii::t('app', 'Brand'),
            'value' => '$data->idProduct->idBrand->name',
            'filterHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'htmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
        ),
        array(
            'header' => Yii::t('app', 'Price'),
            'value' => 'number_format($data->price,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)',
            'htmlOptions' => array(
                'class' => 'text-right'
            ),
            'filterHtmlOptions' => [
                'class' => '  text-mobil',
            ],
            'headerHtmlOptions' => [
                'class' => '  text-mobil',
            ],

        ),
        array(
            'header' => Yii::t('app', 'Quantity'),
            'type' => 'raw',
            'value' => function($data) {
                if($data->idOrder->id_status == 1){//abierta 
                    return CHtml::numberField($data->id, $data->quantity, array('class' => 'text-right', 'style' => 'width:50px','min'=>1)) .
                        CHtml::tag('span', array('class' => 'fa fa-save btn btn-primary', 'onclick' => 'updateDetailQuantity(' . $data->id . ')'));
                }else{
                    return $data->quantity;
                }
            },'filterHtmlOptions' => [
                'class' => '  text-mobil',
            ],
            'headerHtmlOptions' => [
                'class' => '  text-mobil',
            ],
                ),
                array(
                    'header' => Yii::t('app', 'Total'),
                    'value' => 'number_format(($data->price -($data->price * ($data->idDiscount->discount)/100))*$data->quantity,0)',
                    'htmlOptions' => array(
                        'class' => 'text-right'
                    ),
                    'filterHtmlOptions' => [
                        'class' => '  text-mobil',
                    ],
                    'headerHtmlOptions' => [
                        'class' => '  text-mobil',
                    ],
                ),
            ),
        ));
?>