<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
        <h4><?php echo Yii::t('app', 'Client'); ?></h4>
        <?php
        if(empty(Yii::app()->user->getState('id_client'))){
        $this->widget('ext.typeahead.TbTypeAhead', array(
            'name' => 'client',
            'attribute' => 'keyword',
            'value'=>$Client['name'],
            'enableHogan' => true,
            'options' => array(
                array(
                    'limit' => 10,
                    'name' => 'full_name',
                    'valueKey' => 'full_name',
                    'remote' => array(
                        'url' => Yii::app()->createUrl('/client/getClientLists') . '?term=%QUERY',
                    ),
                    'template' => '<p style="font-size:' . Yii::app()->user->getState('ACCESSIBILITY')->ACCESSIBILITY_AUTOCOMPLETE_FONT_SIZE . '"><b>{{code}} | <span class="badge badge-info">{{correlative}}</span> | </b> {{name}} <br><b>{{address}}</span><br><span class="label label-primary">{{city}}, {{stade}}, {{country}}</b> <span class="label label-info">{{zone}}</span></p> ',
                    'engine' => new CJavaScriptExpression('Hogan'),
                ),
            ),
        ));
        }else{
            
            echo BsHtml::textField('client',VClient::model()->find('id='.Yii::app()->user->getState('id_client'))->full_name,['disabled'=>true,'style'=>'color:#000']);
        }
        ?>
        <input type="hidden" id="id_client" value="<?php echo $Client['id'];?>"/>
        <input type="hidden" id="id_order" value="<?php echo $Client['order'];?>"/>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        <h4><?php echo Yii::t('app', 'Valid'); ?></h4>
        <button class="btn btn-primary btn-block" id="search"><i class="fa fa-check fa-x2"></i></button>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3  ">
        <h4><?php echo Yii::t('app', 'Group'); ?></h4>
        <?php
        echo BsHtml::dropDownList('ProGroup', array(), $ProGroup, array(
            'multiple' => 'multiple',
            'class' => 'selectpicker show-tick',
            'data-live-search' => true,
            'label' => 'Group',
            'name' => 'Group[]',
            'title' => Yii::t('app', 'Select'),
            'data-style' => "btn-primary"
        ))
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3  ">
        <h4><?php echo Yii::t('app', 'Brand'); ?></h4>
        <?php
        echo BsHtml::dropDownList('Brand', array(), $Brand, array(
            'multiple' => 'multiple',
            'class' => 'selectpicker show-tick',
            'data-live-search' => true,
            'label' => 'Brand',
            'name' => 'Brand[]',
            'title' => Yii::t('app', 'Select'),
            'data-style' => "btn-primary"
        ))
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3  ">
        <h4><?php echo Yii::t('app', 'Category'); ?></h4>
        <?php
        echo BsHtml::dropDownList('Category', array(), $Category, array(
            'multiple' => 'multiple',
            'class' => 'selectpicker show-tick',
            'data-live-search' => true,
            'label' => 'Category',
            'name' => 'Category[]',
            'title' => Yii::t('app', 'Select'),
            'data-style' => "btn-primary"
        ))
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3  ">
        <?php //if (Yii::app()->user->getState('CATALOG')->CATALOG_SHOW_FILTER_CONDITION == 'true') { ?>
            <h4><?php echo Yii::t('app', 'Condition'); ?></h4>
            <?php
            echo BsHtml::dropDownList('Condition', array(), $Condition, array(
                'multiple' => 'multiple',
                'class' => 'selectpicker show-tick',
                'data-live-search' => true,
                'label' => 'Condition',
                'name' => 'Condition[]',
                'title' => Yii::t('app', 'Select'),
                'data-style' => "btn-primary"
            ));
        //}
        ?>

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8  ">
         <h4><?php echo Yii::t('app', 'Search by Concidence'); ?></h4>
        <input type="text" id="conten" class="col-xs-12 col-md-12 col-sm-12 col-lg-12 " style="height: 30px; font-size: large;" placeholder="<?php echo Yii::t('app', 'Write down any word you want to searh'); ?>">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2  ">
         <h4><?php echo Yii::t('app', 'Search'); ?></h4>
        <button class="btn btn-primary  btn-block" id="lookup" data-toggle="tooltip" data-placement="bottom" title="<?php echo Yii::t('app', 'Search') ?>" data-loading-text="<?php echo Yii::t('app', 'Searching') ?>"><i class="fa fa-search fa-x2"></i></button>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2  ">
         <h4><?php echo Yii::t('app', 'Clear Filters'); ?></h4>
        <button class="btn btn-info btn-block" id="reseat" data-toggle="tooltip" data-placement="bottom" title="<?php echo Yii::t('app', 'Reseat Filters') ?>" data-loading-text="<?php echo Yii::t('app', 'Reseat Filters') ?>"><i class="fa fa-refresh fa-x2"></i></button>
    </div>
</div>
</div>
