<div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-center">
        <label class="label label-info">Se encontraron <?php echo number_format($total, 0); ?> productos </label>&nbsp;<label class="label label-warning"> Página <?php echo $page + 1; ?> de <?php echo $pages; ?></label>&nbsp;
    </div>
</div>
<?php foreach ($model as $value) { ?>
    <?php $value = (object) $value; ?>
    <div class="col-sm-6 col-xs-12 col-md-6 col-lg-3">
        <?php if ($value->discount > 0) { ?>
            <div class="wrapper">
                <div class="product-discount"><?php echo $value->discount_name; ?></div>
            </div>
            <?php $shadow = "blue-box-shadow"; ?>
            <?php
        } else {
            $shadow = "";
        }
        ?>
        <?php if (!empty($value->code_alternative_2)) { ?>
            <?php $shadow = "green-box-shadow"; ?>
            <div class="wrapper">
                <div class="product-codigo-2"><?php echo $value->code_alternative_2; ?></div>
            </div>
        <?php } ?>
        <?php if (!empty($value->code_alternative_3)) { ?>
            <?php $shadow = "red-box-shadow"; ?>
            <div class="wrapper">
                <div class="product-codigo-3"><?php echo $value->code_alternative_3; ?></div>
            </div>
        <?php } ?>
        <div class="thumbnail <?php echo $shadow; ?>" >
            <div class="product-name">
                <h2><?php echo $value->name ?></h2>
            </div>
            <h3><center class="product-sku text-primary"><b><?php echo $value->pro_group; ?></b></center></h3>
            <div class="product-photo">
                <a class="fancybox" rel="gallery1" href="<?php echo Yii::app()->getBaseUrl(FALSE) . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . $value->photo ?>" title="<?php echo $value->sku . ' ' . $value->barcode . ' ' . $value->name; ?>">
                    <?php
                    echo BsHtml::imageResponsive(Yii::app()->getBaseUrl(FALSE) . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . $value->photo, $value->name, ['class' => 'product-img']);
                    ?>
                </a>

            </div>
            <div class="row">
                <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12 text-center">
                    <label class="label label-info"><?php echo $value->category_name; ?></label>
                </div>

            </div>
            <span class="product-sku"><span class="text-primary"><?php echo Yii::t('app', 'SKU') . ': '; ?></span><?php echo $value->sku; ?></span>
            <?php if (Yii::app()->user->getState('CATALOG')->CATALOG_SHOW_BARCODE == 'true') { ?>
                <br>
                <span class="product-sku"><span class="text-primary"><?php echo Yii::t('app', 'Barcode') . ': '; ?></span><?php echo $value->barcode; ?></span>
            <?php } ?>
            <div class="caption ">
                <h3><center class="text-primary"><?php echo $value->brand; ?></center></h3>

                <div class="row">
                    <div class="col-sm-6 col-xs-6 col-md-8 col-lg-8">
                        <?php if ($value->discount > 0) { ?>
                            <span class="product-price-discount"><spna class="fa fa-usd"></spna><?php echo number_format($value->price - (($value->price * $value->discount / 100))); ?></span><br>
                            <span class="product-price "><spna class="fa fa-usd"></spna><?php echo number_format($value->price) ?></span>
                        <?php } else { ?>
                            <span class="product-price-discount"><spna class="fa fa-usd"></spna><?php echo number_format($value->price) ?></span><br><br>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6 col-xs-6 col-md-4 col-lg-4">
                        <input type="number" class="text-right product-quantity" min="1" id="quantity-<?php echo $value->id ?>" name="quantity-<?php echo $value->id ?>" maxlength="2" value=""/>
                        <?php if (Yii::app()->user->getState('CATALOG')->CATALOG_SHOW_QUANTITY == 'true') { ?>
                            <span class="label label-info"><span class=""><?php echo Yii::t('app', 'QTY') . ': '; ?></span><?php echo $value->quantity; ?></span>
                        <?php } ?>
                    </div>
                </div>
                <p class="product-description hidden-md hidden-xs"><?php echo $value->description ?></p>
                <div class="hidden-lg">&nbsp;</div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <a class="btn btn-block btn-primary btn-product" href="<?php echo $this->createUrl('Product/view', array('id' => $value->id)) ?>"><span class="glyphicon glyphicon-plus"></span> <?php echo Yii::t('app', 'View'); ?> </a> 
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <button  class="btn btn-block btn-success btn-product btn-cart" id="<?php echo $value->id; ?>" onclick="addCart(<?php echo $value->id; ?>);"><span class="glyphicon glyphicon-shopping-cart"></span> <?php echo Yii::t('app', 'Buy'); ?></button>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php } ?>
