<?php
/* @var $this CatalogController */

$this->breadcrumbs = array(
    'Catalog',
);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/catalog/_index.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/catalog/index.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/catalog/quick_catalog.js', CClientScript::POS_END);
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/catalog/_index.js', CClientScript::POS_END); ?>
<link href='https://fonts.googleapis.com/css?family=Orbitron|Black+Ops+One|Sigmar+One' rel='stylesheet' type='text/css'>

<input type="hidden" value="0" id="infinity">
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="fa fa-filter fa-2x">
                            </span> <?php echo Yii::t('app', 'Filters'); ?></a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <?php
                        $this->renderPartial('_index_filters', array(
                            'ProGroup' => $ProGroup,
                            'Brand' => $Brand,
                            'Condition' => $Condition,
                            'Category' => $Category,
                            'Client' => $Client
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="page" value="<?php echo $page ?>">
<input type="hidden" id="pages" value="<?php echo $pages ?>">
<div class="row" id="products">
    <?php
    $this->renderPartial('_index', array(
        'model' => [],
        'page' => $page,
        'pages' => $pages,
        'total'=>$total
    ));
    ?>
</div>
<div class="row" id="result"><div style="height: 200px"></div></div>
<div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center">
        <i id="loadmoreProducts" class="fa fa-spinner fa-spin fa-5x"></i>
        <?php echo BsHtml::alert(BsHtml::ALERT_COLOR_INFO, Yii::t('app', 'No more Products'),['id'=>'fin','class'=>'hidden']);?>
    </div>
</div>

