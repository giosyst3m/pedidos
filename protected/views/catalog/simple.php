<?php 
$this->breadcrumbs = array(
    'Simple'=>array('simple')
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/catalog/quick_catalog.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/ordDetail/view.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/catalog/quick_catalog.css'); ?>

<div class="row">
    <?php $this->renderPartial('_simple_form',['Client'=>$Client]);?>
</div>
