<?php
/* @var $this ZoneController */
/* @var $model Zone */
?>

<?php
$this->breadcrumbs = array(
    'Zones' => array('create'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><i class="fa fa-bars"></i> Tabs <small>Float left</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="zone-tab" role="tab" data-toggle="tab" aria-expanded="true"><?php echo Yii::t('app', 'Zone'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="seller-tab" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Seller'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="company-tab2" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Company'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content4" role="tab" id="client-tab2" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Client'); ?></a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="client-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('ZoneUpdateFormView')) {
                        $this->renderPartial('_form', array('model' => $model));
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="seller-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('ZoneSisUsuarioCreateFormView')) {
                        $this->renderPartial('../zoneSisUsuario/_form', array(
                            'model' => $model5,
                            'FormDefault' => $FormDefault2,
                            'Zone' => $Zone,
                            'VSisUsuario' => $VSisUsuario,
                                )
                        );
                    }
                    ?>
                    <hr>
                    <?php
                    if (Yii::app()->user->checkAccess('ZoneSisUsuarioCreateAdminView')) {
                        $this->renderPartial('../zoneSisUsuario/admin', array(
                            'model2' => $model6,
                            'FormDefault' => $FormDefault2,
                            'Zone' => $Zone,
                            'VSisUsuario' => $VSisUsuario,
                                )
                        );
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="company-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('ZoneCompanyCreateFormView')) {
                        $this->renderPartial('../zoneCompany/_form', array(
                            'model' => $model8,
                            'Company' => $Company,
                            'FormDefault' => $FormDefault2,
                                )
                        );
                    }
                    ?>
                    <hr>
                    <?php
                    if (Yii::app()->user->checkAccess('ZoneCompanyreateAdminView')) {
                        $this->renderPartial('../zoneCompany/admin', array(
                            'model2' => $model9,
                            'Company' => $Company,
                            'FormDefault' => $FormDefault2,
                                )
                        );
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="client-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('ZoneClientCreateFormView')) {
                        $this->renderPartial('../zoneClient/_form', array(
                            'model' => $model3,
                            'FormDefault' => $FormDefault2,
                            'VClient' => $VClient,
                                )
                        );
                    }
                    ?>
                    <hr>
                    <?php
                    if (Yii::app()->user->checkAccess('ZoneClientCreateAdminView')) {
                        $this->renderPartial('../zoneClient/admin', array(
                            'model2' => $model4,
                            'FormDefault' => $FormDefault2,
                            'VClient' => $VClient,
                                )
                        );
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('ZoneUpdateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2));
}
?>