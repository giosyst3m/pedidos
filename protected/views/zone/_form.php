<?php
/* @var $this ZoneController */
/* @var $model Zone */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'zone-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php 
    if(Yii::app()->user->checkAccess('ZoneCreateStatusChange') || Yii::app()->user->checkAccess('ZoneUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('ZoneCreateButtonSave') || Yii::app()->user->checkAccess('ZoneUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('ZoneCreateButtonNew') || Yii::app()->user->checkAccess('ZoneUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Zone/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
