<?php
/* @var $this FieTypeController */
/* @var $model FieType */
?>

<?php
$this->breadcrumbs=array(
	'Fie Types'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','FieType').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('FieTypeUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('FieTypeUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>