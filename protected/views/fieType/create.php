<?php
/* @var $this FieTypeController */
/* @var $model FieType */
?>

<?php
$this->breadcrumbs=array(
	'Fie Types'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','FieType')) ?>

<?php 
if(Yii::app()->user->checkAccess('FieTypeCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('FieTypeCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>