<div class="row-fluid">
    <?php 
    for ($index = 0; $index < 4; $index++) {
    ?>
        <div class="col-xs-12 col-med-4 col-lg-3 text-center">
            <div class="panel panel-danger">
                <div class="panel-heading">
                  <h3 class="panel-title">Panel title</h3>
                </div>
                <div class="panel-body">
                    <div class="row-fluid">
                        <span class="fa fa-info-circle fa-2x text-info pull-right " data-toggle="popover" title="Habitación Sencilla"></span>
                    </div>
                    <span class="fa fa-home fa-5x text-danger " ></span>
                    <div class="row-fluid">
                        <?php
                            echo BsHtml::buttonToolbar(array(
                                array(
                                    'items' => array(
                                        array(
                                            'icon' => BsHtml::GLYPHICON_ARROW_DOWN,
                                            'color'=>  BsHtml::BUTTON_COLOR_SUCCESS,
                                            'onClick'=>'alert(1)',
                                        ),
                                        array(
                                            'icon' => BsHtml::GLYPHICON_ARROW_UP ,
                                            'color'=>  BsHtml::BUTTON_COLOR_WARNING,
                                            'onClick'=>'alert(1)',
                                        ),
                                        array(
                                            'icon' => BsHtml::GLYPHICON_WARNING_SIGN ,
                                            'color'=>  BsHtml::BUTTON_COLOR_INFO,
                                            'onClick'=>'alert(1)',
                                        ),
                                        array(
                                            'icon' => BsHtml::GLYPHICON_WRENCH ,
                                            'color'=>  BsHtml::BUTTON_COLOR_DANGER,
                                            'onClick'=>'alert(1)',
                                        ),
                                        array(
                                            'icon' => BsHtml::GLYPHICON_SHOPPING_CART ,
                                            'color'=>  BsHtml::BUTTON_COLOR_DEFAULT,
                                            'onClick'=>'alert(1)',
                                        ),
                                    )
                                ),
                            ));
                            ?>
                    </div>
                </div>
                <div class="panel-footer">
                    <b>Total</b><br>
                    <span class="fa fa-dollar">1.000.000,00</span>
                </div>
            </div>
        </div>

    <?php }?>
</div>
    <?php 
    for ($index = 0; $index < 4; $index++) {
    ?>
        <div class="col-xs-12 col-med-4 col-lg-3 text-center">
            <div class="panel panel-warning">
                <div class="panel-heading">
                  <h3 class="panel-title">Panel title</h3>
                </div>
                <div class="panel-body">
                    <div class="row-fluid">
                        <span class="fa fa-info-circle fa-2x text-info pull-right " data-toggle="popover" title="Habitación Sencilla"></span>
                    </div>
                    <span class="fa fa-home fa-5x text-warning " ></span>
                    <div class="row-fluid">
                        <?php
                            echo BsHtml::buttonToolbar(array(
                                array(
                                    'items' => array(
                                        array(
                                            'icon' => BsHtml::GLYPHICON_ARROW_DOWN,
                                            'color'=>  BsHtml::BUTTON_COLOR_SUCCESS,
                                            'onClick'=>'alert(1)',
                                        ),
                                        array(
                                            'icon' => BsHtml::GLYPHICON_ARROW_UP ,
                                            'color'=>  BsHtml::BUTTON_COLOR_WARNING,
                                            'onClick'=>'alert(1)',
                                        ),
                                        array(
                                            'icon' => BsHtml::GLYPHICON_WARNING_SIGN ,
                                            'color'=>  BsHtml::BUTTON_COLOR_INFO,
                                            'onClick'=>'alert(1)',
                                        ),
                                        array(
                                            'icon' => BsHtml::GLYPHICON_WRENCH ,
                                            'color'=>  BsHtml::BUTTON_COLOR_DANGER,
                                            'onClick'=>'alert(1)',
                                        ),
                                        array(
                                            'icon' => BsHtml::GLYPHICON_SHOPPING_CART ,
                                            'color'=>  BsHtml::BUTTON_COLOR_DEFAULT,
                                            'onClick'=>'alert(1)',
                                        ),
                                    )
                                ),
                            ));
                            ?>
                    </div>
                </div>
                <div class="panel-footer">
                    <b>Total</b><br>
                    <span class="fa fa-dollar">1.000.000,00</span>
                </div>
            </div>
        </div>

    <?php }?>
</div>