<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/site/index.js', CClientScript::POS_END); ?>

<div class="row">
    <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-file-text fa-2x"></i> <?php echo Yii::t('app', 'Pedidos') ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php
                if (Yii::app()->user->checkAccess('SiteOrderView')) {
                    $this->renderPartial('order_list', array(
                        'model' => $model,
                        'status' => $status,
                        'VSisUsuario' => $VSisUsuario
                            )
                    );
                }
                ?>
            </div>
        </div>
    </div>

    <div class="col-lg-2 hidden-md hidden-sm hidden-xs">
        <?php
        if (Yii::app()->user->checkAccess('SiteOrderView')) {
            $this->renderPartial('order', array(
                'VOrderStatusSumary' => $VOrderStatusSumary,
                    )
            );
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-8 col-sm-8 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-line-chart fa-2x"></i> Año  <?php echo date('Y'); ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <div id="echart_line" style="height:350px;"></div>

            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-pie-chart fa-2x"></i> Año <?php echo date('Y'); ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <div id="echart_pie2" style="height:350px;"></div>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2><i class="fa fa-bar-chart fa-2x"></i> <?php echo Yii::t('app', 'Report'); ?></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                    </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="col-lg-3">
                <?php
                echo BsHtml::dropDownList('Document', array(Yii::app()->user->getState('CHART')->CHART_DOCUMENT_BY_DEFAULT), $Document, array(
                    'class' => 'selectpicker show-tick',
                    'data-live-search' => true,
                    'label' => 'Document',
                    'name' => 'Document',
                    'title' => Yii::t('app', '.::Select::.'),
                    'data-style' => "btn-primary",
                ));
                ?>
            </div>
            <div class="col-lg-3">
                <?php
                echo BsHtml::dropDownList('Year', array(date('Y')), $Years, array(
                    'multiple' => 'multiple',
                    'class' => 'selectpicker show-tick',
                    'data-live-search' => true,
                    'label' => 'Year',
                    'name' => 'Year[]',
                    'title' => Yii::t('app', '.::Select::.'),
                    'data-style' => "btn-primary",
                ));
                ?>
            </div>
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-6">
                        <?php
                        echo BsHtml::dropDownList('Periodos', array(), $Periodos, array(
                            'class' => 'selectpicker show-tick',
                            'title' => Yii::t('app', '.::Select::.'),
                            'data-style' => "btn-primary",
                        ));
                        ?>    
                    </div>
                    <div class="col-lg-6">
                        <?php
                        echo BsHtml::dropDownList('Semestres', array(), $Semestres, array(
                            'multiple' => 'multiple',
                            'class' => 'selectpicker show-tick periodos',
                            'data-live-search' => true,
                            'label' => 'Semestres',
                            'name' => 'Semestres[]',
                            'title' => Yii::t('app', '.::Select::.'),
                            'data-style' => "btn-info",
                        ));
                        ?>
                        <?php
                        echo BsHtml::dropDownList('Cuatrimestres', array(), $Cuatrimestres, array(
                            'multiple' => 'multiple',
                            'class' => 'selectpicker show-tick periodos',
                            'label' => 'Cuatrimestres',
                            'name' => 'Cuatrimestres[]',
                            'title' => Yii::t('app', '.::Select::.'),
                            'data-style' => "btn-info",
                        ));
                        ?>
                        <?php
                        echo BsHtml::dropDownList('Trimestres', array(), $Trimestres, array(
                            'multiple' => 'multiple',
                            'class' => 'selectpicker show-tick periodos',
                            'label' => 'Trimestres',
                            'name' => 'Trimestres[]',
                            'title' => Yii::t('app', '.::Select::.'),
                            'data-style' => "btn-info",
                        ));
                        ?>
                        <?php
                        echo BsHtml::dropDownList('Bimestral', array(), $Bimestral, array(
                            'multiple' => 'multiple',
                            'class' => 'selectpicker show-tick periodos',
                            'label' => 'Bimestral',
                            'name' => 'Bimestral[]',
                            'title' => Yii::t('app', '.::Select::.'),
                            'data-style' => "btn-info",
                        ));
                        ?>
                        <?php
                        echo BsHtml::dropDownList('Meses', array(), $Month, array(
                            'multiple' => 'multiple',
                            'class' => 'selectpicker show-tick periodos',
                            'label' => 'Meses',
                            'name' => 'Meses[]',
                            'title' => Yii::t('app', '.::Select::.'),
                            'data-style' => "btn-info",
                        ));
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <?php
                echo BsHtml::dropDownList('Chart', array(), $this->getCharts(), array(
                    'class' => 'selectpicker show-tick',
                    'title' => Yii::t('app', '.::Select::.'),
                    'data-style' => "btn-primary",
                ));
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div id="ChartOrderStatus"></div>
                <div id="ChartOrderStatusTable"></div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div id="ChartOrderBill"></div>
                <div id="ChartOrderBillTable"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-ticket fa-2x"></i> Ticket</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php
                $this->renderPartial('../../modules/ticket/views/activity/_admin_site', array(
                    'model2' => $VActivity,
                        )
                );
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-md-4 col-sm-4 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-comments fa-2x"></i> <?php echo Yii::t('app', 'Comment'); ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <ul class="list-unstyled timeline widget">
                    <?php
                    $this->widget('bootstrap.widgets.BsListView', array(
                        'dataProvider' => $VComment,
                        'itemView' => '../../modules/ticket/views/comment/_comment_site',
                    ));
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>