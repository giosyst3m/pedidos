<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<?php echo BsHtml::pageHeader(Yii::t('app','Login'),Yii::t('app','Usuario')) ?>
<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
//    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_INLINE,
)); ?>
    
    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>
    <?php echo $form->textFieldControlGroup($model,'username'); ?>
    <?php echo $form->passwordFieldControlGroup($model,'password'); ?>
    <?php echo $form->checkBoxControlGroup($model,'rememberMe'); ?>

    <?php echo BsHtml::submitButton(Yii::t('app','Entrar al sistema'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY,'icon'=>  BsHtml::GLYPHICON_USER)); ?>
    <?php
    echo BsHtml::ajaxLink(
        Yii::t('app','Olvide Contraseña'), 
        Yii::app()->createUrl('sistema/usuario/getForm/'), 
        array(
            'cache' => true,
            'data' => array(
                'message' => 'Proccess...'
            ),
            'type' => 'POST',
            'success' => 'js:function(data){
                        $(".modal-body").html(data);
                        $("#myModal").modal("show");
                    }'
        ), 
        array(
            'icon' => BsHtml::GLYPHICON_EXCLAMATION_SIGN,
            'color'=>  BsHtml::BUTTON_COLOR_DANGER,
            'id'=>'id-IN-',
            'log'=>Yii::app()->createUrl('entidad/HabLog/checkIN/', array('id'=>1,'status'=>2,'log'=>1)), 
            'class'=>"",
        )
    );
    ?>

<?php $this->endWidget(); ?>
<?php
$this->widget('bootstrap.widgets.BsModal', 
    array(
        'id' => 'myModal',
        'header' => Yii::t('app', 'Recuperar Contraseña: Escriba el email que tiene registrado en el sistema'),
        'footer' => array(
            BsHtml::ajaxButton(
                Yii::t('app','OK'),
                Yii::app()->createUrl('sistema/usuario/recoveryPassword/'), 
                array(
                    'type'=>'post',
                    'dataType'=>'html',
                    'data'=>'js:{"email":$("#emailX").val()}',
                    'success'=>'function(data){ $("#div-resp").html(data) }',   
                ), 
                array(
                    'class'=>'btn btn-success',
                    'color' => BsHtml::BUTTON_COLOR_SUCCESS ,
                    'icon'=>  BsHtml::GLYPHICON_SEND
                )
            ),
            BsHtml::button(
                Yii::t('app','Cancel'), 
                array(
                    'data-dismiss' => 'modal',
                    'icon'=> BsHtml::GLYPHICON_REMOVE
                )
            )
        )
    )
);
?>
