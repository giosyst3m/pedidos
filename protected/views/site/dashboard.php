<style>
    /* Hiding the checkbox, but allowing it to be focused */
.badgebox
{
    opacity: 0;
}

.badgebox + .badge
{
    /* Move the check mark away when unchecked */
    text-indent: -999999px;
    /* Makes the badge's width stay the same checked and unchecked */
	width: 27px;
}

.badgebox:focus + .badge
{
    /* Set something to make the badge looks focused */
    /* This really depends on the application, in my case it was: */
    
    /* Adding a light border */
    box-shadow: inset 0px 0px 5px;
    /* Taking the difference out of the padding */
}

.badgebox:checked + .badge
{
    /* Move the check mark back when checked */
	text-indent: 0;
}
</style>
http://bootsnipp.com/snippets/featured/badgebox-css-checkbox-badge
<div class="container">
	<div class="row">
        <br>
        <br>
        <h1>Badgebox: CSS only checkbox badge!</h1>
        <h2>Works on Bootstrap 2.3.2 and up</h2>
        <br>
        <label for="default" class="btn btn-default">Lorem ipsum dolor sit amet, consectetur adipiscing elit. <input type="checkbox" id="default" class="badgebox"><span class="badge">&check;</span></label><br>
        <label for="primary" class="btn btn-primary">Ut at ipsum scelerisque, tristique mauris non, malesuada ante. <input type="checkbox" id="primary" class="badgebox"><span class="badge">&check;</span></label><br>
        <label for="info" class="btn btn-info">Cras gravida purus vitae erat dapibus, vestibulum consectetur lacus feugiat. <input type="checkbox" id="info" class="badgebox"><span class="badge">&check;</span></label><br>
        <label for="success" class="btn btn-success">Morbi rutrum elit eu consequat pulvinar. <input type="checkbox" id="success" class="badgebox" checked="checked"><span class="badge">&check;</span></label><br>
        <label for="warning" class="btn btn-warning">Vestibulum nec purus iaculis felis sagittis sodales. <input type="checkbox" id="warning" class="badgebox"><span class="badge">&check;</span></label><br>
        <label for="danger" class="btn btn-danger">Cras vel lacus vestibulum, sodales diam sit amet, semper risus. <input type="checkbox" id="danger" class="badgebox"><span class="badge">&check;</span></label><br>
	</div>
</div>
<style>
    .form-group input[type="checkbox"] {
    display: none;
}

.form-group input[type="checkbox"] + .btn-group > label span {
    width: 20px;
}

.form-group input[type="checkbox"] + .btn-group > label span:first-child {
    display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span:last-child {
    display: inline-block;   
}

.form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
    display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
    display: none;   
}
</style>
<div class="container">
    <div class="[ row ]">
        <h2>Fancy Bootstrap Checkboxes that work with <a href="https://bootswatch.com/" target="_blank">Bootswatch</a></h2>
        <p>My latest project needed some checkboxes that worked very nicely with bootswatch. Use the Theme picker above here to check out what the checkboxes look like in the different themes.</p>
    </div>
    <div class="[ col-xs-12 col-sm-6 ]">
        <h3>Standard Checkboxes</h3><hr />
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-default" id="fancy-checkbox-default" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-default" class="[ btn btn-default ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="fancy-checkbox-default" class="[ btn btn-default active ]">
                    Default Checkbox
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-primary" id="fancy-checkbox-primary" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-primary" class="[ btn btn-primary ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="fancy-checkbox-primary" class="[ btn btn-default active ]">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-success" id="fancy-checkbox-success" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-success" class="[ btn btn-success ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="fancy-checkbox-success" class="[ btn btn-default active ]">
                    Ut at ipsum scelerisque, tristique mauris non, malesuada ante.
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-info" id="fancy-checkbox-info" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-info" class="[ btn btn-info ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="fancy-checkbox-info" class="[ btn btn-default active ]">
                    Cras gravida purus vitae erat dapibus, vestibulum consectetur lacus feugiat.
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-warning" id="fancy-checkbox-warning" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-warning" class="[ btn btn-warning ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="fancy-checkbox-warning" class="[ btn btn-default active ]">
                  Morbi rutrum elit eu consequat pulvinar.
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-danger" id="fancy-checkbox-danger" checked="checked" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-danger" class="[ btn btn-danger ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="fancy-checkbox-danger" class="[ btn btn-default active ]">
                    Vestibulum nec purus iaculis felis sagittis sodales.
                </label>
            </div>
        </div>
    </div>

    <div class="[ col-xs-12 col-sm-6 ]">
        <h3>Custom Icons Checkboxes</h3><hr />
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-default-custom-icons" id="fancy-checkbox-default-custom-icons" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-default-custom-icons" class="[ btn btn-default ]">
                    <span class="[ glyphicon glyphicon-plus ]"></span>
                    <span class="[ glyphicon glyphicon-minus ]"></span>
                </label>
                <label for="fancy-checkbox-default-custom-icons" class="[ btn btn-default active ]">
                    Default Checkbox
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-primary-custom-icons" id="fancy-checkbox-primary-custom-icons" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-primary-custom-icons" class="[ btn btn-primary ]">
                    <span class="[ glyphicon glyphicon-plus ]"></span>
                    <span class="[ glyphicon glyphicon-minus ]"></span>
                </label>
                <label for="fancy-checkbox-primary-custom-icons" class="[ btn btn-default active ]">
                    Primary Checkbox
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-success-custom-icons" id="fancy-checkbox-success-custom-icons" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-success-custom-icons" class="[ btn btn-success ]">
                    <span class="[ glyphicon glyphicon-plus ]"></span>
                    <span class="[ glyphicon glyphicon-minus ]"></span>
                </label>
                <label for="fancy-checkbox-success-custom-icons" class="[ btn btn-default active ]">
                    Success Checkbox
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-info-custom-icons" id="fancy-checkbox-info-custom-icons" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-info-custom-icons" class="[ btn btn-info ]">
                    <span class="[ glyphicon glyphicon-plus ]"></span>
                    <span class="[ glyphicon glyphicon-minus ]"></span>
                </label>
                <label for="fancy-checkbox-info-custom-icons" class="[ btn btn-default active ]">
                    Info Checkbox
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-warning-custom-icons" id="fancy-checkbox-warning-custom-icons" checked="checked" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-warning-custom-icons" class="[ btn btn-warning ]">
                    <span class="[ glyphicon glyphicon-plus ]"></span>
                    <span class="[ glyphicon glyphicon-minus ]"></span>
                </label>
                <label for="fancy-checkbox-warning-custom-icons" class="[ btn btn-default active ]">
                    Warning Checkbox
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" name="fancy-checkbox-danger-custom-icons" id="fancy-checkbox-danger-custom-icons" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="fancy-checkbox-danger-custom-icons" class="[ btn btn-danger ]">
                    <span class="[ glyphicon glyphicon-plus ]"></span>
                    <span class="[ glyphicon glyphicon-minus ]"></span>
                </label>
                <label for="fancy-checkbox-danger-custom-icons" class="[ btn btn-default active ]">
                    Danger Checkbox
                </label>
            </div>
        </div>
    </div>
</div>
<style>
    .material-switch > input[type="checkbox"] {
    display: none;   
}

.material-switch > label {
    cursor: pointer;
    height: 0px;
    position: relative; 
    width: 40px;  
}

.material-switch > label::before {
    background: rgb(0, 0, 0);
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    content: '';
    height: 16px;
    margin-top: -8px;
    position:absolute;
    opacity: 0.3;
    transition: all 0.4s ease-in-out;
    width: 40px;
}
.material-switch > label::after {
    background: rgb(255, 255, 255);
    border-radius: 16px;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
    content: '';
    height: 24px;
    left: -4px;
    margin-top: -8px;
    position: absolute;
    top: -4px;
    transition: all 0.3s ease-in-out;
    width: 24px;
}
.material-switch > input[type="checkbox"]:checked + label::before {
    background: inherit;
    opacity: 0.5;
}
.material-switch > input[type="checkbox"]:checked + label::after {
    background: inherit;
    left: 20px;
}
</style>
http://bootsnipp.com/snippets/featured/material-design-switch
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Material Design Switch Demos</div>
            
                <!-- List group -->
                <ul class="list-group">
                    <li class="list-group-item">
                        Bootstrap Switch Default
                        <div class="material-switch pull-right">
                            <input id="someSwitchOptionDefault" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionDefault" class="label-default"></label>
                        </div>
                    </li>
                    <li class="list-group-item">
                        Bootstrap Switch Primary
                        <div class="material-switch pull-right">
                            <input id="someSwitchOptionPrimary" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionPrimary" class="label-primary"></label>
                        </div>
                    </li>
                    <li class="list-group-item">
                        Bootstrap Switch Success
                        <div class="material-switch pull-right">
                            <input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox" checked="checked"/>
                            <label for="someSwitchOptionSuccess" class="label-success"></label>
                        </div>
                    </li>
                    <li class="list-group-item">
                        Bootstrap Switch Info
                        <div class="material-switch pull-right">
                            <input id="someSwitchOptionInfo" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionInfo" class="label-info"></label>
                        </div>
                    </li>
                    <li class="list-group-item">
                        Bootstrap Switch Warning
                        <div class="material-switch pull-right">
                            <input id="someSwitchOptionWarning" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionWarning" class="label-warning"></label>
                        </div>
                    </li>
                    <li class="list-group-item">
                        Bootstrap Switch Danger
                        <div class="material-switch pull-right">
                            <input id="someSwitchOptionDanger" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionDanger" class="label-danger"></label>
                        </div>
                    </li>
                </ul>
            </div>            
        </div>
    </div>
</div>


<style>
    /* CSS REQUIRED */
.state-icon {
    left: -5px;
}
.list-group-item-primary {
    color: rgb(255, 255, 255);
    background-color: rgb(66, 139, 202);
}

/* DEMO ONLY - REMOVES UNWANTED MARGIN */
.well .list-group {
    margin-bottom: 0px;
}
</style>
<script>
$(function () {
    $('.list-group.checked-list-box .list-group-item').each(function () {
        
        // Settings
        var $widget = $(this),
            $checkbox = $('<input type="checkbox" class="hidden" />'),
            color = ($widget.data('color') ? $widget.data('color') : "primary"),
            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };
            
        $widget.css('cursor', 'pointer')
        $widget.append($checkbox);

        // Event Handlers
        $widget.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });
          

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $widget.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $widget.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$widget.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $widget.addClass(style + color + ' active');
            } else {
                $widget.removeClass(style + color + ' active');
            }
        }

        // Initialization
        function init() {
            
            if ($widget.data('checked') == true) {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
            }
            
            updateDisplay();

            // Inject the icon if applicable
            if ($widget.find('.state-icon').length == 0) {
                $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
            }
        }
        init();
    });
    
    $('#get-checked-data').on('click', function(event) {
        event.preventDefault(); 
        var checkedItems = {}, counter = 0;
        $("#check-list-box li.active").each(function(idx, li) {
            checkedItems[counter] = $(li).text();
            counter++;
        });
        $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
    });
});</script>
http://bootsnipp.com/snippets/featured/checked-list-group
<div class="container" style="margin-top:20px;">
	<div class="row">
        <div class="col-xs-6">
            <h3 class="text-center">Basic Example</h3>
            <div class="well" style="max-height: 300px;overflow: auto;">
        		<ul class="list-group checked-list-box">
                  <li class="list-group-item">Cras justo odio</li>
                  <li class="list-group-item" data-checked="true">Dapibus ac facilisis in</li>
                  <li class="list-group-item">Morbi leo risus</li>
                  <li class="list-group-item">Porta ac consectetur ac</li>
                  <li class="list-group-item">Vestibulum at eros</li>
                  <li class="list-group-item">Cras justo odio</li>
                  <li class="list-group-item">Dapibus ac facilisis in</li>
                  <li class="list-group-item">Morbi leo risus</li>
                  <li class="list-group-item">Porta ac consectetur ac</li>
                  <li class="list-group-item">Vestibulum at eros</li>
                </ul>
            </div>
        </div>
        <div class="col-xs-6">
            <h3 class="text-center">Colorful Example</h3>
            <div class="well" style="max-height: 300px;overflow: auto;">
            	<ul id="check-list-box" class="list-group checked-list-box">
                  <li class="list-group-item">Cras justo odio</li>
                  <li class="list-group-item" data-color="success">Dapibus ac facilisis in</li>
                  <li class="list-group-item" data-color="info">Morbi leo risus</li>
                  <li class="list-group-item" data-color="warning">Porta ac consectetur ac</li>
                  <li class="list-group-item" data-color="danger">Vestibulum at eros</li>
                </ul>
                <br />
                <button class="btn btn-primary col-xs-12" id="get-checked-data">Get Checked Data</button>
            </div>
            <pre id="display-json"></pre>
        </div>
	</div>
    <div class="row">
        <div class="col-xs-6">
            <h3 class="text-center">Using Button Style's</h3>
            <div class="well" style="max-height: 300px;overflow: auto;">
        		<ul class="list-group checked-list-box">
                  <li class="list-group-item" data-style="button">Cras justo odio</li>
                  <li class="list-group-item" data-style="button" data-color="success">Dapibus ac facilisis in</li>
                  <li class="list-group-item" data-style="button" data-color="info">Morbi leo risus</li>
                  <li class="list-group-item" data-style="button" data-color="warning">Porta ac consectetur ac</li>
                  <li class="list-group-item" data-style="button" data-color="danger">Vestibulum at eros</li>
                </ul>
            </div>
        </div>
        <div class="col-xs-6">
            <h3 class="text-center">Just a Small Party</h3>
            <div class="well" style="max-height: 300px;overflow: auto;">
            	<ul class="list-group checked-list-box">
                  <li class="list-group-item" data-style="button">Cras justo odio</li>
                  <li class="list-group-item" data-color="success">Dapibus ac facilisis in</li>
                  <li class="list-group-item" data-style="button" data-color="info">Morbi leo risus</li>
                  <li class="list-group-item" data-color="warning">Porta ac consectetur ac</li>
                  <li class="list-group-item" data-style="button" data-color="danger">Vestibulum at eros</li>
                </ul>
            </div>
        </div>
    </div>
</div>



<script>
    $(document).on('click', '.panel-heading span.clickable', function (e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    })
</script>
<style>
    .row{
        margin-top:40px;
        padding: 0 10px;
    }

    .clickable{
        cursor: pointer;   
    }

    .panel-heading span {
        margin-top: -20px;
        font-size: 15px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Panel 1</h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                </div>
                <div class="panel-body">Panel content</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Panel 2</h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                </div>
                <div class="panel-body">Panel content</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Panel 3</h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                </div>
                <div class="panel-body">Panel content</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">Panel 4</h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                </div>
                <div class="panel-body">Panel content</div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // page is now ready, initialize the calendar...
        // options and github  - http://fullcalendar.io/

        $('#calendar').fullCalendar({
            dayClick: function () {
                alert('a day has been clicked!');
            }
        });

    });</script>
<!-- put this within head tag, this location creates a validation error -->

<link rel='stylesheet' href='http://fullcalendar.io/js/fullcalendar-2.2.3/fullcalendar.css' />

http://bootsnipp.com/snippets/V0PMQ
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <img src="http://i.imgur.com/7nylkQZ.png" alt="">
            <p><strong>A responsive calendar</strong> which is best viewed: <a href="http://bootsnipp.com/iframe/V0PMQ" target="_blank">full screen</a>
                <br>
                FullCalendar is a jQuery plugin that provides a full-sized, drag & drop event calendar like the one below. It uses AJAX to fetch events on-the-fly and is easily configured to use your own feed format. It is visually customizable with a rich API. 

                <a href="http://fullcalendar.io/" target="_blank">FullCalendar Reference and Github</a>
            </p>
            <hr>

            <div id='calendar'></div>

            <p>
                <a href="http://validator.w3.org/check?uri=http%3A%2F%2Fbootsnipp.com%2Fiframe%2FV0PMQ" target="_blank"><small>HTML</small><sup>5</sup></a>
            </p>

        </div>
    </div>

</div>


<script src='http://fullcalendar.io/js/fullcalendar-2.2.3/lib/moment.min.js'></script>
<script src='http://fullcalendar.io/js/fullcalendar-2.2.3/fullcalendar.min.js'></script>

<style>
    .circle {
        margin: 36px;
        display: inline-block;
        padding: 16px;
        text-align: center;
        width: 180px;
        height: 180px;
        border-radius: 50%;
        border: 2px solid #1d2087;
    }

    .circle::before,
    .circle::after {
        position: absolute;
        z-index: -1;
        display: block;
        content: '';
    }
    .circle,
    .circle::before,
    .circle::after {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-transition: all .5s;
        transition: all .5s;
    }

    .circle {
        position: relative;
        z-index: 2;
        background-color: #fff;
        border: 2px solid #5c5eae;
        color: #5c5eae;
        line-height: 50px;
        overflow: hidden;
    }

    .circle:hover {
        color: #fff;
    }
    .circle::after {
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border-radius: 50%;
        -webkit-transform: scale(.5);
        transform: scale(.5);
    }
    .circle:hover::after {
        background: #5c5eae;
        border-radius: 50%;
        -webkit-transform: scale(.9);
        transform: scale(.9);
    }

    .circle1 {
        opacity: 0.4;
    }

    .circle2 {
        opacity: 0.6;
    }

    .circle3 {
        opacity: 0.8;
    }

    .circle a {
        text-decoration: none;
        color: #1d2087;
    }

    .circle h2 {
        font-size: 60px;
    }

    .circle h2 small {
        color: #1d2087;
    }

    .circle p {
        font-size: 24px;
        line-height: 26px;
    }
</style>
http://bootsnipp.com/snippets/3xKmb
<div class="container">
    <div class="row">
        <section class="section_0">
            <div class="col-sm-3">
                <div class="circle circle1">
                    <a href="#section_1"><h2>1<small>st</small><p>Title</p></h2></a>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="circle circle2">
                    <a href="#section_2"><h2>2<small>nd</small><p>Title</p></h2></a>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="circle circle3">
                    <a href="#section_3"><h2>3<small>rd</small><p>Title</p></h2></a>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="circle">
                    <a href="#section_4"><h2>4<small>th</small><p>Title</p></h2></a>
                </div>
            </div>
        </section>
    </div>
</div>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<style>
    .timeline {
        width: 100%;
        position: relative;
        padding: 1px 0;
        list-style: none;
        font-weight: 300;
    }
    .timeline .timeline-item {
        padding-left: 0;
        padding-right: 30px;
    }
    .timeline .timeline-item.timeline-item-right,
    .timeline .timeline-item:nth-of-type(even):not(.timeline-item-left) {
        padding-left: 30px;
        padding-right: 0;
    }
    .timeline .timeline-item .timeline-event {
        width: 100%;
    }
    .timeline:before {
        border-right-style: solid;
    }
    .timeline:before,
    .timeline:after {
        content: " ";
        display: block;
    }
    .timeline:after {
        clear: both;
    }
    .timeline:before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        width: 50%;
        height: 100% !important;
        margin-left: 1px;
        border-right-width: 2px;
        border-right-style: solid;
        border-right-color: #888888;
    }
    .timeline.timeline-single-column.timeline {
        width: 100%;
        max-width: 768px;
    }
    .timeline.timeline-single-column.timeline .timeline-item {
        padding-left: 72px;
        padding-right: 0;
    }
    .timeline.timeline-single-column.timeline .timeline-item.timeline-item-right,
    .timeline.timeline-single-column.timeline .timeline-item:nth-of-type(even):not(.timeline-item-left) {
        padding-left: 72px;
        padding-right: 0;
    }
    .timeline.timeline-single-column.timeline .timeline-item .timeline-event {
        width: 100%;
    }
    .timeline.timeline-single-column.timeline:before {
        left: 42px;
        width: 0;
        margin-left: -1px;
    }
    .timeline.timeline-single-column.timeline .timeline-item {
        width: 100%;
        margin-bottom: 20px;
    }
    .timeline.timeline-single-column.timeline .timeline-item:nth-of-type(even) {
        margin-top: 0;
    }
    .timeline.timeline-single-column.timeline .timeline-item > .timeline-event {
        float: right !important;
    }
    .timeline.timeline-single-column.timeline .timeline-item > .timeline-event:before,
    .timeline.timeline-single-column.timeline .timeline-item > .timeline-event:after {
        right: auto !important;
        border-left-width: 0 !important;
    }
    .timeline.timeline-single-column.timeline .timeline-item > .timeline-event:before {
        left: -15px !important;
        border-right-width: 15px !important;
    }
    .timeline.timeline-single-column.timeline .timeline-item > .timeline-event:after {
        left: -14px !important;
        border-right-width: 14px !important;
    }
    .timeline.timeline-single-column.timeline .timeline-item > .timeline-point {
        transform: translateX(-50%);
        left: 42px !important;
        margin-left: 0;
    }
    .timeline.timeline-single-column.timeline .timeline-label {
        transform: translateX(-50%);
        margin: 0 0 20px 42px;
    }
    .timeline.timeline-single-column.timeline .timeline-label + .timeline-item + .timeline-item {
        margin-top: 0;
    }
    .timeline.timeline-line-solid:before {
        border-right-style: solid;
    }
    .timeline.timeline-line-dotted:before {
        border-right-style: dotted;
    }
    .timeline.timeline-line-dashed:before {
        border-right-style: dashed;
    }
    .timeline .timeline-item {
        position: relative;
        float: left;
        clear: left;
        width: 50%;
        margin-bottom: 20px;
    }
    .timeline .timeline-item:before,
    .timeline .timeline-item:after {
        content: "";
        display: table;
    }
    .timeline .timeline-item:after {
        clear: both;
    }
    .timeline .timeline-item:last-child {
        margin-bottom: 0 !important;
    }
    .timeline .timeline-item.timeline-item-right > .timeline-event,
    .timeline .timeline-item:nth-of-type(even):not(.timeline-item-left) > .timeline-event {
        float: right !important;
    }
    .timeline .timeline-item.timeline-item-right > .timeline-event:before,
    .timeline .timeline-item:nth-of-type(even):not(.timeline-item-left) > .timeline-event:before,
    .timeline .timeline-item.timeline-item-right > .timeline-event:after,
    .timeline .timeline-item:nth-of-type(even):not(.timeline-item-left) > .timeline-event:after {
        right: auto !important;
        border-left-width: 0 !important;
    }
    .timeline .timeline-item.timeline-item-right > .timeline-event:before,
    .timeline .timeline-item:nth-of-type(even):not(.timeline-item-left) > .timeline-event:before {
        left: -15px !important;
        border-right-width: 15px !important;
    }
    .timeline .timeline-item.timeline-item-right > .timeline-event:after,
    .timeline .timeline-item:nth-of-type(even):not(.timeline-item-left) > .timeline-event:after {
        left: -14px !important;
        border-right-width: 14px !important;
    }
    .timeline .timeline-item > .timeline-event:before {
        top: 10px;
        right: -15px;
        border-top: 15px solid transparent;
        border-left-width: 15px;
        border-left-style: solid;
        border-right-width: 0;
        border-right-style: solid;
        border-bottom: 15px solid transparent;
    }
    .timeline .timeline-item > .timeline-event:after {
        top: 11px;
        right: -14px;
        border-top: 14px solid transparent;
        border-left-width: 14px;
        border-left-style: solid;
        border-right-width: 0;
        border-right-style: solid;
        border-bottom: 14px solid transparent;
    }
    .timeline .timeline-item > .timeline-point {
        top: 25px;
    }
    .timeline-single-column.timeline .timeline-item > .timeline-event {
        float: right !important;
    }
    .timeline-single-column.timeline .timeline-item > .timeline-event:before,
    .timeline-single-column.timeline .timeline-item > .timeline-event:after {
        right: auto !important;
        border-left-width: 0 !important;
    }
    .timeline-single-column.timeline .timeline-item > .timeline-event:before {
        left: -15px !important;
        border-right-width: 15px !important;
    }
    .timeline-single-column.timeline .timeline-item > .timeline-event:after {
        left: -14px !important;
        border-right-width: 14px !important;
    }
    .timeline .timeline-item:nth-of-type(2) {
        margin-top: 40px;
    }
    .timeline .timeline-item.timeline-item-left,
    .timeline .timeline-item.timeline-item-right {
        clear: both !important;
    }
    .timeline .timeline-item.timeline-item-right,
    .timeline .timeline-item:nth-of-type(even):not(.timeline-item-left) {
        float: right;
        clear: right;
    }
    .timeline .timeline-item.timeline-item-right > .timeline-point,
    .timeline .timeline-item:nth-of-type(even):not(.timeline-item-left) > .timeline-point {
        left: -24px;
    }
    .timeline .timeline-item.timeline-item-right > .timeline-point.timeline-point-blank,
    .timeline .timeline-item:nth-of-type(even):not(.timeline-item-left) > .timeline-point.timeline-point-blank {
        left: -12px;
    }
    .timeline .timeline-item.timeline-item-arrow-sm.timeline-item-right > .timeline-event,
    .timeline .timeline-item.timeline-item-arrow-sm:nth-of-type(even):not(.timeline-item-left) > .timeline-event {
        float: right !important;
    }
    .timeline .timeline-item.timeline-item-arrow-sm.timeline-item-right > .timeline-event:before,
    .timeline .timeline-item.timeline-item-arrow-sm:nth-of-type(even):not(.timeline-item-left) > .timeline-event:before,
    .timeline .timeline-item.timeline-item-arrow-sm.timeline-item-right > .timeline-event:after,
    .timeline .timeline-item.timeline-item-arrow-sm:nth-of-type(even):not(.timeline-item-left) > .timeline-event:after {
        right: auto !important;
        border-left-width: 0 !important;
    }
    .timeline .timeline-item.timeline-item-arrow-sm.timeline-item-right > .timeline-event:before,
    .timeline .timeline-item.timeline-item-arrow-sm:nth-of-type(even):not(.timeline-item-left) > .timeline-event:before {
        left: -10px !important;
        border-right-width: 10px !important;
    }
    .timeline .timeline-item.timeline-item-arrow-sm.timeline-item-right > .timeline-event:after,
    .timeline .timeline-item.timeline-item-arrow-sm:nth-of-type(even):not(.timeline-item-left) > .timeline-event:after {
        left: -9px !important;
        border-right-width: 9px !important;
    }
    .timeline .timeline-item.timeline-item-arrow-sm > .timeline-event:before {
        top: 4px;
        right: -10px;
        border-top: 10px solid transparent;
        border-left-width: 10px;
        border-left-style: solid;
        border-right-width: 0;
        border-right-style: solid;
        border-bottom: 10px solid transparent;
    }
    .timeline .timeline-item.timeline-item-arrow-sm > .timeline-event:after {
        top: 5px;
        right: -9px;
        border-top: 9px solid transparent;
        border-left-width: 9px;
        border-left-style: solid;
        border-right-width: 0;
        border-right-style: solid;
        border-bottom: 9px solid transparent;
    }
    .timeline .timeline-item.timeline-item-arrow-sm > .timeline-point {
        top: 14px;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-sm > .timeline-event {
        float: right !important;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-sm > .timeline-event:before,
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-sm > .timeline-event:after {
        right: auto !important;
        border-left-width: 0 !important;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-sm > .timeline-event:before {
        left: -10px !important;
        border-right-width: 10px !important;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-sm > .timeline-event:after {
        left: -9px !important;
        border-right-width: 9px !important;
    }
    .timeline .timeline-item.timeline-item-arrow-md.timeline-item-right > .timeline-event,
    .timeline .timeline-item.timeline-item-arrow-md:nth-of-type(even):not(.timeline-item-left) > .timeline-event {
        float: right !important;
    }
    .timeline .timeline-item.timeline-item-arrow-md.timeline-item-right > .timeline-event:before,
    .timeline .timeline-item.timeline-item-arrow-md:nth-of-type(even):not(.timeline-item-left) > .timeline-event:before,
    .timeline .timeline-item.timeline-item-arrow-md.timeline-item-right > .timeline-event:after,
    .timeline .timeline-item.timeline-item-arrow-md:nth-of-type(even):not(.timeline-item-left) > .timeline-event:after {
        right: auto !important;
        border-left-width: 0 !important;
    }
    .timeline .timeline-item.timeline-item-arrow-md.timeline-item-right > .timeline-event:before,
    .timeline .timeline-item.timeline-item-arrow-md:nth-of-type(even):not(.timeline-item-left) > .timeline-event:before {
        left: -15px !important;
        border-right-width: 15px !important;
    }
    .timeline .timeline-item.timeline-item-arrow-md.timeline-item-right > .timeline-event:after,
    .timeline .timeline-item.timeline-item-arrow-md:nth-of-type(even):not(.timeline-item-left) > .timeline-event:after {
        left: -14px !important;
        border-right-width: 14px !important;
    }
    .timeline .timeline-item.timeline-item-arrow-md > .timeline-event:before {
        top: 10px;
        right: -15px;
        border-top: 15px solid transparent;
        border-left-width: 15px;
        border-left-style: solid;
        border-right-width: 0;
        border-right-style: solid;
        border-bottom: 15px solid transparent;
    }
    .timeline .timeline-item.timeline-item-arrow-md > .timeline-event:after {
        top: 11px;
        right: -14px;
        border-top: 14px solid transparent;
        border-left-width: 14px;
        border-left-style: solid;
        border-right-width: 0;
        border-right-style: solid;
        border-bottom: 14px solid transparent;
    }
    .timeline .timeline-item.timeline-item-arrow-md > .timeline-point {
        top: 25px;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-md > .timeline-event {
        float: right !important;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-md > .timeline-event:before,
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-md > .timeline-event:after {
        right: auto !important;
        border-left-width: 0 !important;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-md > .timeline-event:before {
        left: -15px !important;
        border-right-width: 15px !important;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-md > .timeline-event:after {
        left: -14px !important;
        border-right-width: 14px !important;
    }
    .timeline .timeline-item.timeline-item-arrow-lg.timeline-item-right > .timeline-event,
    .timeline .timeline-item.timeline-item-arrow-lg:nth-of-type(even):not(.timeline-item-left) > .timeline-event {
        float: right !important;
    }
    .timeline .timeline-item.timeline-item-arrow-lg.timeline-item-right > .timeline-event:before,
    .timeline .timeline-item.timeline-item-arrow-lg:nth-of-type(even):not(.timeline-item-left) > .timeline-event:before,
    .timeline .timeline-item.timeline-item-arrow-lg.timeline-item-right > .timeline-event:after,
    .timeline .timeline-item.timeline-item-arrow-lg:nth-of-type(even):not(.timeline-item-left) > .timeline-event:after {
        right: auto !important;
        border-left-width: 0 !important;
    }
    .timeline .timeline-item.timeline-item-arrow-lg.timeline-item-right > .timeline-event:before,
    .timeline .timeline-item.timeline-item-arrow-lg:nth-of-type(even):not(.timeline-item-left) > .timeline-event:before {
        left: -18px !important;
        border-right-width: 18px !important;
    }
    .timeline .timeline-item.timeline-item-arrow-lg.timeline-item-right > .timeline-event:after,
    .timeline .timeline-item.timeline-item-arrow-lg:nth-of-type(even):not(.timeline-item-left) > .timeline-event:after {
        left: -17px !important;
        border-right-width: 17px !important;
    }
    .timeline .timeline-item.timeline-item-arrow-lg > .timeline-event:before {
        top: 10px;
        right: -18px;
        border-top: 18px solid transparent;
        border-left-width: 18px;
        border-left-style: solid;
        border-right-width: 0;
        border-right-style: solid;
        border-bottom: 18px solid transparent;
    }
    .timeline .timeline-item.timeline-item-arrow-lg > .timeline-event:after {
        top: 11px;
        right: -17px;
        border-top: 17px solid transparent;
        border-left-width: 17px;
        border-left-style: solid;
        border-right-width: 0;
        border-right-style: solid;
        border-bottom: 17px solid transparent;
    }
    .timeline .timeline-item.timeline-item-arrow-lg > .timeline-point {
        top: 28px;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-lg > .timeline-event {
        float: right !important;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-lg > .timeline-event:before,
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-lg > .timeline-event:after {
        right: auto !important;
        border-left-width: 0 !important;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-lg > .timeline-event:before {
        left: -18px !important;
        border-right-width: 18px !important;
    }
    .timeline-single-column.timeline .timeline-item.timeline-item-arrow-lg > .timeline-event:after {
        left: -17px !important;
        border-right-width: 17px !important;
    }
    .timeline .timeline-item > .timeline-event {
        background: #fff;
        border: 1px solid #888888;
        color: #555;
        position: relative;
        float: left;
        border-radius: 3px;
    }
    .timeline .timeline-item > .timeline-event:before {
        border-left-color: #888888;
        border-right-color: #888888;
    }
    .timeline .timeline-item > .timeline-event:after {
        border-left-color: #fff;
        border-right-color: #fff;
    }
    .timeline .timeline-item > .timeline-event * {
        color: inherit;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-default {
        background: #fff;
        border: 1px solid #888888;
        color: #555;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-default:before {
        border-left-color: #888888;
        border-right-color: #888888;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-default:after {
        border-left-color: #fff;
        border-right-color: #fff;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-default * {
        color: inherit;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-primary {
        background: #f5f5f5;
        border: 1px solid #888888;
        color: #555;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-primary:before {
        border-left-color: #888888;
        border-right-color: #888888;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-primary:after {
        border-left-color: #f5f5f5;
        border-right-color: #f5f5f5;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-primary * {
        color: inherit;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-success {
        background: #F3F8ED;
        border: 1px solid #72b92e;
        color: #3F8100;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-success:before {
        border-left-color: #72b92e;
        border-right-color: #72b92e;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-success:after {
        border-left-color: #F3F8ED;
        border-right-color: #F3F8ED;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-success * {
        color: inherit;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-info {
        background: #F0F8FD;
        border: 1px solid #3e93cf;
        color: #0062A7;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-info:before {
        border-left-color: #3e93cf;
        border-right-color: #3e93cf;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-info:after {
        border-left-color: #F0F8FD;
        border-right-color: #F0F8FD;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-info * {
        color: inherit;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-warning {
        background: #FFF9E9;
        border: 1px solid #d0aa42;
        color: #ac7e00;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-warning:before {
        border-left-color: #d0aa42;
        border-right-color: #d0aa42;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-warning:after {
        border-left-color: #FFF9E9;
        border-right-color: #FFF9E9;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-warning * {
        color: inherit;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-danger {
        background: #FFC4BC;
        border: 1px solid #d25a4b;
        color: #B71500;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-danger:before {
        border-left-color: #d25a4b;
        border-right-color: #d25a4b;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-danger:after {
        border-left-color: #FFC4BC;
        border-right-color: #FFC4BC;
    }
    .timeline .timeline-item > .timeline-event.timeline-event-danger * {
        color: inherit;
    }
    .timeline .timeline-item > .timeline-event:before,
    .timeline .timeline-item > .timeline-event:after {
        content: "";
        display: inline-block;
        position: absolute;
    }
    .timeline .timeline-item > .timeline-event .timeline-heading,
    .timeline .timeline-item > .timeline-event .timeline-body,
    .timeline .timeline-item > .timeline-event .timeline-footer {
        padding: 4px 10px;
    }
    .timeline .timeline-item > .timeline-event .timeline-heading p,
    .timeline .timeline-item > .timeline-event .timeline-body p,
    .timeline .timeline-item > .timeline-event .timeline-footer p,
    .timeline .timeline-item > .timeline-event .timeline-heading ul,
    .timeline .timeline-item > .timeline-event .timeline-body ul,
    .timeline .timeline-item > .timeline-event .timeline-footer ul {
        margin-bottom: 0;
    }
    .timeline .timeline-item > .timeline-event .timeline-heading h4 {
        font-weight: 400;
    }
    .timeline .timeline-item > .timeline-event .timeline-footer a {
        cursor: pointer;
        text-decoration: none;
    }
    .timeline .timeline-item > .timeline-event .panel,
    .timeline .timeline-item > .timeline-event .table,
    .timeline .timeline-item > .timeline-event .blankslate {
        margin: 0;
        border: none;
        border-radius: inherit;
        overflow: hidden;
    }
    .timeline .timeline-item > .timeline-event .table th {
        border-top: 0;
    }
    .timeline .timeline-item > .timeline-point {
        color: #888888;
        background: #fff;
        right: -24px;
        width: 24px;
        height: 24px;
        margin-top: -12px;
        margin-left: 12px;
        margin-right: 12px;
        position: absolute;
        z-index: 100;
        border-width: 2px;
        border-style: solid;
        border-radius: 100%;
        line-height: 20px;
        text-align: center;
    }
    .timeline .timeline-item > .timeline-point.timeline-point-blank {
        right: -12px;
        width: 12px;
        height: 12px;
        margin-top: -6px;
        margin-left: 6px;
        margin-right: 6px;
        color: #888888;
        background: #888888;
    }
    .timeline .timeline-item > .timeline-point.timeline-point-default {
        color: #888888;
        background: #fff;
    }
    .timeline .timeline-item > .timeline-point.timeline-point-primary {
        color: #888888;
        background: #fff;
    }
    .timeline .timeline-item > .timeline-point.timeline-point-success {
        color: #72b92e;
        background: #fff;
    }
    .timeline .timeline-item > .timeline-point.timeline-point-info {
        color: #3e93cf;
        background: #fff;
    }
    .timeline .timeline-item > .timeline-point.timeline-point-warning {
        color: #d0aa42;
        background: #fff;
    }
    .timeline .timeline-item > .timeline-point.timeline-point-danger {
        color: #d25a4b;
        background: #fff;
    }
    .timeline .timeline-label {
        position: relative;
        float: left;
        clear: left;
        width: 50%;
        margin-bottom: 20px;
        top: 1px;
        width: 100%;
        margin-left: auto;
        margin-right: auto;
        padding: 0;
        text-align: center;
    }
    .timeline .timeline-label:before,
    .timeline .timeline-label:after {
        content: "";
        display: table;
    }
    .timeline .timeline-label:after {
        clear: both;
    }
    .timeline .timeline-label:last-child {
        margin-bottom: 0 !important;
    }
    .timeline .timeline-label + .timeline-item {
        margin-top: 0;
    }
    .timeline .timeline-label + .timeline-item + .timeline-item {
        margin-top: 40px;
    }
    .timeline .timeline-label .label-default {
        background-color: #888888;
    }
    .timeline .timeline-label .label-primary {
        background-color: #888888;
    }
    .timeline .timeline-label .label-info {
        background-color: #3e93cf;
    }
    .timeline .timeline-label .label-warning {
        background-color: #d0aa42;
    }
    .timeline .timeline-label .label-danger {
        background-color: #d25a4b;
    }
    @media (max-width: 768px) {
        .timeline.timeline {
            width: 100%;
            max-width: 100%;
        }
        .timeline.timeline .timeline-item {
            padding-left: 72px;
            padding-right: 0;
        }
        .timeline.timeline .timeline-item.timeline-item-right,
        .timeline.timeline .timeline-item:nth-of-type(even):not(.timeline-item-left) {
            padding-left: 72px;
            padding-right: 0;
        }
        .timeline.timeline .timeline-item .timeline-event {
            width: 100%;
        }
        .timeline.timeline:before {
            left: 42px;
            width: 0;
            margin-left: -1px;
        }
        .timeline.timeline .timeline-item {
            width: 100%;
            margin-bottom: 20px;
        }
        .timeline.timeline .timeline-item:nth-of-type(even) {
            margin-top: 0;
        }
        .timeline.timeline .timeline-item > .timeline-event {
            float: right !important;
        }
        .timeline.timeline .timeline-item > .timeline-event:before,
        .timeline.timeline .timeline-item > .timeline-event:after {
            right: auto !important;
            border-left-width: 0 !important;
        }
        .timeline.timeline .timeline-item > .timeline-event:before {
            left: -15px !important;
            border-right-width: 15px !important;
        }
        .timeline.timeline .timeline-item > .timeline-event:after {
            left: -14px !important;
            border-right-width: 14px !important;
        }
        .timeline.timeline .timeline-item > .timeline-point {
            transform: translateX(-50%);
            left: 42px !important;
            margin-left: 0;
        }
        .timeline.timeline .timeline-label {
            transform: translateX(-50%);
            margin: 0 0 20px 42px;
        }
        .timeline.timeline .timeline-label + .timeline-item + .timeline-item {
            margin-top: 0;
        }
    }
</style>
http://bootsnipp.com/snippets/z4bEn
<div class="container-fluid">
    <div class="col-md-12">
        <div class="row">
            <h1>bootstrap-responsive-timeline</h1>
            <h3>Download sources for LESS, SASS and SCSS at <a href="https://github.com/sanex3339/bootstrap-responsive-timeline">https://github.com/sanex3339/bootstrap-responsive-timeline</a></h3>
        </div>
        <div class="row">
            <h2>Usage example</h2>
            <div class="timeline timeline-line-dotted">
                <span class="timeline-label">
                    <span class="label label-primary">17.03.2016</span>
                </span>
                <div class="timeline-item">
                    <div class="timeline-point timeline-point-success">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>MoneyService Transfer</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Money transfer. By Alex, Wallet ID: 1234567890, Amount: 10$</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">17.03.2016 10:00</p>
                        </div>
                    </div>
                </div>
                <span class="timeline-label">
                    <span class="label label-primary">09.03.2016</span>
                </span>
                <div class="timeline-item">
                    <div class="timeline-point timeline-point-success">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>MoneyService Transfer</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Money transfer. By Alex, Wallet ID: 1234567890, Amount: 2220$</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">09.03.2016 09:00</p>
                        </div>
                    </div>
                </div>
                <span class="timeline-label">
                    <span class="label label-primary">09.02.2016</span>
                </span>
                <div class="timeline-item">
                    <div class="timeline-point timeline-point-success">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>MoneyService Transfer</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Money transfer. By John, Wallet ID: 1234567890, Amount: 261$</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">09.02.2016 08:00</p>
                        </div>
                    </div>
                </div>
                <span class="timeline-label">
                    <span class="label label-primary">08.02.2016</span>
                </span>
                <div class="timeline-item">
                    <div class="timeline-point timeline-point-success">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>MoneyService Transfer</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Money transfer. By John, Wallet ID: 1234567890, Amount: 100$</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">08.02.2016 14:00</p>
                        </div>
                    </div>
                </div>
                <div class="timeline-item">
                    <div class="timeline-point timeline-point-success">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>MoneyService Transfer</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Money transfer. By Alex, Wallet ID: 1234567890, Amount: 250$</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">08.02.2016 12:00</p>
                        </div>
                    </div>
                </div>
                <div class="timeline-item">
                    <div class="timeline-point timeline-point-danger">
                        <i class="fa fa-times"></i>
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>MoneyService Transfer</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Money transfer. By Tom, Wallet ID: 1234567890, Amount: 10$</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">08.02.2016 11:30</p>
                        </div>
                    </div>
                </div>
                <div class="timeline-item">
                    <div class="timeline-point timeline-point-danger">
                        <i class="fa fa-times"></i>
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>MoneyService Transfer</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Money transfer. By Alex, Wallet ID: 1234567890, Amount: 3$</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">08.02.2016 11:00</p>
                        </div>
                    </div>
                </div>
                <div class="timeline-item">
                    <div class="timeline-point timeline-point-danger">
                        <i class="fa fa-times"></i>
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>MoneyService Transfer</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Money transfer. By Alex, Wallet ID: 1234567890, Amount: 25$</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">08.02.2016 10:00</p>
                        </div>
                    </div>
                </div>
                <span class="timeline-label">
                    <span class="label label-primary">13.01.2016</span>
                </span>
                <div class="timeline-item">
                    <div class="timeline-point timeline-point-success">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>MoneyService Transfer</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Money transfer. By John, Wallet ID: 1234567890, Amount: 240$</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">13.01.2016 15:00</p>
                        </div>
                    </div>
                </div>
                <div class="timeline-item">
                    <div class="timeline-point timeline-point-success">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>MoneyService Transfer</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Money transfer. By Steve, Wallet ID: 1234567890, Amount: 100'000'000$</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">13.01.2016 10:00</p>
                        </div>
                    </div>
                </div>
                <span class="timeline-label">
                    <a href="#" class="btn btn-default" title="More...">
                        <i class="fa fa-fw fa-history"></i>
                    </a>
                </span>
            </div>
        </div>
        <div class="row">
            <h2>Two columns</h2>
            <div class="timeline">
                <div class="timeline-item">
                    <div class="timeline-point timeline-point-default">
                        <i class="fa fa-circle"></i>
                    </div>
                    <div class="timeline-event timeline-event-default">
                        <div class="timeline-heading">
                            <h4>timeline-event timeline-event-default</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-21-2014</p>
                        </div>
                    </div>
                </div>

                <span class="timeline-label">
                    <span class="label label-primary">timeline-label</span>
                </span>

                <div class="timeline-item">
                    <div class="timeline-point timeline-point-primary">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="timeline-event timeline-event-primary">
                        <div class="timeline-heading">
                            <h4>timeline-event timeline-event-primary</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Stranguillione in deinde cepit roseo commendavit patris super color est se sed. Virginis plus plorantes abscederem assignato ipsum ait regem Ardalio nos filiae Hellenicus mihi cum. Theophilo litore in lucem in modo invenit quasi nomen magni ergo est se est Apollonius, habet clementiae venit ad nomine sed dominum depressit filia navem.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-23-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item">
                    <div class="timeline-point timeline-point-info">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="timeline-event timeline-event-info">
                        <div class="timeline-heading">
                            <h4>timeline-event timeline-event-info</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Advenientibus aliorum eam ad per te sed. Facere debetur aut veneris accedens.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-22-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item">
                    <div class="timeline-point timeline-point-warning">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="timeline-event timeline-event-warning">
                        <div class="timeline-heading">
                            <h4>timeline-event timeline-event-warning</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Stranguillione in deinde cepit roseo commendavit patris super color est se sed. Virginis plus plorantes abscederem assignato ipsum ait regem Ardalio nos filiae Hellenicus mihi cum. Theophilo litore in lucem in modo invenit quasi nomen magni ergo est se est Apollonius, habet clementiae venit ad nomine sed dominum depressit filia navem.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-23-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item">
                    <div class="timeline-point timeline-point-danger">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="timeline-event timeline-event-danger">
                        <div class="timeline-heading">
                            <h4>timeline-event timeline-event-danger</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Advenientibus aliorum eam ad per te sed. Facere debetur aut veneris accedens.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-22-2014</p>
                        </div>
                    </div>
                </div>

                <span class="timeline-label">
                    <span class="label label-info">timeline-label with label label-info</span>
                </span>

                <div class="timeline-item timeline-item-arrow-sm">
                    <div class="timeline-point timeline-point-primary">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="timeline-event timeline-event-primary">
                        <div class="timeline-heading">
                            <h4>timeline-item timeline-item-arrow-sm</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Volvitur ingreditur id ait mea vero cum autem quod ait Cumque ego illum vero cum unde beata. Commendavi si non dum est in. Dionysiadem tuos ratio puella ut casus, tunc lacrimas effunditis magister cives Tharsis. Puellae addita verbaque' capellam sanctissima quid, apollinem existimas filiam rex cum autem quod tamen adnuente rediens eam est se in. Peracta licet ad nomine Maria non ait in modo compungi mulierem volutpat.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-27-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item timeline-item-arrow-lg">
                    <div class="timeline-point timeline-point-blank">
                    </div>
                    <div class="timeline-event timeline-event-warning">
                        <div class="timeline-heading">
                            <h4>timeline-item timeline-item-arrow-lg timeline-point timeline-point-blank</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Adfertur guttae sapientiae ducitur est Apollonius ut a a his domino Lycoridem in lucem. Theophile atque bona dei cenam veniebant est cum. Iusto opes mihi Tyrum in modo compungi mulierem ubi augue eiusdem ordo quos vero diam omni catervis famulorum. Bene dictis ut diem finito servis unde.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Mar-01-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item">
                    <div class="timeline-point timeline-point-blank"></div>
                    <div class="timeline-event timeline-event-success">
                        <div class="timeline-heading">
                            <h4>timeline-point timeline-point-blank</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Crede respiciens loco dedit beneficio ad suis alteri si puella eius in. Acceptis codicello lenonem in deinde plectrum anni ipsa quod eam est Apollonius.</p>
                        </div>
                        <div class="timeline-footer primary">
                            <p class="text-right">Mar-02-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item timeline-item-left">
                    <div class="timeline-point timeline-point-blank"></div>
                    <div class="timeline-event timeline-event-default">
                        <div class="timeline-heading">
                            <h4>timeline-item timeline-item-left</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-21-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item timeline-item-left">
                    <div class="timeline-point timeline-point-blank"></div>
                    <div class="timeline-event timeline-event-default">
                        <div class="timeline-heading">
                            <h4>timeline-item timeline-item-left</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-21-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item timeline-item-left">
                    <div class="timeline-point timeline-point-blank"></div>
                    <div class="timeline-event timeline-event-default">
                        <div class="timeline-heading">
                            <h4>timeline-item timeline-item-left</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-21-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item timeline-item-left">
                    <div class="timeline-point timeline-point-blank"></div>
                    <div class="timeline-event timeline-event-default">
                        <div class="timeline-heading">
                            <h4>timeline-item timeline-item-left</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-21-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item timeline-item-right">
                    <div class="timeline-point timeline-point-blank"></div>
                    <div class="timeline-event timeline-event-default">
                        <div class="timeline-heading">
                            <h4>timeline-item timeline-item-right</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-21-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item timeline-item-right">
                    <div class="timeline-point timeline-point-blank"></div>
                    <div class="timeline-event timeline-event-default">
                        <div class="timeline-heading">
                            <h4>timeline-item timeline-item-right</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-21-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item timeline-item-right">
                    <div class="timeline-point timeline-point-blank"></div>
                    <div class="timeline-event timeline-event-default">
                        <div class="timeline-heading">
                            <h4>timeline-item timeline-item-right</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-21-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item timeline-item-right">
                    <div class="timeline-point timeline-point-blank"></div>
                    <div class="timeline-event timeline-event-default">
                        <div class="timeline-heading">
                            <h4>timeline-item timeline-item-right</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-21-2014</p>
                        </div>
                    </div>
                </div>

                <span class="timeline-label">
                    <button class="btn btn-danger"><i class="fa fa-ambulance"></i></button>
                </span>
            </div>
        </div>

        <div class="row">
            <h2>Single column</h2>
            <div class="timeline timeline-single-column">
                <div class="timeline-item timeline-item-arrow-sm">
                    <div class="timeline-point timeline-point-primary">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="timeline-event timeline-event-primary">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">timeline-event timeline-event-primary with panel panel-default</h3>
                            </div>
                            <div class="panel-body">
                                Panel content
                            </div>
                        </div>
                    </div>
                </div>

                <span class="timeline-label">
                    <span class="label label-primary">label</span>
                </span>

                <div class="timeline-item timeline-item-arrow-sm">
                    <div class="timeline-point timeline-point-primary">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="timeline-event timeline-event-primary">
                        <table class="table table-striped table-hover">
                            <tr>
                                <th>timeline-event timeline-event-primary</th>
                                <th>with table table-stripped table-hover</th>
                            </tr>
                            <tr>
                                <td>cell</td>
                                <td>cell</td>
                            </tr>
                            <tr>
                                <td>cell</td>
                                <td>cell</td>
                            </tr>
                            <tr>
                                <td>cell</td>
                                <td>cell</td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="timeline-item">
                    <div class="timeline-point">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>Timeline Event</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Advenientibus aliorum eam ad per te sed. Facere debetur aut veneris accedens.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-22-2014</p>
                        </div>
                    </div>
                </div>

                <span class="timeline-label">
                    <span class="label label-primary">label</span>
                </span>

                <div class="timeline-item">
                    <div class="timeline-point timeline-point-primary">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="timeline-event timeline-event-primary">
                        <div class="timeline-heading">
                            <h4>Timeline Event</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Volvitur ingreditur id ait mea vero cum autem quod ait Cumque ego illum vero cum unde beata. Commendavi si non dum est in. Dionysiadem tuos ratio puella ut casus, tunc lacrimas effunditis magister cives Tharsis. Puellae addita verbaque' capellam sanctissima quid, apollinem existimas filiam rex cum autem quod tamen adnuente rediens eam est se in. Peracta licet ad nomine Maria non ait in modo compungi mulierem volutpat.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Feb-27-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item">
                    <div class="timeline-point timeline-point-warning">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="timeline-event timeline-event-warning">
                        <div class="timeline-heading">
                            <h4>Timeline Event</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Adfertur guttae sapientiae ducitur est Apollonius ut a a his domino Lycoridem in lucem. Theophile atque bona dei cenam veniebant est cum. Iusto opes mihi Tyrum in modo compungi mulierem ubi augue eiusdem ordo quos vero diam omni catervis famulorum. Bene dictis ut diem finito servis unde.</p>
                        </div>
                        <div class="timeline-footer">
                            <p class="text-right">Mar-01-2014</p>
                        </div>
                    </div>
                </div>

                <div class="timeline-item">
                    <div class="timeline-point timeline-point-success">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="timeline-event timeline-event-success">
                        <div class="timeline-heading">
                            <h4>Timeline Event</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Crede respiciens loco dedit beneficio ad suis alteri si puella eius in. Acceptis codicello lenonem in deinde plectrum anni ipsa quod eam est Apollonius.</p>
                        </div>
                        <div class="timeline-footer primary">
                            <p class="text-right">Mar-02-2014</p>
                        </div>
                    </div>
                </div>

                <span class="timeline-label">
                    <span class="label label-info">label</span>
                </span>

                <div class="timeline-item">
                    <div class="timeline-point timeline-point-blank">
                    </div>
                    <div class="timeline-event">
                        <div class="timeline-heading">
                            <h4>Timeline Event</h4>
                        </div>
                        <div class="timeline-body">
                            <p>Crede respiciens loco dedit beneficio ad suis alteri si puella eius in. Acceptis codicello lenonem in deinde plectrum anni ipsa quod eam est Apollonius.</p>
                        </div>
                        <div class="timeline-footer primary">
                            <p class="text-right">Mar-02-2014</p>
                        </div>
                    </div>
                </div>

                <span class="timeline-label">
                    <button class="btn btn-danger"><i class="fa fa-ambulance"></i></button>
                </span>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type='text/javascript'>
    google.charts.load('current', {'packages': ['geochart']});
    google.charts.setOnLoadCallback(drawMarkersMap);

    function drawMarkersMap() {
        var data = google.visualization.arrayToDataTable([
            ['Ciudad', '$', 'QTY'],
            ['Milan', 5432, 100],
            ['Palermo', 567393, 10],
            ['Bogota', 500000, 110],
        ]);

        var options = {
            region: 'CO',
            displayMode: 'markers',
            colorAxis: {colors: ['red', 'blue']}
        };

        var chart = new google.visualization.GeoChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
    ;
</script>
<div id="chart_div" class="col-lg-2" style="width: 1200px; height: 500px;"></div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

        <div class="panel panel-primary">
            <div class="panel-leftheading">
                <h3 class="panel-lefttitle"><?php echo Yii::t('app', 'Pedidos'); ?></h3>
            </div>
            <div class="panel-rightbody">
                <div class="row">
                    <div class="col-xs-6 col-md-3 ">

                        <div class="panel status panel-danger">
                            <div class="panel-heading">
                                <h1 class="panel-title text-center">25</h1>
                            </div>
                            <div class="panel-body text-center">                        
                                <strong>Late</strong>
                            </div>
                        </div>

                    </div>          
                    <div class="col-xs-6 col-md-3">

                        <div class="panel status panel-warning">
                            <div class="panel-heading">
                                <h1 class="panel-title text-center">17</h1>
                            </div>
                            <div class="panel-body text-center">                        
                                <strong>Today</strong>
                            </div>
                        </div>

                    </div>
                    <div class="col-xs-6 col-md-3">

                        <div class="panel status panel-success">
                            <div class="panel-heading">
                                <h1 class="panel-title text-center">2</h1>
                            </div>
                            <div class="panel-body text-center">                        
                                <strong>Tomorrow</strong>
                            </div>
                        </div>


                    </div>
                    <div class="col-xs-6 col-md-3">

                        <div class="panel status panel-info">
                            <div class="panel-heading">
                                <h1 class="panel-title text-center">18</h1>
                            </div>
                            <div class="panel-body text-center">                        
                                <strong>Never Die</strong>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-3 col-lg-2">
        <div class="hero-widget well well-sm btn-lgreen">
            <div class="icon">
                <i class="glyphicon glyphicon-star"></i>
            </div>
            <div class="text">
                <var>614</var>
            </div>
            <div>
                <h3>Abiertas</h3>
            </div>
        </div>
    </div>
</div>
<hr>


<br />
<style>

</style>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="hero-widget well well-sm">
                <div class="icon">
                    <i class="glyphicon glyphicon-user"></i>
                </div>
                <div class="text">
                    <var>3</var>
                    <label class="text-muted">invited guests</label>
                </div>
                <div class="options">
                    <a href="javascript:;" class="btn btn-primary btn-lg"><i class="glyphicon glyphicon-plus"></i> Add a guest</a>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="hero-widget well well-sm btn-lgreen">
                <div class="icon">
                    <i class="glyphicon glyphicon-star"></i>
                </div>
                <div class="text">
                    <var>614</var>
                </div>
                <div>
                    <h3>Abiertas</h3>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="hero-widget well well-sm">
                <div class="icon">
                    <i class="glyphicon glyphicon-tags"></i>
                </div>
                <div class="text">
                    <var>73</var>
                    <label class="text-muted">total orders</label>
                </div>
                <div class="options">
                    <a href="javascript:;" class="btn btn-default btn-lg"><i class="glyphicon glyphicon-search"></i> View orders</a>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="hero-widget well well-sm">
                <div class="icon">
                    <i class="glyphicon glyphicon-cog"></i>
                </div>
                <div class="text">
                    <var>75%</var>
                    <label class="text-muted">profile complete</label>
                </div>
                <div class="options">
                    <a href="javascript:;" class="btn btn-default btn-lg"><i class="glyphicon glyphicon-wrench"></i> Edit profile</a>
                </div>
            </div>
        </div>
    </div>
</div>


<style>


</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="offer offer-default btn-lgray">
                <div class="shape">
                    <div class="shape-text">
                        <span class="glyphicon  glyphicon-user"></span>								
                    </div>
                </div>
                <div class="offer-content">
                    <h3 class="lead">
                        A default offer
                    </h3>
                    <p>
                        And a little description.
                        <br> and so one
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="offer offer-success">
                <div class="shape">
                    <div class="shape-text">
                        top							
                    </div>
                </div>
                <div class="offer-content text-green">
                    <h3 class="lead">
                        A success offer
                    </h3>						
                    <p>
                        And a little description.
                        <br> and so one
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="offer offer-radius offer-primary">
                <div class="shape">
                    <div class="shape-text">
                        top								
                    </div>
                </div>
                <div class="offer-content">
                    <h3 class="lead">
                        A primary-radius
                    </h3>						
                    <p>
                        And a little description.
                        <br> and so one
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="offer offer-info">
                <div class="shape">
                    <div class="shape-text">
                        top							
                    </div>
                </div>
                <div class="offer-content">
                    <h3 class="lead">
                        An new info offer
                    </h3>						
                    <p>
                        And a little description.
                        <br> and so one
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="offer offer-warning">
                <div class="shape">
                    <div class="shape-text">
                        top							
                    </div>
                </div>
                <div class="offer-content">
                    <h3 class="lead">
                        A warning offer
                    </h3>						
                    <p>
                        And a little description.
                        <br> and so one
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="offer offer-radius offer-danger">
                <div class="shape">
                    <div class="shape-text">
                        top							
                    </div>
                </div>
                <div class="offer-content">
                    <h3 class="lead">
                        A danger-radius
                    </h3>						
                    <p>
                        And a little description.
                        <br> and so one
                    </p>
                </div>
            </div>
        </div>	
    </div>
</div>
Fork from benjaminb10, thanks to him.
<a href="http://bootsnipp.com/snippets/featured/little-offers-with-colors-and-radius" TARGET="_blank">Link</a>

<div class="container">
    <div class="page-header">
        <h1>Panels with header on the side.</h1>
    </div>
    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-default">
                <div class="panel-leftheading">
                    <h3 class="panel-lefttitle">Panel title</h3>
                </div>
                <div class="panel-rightbody">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="clearfix">
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-leftheading">
                    <h3 class="panel-lefttitle">Panel title</h3>
                </div>
                <div class="panel-rightbody">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                </div>
                <div class="clearfix">
                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-leftheading">
                    <h3 class="panel-lefttitle">Panel title</h3>
                </div>
                <div class="panel-rightbody">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <div class="col-md-6">

            <div class="panel panel-info">  
                <div class="panel-leftheading">
                    <h3 class="panel-lefttitle">Panel title</h3>
                </div>
                <div class="panel-rightbody">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="clearfix">
                </div>   
            </div>

            <div class="panel panel-warning">  
                <div class="panel-leftheading">
                    <h3 class="panel-lefttitle">Panel title</h3>
                </div>
                <div class="panel-rightbody">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="clearfix">
                </div>
            </div>

            <div class="panel panel-danger">  
                <div class="panel-leftheading">
                    <h3 class="panel-lefttitle">Panel title</h3>
                </div>
                <div class="panel-rightbody">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="clearfix">
                </div>     
            </div>

        </div>
    </div>

    <hr>

    <?php
    $detect = Yii::app()->mobileDetect;
// call methods
    if ($detect->isMobile()) {
        echo 1;
    } else {
        echo 2;
    }
    $detect->isTablet();
    $detect->isIphone();
    ?>
    gio