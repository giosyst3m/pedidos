<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
      <tr class="danger">
          <th><?php echo Yii::t('app', 'Fecha');?></th>
          <th class="text-right"><?php echo Yii::t('app', 'Total');?></th>
      </tr>
    <?php 
    foreach ($model as $data ) {
        echo BsHtml::tag('tr', array(),  BsHtml::tag('td', array(),
                $data->dia
                ).BsHtml::tag('td',array('class'=>'text-right'),
                BsHtml::icon(BsHtml::GLYPHICON_USD). number_format($data->total,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)    
                        ));
        $labes[] = $data->dia;
        
        $datos[] = $data->total;
    }
    
    ?>
      <tfoot>
          <tr class="danger">
              <th>Total</th>
              <th class="text-right"><?php echo BsHtml::icon(BsHtml::GLYPHICON_USD).' '.number_format(array_sum($datos),Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS);?></th>
          </tr>
      </tfoot>
  </table>
</div>
<?php
$this->widget(
    'chartjs.widgets.ChLine', 
    array(
        'htmlOptions' => array(),
        'labels' => $labes,
        'datasets' => array(
            array(
                "label"=> "My Second dataset",
                "fillColor"=> "#f2dede",
                "strokeColor"=> "rgba(151,187,205,0.8)",
                "highlightFill"=> "rgba(151,187,205,0.75)",
                "highlightStroke"=> "rgba(151,187,205,1)",
                "data" => $datos
            )
        ),
        'options' => array(
            'responsive'=>true
        )
    )
); 
?>


