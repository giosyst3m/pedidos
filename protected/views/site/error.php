<div class="container body">
    <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">
                    <h1 class="error-number "><?php echo $code ?></h1>
                    <h2><?php echo Yii::t('app', CHtml::encode($message)); ?></h2>
                    <p><?php echo Yii::t('msg', 'SOPORTE'); ?></p>
                    <div class="mid_center">
                        <?php echo BsHtml::link(BsHtml::icon(BsHtml::GLYPHICON_HOME).' '.Yii::t('app', 'Go to Home'), Yii::app()->homeUrl, ['class'=>'btn btn-success'] ) ;?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>