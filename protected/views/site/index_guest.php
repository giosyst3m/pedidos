<?php $themePath = Yii::app()->theme->baseUrl;?>
<!-- work section -->
        <section id="work">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="section-title">
                            <strong>EXPERIENCIA</strong>
                            
                            <hr>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.6s">
                        <i class=" color icon-cloud medium-icon"></i>
                        <h3>Aplicaciones Web</h3>
                        <hr>
                        <p>Desarrollo a la medida, intranet, extranet, automatización de procesos, sistema integrados en la nube </p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.9s">
                        <i class="color icon-mobile medium-icon"></i>
                        <h3>UI &amp; UX DESIGN</h3>
                        <hr>
                        <p>Desarrollo de sistemas basado en las Mejores de prácticas de usabilidad para garantizar una buena experienza</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="1s">
                        <i class="color icon-search medium-icon"></i>
                        <h3>SEO</h3>
                        <hr>
                        <p>Visibilidad en los navegadores, optimizando los sitios para lograr busquedas mas efectivas, en palabras, descripciones, etc</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="1s">
                        <i class="color icon-bike medium-icon"></i>
                        <h3>ASESORIAS</h3>
                        <hr>
                        <p>Enteder la necesidad, buscar la mejor alternativa evitando reinventar la rueda.</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="1s">
                        <i class="color icon-global medium-icon"></i>
                        <h3>DOMINIOS + HOSTING + SERVER</h3>
                        <hr>
                        <p>Ofrecer el lugar indicado para tu Sitio Web, con espacio <strong>ilimitado</strong>, hosting, servidores según la necesidad</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="1s">
                        <i class="color icon-browser medium-icon"></i>
                        <h3>RESPONSIVE LAYOUT</h3>
                        <hr>
                        <p>En la búsqueda de tener el sitio, accesible y posible de usar desde cualquier dispositivo </p>
                    </div>
                </div>
            </div>
        </section>

        <!-- about section -->
        <section id="about">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center">
                        <div class="section-title">
                            <strong>ACERCA DE</strong>
                            <h1 class="heading bold">OPINIÓN</h1>
                            <hr>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <img src="<?php echo $themePath; ?>/landing/images/about-img.jpg" class="img-responsive" alt="about img">
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <h3 class="bold">DIGITAL CONECTA</h3>
                        <h1 class="heading bold">El mundo <span class="color">DIGITAL</span> te <span class="color">CONECTA</span> con todo</h1>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#design" aria-controls="design" role="tab" data-toggle="tab">Mundo Digital</a></li>
                            <li><a href="#mobile" aria-controls="mobile" role="tab" data-toggle="tab">Marketing Digital</a></li>
                            <li><a href="#social" aria-controls="social" role="tab" data-toggle="tab">Networking </a></li>
                        </ul>
                        <!-- tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="design">
                                <p>Estamos viviendo en la era de la información cada día la información va cambiado en segundos, nos enteramos de nosticas de un extremo del mundo</p>
                                <p></p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="mobile">
                                <p>Aenean commodo ligula eget dolor. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                                <p><a href="#">Duis aute irure dolor</a> in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="social">
                                <p>Pellentesque elementum, lacus sit amet <a href="#">hendrerit</a> posuere, quam quam tristique nisi, nec ornare ligula magna id nisl. Donec blandit enim ac semper facilisis. Curabitur eu laoreet mauris, eget fermentum velit.</p>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet. Dolore magna aliquam erat volutpat.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- team section -->
        <!--<section id="team">
                <div class="container">
                        <div class="row">
                                <div class="col-md-12 col-sm-12">
                                        <div class="section-title">
                                                <strong>03</strong>
                                                <h1 class="heading bold">TALENTED TEAM</h1>
                                                <hr>
                                        </div>
                                </div>
                                <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay="0.9s">
                                        <div class="team-wrapper">
                                                <img src="<?php echo $themePath; ?>/landing/images/team1.jpg" class="img-responsive" alt="team img">
                                                        <div class="team-des">
                                                                <h4>Cindy</h4>
                                                                <h3>Senior Designer</h3>
                                                                <hr>
                                                                <ul class="social-icon">
                                                                        <li><a href="#" class="fa fa-facebook wow fadeIn" data-wow-delay="0.3s"></a></li>
                                                                        <li><a href="#" class="fa fa-twitter wow fadeIn" data-wow-delay="0.6s"></a></li>
                                                                        <li><a href="#" class="fa fa-dribbble wow fadeIn" data-wow-delay="0.9s"></a></li>
                                                                </ul>
                                                        </div>
                                        </div>
                                </div>
                                <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay="1.3s">
                                        <div class="team-wrapper">
                                                <img src="<?php echo $themePath; ?>/landing/images/team2.jpg" class="img-responsive" alt="team img">
                                                        <div class="team-des">
                                                                <h4>Mary</h4>
                                                                <h3>Core Developer</h3>
                                                                <hr>
                                                                <ul class="social-icon">
                                                                        <li><a href="#" class="fa fa-facebook wow fadeIn" data-wow-delay="0.3s"></a></li>
                                                                        <li><a href="#" class="fa fa-twitter wow fadeIn" data-wow-delay="0.6s"></a></li>
                                                                        <li><a href="#" class="fa fa-dribbble wow fadeIn" data-wow-delay="0.9s"></a></li>
                                                                </ul>
                                                        </div>
                                        </div>
                                </div>
                                <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay="1.6s">
                                        <div class="team-wrapper">
                                                <img src="<?php echo $themePath; ?>/landing/images/team3.jpg" class="img-responsive" alt="team img">
                                                        <div class="team-des">
                                                                <h4>Linda</h4>
                                                                <h3>Manager</h3>
                                                                <hr>
                                                                <ul class="social-icon">
                                                                        <li><a href="#" class="fa fa-facebook wow fadeIn" data-wow-delay="0.3s"></a></li>
                                                                        <li><a href="#" class="fa fa-twitter wow fadeIn" data-wow-delay="0.6s"></a></li>
                                                                        <li><a href="#" class="fa fa-dribbble wow fadeIn" data-wow-delay="0.9s"></a></li>
                                                                </ul>
                                                        </div>
                                        </div>
                                </div>
                                <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay="1.6s">
                                        <div class="team-wrapper">
                                                <img src="<?php echo $themePath; ?>/landing/images/team4.jpg" class="img-responsive" alt="team img">
                                                        <div class="team-des">
                                                                <h4>Sandar</h4>
                                                                <h3>Accountant</h3>
                                                                <hr>
                                                                <ul class="social-icon">
                                                                        <li><a href="#" class="fa fa-facebook wow fadeIn" data-wow-delay="0.3s"></a></li>
                                                                        <li><a href="#" class="fa fa-twitter wow fadeIn" data-wow-delay="0.6s"></a></li>
                                                                        <li><a href="#" class="fa fa-dribbble wow fadeIn" data-wow-delay="0.9s"></a></li>
                                                                </ul>
                                                        </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </section>-->

        <!-- portfolio section -->
        <div id="portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="section-title">
                            <strong>PROTAFOLIO</strong>
                            
                            <hr>
                        </div>
                        <!-- ISO section -->
                        <div class="iso-section">
                            <ul class="filter-wrapper clearfix">
                                <li><a href="#" data-filter="*" class="selected opc-main-bg">All</a></li>
                                <li><a href="#" class="opc-main-bg" data-filter=".habitacion">Hoteles</a></li>
                                <li><a href="#" class="opc-main-bg" data-filter=".pedidos">Pedidos Integrales</a></li>
                                <li><a href="#" class="opc-main-bg" data-filter=".wordpress">Proyectos</a></li>
                                <li><a href="#" class="opc-main-bg" data-filter=".hosting">Dominios + Hosting + Servers</a></li>
                            </ul>
                            <div class="iso-box-section wow fadeIn" data-wow-delay="0.9s">
                                <div class="iso-box-wrapper col4-iso-box">

                                    <div class="iso-box hosting col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/linux-shared-hosting.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/linux-shared-hosting.png" alt="Hosting"></a>
                                    </div>
                                    <div class="iso-box pedidos col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digita-conecta-pedidos-integrados-catalogo-productos.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digita-conecta-pedidos-integrados-catalogo-productos.png" alt="Pedidos Integrados - Dashboard 1"></a>
                                    </div>
                                    <div class="iso-box habitacion col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-productos.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-productos.png" alt="Hotel"></a>
                                    </div>
                                    <div class="iso-box hosting col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/domain-registration-search-and-buy-domain.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/domain-registration-search-and-buy-domain.png" alt="Dominios"></a>
                                    </div>
                                    <div class="iso-box habitacion col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-habitaciones-estados.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-habitaciones-estados.png" alt="Hotel"></a>
                                    </div>
                                    <div class="iso-box pedidos col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digital-conecta-pedidos-dashboard.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digital-conecta-pedidos-dashboard.png" alt="Pedidos Integrados - Dashboard 1"></a>
                                    </div>

                                    <div class="iso-box hosting col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/cloud-hosting.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/cloud-hosting.png" alt="Cloud Server"></a>
                                    </div>
                                    <div class="iso-box habitacion col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-empleados.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-empleados.png" alt="Hotel"></a>
                                    </div>
                                    <div class="iso-box pedidos col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digita-conecta-pedidos-integrados-pedidos.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digita-conecta-pedidos-integrados-pedidos.png" alt="Pedidos Integrados - Dashboard 1"></a>
                                    </div>
                                    <div class="iso-box hosting col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digital-ssl-certificates.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digital-ssl-certificates.png" alt="SSL"></a>
                                    </div>

                                    <div class="iso-box pedidos col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digita-conecta-pedidos-integrados-dashboard-2.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digita-conecta-pedidos-integrados-dashboard-2.png" alt="Pedidos Integrados - Dashboard 1"></a>
                                    </div>
                                    <div class="iso-box habitacion col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-habitaciones.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-habitaciones.png" alt="Hotel"></a>
                                    </div>

                                    <div class="iso-box hosting col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/dominios-hosting-server.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/dominios-hosting-server.png" alt="Servers"></a>
                                    </div>
                                    <div class="iso-box pedidos col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digita-conecta-pedidos-integrados-productos" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digita-conecta-pedidos-integrados-productos.png" alt="Pedidos Integrados - Dashboard 1"></a>
                                    </div>
                                    <div class="iso-box habitacion col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-caja.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-caja.png" alt="Hotel"></a>
                                    </div>
                                    <div class="iso-box hosting col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/business-email.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/business-email.png" alt="Emails"></a>
                                    </div>

                                    <div class="iso-box pedidos col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digita-conecta-pedidos-integrados-dashboard-3.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digita-conecta-pedidos-integrados-dashboard-3.png" alt="Pedidos Integrados - Dashboard 1"></a>
                                    </div>
                                    <div class="iso-box habitacion col-lg-4 col-md-4 col-sm-6">
                                        <a href="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-dashboard.png" data-lightbox-gallery="portfolio-gallery"><img src="<?php echo $themePath; ?>/landing/images/digital-conecta-hotel-dashboard.png" alt="Hotel"></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>		

        <!-- pricing section -->
        <section id="pricing">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center">
                        <div class="section-title">
                            <strong>Productos y Servicios</strong>
                            <h1 class="heading bold">en <a href="http://www.giosyst3m.com/web-hosting/index.php#utm_source=digital-conecta&utm_medium=price&utm_campaign=fija&utm_term=domain%2Bhosting%2Bservers&utm_content=textlink" target="_blank"> GioSyst3m.com</a> puedes obtener los mejores precios</h1>
                            <hr>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="plan plan-one wow bounceIn" data-wow-delay="0.3s">
                            <div class="plan_title">
                                <i class=" color icon-global medium-icon"></i>
                                <h3>Dominios</h3>
                                <div>desde <h2>USD$11 <span>al año</span></h2></div>
                            </div>
                            <ul>
                                <li>.com, .net, .org</li>
                                <li>.online, .co, com.co</li>
                                <li>.bio, education</li>
                                <li>.club, .info y más</li>
                            </ul>
                            <a class="btn btn-warning btn-block" href="http://www.giosyst3m.com/domain-registration/index.php#utm_source=digital-conecta&utm_medium=price&utm_campaign=fija&utm_term=domain&utm_content=buttonlink" target="_blank">Buscar Dominios</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="plan plan-two wow bounceIn" data-wow-delay="0.3s">
                            <div class="plan_title">
                                <i class=" color icon-cloud medium-icon"></i>
                                <h3>Hosting</h3>
                                <div>desde<h2>USD4.19 <span>el mes</span></h2></div>
                            </div>
                            <ul>
                                <li>Espacio Ilimitado</li>
                                <li>Emails ilimitados</li>
                                <li>Base de datos Ilimitadas</li>
                                <li>Linux / Windows</li>
                            </ul>
                            <a class="btn btn-warning btn-block" href="http://www.giosyst3m.com/web-hosting/index.php#utm_source=digital-conecta&utm_medium=price&utm_campaign=fija&utm_term=hosting%2Bservers&utm_content=buttonlink" target="_blank">Ver Planes</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="plan plan-three wow bounceIn" data-wow-delay="0.3s">
                            <div class="plan_title">
                                <i class="color icon-layers medium-icon"></i>
                                <h3>Aplicaciones</h3>
                                <div>Prespuestos<h2>a la medida<span></span></h2></div>
                            </div>
                            <ul>
                                <li>Aplicaciones Web</li>
                                <li>Diseño</li>
                                <li>Desarrollo</li>
                                <li>Implementación</li>
                            </ul>
                            <a class="btn btn-warning btn-block" href="#contact">Contáctatos</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="plan plan-three wow bounceIn" data-wow-delay="0.3s">
                            <div class="plan_title">
                                <i class="color icon-browser medium-icon"></i>
                                <h3>WEB</h3>
                                <div>Prespuestos<h2>a la medida<span></span></h2></div>
                            </div>
                            <ul>
                                <li>Landing Page</li>
                                <li>Páginas Web</li>
                                <li>E-Commerce</li>
                                <li>y más</li>
                            </ul>
                            <a class="btn btn-warning  btn-block" href="#contact">Contáctatos</a>
                        </div>
                    </div>
                </div>
            </div>		
        </section>

        <!-- contact section -->
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center">
                        <div class="section-title">
                            <strong>Cantáctanos</strong>
                            <h1 class="heading bold">Déjanos tu mensaje</h1>
                            <hr>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 contact-info">
                        <h2 class="heading bold">Información de Contacto</h2>

                        <div class="col-md-6 col-sm-4">
                            <h3><i class="icon-envelope medium-icon wow bounceIn" data-wow-delay="0.6s"></i>EMAIL</h3>
                            <p><a style="color:#fff !important" href="mailto:info@digitalconecta.com">info@digtalconecta.com</a></p>
                        </div>
                        <div class="col-md-6 col-sm-4">
                            <h3><i class="icon-phone medium-icon wow bounceIn" data-wow-delay="0.6s"></i> Celular</h3>
                            <p>+57 304 495 89 59</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <?php
                            $flashMessages = Yii::app()->user->getFlashes();
                            if ($flashMessages) {
                                foreach ($flashMessages as $key => $message) {
                                    echo BsHtml::tag('div', array('class' => 'info'), BsHtml::alert('alert alert-' . $key, $message), true);
                                }
                            }
                            ?>
                        <?php $this->renderPartial('../../modules/sistema/views/contacto/_form',['model'=>$SisContacto]); ?>
                    </div>
                </div>
            </div>
        </section>

