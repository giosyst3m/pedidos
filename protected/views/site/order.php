<div class="row tile_count">
    <?php foreach ($VOrderStatusSumary as $value) { ?>
        <div class="tile_stats_count">
            <span class="count_top"><i class="<?php echo $value->icon; ?>"></i> <?php echo $value->status_name; ?></span>
            <div class="count <?php echo $value->color; ?>" ><?php echo number_format($value->total, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS); ?></div>
            <span class="count_bottom"><i class="green"><?php echo $value->quantity; ?></i> Total</span>
        </div>
    
    <?php } ?>
</div>

