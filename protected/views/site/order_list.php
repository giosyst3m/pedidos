<?php

$this->widget('ext.LiveGridView.RefreshGridView', array(
    'id' => 'order-grid',
    'updatingTime' => Yii::app()->params['DASHBOARD_ACTUALIZACION_TIEMPOS_VALES'],
    'dataProvider' => $model->search(),
    'filter' => $model,
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        array(
            'name' => 'number',
            'header' => Yii::t('app', 'Nro'),
            'type' => 'raw',
            'value' => function($data) {
                return BsHtml::link($data->number,Yii::app()->createUrl("ordDetail/view/", array("id" => $data->orderNumber)),['class'=>'btn btn-primary','icon'=>  BsHtml::GLYPHICON_EDIT]). '<br>' . BsHtml::tag('b', [], $data->complete_name, true);
            },
                    'filterHtmlOptions' => [
                        'class' => 'hidden-lg',
                    ],
                    'headerHtmlOptions' => [
                        'class' => 'hidden-lg',
                    ],
                    'htmlOptions' => [
                        'class' => 'hidden-lg text-mobil',
                    ],
                ),
                array(
                    'name' => 'number',
                    'header' => Yii::t('app', 'Nro'),
                    'filterHtmlOptions' => [
                        'class' => 'hidden-xs',
                    ],
                    'headerHtmlOptions' => [
                        'class' => 'hidden-xs',
                    ],
                    'htmlOptions' => [
                        'class' => 'hidden-xs',
                    ]
                ),
                array(
                    'name' => 'code',
                    'header' => Yii::t('app', 'RUT'),
                    'filterHtmlOptions' => [
                        'class' => 'hidden-xs',
                    ],
                    'headerHtmlOptions' => [
                        'class' => 'hidden-xs',
                    ],
                    'htmlOptions' => [
                        'class' => 'hidden-xs',
                    ]
                ),
                array(
                    'name' => 'correlative',
                    'header' => Yii::t('app', 'Suc'),
                    'filterHtmlOptions' => [
                        'class' => 'hidden-xs  hidden-md',
                    ],
                    'headerHtmlOptions' => [
                        'class' => 'hidden-xs  hidden-md',
                    ],
                    'htmlOptions' => [
                        'class' => 'hidden-xs  hidden-md',
                    ]
                ),
                array(
                    'name' => 'client',
                    'header' => Yii::t('app', 'Client'),
                    'type'=>'raw',
                    'value' => function($data){
                        return BsHtml::tag('b',[],$data->client).'<br>'.
                        BsHtml::tag('small',[],$data->address).'<br>'.
                        BsHtml::tag('label', ['class'=>'label label-primary hidden-xs','styele'=> 'font-size: xx-small;'],$data->state.', '.$data->city).
                            BsHtml::tag('label', ['class'=>'label label-primary hidden-md hidden-sm hidden-lg ','styele'=> 'font-size: xx-small;'],$data->city);
                    },
                    'htmlOptions' => [
                        'class' => 'text-mobil',
                    ]
                ),
                array(
                    'name' => 'id_status',
                    'header' => Yii::t('app', 'Status'),
                    'type' => 'raw',
                    'filter' => $status,
                    'value' => function($data) {
                        return BsHtml::tag('b', [
                                    'class' => $data->color,
                                    'data-toggle' => "tooltip",
                                    'title' => $data->status_description,
                                    'data-placement' => "bottom"
                                        ], $data->status.
                                '<br><span class="hidden-lg hidden-md">'.number_format($data->total,  Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS), true).'</span>';
                    },
                            'htmlOptions' => [
                                'class' => 'text-mobil',
                            ],
                            'filterHtmlOptions' => [
                                'class' => '',
                            ],
                            'headerHtmlOptions' => [
                                'class' => '',
                            ],
                            'htmlOptions' => [
                                'class' => '',
                            ],
                        ),
                        array(
                            'name' => 'r_c_u',
                            'type' => 'raw',
                            'value' => function($data) {
                                $VSisUsuario = VSisUsuario::model()->findByAttributes(['id' => $data->r_c_u]);
                                return '<div class="row"><div class="col-lg-6">' . BsHtml::image(Yii::app()->getBaseUrl(FALSE) . Yii::app()->user->getState('USER')->USER_FILE_PATH . $VSisUsuario->photo, '', ['class' => 'img-circle profile_img']) . '</div><div class="col-lg-6"> ' . $data->complete_name . '</div>';
                            },
                                    'filter' => $VSisUsuario,
                                    'filterHtmlOptions' => [
                                        'class' => 'hidden-xs hidden-sm',
                                    ],
                                    'headerHtmlOptions' => [
                                        'class' => 'hidden-xs hidden-sm',
                                    ],
                                    'htmlOptions' => [
                                        'class' => 'hidden-xs hidden-sm',
                                    ]
                                ),
                                array(
                                    'name' => 'total',
                                    'value' => 'number_format($data->total,0)',
                                    'htmlOptions' => [
                                        'class' => 'text-right text-mobil text-mobil',
                                    ],
                                    'filterHtmlOptions' => [
                                            'class' => 'hidden-xs',
                                        ],
                                        'headerHtmlOptions' => [
                                            'class' => 'hidden-xs',
                                        ],
                                        'htmlOptions' => [
                                            'class' => 'hidden-xs text-mobil',
                                        ],
                                ),
                                [
                                    'name' => 'r_c_d',
                                    'filterHtmlOptions' => [
                                        'class' => 'hidden-xs hidden-sm hidden-md',
                                    ],
                                    'headerHtmlOptions' => [
                                        'class' => 'hidden-xs hidden-sm hidden-md',
                                    ],
                                    'htmlOptions' => [
                                        'class' => 'hidden-xs hidden-sm hidden-md',
                                    ]
                                ],
                                array(
                                    'template' => '{view}',
                                    'class' => 'bootstrap.widgets.BsButtonColumn',
                                    'buttons' => array(
                                        'view' => array(
                                            'url' => function($data) {
                                                return Yii::app()->createUrl("ordDetail/view/", array("id" => $data->orderNumber));
                                            },
                                            'options' => [
                                                'class' => 'hidden-xs hidden-sm hidden-md btn btn-primary',
                                            ]
                                                ),
                                                        
                                            ),
                                        ),
                                    ),
                                ));
                                ?>




