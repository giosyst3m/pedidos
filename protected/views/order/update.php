<?php
/* @var $this OrderController */
/* @var $model Order */
?>

<?php
$this->breadcrumbs=array(
	'Orders'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Order').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('OrderUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
                'client'=>$client,
        'status'=>$status,
        'delivery'=>$delivery,
        
        )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('OrderUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>