<?php

if ($FormDefault) {
    $Column = array(
        'name' => 'id_company',
        'header' => Yii::t('app', 'Company'),
        'value' => '$data->idCompany->name',
        'filter' => $Company
    );
} else {
    $Column = array(
        'name' => 'r_c_u',
        'value' => '$data->rcu->nombre." ".$data->rcu->apellido',
        'filter' => $VSisUsuario
    );
}
if ($FormDefault2) {
    $Column2 = array(
        'name' => 'r_c_u',
        'value' => '$data->rcu->nombre." ".$data->rcu->apellido',
        'filter' => $VSisUsuario
    );
} else {
    $Column2 = array(
        'name' => 'id_client',
        'header' => Yii::t('app', 'Client'),
        'value' => '$data->idClient->name',
        'filter' => $client
    );
}
$this->widget('bootstrap.widgets.BsGridView', array(
    'id' => 'order-grid',
    'dataProvider' => $model2->search(),
    'filter' => $model2,
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        array(
            'name' => 'number',
            'header' => Yii::t('app', 'Order Nro'),
        ),
        $Column2,
        $Column,
        'r_c_d',
        array(
            'template' => '{view}',
            'class' => 'bootstrap.widgets.BsButtonColumn',
            'buttons' => array(
                'view' => array(
                    'url' => function($data) {
                        return Yii::app()->createUrl("ordDetail/view/", array("id" => $data->id));
                    }
                        ),
                    ),
                ),
            ),
        ));
        ?>





