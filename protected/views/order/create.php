<?php
/* @var $this OrderController */
/* @var $model Order */
?>

<?php
$this->breadcrumbs=array(
	'Orders'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Order')) ?>

<?php 
if(Yii::app()->user->checkAccess('OrderCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model,
            'client'=>$client,
        'status'=>$status,
        'delivery'=>$delivery,
            )); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('OrderCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2,
            'client'=>$client,
        'status'=>$status,
        'delivery'=>$delivery,
        'VSisUsuario'=>$VSisUsuario
            )); 
}
?>