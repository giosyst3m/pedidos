<?php
/* @var $this CityController */
/* @var $model City */


$this->breadcrumbs=array(
	'Cities'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-list','label'=>'List City', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create City', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#city-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="x_panel">
    <div class="x_title">
        <h2>Pedidos pasado a Pagados cuando NO tienen facturas pendientes.</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            
            </li>
            
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />

       <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'brand-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
            'type' => BsHtml::GRID_TYPE_RESPONSIVE,
			'columns'=>array(
                            'id',
                            'client',
                            [
                                'name'=>'total',
                                'value'=> function($data){
                                    return number_format($data->total,  Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS);
                                }
                            ]
                            
			),
        )); ?>


    </div>
</div>



