<?php

$this->widget('bootstrap.widgets.BsGridView', array(
    'id' => 'discount-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        array(
            'name' => 'id',
            'filter' => false,
            'filterHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'htmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
        ),
        array(
            'name' => 'id_document',
            'type'  => 'raw',
            'value' => function($data){
                return $data->idDocument->name.'<br>'.BsHtml::tag('span',['class'=>$data->idStatus->color],$data->idStatus->name).'<br>'.
                '<b>'.BsHtml::tag('span',['class'=>'hidden-lg hidden-md hidden-sm'],$data->number).'</b><br>'.
                BsHtml::tag('span',['class'=>'hidden-lg hidden-md hidden-sm'],$data->date);
            
            },
            'header' => 'Tipo Documento',
            'filter' => false,
            'filterHtmlOptions' => [
                'class' => 'text-mobil',
            ],
            'headerHtmlOptions' => [
                'class' => 'text-mobil',
            ],
            'htmlOptions' => [
                'class' => 'text-mobil',
            ],
        ),
        array(
            'name' => 'number',
            'filter' => false,
            'filterHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'htmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
        ),
        array(
            'name' => 'date',
            'filter' => false,
            'filterHtmlOptions' => [
                'class' => 'hidden-xs  text-mobil',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-xs  text-mobil',
            ],
            'htmlOptions' => [
                'class' => 'hidden-xs  text-mobil',
            ],
        ),
        array(
            'name' => 'total',
            'value' => 'number_format($data->total,Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS)',
            'htmlOptions' => array('class' => 'text-right'),
            'filter' => false,
        ),
        array(
            'name' => 'r_c_d',
            'filter' => false,
            'filterHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'htmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
        ),
        array(
            'name' => 'note',
            'filter' => false,
            'filterHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'headerHtmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
            'htmlOptions' => [
                'class' => 'hidden-xs  ',
            ],
        ),
        array(
            'name' => 'r_c_u',
            'filter' => false,
            'value'=>'$data->rcu->nombre." ".$data->rcu->apellido',
            'filterHtmlOptions' => [
                'class' => 'text-mobil',
            ],
            'headerHtmlOptions' => [
                'class' => 'text-mobil',
            ],
            'htmlOptions' => [
                'class' => 'text-mobil',
            ],
        ),
        array(
            'header' => Yii::t('app', ''),
            'type' => 'raw',
            'value' => function($data) {
                $param = array('post' => $data);
                if ($data->idOrder->id_status <= 5 && $data->id_document > 1) {
                    if (Yii::app()->user->checkAccess('OrdDocumentDeleteOwn', $param)) {
                        return CHtml::tag('span', array('class' => 'fa fa-trash btn btn-danger', 'onclick' => 'deleteOrdDocument(' . $data->id . ')'));
                    }
                } else {
                    return '<i></i>';
                }
            },
            'filterHtmlOptions' => [
                'class' => 'text-mobil',
            ],
            'headerHtmlOptions' => [
                'class' => 'text-mobil',
            ],
            'htmlOptions' => [
                'class' => 'text-mobil',
            ],
                ),
            ),
        ));
        ?>

