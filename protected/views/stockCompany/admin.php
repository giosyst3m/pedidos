<?php

$this->widget('bootstrap.widgets.BsGridView', array(
    'id' => 'stock-company-grid',
    'dataProvider' => $model2->search(),
    'filter' => $model2,
    'type' => BsHtml::GRID_TYPE_RESPONSIVE,
    'columns' => array(
        array(
            'header'=>  Yii::t('app','Store'),
            'name' => 'id_stock',
            'value' => '$data->idStock->name',
        ),
        array(
            'name' => 'r_d_s',
            'value' => '$data->RegistroEstado($data->r_d_s)',
            'filter' => array(0 => 'Inactivo', 1 => 'Activo')
        ),
        array(
            'class' => 'bootstrap.widgets.BsButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array
                    (
                    'label' => Yii::t('app', 'Change Status Seller in the Company'), //Text label of the button.
                    'url' => 'Yii::app()->createUrl("StockCompany/delete", array("id"=>$data->id))', //A PHP expression for generating the URL of the button.
                )
            )
        ),
    ),
));
?>




