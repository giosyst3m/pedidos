<?php
/* @var $this StockCompanyController */
/* @var $model StockCompany */
?>

<?php
$this->breadcrumbs=array(
	'Stock Companies'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','StockCompany')) ?>

<?php 
if(Yii::app()->user->checkAccess('StockCompanyCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('StockCompanyCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>