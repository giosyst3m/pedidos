<?php
/* @var $this StockCompanyController */
/* @var $model StockCompany */
?>

<?php
$this->breadcrumbs=array(
	'Stock Companies'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','StockCompany').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('StockCompanyUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('StockCompanyUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>