<?php
/* @var $this ProGroupController */
/* @var $model ProGroup */
?>

<?php
$this->breadcrumbs = array(
    'Pro Groups' => array('create'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Update') ?> <?php echo Yii::t('app', 'ProGroup') . ': ' . BsHtml::tag('b', [], $model->name); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        if (Yii::app()->user->checkAccess('ProGroupUpdateFormView')) {
            $this->renderPartial('_form', array('model' => $model));
        }
        ?>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('ProGroupUpdateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2));
}
?>