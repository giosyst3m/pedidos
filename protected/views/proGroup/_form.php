<?php
/* @var $this ProGroupController */
/* @var $model ProGroup */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'pro-group-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'orden',array('maxlength'=>3)); ?>
    <?php echo $form->fileFieldControlGroup($model, 'imagen'); ?>
        <?php
        if (!empty($model->imagen)) {
            echo '<div class="center-block text-center">';
            echo $model->ShowImagen($model->imagen, Yii::app()->params['URL_PRODUCTO_IMAGEN'].'progroup/');
            echo '</div><div class="clearfix">&nbsp;</div>';
        }
        ?>
    <?php 
    if(Yii::app()->user->checkAccess('ProGroupCreateStatusChange') || Yii::app()->user->checkAccess('ProGroupUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('ProGroupCreateButtonSave') || Yii::app()->user->checkAccess('ProGroupUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('ProGroupCreateButtonNew') || Yii::app()->user->checkAccess('ProGroupUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('ProGroup/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
