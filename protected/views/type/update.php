<?php
/* @var $this TypeController */
/* @var $model Type */
?>

<?php
$this->breadcrumbs=array(
	'Types'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Type').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('TypeUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('TypeUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>