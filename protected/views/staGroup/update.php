<?php
/* @var $this StaGroupController */
/* @var $model StaGroup */
?>

<?php
$this->breadcrumbs=array(
	'Sta Groups'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','StaGroup').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('StaGroupUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('StaGroupUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>