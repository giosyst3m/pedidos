<?php
/* @var $this StaGroupController */
/* @var $model StaGroup */
?>

<?php
$this->breadcrumbs=array(
	'Sta Groups'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','StaGroup')) ?>

<?php 
if(Yii::app()->user->checkAccess('StaGroupCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('StaGroupCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>