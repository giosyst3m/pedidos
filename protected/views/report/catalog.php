<?php
/* @var $this CatalogController */

$this->breadcrumbs = array(
    'Report',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/report/catalog.js', CClientScript::POS_END);
?>
<div class="row">
    <div class="col-sm-12 col-sm-12 col-md-6 col-lg-6">
        <h4><?php echo Yii::t('app', 'Group'); ?></h4>
        <?php
        echo BsHtml::dropDownList('ProGroup', array(), $ProGroup, array(
            'multiple' => 'multiple',
            'class' => 'selectpicker show-tick',
            'data-live-search' => true,
            'label' => 'Group',
            'name' => 'Group[]',
            'title' => Yii::t('app', 'Select'),
            'data-style' => "btn-primary",
            'id'=>'ProGroup',
        ))
        ?>
    </div>
    <div class="col-sm-12 col-sm-12 col-md-6 col-lg-6">
        <button class="btn btn-primary btn-block" id='report_pdf_pict'><i class="fa fa-file-pdf-o"></i>&nbsp;<?php echo Yii::t('app', 'PDF Products list with Picture'); ?></button>
        <button class="btn btn-primary btn-block" id='report_pdf_no_pict'><i class="fa fa-file-pdf-o"></i>&nbsp;<?php echo Yii::t('app', 'PDF Products list without Picture'); ?></button>
        <button class="btn btn-primary btn-block" id='report_excel'><i class="fa fa-file-excel-o"></i>&nbsp;<?php echo Yii::t('app', 'Excel'); ?></button>
    </div>
</div>
