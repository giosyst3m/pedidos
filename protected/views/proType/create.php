<?php
/* @var $this ProTypeController */
/* @var $model ProType */
?>

<?php
$this->breadcrumbs=array(
	'Pro Types'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','ProType')) ?>

<?php 
if(Yii::app()->user->checkAccess('ProTypeCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ProTypeCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>