<?php
/* @var $this ProTypeController */
/* @var $model ProType */
/* @var $form BSActiveForm */

?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'pro-type-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_VERTICAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p > <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>50,'class'=>'form-control','required'=>'required')); ?>
    <?php echo $form->textAreaControlGroup($model,'descripction',array('rows'=>6)); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('ProTypeCreateStatusChange') || Yii::app()->user->checkAccess('ProTypeUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('ProTypeCreateButtonSave') || Yii::app()->user->checkAccess('ProTypeUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('ProTypeCreateButtonNew') || Yii::app()->user->checkAccess('ProTypeUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('ProType/index'),array('class'=>  'btn btn-primary')); 
    }?>  
    
<?php $this->endWidget(); ?>
