<?php
/* @var $this ProTypeController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Pro Types',
);
?>

<?php echo BsHtml::pageHeader('Pro Types') ?>
<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'pro-type-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>
<div class="row">
    <div class="col-xs-6 col-med-6">
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
    <div class="col-xs-6 col-med-6">
        <?php $this->renderPartial('_list', array(
            'items'=>$items,
            'item'=>$item,
            'id'=>$id
            )); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
<hr>
<?php $this->renderPartial('index', array('model2'=>$model2)); ?>