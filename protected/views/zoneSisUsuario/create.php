<?php
/* @var $this ZoneSisUsuarioController */
/* @var $model ZoneSisUsuario */
?>

<?php
$this->breadcrumbs=array(
	'Zone Sis Usuarios'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','ZoneSisUsuario')) ?>

<?php 
if(Yii::app()->user->checkAccess('ZoneSisUsuarioCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ZoneSisUsuarioCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>