<?php
/* @var $this ZoneSisUsuarioController */
/* @var $model ZoneSisUsuario */
?>

<?php
$this->breadcrumbs=array(
	'Zone Sis Usuarios'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','ZoneSisUsuario').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('ZoneSisUsuarioUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('ZoneSisUsuarioUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>