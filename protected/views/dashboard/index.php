<?php
/* @var $this DashboardController */

$this->breadcrumbs=array(
	'Dashboard',
);
?>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <?php
        $this->beginWidget('bootstrap.widgets.BsPanel', array(
            'title' => 'Tickets',
            'type'  => BsHtml::PANEL_TYPE_INFO
        ));
        ?>
        Panel content
        <?php
        $this->endWidget();
        ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <?php
        $this->beginWidget('bootstrap.widgets.BsPanel', array(
            'title' => 'Products',
            'type'  => BsHtml::PANEL_TYPE_SUCCESS
        ));
        ?>
        Panel content
        <?php
        $this->endWidget();
        ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <?php
        $this->beginWidget('bootstrap.widgets.BsPanel', array(
            'title' => 'Agenda',
            'type'  => BsHtml::PANEL_TYPE_DANGER
        ));
        ?>
        Panel content
        <?php
        $this->endWidget();
        ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <?php
        $this->beginWidget('bootstrap.widgets.BsPanel', array(
            'title' => 'Clients',
            'type'  => BsHtml::PANEL_TYPE_WARNING
        ));
        ?>
        Panel content
        <?php
        $this->endWidget();
        ?>
    </div>
</div>
