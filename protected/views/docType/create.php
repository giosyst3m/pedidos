<?php
/* @var $this DocTypeController */
/* @var $model DocType */
?>

<?php
$this->breadcrumbs=array(
	'Doc Types'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','DocType')) ?>

<?php 
if(Yii::app()->user->checkAccess('DocTypeCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('DocTypeCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>