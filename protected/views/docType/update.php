<?php
/* @var $this DocTypeController */
/* @var $model DocType */
?>

<?php
$this->breadcrumbs=array(
	'Doc Types'=>array('create'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','DocType').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('DocTypeUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('DocTypeUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>