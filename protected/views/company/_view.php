<?php
/* @var $this CompanyController */
/* @var $data Company */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
		<?php echo CHtml::encode($data->name); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
		<?php echo CHtml::encode($data->address); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
		<?php echo CHtml::encode($data->phone); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
		<?php echo CHtml::encode($data->email); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('web')); ?>:</b>
		<?php echo CHtml::encode($data->web); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('logo')); ?>:</b>
		<?php echo CHtml::encode($data->logo); ?>
		<br />

	<?php if(Yii::app()->user->checkAccess('CompanyViewAuthView')){?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('tagline')); ?>:</b>
		<?php echo CHtml::encode($data->tagline); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_sis_ususario')); ?>:</b>
		<?php echo CHtml::encode($data->id_sis_ususario); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_s); ?>
		<br />

	<?php }?>

</div>