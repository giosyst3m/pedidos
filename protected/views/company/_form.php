<?php
/* @var $this CompanyController */
/* @var $model Company */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'company-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->textAreaControlGroup($model,'address',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'phone',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'web',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'tagline',array('maxlength'=>255)); ?>
    <?php echo $form->fileFieldControlGroup($model, 'logo'); ?>

        <?php
        if (!empty($model->logo)) {
            echo '<div class="center-block text-center">';
            echo $model->ShowImagen($model->logo, Yii::app()->params['URL_CLIENT_LOGO']);
            echo '</div><div class="clearfix">&nbsp;</div>';
        }
        ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('CompanyCreateStatusChange') || Yii::app()->user->checkAccess('CompanyUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('CompanyCreateButtonSave') || Yii::app()->user->checkAccess('CompanyUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('CompanyCreateButtonNew') || Yii::app()->user->checkAccess('CompanyUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Company/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
