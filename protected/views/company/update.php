<?php
/* @var $this CompanyController */
/* @var $model Company */
?>

<?php
$this->breadcrumbs = array(
    'Companies' => array('create'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><i class="fa fa-bars"></i> <?php echo Yii::t('app', 'Company'); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="company-tab" role="tab" data-toggle="tab" aria-expanded="true"><?php echo Yii::t('app', 'Company'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="seller-tab" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Sellers'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="store-tab2" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Store'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="order-tab2" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Order'); ?></a>
                </li>
                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="zone-tab2" data-toggle="tab" aria-expanded="false"><?php echo Yii::t('app', 'Zone'); ?></a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="company-tab">

                    <?php
                    if (Yii::app()->user->checkAccess('CompanyUpdateFormView')) {
                        $this->renderPartial('_form', array('model' => $model));
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content2" aria-labelledby="seller-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('CompanySisUsuarioCreateFormView')) {
                        $this->renderPartial('../companySisUsuario/_form', array(
                            'model' => $model3,
                            'VSisUsuario' => $VSisUsuario,
                            'FormDefault' => $FormDefault,
                        ));
                    }
                    ?>
                    <hr>
                    <?php
                    if (Yii::app()->user->checkAccess('CompanySisUsuarioCreateAdminView')) {
                        $this->renderPartial('../companySisUsuario/admin', array(
                            'model2' => $model4,
                            'VSisUsuario' => $VSisUsuario,
                            'FormDefault' => $FormDefault,
                        ));
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content3" aria-labelledby="store-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('StockCompanyCreateFormView')) {
                        $this->renderPartial('../stockCompany/_form', array(
                            'model' => $model5,
                            'Stock' => $Stock,
                        ));
                    }
                    ?>
                    <hr>
                    <?php
                    if (Yii::app()->user->checkAccess('StockCompanyCreateAdminView')) {
                        $this->renderPartial('../stockCompany/admin', array(
                            'model2' => $model6,
                            'Stock' => $Stock,
                        ));
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content4" aria-labelledby="order-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('StockCompanyCreateAdminView')) {
                        $this->renderPartial('../order/admin', array(
                            'model2' => $model7,
                            'VSisUsuario' => $VSisUsuario,
                            'client' => $Client,
                            'status' => $Status,
                            'FormDefault' => $FormDefault,
                            'FormDefault2' => false,
                        ));
                    }
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content5" aria-labelledby="zone-tab">
                    <?php
                    if (Yii::app()->user->checkAccess('ZoneCompanyCreateFormView')) {
                        $this->renderPartial('../zoneCompany/_form', array(
                            'model' => $model8,
                            'Zone' => $Zone,
                            'FormDefault' => $FormDefault2,
                                )
                        );
                    }
                    ?>
                    <hr>
                    <?php
                    if (Yii::app()->user->checkAccess('ZoneCompanyCreateAdminView')) {
                        $this->renderPartial('../zoneCompany/admin', array(
                            'model2' => $model9,
                            'Zone' => $Zone,
                            'FormDefault' => $FormDefault2,
                                )
                        );
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('CompanyUpdateAdminView')) {
    $this->renderPartial('admin', array(
        'model2' => $model2,
    ));
}
?>



