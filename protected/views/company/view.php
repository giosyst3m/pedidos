<?php
/* @var $this CompanyController */
/* @var $model Company */
?>

<?php
$this->breadcrumbs=array(
	'Companies'=>array('index'),
	$model->name,
);

?>

<?php echo BsHtml::pageHeader('View','Company '.$model->id) ?>
<?php 
 $reg = array();
if(Yii::app()->user->checkAccess('CompanyViewAuthView')){
    $reg = array(	
						'id',
		'name',
		'address',
		'phone',
		'email',
		'web',
		'logo',
		'tagline',
		'id_sis_ususario',
		'r_d_s',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
        
    );
}else{
    $reg = array(	
				'id',
		'name',
		'address',
		'phone',
		'email',
		'web',
		'logo',
		'tagline',
		'id_sis_ususario',
		'r_d_s',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
    );
}
?><?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>$reg
	,
)); ?>

<?php echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => 'history.go(-1)',
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Company/create'),array('class'=>  'btn btn-primary')); ?>