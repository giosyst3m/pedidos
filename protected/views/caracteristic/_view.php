<?php
/* @var $this CaracteristicController */
/* @var $data Caracteristic */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_product')); ?>:</b>
		<?php echo CHtml::encode($data->id_product); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_pro_type_field')); ?>:</b>
		<?php echo CHtml::encode($data->id_pro_type_field); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('value')); ?>:</b>
		<?php echo CHtml::encode($data->value); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_d_s')); ?>:</b>
		<?php echo CHtml::encode($data->r_d_s); ?>
		<br />


</div>