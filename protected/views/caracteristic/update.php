<?php
/* @var $this CaracteristicController */
/* @var $model Caracteristic */
?>

<?php
$this->breadcrumbs=array(
	'Caracteristics'=>array('create'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','Caracteristic').' Nro: '.$model->id) ?>

<?php 
if(Yii::app()->user->checkAccess('CaracteristicUpdateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('CaracteristicUpdateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>