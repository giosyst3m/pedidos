<?php
/* @var $this CaracteristicController */
/* @var $model Caracteristic */
?>

<?php
$this->breadcrumbs=array(
	'Caracteristics'=>array('index'),
	$model->id,
);

?>

<?php echo BsHtml::pageHeader('View','Caracteristic '.$model->id) ?>
<?php 
 $reg = array();
if(Yii::app()->user->checkAccess('CaracteristicViewAuthView')){
    $reg = array(	
						'id',
		'id_product',
		'id_pro_type_field',
		'value',
		'r_d_s',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
        
    );
}else{
    $reg = array(	
				'id',
		'id_product',
		'id_pro_type_field',
		'value',
		'r_d_s',

        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> $model->RegistroEstado( $model->r_d_s)
        ),
    );
}
?><?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>$reg
	,
)); ?>

<?php echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => 'history.go(-1)',
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Caracteristic/create'),array('class'=>  'btn btn-primary')); ?>