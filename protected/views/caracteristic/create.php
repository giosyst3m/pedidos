<?php
/* @var $this CaracteristicController */
/* @var $model Caracteristic */
?>

<?php
$this->breadcrumbs=array(
	'Caracteristics'=>array('create'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','Caracteristic')) ?>

<?php 
if(Yii::app()->user->checkAccess('CaracteristicCreateFormView')){
    $this->renderPartial('_form', array('model'=>$model)); 
}
?>
<hr>
<?php 
if(Yii::app()->user->checkAccess('CaracteristicCreateAdminView')){
    $this->renderPartial('admin', array('model2'=>$model2)); 
}
?>