
<h1><?php echo Yii::t('app', 'Order', array('{numero}' => $id)); ?></h1>
<h2><?php echo Yii::t('app', 'Status') . ': ' . $status; ?></h2>
<?php if (isset($reason) && !empty($reason)) { ?>
<h4><?php echo Yii::t('app', 'Reason'); ?><h4>
    <?php echo $reason; ?>
    <hr>
<?php } ?>
    
<?php echo BsHtml::link(Yii::t('app', 'Click here') . Yii::t('app', 'Download') . ' PDF', Yii::app()->createAbsoluteUrl('OrdDetail/pdf', array('id' => $id))); ?> 
<br/>
<br/>
<?php
echo BsHtml::link(Yii::t('app', 'Click here') . Yii::t('app', 'View') . ' ' . Yii::t('app', 'Order', array('{numero}' => $id)), Yii::app()->createAbsoluteUrl('OrdDetail/view/', array('id' => $id)));
?>
<br/>
<br/>