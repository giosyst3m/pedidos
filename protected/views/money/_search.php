<?php
/* @var $this MoneyController */
/* @var $model Money */
/* @var $form BSActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
        ));
?>

<?php echo $form->textFieldControlGroup($model, 'id'); ?>
<?php echo $form->textFieldControlGroup($model, 'name', array('maxlength' => 255)); ?>
<?php echo $form->textFieldControlGroup($model, 'symbol', array('maxlength' => 255)); ?>
<?php echo $form->textFieldControlGroup($model, 'acronym', array('maxlength' => 3)); ?>
    <?php echo $form->textFieldControlGroup($model, 'id_country'); ?>

<div class="form-actions">
<?php echo BsHtml::submitButton(Yii::t('app', 'Search'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY,)); ?>
</div>

<?php $this->endWidget(); ?>
