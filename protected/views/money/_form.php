<?php
/* @var $this MoneyController */
/* @var $model Money */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'money-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>

    <p class="help-block"> <?php echo  Yii::t('app','Fields with');?> <span class="required">*</span> <?php echo Yii::t('app','are required.')?></p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'symbol',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'acronym',array('maxlength'=>3)); ?>
    <?php echo $form->dropDownListControlGroup($model,'id_country',$Country,array('data-style'=>'btn-primary','class'=>'selectpicker show-tick','empty'=>'Select one','onchange'=>'getAttr(this);','data-live-search'=>true,)); ?>
    
    <?php 
    if(Yii::app()->user->checkAccess('MoneyCreateStatusChange') || Yii::app()->user->checkAccess('MoneyUpdateStatusChange') ){
        echo $form->dropDownListControlGroup($model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>    <?php 
    if(Yii::app()->user->checkAccess('MoneyCreateButtonSave') || Yii::app()->user->checkAccess('MoneyUpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>    
    <?php 
    if(Yii::app()->user->checkAccess('MoneyCreateButtonNew') || Yii::app()->user->checkAccess('MoneyUpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('Money/create'),array('class'=>  'btn btn-primary')); 
    }?>  
<?php $this->endWidget(); ?>
