<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="language" content="en" />
    <style>
        
    </style>
    <?php
    $cs = Yii::app()->clientScript;
    $themePath = Yii::app()->request->hostInfo.Yii::app()->theme->getBaseUrl();


     /**
     * StyleSHeets
     */
    $cs
        ->registerCssFile($themePath.'/css/bootstrap.css')
        ->registerCssFile($themePath.'/css/bootstrap-theme.css')
        ->registerCssFile($themePath.'/css/bootstrap-select.css')
        ->registerCssFile($themePath.'/css/bootstrap-timepicker.css')
        ->registerCssFile($themePath.'/css/bootstrap-datetimepicker.css');

    ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5shiv.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
    <![endif]-->
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
    <input type="hidden" value="<?php echo Yii::app()->homeUrl ?>/" id="homeUrl">
<div class="container">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

    <div class="row-fluid">
    </div>
	<?php echo $content; ?>

	<div class="clear"></div>

    <div class="row-fluid">
        <span class="col-lg-12 text-center">
            <small >Todos derechos reservados</small>
        </span>
    </div>
</div><!-- page -->

</body>
</html>
<?php
