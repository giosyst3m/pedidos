<?php
/* @var $this CityController */
/* @var $model City */
?>

<?php
$this->breadcrumbs = array(
    'Cities' => array('create'),
    'Create',
);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Create') ?><b> <?php echo Yii::t('app', 'City'); ?></b></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        if (Yii::app()->user->checkAccess('CityCreateFormView')) {
            $this->renderPartial('_form', array('model' => $model, 'State' => $State));
        }
        ?>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('CityCreateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2, 'State' => $State));
}
?>