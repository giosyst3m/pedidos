<?php
/* @var $this CityController */
/* @var $model City */
?>

<?php
$this->breadcrumbs = array(
    'Cities' => array('create'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);
?>

<?php echo BsHtml::pageHeader(Yii::t('app', 'Update'), Yii::t('app', 'City') . ' Nro: ' . $model->id) ?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo Yii::t('app', 'Update') ?> <?php echo Yii::t('app', 'Country') . ': ' . BsHtml::tag('b', [], $model->name, true); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>

            </li>

        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br />
        <?php
        if (Yii::app()->user->checkAccess('CityUpdateFormView')) {
            $this->renderPartial('_form', array('model' => $model, 'State' => $State));
        }
        ?>
    </div>
</div>
<hr>
<?php
if (Yii::app()->user->checkAccess('CityUpdateAdminView')) {
    $this->renderPartial('admin', array('model2' => $model2, 'State' => $State));
}
?>