<?php

class ClientController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    private $file = '';
    private $data = array();

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('ClientActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('ClientActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('ClientActionCreate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('ClientActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('ClientActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('ClientActionDelete'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('upload', 'saveUpload', 'processFileExcel'),
                'roles' => array('ClientActionUpload'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('getClientLists'),
                'roles' => array('ClientActionClientLists'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('getIdFromFullName'),
                'roles' => array('ClientActiongetIdFromFullName'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Client;
        $model2 = new VZoneClient('search');
        $model2->unsetAttributes();  // clear any default values

        if (isset($_GET['VZoneClient']))
            $model2->attributes = $_GET['VZoneClient'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Client'])) {
            try {
                $model->attributes = $_POST['Client'];
                $Image = CUploadedFile::getInstance($model, 'logo');
                $model->r_c_o = 'form';
                if (!empty($Image)) {
                    $model->logo = $this->generateRandomString(5) . '-' . $this->url_slug($Image->name) . '.' . $Image->getExtensionName();
                    $Image->saveAs(Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_CLIENT_LOGO'] . $model->logo);
                }
                if ($model->save()) {
                    if (Yii::app()->user->getState('CLIENT')->CLIENT_CREATE_SEND_EMAIL == 'true') {
                        $from = Yii::app()->user->nombre . '<' . Yii::app()->user->email . '>';
                        $subject = Yii::t('json', 1013, ['{codigo}' => $model->code, '{nombre}' => $model->name]);
                        $ok = Yii::t('email', 'Email-2004', array('{grupo}' => Yii::app()->user->getState('CLIENT')->CLIENT_CREATE_SEND_EMAIL_GROUP));
                        $error = Yii::t('email', 'Email-5003');
                        $this->layout = 'mail';
                        $msg = $this->render('../mail/client', ['model' => $model], true);
                        $to = $this->getEmailsListByPerfil(Yii::app()->user->getState('CLIENT')->CLIENT_CREATE_SEND_EMAIL_GROUP);
                        $this->sendEmail($from, $to, $subject, $msg, $ok, $error);
                    }
                    $this->saveData($model);
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Client'))));
                    $this->redirect(array('update', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Client'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 5020, [
                                '{code}' => Yii::app()->request->getPost('Client')['code'],
                                '{correlativo}' => Yii::app()->request->getPost('Client')['correlative']
                                    ]
                    ));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
            'model2' => $model2,
            'City' => BsHtml::listData(City::model()->findAll('r_d_s=1'), 'id', 'name'),
            'attr' => $this->render_filelds(ClientField::model()->findAll('r_d_s=1')),
        ));
    }

    /**
     * Save/update Cli_data from prodduct
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 1.0.0 
     * @param model $model
     */
    public function saveData($model) {
        CliData::model()->deleteAll('id_client=' . $model->id);
        if (isset($_POST['car'])) {
            $value = array_values($_POST['car']);
            $key = array_keys($_POST['car']);
            for ($index = 0; $index < count($value); $index++) {
                if (!empty($key[$index])) {
                    $modelCara = new CliData();
                    $modelCara->id_client = $model->id;
                    $modelCara->id_field_client = $key[$index];
                    if (is_array($value[$index])) {
                        $modelCara->value = implode(',', $value[$index]);
                    } else {
                        $modelCara->value = $value[$index];
                    }
                    $modelCara->r_c_o = 'form';
                    $modelCara->save();
                }
            }
        }
        if (isset($_FILES['car']) && !empty($_FILES['car'])) {
            $files = $_FILES['car'];
            $key = array_keys($files['name']);
            for ($index = 0; $index < count($key); $index++) {
                if (!empty($files['name'][$key[$index]])) {
                    $modelCara = new CliData();
                    $modelCara->id_client = $model->id;
                    $modelCara->id_field_client = $key[$index];
                    $modelCara->value = $this->generateRandomString(5) . '-' . $this->url_slug($files['name'][$key[$index]]);
                    move_uploaded_file($files['tmp_name'][$key[$index]], Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_CLIENTE_FILE'] . $modelCara->value);
                    $modelCara->r_c_o = 'form';
                    $modelCara->save();
                }
            }
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model7 = new Order('search');
        $model7->unsetAttributes();  // clear any default values
        $model7->id_client = $id;
        if (isset($_GET['Order']))
            $model7->attributes = $_GET['Order'];

        $model3 = new ZoneClient;
        $model4 = new ZoneClient('search');
        $model4->unsetAttributes();  // clear any default values
        $model4->id_client = $id;
        if (isset($_GET['ZoneClient']))
            $model4->attributes = $_GET['ZoneClient'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model3);

        if (isset($_POST['ZoneClient'])) {
            try {
                $model3->attributes = $_POST['ZoneClient'];
                $model3->id_client = $id;
                if ($model3->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'ZoneClient'))));
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'ZoneClient'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 5007));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $model = $this->loadModel($id);
        $model2 = new VZoneClient('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['VZoneClient']))
            $model2->attributes = $_GET['VZoneClient'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Client'])) {
            try {
                $model->attributes = $_POST['Client'];
                $Image = CUploadedFile::getInstance($model, 'logo');
                $model->r_u_o = 'form';
                if (!empty($Image)) {
                    $model->logo = $this->generateRandomString() . '-' . $this->url_slug($Image->name) . '.' . $Image->getExtensionName();
                    $Image->saveAs(Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_CLIENT_LOGO'] . $model->logo);
                }
                if ($model->save()) {
                    $this->saveData($model);
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'Client'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'Client'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
            'model2' => $model2,
            'model3' => $model3,
            'model4' => $model4,
            'model7' => $model7,
            'FormDefault2' => 3,
            'FormDefault' => true,
            'Company' => BsHtml::listData(Company::model()->findAll(), 'id', 'name'),
            'Client' => CHtml::listData(Client::model()->findAll('r_d_s=1'), 'id', 'name'),
            'VSisUsuario' => BsHtml::listData(VSisUsuario::model()->findAll(), 'id', 'complete_name'),
            'Status' => CHtml::listData(Status::model()->findAll('r_d_s=1'), 'id', 'name'),
            'Zone' => BsHtml::listData(VZoneSisUsuario::model()->findAllByAttributes(['id_sis_usuario'    => Yii::app()->user->id,]),'id_zone','zone'),
            'City' => BsHtml::listData(City::model()->findAll('r_d_s=1'), 'id', 'name'),
            'attr' => $this->render_filelds(ClientField::model()->findAll('r_d_s=1'), CHtml::listData(CliData::model()->findAll('id_client=' . $id), 'id_field_client', 'value'), 'URL_CLIENTE_FILE'),
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('ClientDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Client'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Client');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Client('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Client']))
            $model->attributes = $_GET['Client'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Client the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Client::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Client $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'client-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'zone-client-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Get Client Lisft using to Autocpmplete
     * @author Giosyst3m <me@giosyst3m.net>
     * @since Feb 11, 2106
     * @version 1.0.0
     * @return Json Client List
     */
    public function actiongetClientLists() {
        //Instancia DBCriteria
        $db = new CDbCriteria();
        //Agregar la concidión de busqueda con el LIKE
        $db->addSearchCondition('full_name', Yii::app()->request->getQuery('term'));
        //Impirmi JSON con resultados
        if (!Yii::app()->user->checkAccess('CatalogSeeAllClient')) {
            $ZoneSisUsuario = BsHtml::listData(ZoneSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes([
                                'id_sis_usuario' => Yii::app()->user->id,
                            ]), 'id_zone', 'id_zone');
            $db->addInCondition('id_zone', $ZoneSisUsuario);
        }
        $db->addColumnCondition([
            'client_r_d_s' => 1,
            'zone_client_r_d_s' => 1,
            'zone_r_d_s' => 1,]);
        echo CJSON::encode(VZoneClient::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll($db));
    }

    public function actiongetIdFromFullName($name = '', $print = true) {
        extract($_POST);
        try {
            if (!empty($name)) {
                $data = explode('|', $name);
                if (is_array($data) && isset($data[1])) {
                    $model = Client::model()->findByAttributes(array(
                        'code' => $data[0],
                        'correlative' => $data[1]
                    ));
                    if ($model) {
                        $msg = $this->getJSon(200, 2013, array('id' => $model->id));
                    } else {
                        $msg = $this->getJSon(400, 4025, array('id' => 0));
                    }
                } else {
                    $msg = $this->getJSon(400, 4001);
                }
            } else {
                $msg = $this->getJSon(400, 4001);
            }
            if ($print) {
                echo $msg;
            } else {
                return $msg;
            }
        } catch (Exception $e) {
            echo array(500, 'API', array(), $e->getCode(), $e->getMessage(), array(
            'line' => $e->getLine(),
            'file' => $e->getFile(),
            'previos' => $e->getPrevious(),
            'trace' => $e->getTrace(),
            'message' => $e->getMessage(),
            ));
        }
    }

}
