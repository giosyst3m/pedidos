<?php

class ZoneController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('ZoneActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('ZoneActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('ZoneActionCreate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('ZoneActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('ZoneActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('ZoneActionDelete'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Zone;
        $model2 = new Zone('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Zone']))
            $model2->attributes = $_GET['Zone'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Zone'])) {
            try {
                $model->attributes = $_POST['Zone'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Zone'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Zone'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model, 'model2' => $model2
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model3 = new ZoneClient;
        $model4 = new ZoneClient('search');
        $model4->unsetAttributes();  // clear any default values
        $model4->id_zone = $id;
        if (isset($_GET['ZoneClient']))
            $model4->attributes = $_GET['ZoneClient'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model3);

        if (isset($_POST['ZoneClient'])) {
            try {
                $model3->attributes = $_POST['ZoneClient'];
                $model3->id_zone = $id;
                if ($model3->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'ZoneClient'))));
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'ZoneClient'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 5007));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }
        
        $model8 = new ZoneCompany;
        $model9 = new ZoneCompany('search');
        $model9->unsetAttributes();  // clear any default values
        $model9->id_zone = $id;
        if (isset($_GET['ZoneCompany']))
            $model9->attributes = $_GET['ZoneCompany'];
        
        $this->performAjaxValidation($model8);
        if (isset($_POST['ZoneCompany'])) {
            try {
                $model8->attributes = $_POST['ZoneCompany'];
                $model8->id_zone = $id;
                if ($model8->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'ZoneClient'))));
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('update', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'ZoneClient'))));
                }
            } catch (Exception $e) {
                if($e->getCode() === 23000){
                    Yii::app()->user->setFlash('danger', Yii::t('json',5007)); 
                }else{
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }
        
        $model5 = new ZoneSisUsuario();
        $model6 = new ZoneSisUsuario('search');
        $model6->unsetAttributes();  // clear any default values
        $model6->id_zone = $id;
        if (isset($_GET['ZoneSisUsuario']))
            $model6->attributes = $_GET['ZoneSisUsuario'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model5);

        if (isset($_POST['ZoneSisUsuario'])) {
            try {
                $model5->attributes = $_POST['ZoneSisUsuario'];
                $model5->id_zone = $id;
                if ($model5->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'ZoneSisUsuario'))));
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'ZoneSisUsuario'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 5007));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $model = $this->loadModel($id);
        $model2 = new Zone('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Zone']))
            $model2->attributes = $_GET['Zone'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Zone'])) {
            try {
                $model->attributes = $_POST['Zone'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'Zone'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'Zone'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
            'model2' => $model2,
            'model3' => $model3,
            'model4' => $model4,
            'model5' => $model5,
            'model6' => $model6,
            'model8' => $model8,
            'model9' => $model9,
            'Zone' => CHtml::listData(Zone::model()->findAll('r_d_s=1'), 'id', 'name'),
            'VSisUsuario' => CHtml::listData(VSisUsuario::model()->findAll('r_d_s=1'), 'id', 'complete_name'),
            'VClient' => CHtml::listData(VClient::model()->findAll(), 'id', 'full_name'),
            'Company' => CHtml::listData(Company::model()->findAll('r_d_s=1'), 'id', 'name'),
            'FormDefault2' => false,
                )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('ZoneDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Zone'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Zone');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Zone('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Zone']))
            $model->attributes = $_GET['Zone'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Zone the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Zone::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Zone $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'zone-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'zone-sis-usuario-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'zone-company-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'zone-client-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
