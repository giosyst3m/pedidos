<?php

class InventoryController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('InventoryActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('InventoryActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('InventoryActionCreate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('InventoryActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('InventoryActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('InventoryActionDelete'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('PackOff'),
                'roles' => array('InventoryActionPackOff'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Inventory;
        $model2 = new Inventory('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Inventory']))
            $model2->attributes = $_GET['Inventory'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Inventory'])) {
            try {
                $model->attributes = $_POST['Inventory'];
                if ($model->type == 0) {
                    $agregar = false;
                } else {
                    $agregar = true;
                }
                $result = $this->addInventory($model->id_product, $model->quantity, 1, 0, 0, Yii::app()->user->getState('PRODUCT')->PRODUCT_STOCK_DEFAULT, $agregar, $model->comment);
                if ($result) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Inventory'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Inventory'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model,
            'model2' => $model2,
            'product' => CHtml::listData(VProduct::model()->findAll('r_d_s'), 'id', 'complete_name'),
            'stock' => CHtml::listData(Stock::model()->findAll('r_d_s=1'), 'id', 'name')
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model2 = new Inventory('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Inventory']))
            $model2->attributes = $_GET['Inventory'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Inventory'])) {
            try {
                $model->attributes = $_POST['Inventory'];
                if ($model->type == 0) {
                    $agregar = false;
                } else {
                    $agregar = true;
                }
                $result = $this->addInventory($model->id_product, $model->quantity, 1, 0, 0, Yii::app()->user->getState('PRODUCT')->PRODUCT_STOCK_DEFAULT, $agregar, $model->comment);
                if ($result) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'Inventory'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'Inventory'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
            'model2' => $model2,
            'product' => CHtml::listData(VProduct::model()->findAll('r_d_s'), 'id', 'complete_name'),
            'stock' => CHtml::listData(Stock::model()->findAll('r_d_s=1'), 'id', 'name')
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('InventoryDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Inventory'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Inventory');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Inventory('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Inventory']))
            $model->attributes = $_GET['Inventory'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Inventory the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Inventory::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Inventory $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'inventory-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionPackOff() {
        $model = new VOrderDetail('search');
        $model->unsetAttributes();  // clear any default values
        $model->dbCriteria->order = 'barcode ASC';
        $model->dbCriteria->addColumnCondition([
            'ord_detail_r_d_s' => 1,
        ]);
        $model->dbCriteria->addInCondition('id_status', explode(',', Yii::app()->user->getState('ORDER')->ORDER_STATUS_TO_PACK_OFF));
        $model->dbCriteria->group = 'id_product';
        
        if (isset($_GET['VOrderDetail']))
            $model->attributes = $_GET['VOrderDetail'];
        $this->render('pack_off', array(
            'dataProvider' => $model,
        ));
    }

}
