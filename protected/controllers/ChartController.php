<?php

class ChartController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index', 'filterProduct'),
                'roles' => array('ChartActionIndex'),
            ),
            array('allow', // allow all users to perform 'index' 
                'actions' => array('getOrderStatus'),
                'roles' => array('ChartActionGetOrderStatus'),
            ),
            array('allow', // allow all users to perform 'index' 
                'actions' => array('getOrderBill'),
                'roles' => array('ChartActionGetOrderBill'),
            ),
            array('allow', // allow all users to perform 'index' 
                'actions' => array('getMapCity'),
                'roles' => array('ChartActionGetMapCity'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actiongetOrderStatus($year = null, $periodo = null, $chart = null, $semestral = '', $cuatrimestral = '', $trimestral = '', $bimestral = '', $mensual = '', $document = 0) {
        extract($_POST);
        $command = Yii::app()->db->createCommand();
        $command->select('status as label, count(status) as qty');
        $command->from('v_order_sumary');
        $command->group('status');
        if (!Yii::app()->user->checkAccess('SiteChartOrderStatusAll')) {
            $user = ' AND r_c_u=' . Yii::app()->user->id;
        } else {
            $user = '';
        }
        if (!empty($year)) {
            $year = " year(r_c_d) in (" . implode(',', $year) . ")";
        } else {
            echo $this->getJSon(400, Yii::t('json', 4030));
            return;
        }
        if (!empty($document)) {
            $document = ' and id_document =' . $document;
        } else {
            echo $this->getJSon(400, Yii::t('json', 4012));
            return;
        }
        if (!empty($semestral)) {
            $semestral = ' and semestre in(' . $this->clearString($semestral) . ')';
            $command->group('status');
            $command->select('status as label,  count(status) as qty');
        }
        if (!empty($cuatrimestral)) {
            $cuatrimestral = ' and cuatrimestre in(' . $this->clearString($cuatrimestral) . ')';
            $command->group('status');
            $command->select('status as label,  count(status) as qty');
        }
        if (!empty($trimestral)) {
            $trimestral = ' and trimestre in(' . $this->clearString($trimestral) . ')';
            $command->group('status');
            $command->select('status as label,  count(status) as qty');
        }
        if (!empty($bimestral)) {
            $bimestral = ' and bimestre in(' . $this->clearString($bimestral) . ')';
            $command->group('status');
            $command->select('status as label,  count(status) as qty');
        }
        if (!empty($mensual)) {
            $mensual = ' and mes in(' . $this->clearString($mensual) . ')';
            $command->group('status');
            $command->select('status as label,  count(status) as qty');
        }

        $where = $year . $semestral . $cuatrimestral . $trimestral . $bimestral . $mensual . $user . $document . ' AND id_company=' . Yii::app()->user->getState('id_company');
        $command->where($where);
        $model = Yii::app()->cache->get($this->url_slug($year . $semestral . $cuatrimestral . $trimestral . $bimestral . $mensual . $user . $chart . $document . ' OrderStatus ' . Yii::app()->user->getState('id_company')));
        if (!$model) {
            $model = $command->queryAll();
            Yii::app()->cache->set($this->url_slug($year . $semestral . $cuatrimestral . $trimestral . $bimestral . $mensual . $user . $chart . $document . ' OrderStatus ' . Yii::app()->user->getState('id_company')), $model, Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CHART_TIME);
        }
        $datos[] = array('Status', 'Cantidad');
        $total = 0;
        foreach ($model as $data) {
            $datos[] = array($data['label'], (int) $data['qty']);
            $tables[] = array($data['label'], (int) $data['qty']);
            $total = $total + (int) $data['qty'];
        }
        $tables[] = array('Total', $total);
        $tablesName[] = array('type' => 'string', 'label' => 'Estatus');
        $tablesName[] = array('type' => 'number', 'label' => 'Cantidad');
        echo $this->getJSon(200, Yii::t('json', 2000), array(
            'chartData' => $datos,
            'chartTitle' => 'Ordenes por Status',
            'chartId' => 'ChartOrderStatus',
            'tablesId' => 'ChartOrderStatusTable',
            'tablesName' => $tablesName,
            'tablesData' => $tables,
            'tablesTotal' => $total
        ));
    }

    public function actiongetOrderBill($year = null, $periodo = null, $chart = null, $semestral = '', $cuatrimestral = '', $trimestral = '', $bimestral = '', $mensual = '', $document = 0) {
        extract($_POST);
        $command = Yii::app()->db->createCommand();
        $command->select('year(r_c_d) as label,  sum(total) as qty');
        $command->group('year(r_c_d)');
        $command->from('v_order_sumary');
        if (!Yii::app()->user->checkAccess('SiteChartOrderStatusAll')) {
            $user = ' AND r_c_u=' . Yii::app()->user->id . ' AND id_status in(' . Yii::app()->user->getState('ORDER')->ORDER_STATUS_CONSIDERAED_AS_BILL . ') ';
        } else {
            $user = ' AND id_status in(' . Yii::app()->user->getState('ORDER')->ORDER_STATUS_CONSIDERAED_AS_BILL . ') ';
        }
        if (!empty($year)) {
            $year = " year(r_c_d) in (" . implode(',', $year) . ")";
        } else {
            echo $this->getJSon(400, Yii::t('json', 4030));
            return;
        }
        if (!empty($document)) {
            $document = ' and id_document =' . $document;
        } else {
            echo $this->getJSon(400, Yii::t('json', 4012));
            return;
        }
        if (!empty($semestral)) {
            $semestral = ' and semestre in(' . $this->clearString($semestral) . ')';
            $command->group('semestre');
            $command->select('semestre as label,  sum(total) as qty');
        }
        if (!empty($cuatrimestral)) {
            $cuatrimestral = ' and cuatrimestre in(' . $this->clearString($cuatrimestral) . ')';
            $command->group('cuatrimestre');
            $command->select('cuatrimestre as label,  sum(total) as qty');
        }
        if (!empty($trimestral)) {
            $trimestral = ' and trimestre in(' . $this->clearString($trimestral) . ')';
            $command->group('trimestre');
            $command->select('trimestre as label,  sum(total) as qty');
        }
        if (!empty($bimestral)) {
            $bimestral = ' and bimestre in(' . $this->clearString($bimestral) . ')';
            $command->group('bimestre');
            $command->select('bimestre as label,  sum(total) as qty');
        }
        if (!empty($mensual)) {
            $mensual = ' and mes in(' . $this->clearString($mensual) . ')';
            $command->group('mes');
            $command->select('mes as label,  sum(total) as qty');
        }

        $where = $year . $semestral . $cuatrimestral . $trimestral . $bimestral . $mensual . $user . $document . ' AND id_company=' . Yii::app()->user->getState('id_company');
        $command->where($where);
        $model = Yii::app()->cache->get($this->url_slug($year . $semestral . $cuatrimestral . $trimestral . $bimestral . $mensual . $user . $chart . $document . ' OrderBill ' . Yii::app()->user->getState('id_company')));
        if (!$model) {
            $model = $command->queryAll();
            Yii::app()->cache->set($this->url_slug($year . $semestral . $cuatrimestral . $trimestral . $bimestral . $mensual . $user . $chart . $document . ' OrderBill ' . Yii::app()->user->getState('id_company')), $model, Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CHART_TIME);
        }
        $datos[] = array('Meses', '$');
        $total = 0;
        foreach ($model as $data) {
            $datos[] = array($data['label'], (int) $data['qty']);
            $tables[] = array($data['label'], (int) $data['qty']);
            $total = $total + (int) $data['qty'];
        }
        $tables[] = array('Total', $total);
        $tablesName[] = array('type' => 'string', 'label' => 'Item');
        $tablesName[] = array('type' => 'number', 'label' => '$');
        echo $this->getJSon(200, Yii::t('json', 2000), array(
            'chartData' => $datos,
            'chartTitle' => 'Facturado',
            'chartId' => 'ChartOrderBill',
            'tablesId' => 'ChartOrderBillTable',
            'tablesName' => $tablesName,
            'tablesData' => $tables,
            'tablesTotal' => $total,
//            '$' => $command->getText(),
        ));
    }

    /**
     * Genera Json por ciudad, usuario y compañía
     * @param int $id_status
     * @author Giovanni Ariza <sistema@hiosyst3m.net>
     * @version 1.0.0
     * @return  JSON Info to MapCity
     */
    public function actiongetMapCity($id_status = 0) {
        if (Yii::app()->user->checkAccess('SiteChartOrderStatusAll')) {
            $model = VOrderStatusCitySumaryTotal::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CHART_TIME)->findAllByAttributes([
                'id_company' => Yii::app()->user->getState('id_company'),
                'id_status' => Yii::app()->request->getPost('id_status'),
            ]);
        } else {
            $model = VOrderStatusCitySumary::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CHART_TIME)->findAllByAttributes([
                'id' => Yii::app()->user->id,
                'id_company' => Yii::app()->user->getState('id_company'),
                'id_status' => Yii::app()->request->getPost('id_status'),
            ]);
        }
        $data[] = ['Ciudad', '$', 'Cant'];
        foreach ($model as $value) {
            $data[] = [$value['city_name'], (int) $value['total'], (int) $value['quantity']];
        }
        echo $this->getJSon(200, Yii::t('json', 2000), array(
            'chartData' => $data,
        ));
    }

}
