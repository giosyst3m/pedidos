<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    function actionDashboard() {
        $this->render('dashboard');
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->layout = '//layouts/main';

        $VActivity = new VActivity('search');
        $VActivity->unsetAttributes();  // clear any default values
        if (isset($_GET['Activity']))
            $VActivity->attributes = $_GET['Activity'];
        $CDbCriteria = new CDbCriteria();
        $CDbCriteria->order = 'id DESC';
        if (!Yii::app()->user->checkAccess('ActivityShowAllActivity')) {
            $VActivity->r_c_u = Yii::app()->user->id;
        }
        $VActivity->setDbCriteria($CDbCriteria);

        $SisContacto = new SisContacto;
        $this->performAjaxValidation($SisContacto);
        if (Yii::app()->user->isGuest) {
            $this->layout = '//layouts/landign';


            if (isset($_POST['SisContacto'])) {
                try {
                    $SisContacto->attributes = $_POST['SisContacto'];
                    if ($SisContacto->save()) {
                        $SisContacto->verificacion = str_pad($SisContacto->getPrimaryKey(), Yii::app()->params['LPAD'], 0, STR_PAD_LEFT);
                        //$this->sendEmail($SisContacto->email, 'contacto_apertura', Yii::t('msg', 'EMAIL_CONTACTO_APERTURA', array('{numero}' => $numero)), 'EMAIL_CONTACTO_OK', 'EMAIL_CONTACTO_ERROR', array('nombre' => $SisContacto->nombre, 'asunto' => $SisContacto->asunto, 'notas' => $SisContacto->detalle, 'numero' => $numero));
                        Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('msg', 'SOPORTE'))));
                        $this->redirect(array('site/index#contact'));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR'));
                    }
                } catch (Exception $e) {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
            $this->layout = '//layouts/main_guest';
            $this->render('index_guest', ['SisContacto' => $SisContacto]);
        } else {
            $model = new VOrderSumary('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['VOrderSumary']))
                $model->attributes = $_GET['VOrderSumary'];
            if (Yii::app()->user->checkAccess('SiteOrderViewAll')) {
                $VSisUsuario = CHtml::listData(VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'complete_name');
            } else {
                $model->r_c_u = Yii::app()->user->id;
                $VSisUsuario = array();
            }
            $CDbCriteria = new CDbCriteria();
            $CDbCriteria->addNotInCondition('id_status', explode(',', Yii::app()->user->getState('ORDER')->ORDER_HIDE_STATUS));
            $CDbCriteria->order = 'id DESC';
            $model->setDbCriteria($CDbCriteria);
            $years = BsHtml::listData(Order::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll(array(
                                'select' => 'year(r_c_d) as id',
                                'group' => 'year(r_c_d)',
                                'distinct' => true,
                                'order' => 'year(r_c_d) desc'
                            )), 'id', 'id');
            $model->id_company = Yii::app()->user->getState('id_company');
            $model->id_document = 14;
            if (empty(Yii::app()->user->getState('id_company'))) {
                $this->changeCompany(CompanySisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['id_sis_usuario' => Yii::app()->user->id, 'r_d_s' => 1])->id_company, false);
            }
            if (Yii::app()->user->checkAccess('SiteChartOrderStatusAll')) {
                $VOrderStatusSumary = VOrderStatusSumaryTotal::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_DASHBOARD_ORDER_SUMARY)->findAllByAttributes([
                    'id_company' => (!empty(Yii::app()->user->getState('id_company')) ? Yii::app()->user->getState('id_company') : 0),
                ]);
            } else {
                $VOrderStatusSumary = VOrderStatusSumary::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_DASHBOARD_ORDER_SUMARY)->findAllByAttributes([
                    'id_company' => (!empty(Yii::app()->user->getState('id_company')) ? Yii::app()->user->getState('id_company') : 0),
                    'id' => Yii::app()->user->id
                ]);
            }
            $VComment = new CActiveDataProvider('VComment');
            $VComment->criteria->order = 'r_c_d DESC';
            if (!Yii::app()->user->checkAccess('SiteIndexShowAllTickeComment')) {
                $VComment->criteria->addColumnCondition(['activity_r_c_u' => Yii::app()->user->id]);
            }
            $this->render('index', array(
                'model' => $model,
                'status' => CHtml::listData(Status::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1 AND id_sta_group=1'), 'id', 'name'),
                'delivery' => CHtml::listData(Delivery::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1'), 'id', 'name'),
                'VSisUsuario' => $VSisUsuario,
                'Years' => $years,
                'VActivity' => $VActivity,
                'Semestres' => $this->getSemestres(),
                'Cuatrimestres' => $this->getCuatrimestres(),
                'Trimestres' => $this->getTrimestres(),
                'Month' => $this->getMonth(),
                'Periodos' => $this->getPeriodos(),
                'Charts' => $this->getCharts(),
                'Bimestral' => $this->getBimestres(),
                'Status' => Status::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes(['r_d_s' => 1, 'id_sta_group' => 1]),
                'Document' => CHtml::listData(Document::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1 AND id_doc_type IN (' . Yii::app()->user->getState('CHART')->CHART_DOCUMENT_TYPE_SHOW . ')'), 'id', 'name'),
                'VOrderStatusSumary' => $VOrderStatusSumary,
                'VComment' => $VComment,
//                'VCommentOrder' => VCommentOrder::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll(
//                        'id_company = ' . Yii::app()->user->getState('id_company') .
//                        ' AND order_r_c_u = ' . Yii::app()->user->id .
//                        ' order by r_c_d DESC limit ' . Yii::app()->user->getState('COMMENT')->COMMENT_DISPLAY_HOME)
            ));
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        $this->layout =  '//layouts/error';
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $this->layout = '//layouts/login';
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionchangeCompany($id = 0) {
        extract($_POST);
        $this->changeCompany($id);
    }

    public function actiongeteChart() {
        $CDbCriteria = new CDbCriteria();
        $CDbCriteria->select = 'month(r_c_d) as mes,sum(total) as total';
        $CDbCriteria->addCondition('year(r_c_d)=' . date('Y'));
        $CDbCriteria->group = 'mes';
        $CDbCriteria->addColumnCondition(['id_status' => 5]);
        if (!Yii::app()->user->checkAccess('SiteChartOrderStatusAll')) {
            $CDbCriteria->addColumnCondition(['r_c_u' => Yii::app()->user->id]);
        }
        $model = VOrderSumary::model()->findAll($CDbCriteria);

        for ($index = 1; $index <= 12; $index++) {
            $facturado[$index] = 0;
            $pagado[$index] = 0;
        }
        foreach ($model as $value) {
            $facturado[(int) $value->mes] = (int) $value->total;
        }

        unset($CDbCriteria);
        unset($model);
        $CDbCriteria = new CDbCriteria();
        $CDbCriteria->select = 'month(r_c_d) as mes,sum(total) as total';
        $CDbCriteria->addCondition('year(r_c_d)=' . date('Y'));
        $CDbCriteria->group = 'mes';
        $CDbCriteria->addColumnCondition(['id_status' => 6]);
        if (!Yii::app()->user->checkAccess('SiteChartOrderStatusAll')) {
            $CDbCriteria->addColumnCondition(['r_c_u' => Yii::app()->user->id]);
        }
        $model = VOrderSumary::model()->findAll($CDbCriteria);
        foreach ($model as $value) {
            $pagado[(int) $value->mes] = (int) $value->total;
        }
        unset($CDbCriteria);
        unset($model);
        $CDbCriteria = new CDbCriteria();
        $CDbCriteria->select = 'status, count(*) as id_status';
        $CDbCriteria->addCondition('year(r_c_d)=' . date('Y'));
        $CDbCriteria->group = 'id_status';
        if (!Yii::app()->user->checkAccess('SiteChartOrderStatusAll')) {
            $CDbCriteria->addColumnCondition(['r_c_u' => Yii::app()->user->id]);
        }
        $CDbCriteria->addNotInCondition('id_status', [5, 6]);
        $model = VOrderSumary::model()->findAll($CDbCriteria);
        $index = 0;
        foreach ($model as $value) {
            $index++;
            $status[] = ['name' => $value->status, 'value' => $value->id_status];
        }
        echo $this->getJSon(2000, 200, ['facturado' => array_values($facturado), 'pagado' => array_values($pagado), 'estado' => $status]);
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'sis-contacto-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
