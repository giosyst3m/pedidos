<?php

class ProductController extends Controller {

    private $data = array();

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    private $file = '';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('ProductActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('ProductActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('ProductActionCreate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('ProductActionUpdate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('upload', 'saveUpload', 'processFileExcel', 'UploadImagen', 'saveUploadImagen', 'processFileZip'),
                'roles' => array('ProductActionUpload'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('ProductActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('ProductActionDelete'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('getAttr'),
                'users' => array('*'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('getProductLists', 'listPDF'),
                'roles' => array('ProductActionProductLists'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('CatalogPrint'),
                'roles' => array('ProductActionCatalogPrint'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actiongetAttr($id, $product = NULL) {
        $arr = array();
        if (!empty($product)) {
            $arr = CHtml::listData(Caracteristic::model()->findAll('r_d_s=1 and id_product=' . $product), 'id_pro_type_field', 'value');
        }
        $this->renderPartial('_attr', array(
            'model' => VProductAttribiute::model()->findAll('pro_type_r_d_s=1 and id_pro_type=' . $id),
            'car' => $arr
                )
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
            'ProductRate' => ProductRate::model()->findAllByAttributes(array(
                'r_d_s' => 1,
                'id_product' => $id,
                'id_rate' => 1
            )),
            'ProductCategory' => ProductCategory::model()->findAllByAttributes(array(
                'r_d_s' => 1,
                'id_product' => $id
            )),
            'ProImagen' => ProImagen::model()->findAllByAttributes(array(
                'r_d_s' => 1,
                'id_product' => $id,
            )),
            'Caracteristic' => Caracteristic::model()->findAllByAttributes(array(
                'r_d_s' => 1,
                'id_product' => $id,
            )),
            'Client' => CHtml::listData(Client::model()->findAll('r_d_s=1'), 'id', 'name'),
//            ''=> ProductCategory::model()->findAllByAttributes(array(
//                'r_d_s'=>1,
//                'id_cateogory'=>
//            ))
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {

        $model = new Product;
        $model2 = new Product('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Product']))
            $model2->attributes = $_GET['Product'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Product'])) {
            try {
                $model->attributes = $_POST['Product'];
                $model->sku = strtoupper($model->sku);
                $model->slug = $this->url_slug($model->name);
                $Image = CUploadedFile::getInstance($model, 'photo');
                if (!empty($Image)) {
                    $model->photo = $this->url_slug($model->sku) . '-' . $this->url_slug($model->name) . '-' . $this->generateRandomString(5) . '.' . $Image->getExtensionName();
                    $Image->saveAs(Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . $model->photo);
                } else {
                    $model->photo = Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_DEFAULT;
                }
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Product'))));
                    $this->saveCaracteristic($model);
                    $this->saveImagen($model, $_FILES['imagen']);
                    $this->saveSplit($_POST['Split'], $model->getPrimaryKey());
                    $this->saveCategory($_POST['Category'], $model->getPrimaryKey());
                    $this->redirect(array('update', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Product'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model,
            'model2' => $model2,
            'product' => CHtml::listData(Product::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name', 'sku'),
            'proType' => CHtml::listData(ProType::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'imagen' => array(),
            'split' => CHtml::listData(VSplit::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll(), 'id', 'name', 'father'),
            'splits' => array(),
            'brand' => CHtml::listData(Brand::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'categories' => CHtml::listData(VCategory::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll(), 'id', 'name', 'father_name'),
            'category' => array(),
            'Condition' => CHtml::listData(Condition::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'ProGroup' => BsHtml::listData(ProGroup::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name')
        ));
    }

    /**
     * Save Category on Product
     * @param array $split array from id Category
     * @param id $id product id
     */
    protected function saveCategory($data, $id) {
        //Eliminar todos los productos en la acategoria
        ProductCategory::model()->deleteAll('id_product=:id', array(':id' => $id));
        try {
            //Ciclo de validar para Incluir
            foreach ($data as $value) {
                //instanciar para crear un nuevo producto-categoria
                $ProductCategory = New ProductCategory;
                //Asignar el valor del ID del producto
                $ProductCategory->id_product = $id;
                //Aisgnar el valor del ID de la categoria
                $ProductCategory->id_category = $value;
                //Guardar la información
                $ProductCategory->save();
            }
        } catch (Exception $e) {
            //Error en caso que se presente algun error
            Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
        }
    }

    /**
     * Save Split on Product
     * @param array $split array from id split
     * @param id $id product id
     */
    protected function saveSplit($split, $id) {
        //Eliminar todos los productos en la acategoria
        ProductSplit::model()->deleteAll('id_product=:id', array(':id' => $id));
        try {
            //Ciclo de validar para Incluir
            foreach ($split as $value) {
                //instanciar para crear un nuevo producto-categoria
                $ProductSplit = New ProductSplit;
                //Asignar el valor del ID del producto
                $ProductSplit->id_product = $id;
                //Aisgnar el valor del ID de la categoria
                $ProductSplit->id_split = $value;
                //Guardar la información
                $ProductSplit->save();
            }
        } catch (Exception $e) {
            //Error en caso que se presente algun error
            Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
        }
    }

    /**
     * Save imagen products
     * @param type $model
     * @param type $files
     */
    public function saveImagen($model, $files) {
        for ($index = 0; $index < count($files['name']); $index++) {
            if (!empty($files['name'][$index])) {
                $proImg = new ProImagen();
                $proImg->id_product = $model->id;
                $proImg->image = $this->generateRandomString(5) . '-' . strtolower(str_replace(' ', '-', $files['name'][$index]));
                move_uploaded_file($files['tmp_name'][$index], Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . $proImg->image);
                $proImg->save();
            }
        }
    }

    /**
     * Save/update Caractersitic from prodduct
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 1.0.0 
     * @param model $model
     */
    public function saveCaracteristic($model) {
        Caracteristic::model()->deleteAll('id_product=' . $model->id);
        if (isset($_POST['car'])) {
            $value = array_values($_POST['car']);
            $key = array_keys($_POST['car']);
            for ($index = 0; $index < count($value); $index++) {
                if (!empty($key[$index])) {
                    $modelCara = new Caracteristic();
                    $modelCara->id_product = $model->id;
                    $modelCara->id_pro_type_field = $key[$index];
                    if (is_array($value[$index])) {
                        $modelCara->value = implode(',', $value[$index]);
                    } else {
                        $modelCara->value = $value[$index];
                    }
                    $modelCara->save();
                }
            }
        }
        if (isset($_FILES['car']) && !empty($_FILES['car'])) {
            $files = $_FILES['car'];
            $key = array_keys($files['name']);
            for ($index = 0; $index < count($key); $index++) {
                if (!empty($files['name'][$key[$index]])) {
                    $modelCara = new Caracteristic();
                    $modelCara->id_product = $model->id;
                    $modelCara->id_pro_type_field = $key[$index];
                    $modelCara->value = $this->generateRandomString(5) . '-' . strtolower(str_replace(' ', '-', $files['name'][$key[$index]]));
                    move_uploaded_file($files['tmp_name'][$key[$index]], Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . $modelCara->value);
                    $modelCara->save();
                }
            }
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $idRate = 0, $idImage = 0, $idInventory = 0) {
        if (empty($idInventory)) {
            $Inventory = new Inventory;
        } else {
            $Inventory = Inventory::model()->findByPk($idInventory);
        }
        $InventoryAdm = new Inventory('search');
        $InventoryAdm->unsetAttributes();  // clear any default values
        $InventoryAdm->id_product = $id;
        if (isset($_GET['Inventory']))
            $InventoryAdm->attributes = $_GET['Inventory'];
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($Inventory);

        if (isset($_POST['Inventory'])) {
            try {
                $Inventory->attributes = $_POST['Inventory'];
                if ($Inventory->type == 0) {
                    $agregar = false;
                } else {
                    $agregar = true;
                }
                $result = $this->addInventory($id, $Inventory->quantity, 11, 0, 0, Yii::app()->user->getState('PRODUCT')->PRODUCT_STOCK_DEFAULT, $agregar, $Inventory->comment);
                if ($result) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Inventory'))));
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Inventory'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        if (empty($idImage)) {
            $ProImagen = new ProImagen;
        } else {
            $ProImagen = ProImagen::model()->findByPk($idImage);
        }


        $ProImagenAdmin = new ProImagen('search');
        $ProImagenAdmin->unsetAttributes();  // clear any default values
        $ProImagenAdmin->id_product = $id;
        if (isset($_GET['ProImagen']))
            $ProImagenAdmin->attributes = $_GET['ProImagen'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($ProImagen);

        if (isset($_POST['ProImagen'])) {
            try {
                $ProImagen->attributes = $_POST['ProImagen'];
                $Image = CUploadedFile::getInstance($ProImagen, 'image');
                $ProImagen->id_product = $id;
                if (!empty($Image)) {
                    $ProImagen->image = $this->url_slug($ProImagen->title) . '-' . $this->generateRandomString(5) . '.' . $Image->getExtensionName();
                    $Image->saveAs(Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . $ProImagen->image);
                }
                if ($ProImagen->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'ProImagen'))));
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'ProImagen'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        if (empty($idRate)) {
            $ProductRate = new ProductRate;
        } else {
            $ProductRate = ProductRate::model()->findByPk($idRate);
        }

        $ProductRateAdmin = new ProductRate('search');
        $ProductRateAdmin->unsetAttributes();  // clear any default values
        $ProductRateAdmin->id_product = $id;

        if (isset($_GET['ProductRate']))
            $ProductRateAdmin->attributes = $_GET['ProductRate'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($ProductRate);

        if (isset($_POST['ProductRate'])) {
            try {
                $ProductRate->attributes = $_POST['ProductRate'];
                $ProductRate->id_product = $id;
                if ($ProductRate->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'ProductRate'))));
                    $this->redirect(array('update', 'id' => $id, 'idRate' => $ProductRate->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'ProductRate'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', '5007'));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $model = $this->loadModel($id);
        $model2 = new Product('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Product']))
            $model2->attributes = $_GET['Product'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Product'])) {
            try {
                $model->attributes = $_POST['Product'];
                $model->sku = strtoupper($model->sku);
                $model->slug = $this->url_slug($model->name);
                $Image = CUploadedFile::getInstance($model, 'photo');
                if (!empty($Image)) {
                    $model->photo = $this->url_slug($model->sku) . '-' . $this->url_slug($model->name) . '-' . $this->generateRandomString(5) . '.' . $Image->getExtensionName();
                    $Image->saveAs(Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_PRODUCTO_IMAGEN'] . $model->photo);
                }
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'Product'))));
                    $this->saveCaracteristic($model);
                    $this->saveImagen($model, $_FILES['imagen']);
                    if (isset($_POST['Split'])) {
                        $this->saveSplit($_POST['Split'], $model->getPrimaryKey());
                    }
                    if (isset($_POST['Category'])) {
                        $this->saveCategory($_POST['Category'], $model->getPrimaryKey());
                    }
                    $this->redirect(array('update', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'Product'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
            'model2' => $model2,
            'product' => CHtml::listData(Product::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'proType' => CHtml::listData(ProType::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'imagen' => ProImagen::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1 and id_product=' . $model->id),
            'split' => CHtml::listData(VSplit::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll(), 'id', 'name', 'father'),
            'splits' => CHtml::listData(ProductSplit::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('id_product=:id', array(':id' => $id)), 'id_split', 'id_split'),
            'brand' => CHtml::listData(Brand::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'category' => CHtml::listData(ProductCategory::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('id_product=:id', array(':id' => $id)), 'id_category', 'id_category'),
            'categories' => CHtml::listData(VCategory::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll(), 'id', 'name', 'father_name'),
            /* Rate */
            'Rate' => CHtml::listData(Rate::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Tax' => CHtml::listData(Tax::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'ProductRate' => $ProductRate,
            'ProductRateAdmin' => $ProductRateAdmin,
            'VProduct' => VProduct::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findByAttributes(array('id' => $id)),
            'Discount' => CHtml::listData(Discount::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'id' => $id,
            'ProImagen' => $ProImagen,
            'ProImagenAdmin' => $ProImagenAdmin,
            'InventoryAdmin' => $InventoryAdm,
            'Inventory' => $Inventory,
            'Stock' => CHtml::listData(Stock::model()->findAll('r_d_s=1'), 'id', 'name'),
            'InvOrigin' => CHtml::listData(InvOrigin::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Condition' => CHtml::listData(Condition::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'ProGroup' => BsHtml::listData(ProGroup::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Money'     => BsHtml::listData(Money::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_PRODUCT_TIME)->findAllByAttributes(['r_d_s' => 1]), 'id', 'acronym')
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('ProductDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Product'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex($id, $idRate = 0) {
        
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Product('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Product']))
            $model->attributes = $_GET['Product'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Product the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Product::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Product $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'product-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'product-rate-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pro-imagen-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'inventory-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionUpload() {
        $this->render('upload');
    }

    public function actionsaveUpload() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_EXCEL_PATH; // folder for uploaded files
        $allowedExtensions = array("xls", 'xlsx'); //array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = Yii::app()->user->getState('FILE')->FILES_UPLOAD_MAX; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
        $fileName = $result['filename']; //GETTING FILE NAME
        Yii::app()->user->setState('file', NULL);
        echo $return; // it's array
    }

    /**
     * Process Excel File after Upload
     * @param string $file
     * @author Giovanni Ariza <giosyst3m@gmail.com>
     * @version 1.0.0
     */
    public function actionprocessFileExcel($file = null) {
        extract($_POST);
        if (Yii::app()->user->getState('file') === NULL) {
            $this->file = $file;
            ini_set('max_execution_time', Yii::app()->user->getState('PRODUCT')->PRODUCT_EXCEL_MAX_EXECUTION_TIME);
            $file_path = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_EXCEL_PATH . $this->file;
            $sheet_array = Yii::app()->yexcel->readActiveSheet($file_path);
            $index = 1;
            foreach ($sheet_array as $row) {
                $a = array('id' => $index, 'result' => $this->getProduct($row));
                $b = array_merge($a, $row);
                $this->data[] = $b;
                $index++;
            }
            Yii::app()->user->setState('file', $this->data);
        }
        $arrayDataProvider = new CArrayDataProvider(Yii::app()->user->getState('file'), array(
            'id' => 'id',
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('PRODUCT')->PRODUCT_EXCEL_MAX_REG_GRID,
            ),
        ));
        if (isset($_GET['ajax'])) {
            $this->renderPartial('_result_excel', array(
                'arrayDataProvider' => $arrayDataProvider,
            ));
        } else {
            $this->render('_result_excel', array(
                'arrayDataProvider' => $arrayDataProvider,
            ));
        }
    }

    protected function getProduct($row) {
        $id_brand = $this->getBrand($row['D']);
        $Product = Product::model()->findByAttributes(array(
            Yii::app()->user->getState('PRODUCT')->PRODUCT_FIELD_KEY => trim($row['A']),
        ));
        try {
            if (!$Product) {
                $Product = new Product();
                $Product->sku = $row['B'];
                $Product->name = $row['C'];
                $Product->slug = $this->url_slug($row['C']);
                $Product->description = $row['C'];
                $Product->barcode = $row['A'];
                $Product->id_brand = $id_brand;
                $Product->id_condition = Yii::app()->user->getState('PRODUCT')->PRODUCT_CONDTION_DEFAULT;
                $Product->id_pro_type = Yii::app()->user->getState('PRODUCT')->PRODUCT_TYPE_DEFAULT;
                $Product->r_c_o = 'excel';
                $Product->r_c_f = $this->file;
                if ($Product->save()) {
                    $this->getCategory($row, $Product->id);
                    $this->getVechiculos($row, $Product->id);
                    $this->getRate($Product->id, $row['E']);
                    $this->addInventory($Product->id, $row['H'], 5);
                    return Yii::t('json', '2004');
                } else {
                    return BsHtml::errorSummary($Product);
                }
            } else {
                $Product->sku = $row['B'];
                $Product->description = $row['C'];
                $Product->barcode = $row['A'];
                $Product->id_brand = $id_brand;
                $Product->r_u_o = 'excel';
                $Product->r_u_f = $this->file;
                if ($Product->update()) {
                    $this->getCategory($row, $Product->id);
                    $this->getVechiculos($row, $Product->id);
                    $this->getRate($Product->id, $row['E']);
                    $this->addInventory($Product->id, $row['H'], 5);
                    return Yii::t('json', '2005');
                } else {
                    return BsHtml::errorSummary($Product);
                }
            }
        } catch (Exception $exc) {
            return Yii::t('json', $exc->getCode());
        }
    }

    protected function getBrand($name = null) {
        $model = Brand::model()->findByAttributes(array(
            'slug' => $this->url_slug($name)
        ));
        if ($model) {
            return $model->id;
        } else {
            $model = new Brand();
            $model->name = $name;
            $model->slug = $this->url_slug($name);
            $model->r_c_o = 'excel';
            $model->r_c_f = $this->file;
            $model->save();
            return $model->getPrimaryKey();
        }
    }

    protected function getCategory($row = array(), $id_product = 0) {
        try {
            ProductCategory::model()->deleteAllByAttributes(array(
                'id_product' => $id_product
            ));
            $category = explode(',', $row['F']);
            if (is_array($category)) {
                foreach ($category as $value) {
                    $cateModel = Category::model()->findByAttributes(array(
                        'slug' => $this->url_slug($value)
                    ));
                    if (!$cateModel) {
                        $cateModel = new Category;
                        $cateModel->name = $value;
                        $cateModel->slug = $this->url_slug($value);
                        $cateModel->father = 1;
                        $cateModel->r_c_o = 'excel';
                        $cateModel->r_c_f = $this->file;
                        $cateModel->save();
                    }
                    $model = new ProductCategory;
                    $model->id_category = $cateModel->id;
                    $model->id_product = $id_product;
                    $model->r_c_o = 'excel';
                    $model->r_c_f = $this->file;
                    $model->save();
                }
            } else {
                $cateModel = Category::model()->findByAttributes(array(
                    'slug' => $this->url_slug($value)
                ));
                if (!$cateModel) {
                    $cateModel = new Category;
                    $cateModel->name = $row['F'];
                    $cateModel->slug = $this->url_slug($row['F']);
                    $cateModel->father = 1;
                    $cateModel->r_c_o = 'excel';
                    $cateModel->r_c_f = $this->file;
                    $cateModel->save();
                }
                $model = new ProductCategory;
                $model->id_category = $cateModel->id;
                $model->id_product = $id_product;
                $model->r_c_o = 'excel';
                $model->r_c_f = $this->file;
                $model->save();
            }
        } catch (Exception $ex) {
            return;
            $exc->getTraceAsString();
        }
    }

    protected function getVechiculos($row = array(), $id_product = 0) {
        try {
            $category = explode(',', $row['G']);
            if (is_array($category)) {
                foreach ($category as $value) {
                    $cateModel = Category::model()->findByAttributes(array(
                        'slug' => $this->url_slug($value)
                    ));
                    if (!$cateModel) {
                        $cateModel = new Category;
                        $cateModel->name = $value;
                        $cateModel->slug = $this->url_slug($value);
                        $cateModel->father = 2;
                        $cateModel->r_c_o = 'excel';
                        $cateModel->r_c_f = $this->file;
                        $cateModel->save();
                    }
                    $model = new ProductCategory;
                    $model->id_category = $cateModel->id;
                    $model->id_product = $id_product;
                    $model->r_c_o = 'excel';
                    $model->r_c_f = $this->file;
                    $model->save();
                }
            } else {
                $cateModel = Category::model()->findByAttributes(array(
                    'slug' => $this->url_slug($value)
                ));
                if (!$cateModel) {
                    $cateModel = new Category;
                    $cateModel->name = $row['G'];
                    $cateModel->slug = $this->url_slug($row['G']);
                    $cateModel->father = 2;
                    $cateModel->r_c_o = 'excel';
                    $cateModel->r_c_f = $this->file;
                    $cateModel->save();
                }
                $model = new ProductCategory;
                $model->id_category = $cateModel->id;
                $model->id_product = $id_product;
                $model->r_c_o = 'excel';
                $model->r_c_f = $this->file;
                $model->save();
            }
        } catch (Exception $exc) {
            return;
            $exc->getTraceAsString();
        }
    }

    protected function getRate($id_product = 0, $price = 0) {
        $ProductRate = ProductRate::model()->findByAttributes(array(
            'id_product' => $id_product,
            'r_d_s' => 1,
            'id_rate' => Yii::app()->user->getState('PRODUCT')->PRODUCT_RATE_DEFAULT,
        ));
        if ($ProductRate) {
            $ProductRate->price = $price;
            $ProductRate->r_u_o = 'excel';
            $ProductRate->r_u_f = $this->file;
            $ProductRate->update();
        } else {
            $ProductRate = new ProductRate();
            $ProductRate->id_product = $id_product;
            $ProductRate->id_rate = Yii::app()->user->getState('PRODUCT')->PRODUCT_RATE_DEFAULT;
            $ProductRate->id_tax = Yii::app()->user->getState('PRODUCT')->PRODUCT_TAX_DEFAULT;
            $ProductRate->id_discount = Yii::app()->user->getState('PRODUCT')->PRODUCT_DISCOUNT_DEFAULT;
            $ProductRate->price = $price;
            $ProductRate->r_c_o = 'excel';
            $ProductRate->r_c_f = $this->file;
            $ProductRate->save();
        }
    }

    public function actionUploadImagen() {
        $this->render('uploadImagen');
    }

    public function actionsaveUploadImagen() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_FILE_PATH; // folder for uploaded files
        $allowedExtensions = array("zip"); //array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = Yii::app()->user->getState('FILE')->FILES_UPLOAD_MAX; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
        $fileName = $result['filename']; //GETTING FILE NAME
        Yii::app()->user->setState('file', NULL);
        echo $return; // it's array
    }

    public function actionprocessFileZip($file = null) {
        extract($_POST);
        $zip = Yii::app()->zip;
        $folder = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_FILE_PATH . $file;
        $data = array_keys($zip->infosZip($folder, false));
        $tmp = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('SYSTEM')->SYSTEM_TMP_PATH . $this->generateRandomString() . '/';
        mkdir($tmp, 07777);
        $zip->extractZip($folder, $tmp);
        $this->saveImagenfromZip($data, $tmp);
    }

    protected function saveImagenfromZip($data, $tmp) {
        $index = 0;
        foreach ($data as $key => $img) {
            if (!preg_match('/__MACOSX/', $img)) {
                $imgtemp = explode('.', $img);
                $imgs = explode('-', $imgtemp[0]);
                $Product = Product::model()->findByAttributes(array(
                    'r_d_s' => 1,
                    Yii::app()->user->getState('PRODUCT')->PRODUCT_FIELD_KEY => $imgs[0],
                        )
                );
                $result[$index]['id'] = $index;
                $result[$index]['sku'] = isset($Product->sku) ? $Product->sku : NULL;
                $result[$index]['barcode'] = isset($Product->barcode) ? $Product->barcode : null;
                $result[$index]['name'] = isset($Product->name) ? $Product->name : $this->url_slug($img);
                if (isset($imgtemp[1])) {
                    $result[$index]['save'] = $imgeName = $this->url_slug($result[$index]['sku']) . '-' . $this->url_slug($result[$index]['name']) . '-' . $this->generateRandomString(5) . '.' . $imgtemp[1];
                } else {
                    $result[$index]['save'] = null;
                }
                $result[$index]['imagen'] = $img;
                if (!isset($imgs[1]) && $Product) {
                    $Product->photo = $imgeName;
                    if ($Product->update()) {
                        rename($tmp . $img, Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH . $imgeName);
                        $result[$index]['result'] = Yii::t('json', 2008);
                    } else {
                        $result[$index]['result'] = BsHtml::errorSummary($Product);
                    }
                } elseif ($Product) {
                    $model = new ProImagen();
                    $model->id_product = $Product->id;
                    $model->title = $Product->sku . ' - ' . $Product->name;
                    $model->image = $imgeName;
                    if ($model->save()) {
                        rename($tmp . $img, Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH . $imgeName);
                        $result[$index]['result'] = Yii::t('json', 2009);
                    } else {
                        $result[$index]['result'] = BsHtml::errorSummary($model);
                    }
                } else {
                    $result[$index]['result'] = Yii::t('json', 4006);
                }
            }
            $index++;
        }
        $arrayDataProvider = new CArrayDataProvider($result, array(
            'id' => 'id',
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('PRODUCT')->PRODUCT_EXCEL_MAX_REG_GRID,
            ),
        ));
        if (isset($_GET['ajax'])) {
            $this->renderPartial('_result_zip', array(
                'arrayDataProvider' => $arrayDataProvider,
            ));
        } else {
            $this->render('_result_zip', array(
                'arrayDataProvider' => $arrayDataProvider,
            ));
        }
    }

    public function actiongetProductLists() {
        //Instancia DBCriteria
        $db = new CDbCriteria();
        //Agregar la concidión de busqueda con el LIKE
        $db->addSearchCondition('complete_name', Yii::app()->request->getQuery('term'));
        //Agregar la condición del Almacen stock
        $db->addInCondition('id_stock', array(Yii::app()->user->getState('id_stock')));
        //Impirmi JSON con resultados
        echo CJSON::encode(VProduct::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAll($db));
    }

    /**
     * Generate Product's List PDF
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 1.0.0
     * @version 1.0.0
     * @param array $progroup Selección de la Líneas
     * @param boolean $img 1=Show Product's images 0=No show product's images
     * @return pdf PDF Product's List
     */
    function actionListPDF($progroup = []) {
        extract($_REQUEST);
        ini_set('max_execution_time', Yii::app()->user->getState('SYSTEM')->SYSTEM_PDF_MAX_EXECUTION_TIME);
        ini_set('memory_limit', Yii::app()->user->getState('SYSTEM')->SYSTEM_PDF_MEMEORY_LIMIT);
        //Asignar el layour para PDF
        # mPDF
        $stylesheet = file_get_contents(Yii::app()->request->hostInfo . Yii::app()->theme->baseUrl . '/css/bootstrap-custom.min.css');
        $mPDF1 = Yii::app()->ePdf->mpdf($mode = '', $format = 'letter', $default_font_size = 0, $default_font = '', $mgl = 2, $mgr = 2, $mgt = 2, $mgb = 2, $mgh = 2, $mgf = 2, $orientation = 'P');
        $this->layout = '//layouts/pdf';
        $mPDF1->WriteHTML($stylesheet, 1);
        $CDbCriteria = new CDbCriteria();
        $CDbCriteria->select = 'pro_group';
        $CDbCriteria->distinct = true;
        $CDbCriteria->order = 'pro_group_orden';
        $CDbCriteria->addColumnCondition(['r_d_s' => 1]);
        $CDbCriteria->addInCondition('id_pro_group', explode(',', $progroup));
        $mPDF1->WriteHTML($this->render('product_list_pdf', array(
                    'ProGroups' => VProduct::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll($CDbCriteria),
                        ), true)
        );

//      # Load a stylesheet
        # renderPartial (only 'view' of current controller)
        # Outputs ready PDF

        $mPDF1->Output();
        unset($mPDF1);
    }

    function actionCatalogPrint($progroup = [],$type=0) {
        extract($_REQUEST);
        $progroup = explode(',', $progroup);
        $render = 'product_list_pdf_img';
        $CDbCriteria = new CDbCriteria();
        $CDbCriteria->addColumnCondition(['r_d_s' => 1]);
        $CDbCriteria->addCondition('quantity > 0');
        $CDbCriteria->order = Yii::app()->user->getState('CATALOG')->CATALOG_ORDER_PRODUCT;
        $CDbCriteria->addInCondition('id_pro_group', $progroup);
        switch ($type) {
            case 0:
                $this->layout = '//layouts/catalog_print';
                $render = 'product_list_pdf_img';
                break;
            case 1:
                $this->layout = '//layouts/catalog_print_big';
                $render = 'product_list_pdf_img_big';
                break;
            default:
                $render = 'product_list_pdf_img';
                break;
        }
        $this->render($render, array(
            'VProduct' => VProduct::model()->findAll($CDbCriteria),
        ));
    }

}
