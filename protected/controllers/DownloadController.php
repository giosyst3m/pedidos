<?php

class DownloadController extends Controller {
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'update' 
                'actions' => array('product'),
                'roles' => array('DownloadActionProduct'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('catalog'),
                'roles' => array('DownloadActionCatalog'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('client'),
                'roles' => array('DownloadActionClient'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    /**
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @version 1.0.0
     * @return xls Archivo de Excel con todos los productos
     */
    public function actionProduct() {
        $content = $this->renderPartial('_product',array('model'=> VProduct::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll()),true);
        Yii::app()->request->sendFile(Yii::t('app', 'Product').'-'.date('Y-m-d-h-i-s').'.xls',$content);
        
    }
    /**
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @version 1.0.0
     * @return xls Archivo de Excel con todos los productos por catalgos
     */
    public function actionCatalog($progroup) {
        $CDbCriteria = new CDbCriteria();
        $CDbCriteria->addInCondition('id_pro_group', explode(',', $progroup));
        $CDbCriteria->addCondition('quantity > 0');
        $CDbCriteria->addColumnCondition(['r_d_s'=>1]);
        $CDbCriteria->order = Yii::app()->user->getState('CATALOG')->CATALOG_ORDER_PRODUCT;
        $content = $this->renderPartial('_catalog',array('VProduct'=> VProduct::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll($CDbCriteria)),true);
        Yii::app()->request->sendFile(Yii::t('app', 'Catalog').'-'.date('Y-m-d-h-i-s').'.xls',$content);
        
    }
    
    /**
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @version 1.0.0
     * @return xls Archivo de Excel con todos los productos
     */
    public function actionClient() {
        $content = $this->renderPartial('_client',array('model'=> VZoneClient::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll()),true);
        Yii::app()->request->sendFile(Yii::t('app', 'Client').'-'.date('Y-m-d-h-i-s').'.xls',$content);
        
    }

    public function actionIndex() {
        $this->render('index');
    }

}
