<?php

class CompanyController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('CompanyActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('CompanyActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('CompanyActionCreate'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('CompanyActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('CompanyActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('CompanyActionDelete'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {

        $model = new Company;
        $model2 = new Company('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Company']))
            $model2->attributes = $_GET['Company'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Company'])) {
            try {
                $model->attributes = $_POST['Company'];
                $Image = CUploadedFile::getInstance($model, 'logo');
                if (!empty($Image)) {
                    $model->logo = 'company_logo.' . $Image->getExtensionName();
                    $Image->saveAs(Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_CLIENT_LOGO'] . $model->logo);
                }
                $model->father = 1;
                if ($model->save()) {
                    $this->createCounters($model->id);
                    $this->setStock($model);
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Company'))));
                    $this->redirect(array('update', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Company'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model, 'model2' => $model2
        ));
    }

    private function setStock($company) {
        $model = new Stock();
        $model->id_sto_location = 1;
        $model->id_sto_type = 1;
        $model->name = Yii::t('App', 'Store') . ' ' . $company->name;
        $model->save();
        $model2 = new StockCompany();
        $model2->id_company = $company->id;
        $model2->id_stock = $model->id;
        $model2->save();
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model8 = new ZoneCompany;
        $model9 = new ZoneCompany('search');
        $model9->unsetAttributes();  // clear any default values
        $model9->id_company = $id;
        if (isset($_GET['ZoneCompany']))
            $model9->attributes = $_GET['ZoneCompany'];
        
        $this->performAjaxValidation($model8);
        if (isset($_POST['ZoneCompany'])) {
            try {
                $model8->attributes = $_POST['ZoneCompany'];
                $model8->id_company = $id;
                if ($model8->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'ZoneClient'))));
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('update', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'ZoneClient'))));
                }
            } catch (Exception $e) {
                if($e->getCode() === 23000){
                    Yii::app()->user->setFlash('danger', Yii::t('json',5007)); 
                }else{
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }
        $model7 = new Order('search');
        $model7->unsetAttributes();  // clear any default values
        $model7->id_company = $id;
        if (isset($_GET['Order']))
            $model7->attributes = $_GET['Order'];

        $model5 = new StockCompany;
        $model6 = new StockCompany('search');
        $model6->unsetAttributes();  // clear any default values
        $model6->id_company = $id;
        if (isset($_GET['StockCompany']))
            $model6->attributes = $_GET['StockCompany'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model5);

        if (isset($_POST['StockCompany'])) {
            try {
                $model5->attributes = $_POST['StockCompany'];
                $model5->id_company = $id;
                if ($model5->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'StockCompany'))));
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'StockCompany'))));
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    Yii::app()->user->setFlash('danger', Yii::t('json', 5015));
                } else {
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $model3 = new CompanySisUsuario();
        $model3->id_company = $id;
        $this->performAjaxValidation($model3);

        if (isset($_POST['CompanySisUsuario'])) {
            try {
                $model3->attributes = $_POST['CompanySisUsuario'];
                if ($model3->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'CompanySisUsuario'))));
                    $this->redirect(array('update', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'CompanySisUsuario'))));
                }
            } catch (Exception $e) {
                if($e->getCode() === 23000){
                    Yii::app()->user->setFlash('danger', Yii::t('json',5007)); 
                }else{
                    Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
                }
            }
        }

        $model = $this->loadModel($id);
        $model2 = new Company('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Company']))
            $model2->attributes = $_GET['Company'];


        $model4 = new CompanySisUsuario('search');
        $model4->unsetAttributes();  // clear any default values
        $model4->id_company = $id;
        if (isset($_GET['CompanySisUsuario']))
            $model4->attributes = $_GET['CompanySisUsuario'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        if (isset($_POST['Company'])) {
            try {
                $model->attributes = $_POST['Company'];
                $Image = CUploadedFile::getInstance($model, 'logo');
                if (!empty($Image)) {
                    $model->logo = 'company_logo.' . $Image->getExtensionName();
                    $Image->saveAs(Yii::getPathOfAlias('webroot') . Yii::app()->params['URL_CLIENT_LOGO'] . $model->logo);
                }
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'Company'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'Company'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model,
            'model2' => $model2,
            'model3' => $model3,
            'model4' => $model4,
            'model5' => $model5,
            'model6' => $model6,
            'model7' => $model7,
            'model8' => $model8,
            'model9' => $model9,
            'FormDefault'=>false,//Show SisUsuario
            'FormDefault2' => true,
            'VSisUsuario' => BsHtml::listData(VSisUsuario::model()->findAll(), 'id', 'complete_name'),
            'Stock' => BsHtml::listData(Stock::model()->findAll('r_d_s=1'), 'id', 'name'),
            'Client' => CHtml::listData(Client::model()->findAll('r_d_s=1'), 'id', 'name'),
            'Status' => CHtml::listData(Status::model()->findAll('r_d_s=1'), 'id', 'name'),
            'Zone' => CHtml::listData(Zone::model()->findAll('r_d_s=1'), 'id', 'name'),
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('CompanyDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Company'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Company');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Company('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Company']))
            $model->attributes = $_GET['Company'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Company the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Company::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Company $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'company-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'company-sis-usuario-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'stock-company-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'zone-company-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
