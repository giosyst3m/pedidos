<?php

class ProgramController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('licence'),
                'roles' => array('ProgramActionLicence'),
            ),
            array('allow', // allow all users to perform 'index' 
                'actions' => array('legal','price'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    function actionLegal($info = null, $program = 1, $country = 1) {
        switch ($info) {
            case 'terminos-condiciones':
            case 'services-agreement':
                $this->render('servicesagreement', [
                    'Program' => Program::model()->cache(Yii::app()->params['CACHE_TIME'])->findByPk($program),
                    'Legal' => Legal::model()->cache(Yii::app()->params['CACHE_TIME'])->findAllByAttributes([
                        'id_program' => $program,
                        'id_country' => $country,
                        'r_d_s'=>1
                    ]),
                ]);
                break;
            case 'politicas-privacidad':
            case 'privacy-policy':
                $this->render('privacypolicy');
                break;
            case 'licence2':
                $model = new PlanPlaConfig('search');
                $criteria = new CDbCriteria();
                $criteria->addInCondition('id_sis_con_group', array(12));
                $dataProvider = new CActiveDataProvider($model, array(
                    'pagination' => array(
                        'pageSize' => 50,
                    ),
                    'criteria' => $criteria,
                ));


                $this->render('licence', array(
                    'model' => $dataProvider,
                ));
                break;
            default:
                $this->render('legal');
                break;
        }
    }

    public function actionPrice($id = 1) {
        $this->render('price', [
            'Plan' => Plan::model()->cache(Yii::app()->params['CACHE_TIME'])->findAll('r_d_s=1'),
            'PlanConfig' => PlaConfig::model()->cache(Yii::app()->params['CACHE_TIME'])->findAllByAttributes([
                'r_d_s' => 1,
                'show' => 1,
            ]),
            'Program' => Program::model()->cache(Yii::app()->params['CACHE_TIME'])->find('id=' . $id),
                ]
        );
    }

    /**
     * Mustra la licencia del sitio
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @version 1.0.0
     */
    public function actionLicence() {
        $this->render('licence', [
            'VSysUserPlan' => VSysUserPlan::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll("sub_domain='".Yii::app()->user->getState('SYSTEM')->SYSTEM_SUB_DOMAIN."' order by id DESC") ,
        ]);
    }

}
