<?php

class OrdDetailController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('OrdDetailActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('OrdDetailActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('OrdDetailActionCreate'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('refreshSumary'),
                'roles' => array('OrdDetailActionRefreshSumary'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('refreshSumaryByClient'),
                'roles' => array('OrdDetailActionrefreshSumaryByClient'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('updateDetailQuantity'),
                'roles' => array('OrdDetailActionUpdateDetailQuantity'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('OrdDetailActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('OrdDetailActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('OrdDetailActionDelete'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('ApprovedRequest'),
                'roles' => array('OrdDetailActionApprovedRequest'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('PDF'),
                'roles' => array('OrderActionPDF'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    function actionrefreshSumaryByClient($id = 0, $order = 0) {
        extract($_POST);
        if (empty($order)) {
            $model = VOrderSumary::model()->findByAttributes(array('clientId' => $id, 'id_status' => 1));
        } else {
            $model = VOrderSumary::model()->findByAttributes(array('clientId' => $id, 'orderNumber' => $order));
        }
        if ($model) {
            if (Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == "true") {
                $OrderDelRate = OrderDelRate::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $model->id));
            } else {
                $OrderDelRate = OrderDelivery::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $model->id));
            }
            $view = $this->renderPartial('_sumary', array(
                'orderSumary' => $model,
                'OrderTax' => OrderTax::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $model->orderNumber)),
                'OrderDelRate' => $OrderDelRate,
                'Discount' => BsHtml::listData(Discount::model()->findAll('r_d_s=1'), 'id', 'name'),
                    ), true);
            $msg = Yii::t('json', 2002, ['{status}' => $model->status]);
            $model = CJSON::decode(CJSON::encode($model));
            $data = array_merge($model, array('htlm' => $view));
            $response = $this->getJSon(200, $msg, $data);
        } else {
            $response = $this->getJSon(400, 4004);
        }

        echo $response;
    }

    function actionrefreshSumary($id = 0) {
        extract($_POST);
        if (Yii::app()->user->getState('ORDER')->ORDER_DISCOUNT_BY_PRODUCTO === 'true') {
            $VOrderSumaryItems = VOrderSumaryItems::model()->findByAttributes(array('orderNumber' => $id));
        } else {
            $VOrderSumaryItems = VOrderSumary::model()->findByAttributes(array('orderNumber' => $id));
        }
        if (Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == 'true') {
            $Delivery = OrderDelRate::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $id));
        } else {
            $Delivery = OrderDelivery::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $id));
        }
        if (!($VOrderSumaryItems->id_status == 7) && !($VOrderSumaryItems->id_status == 8)) {
            if (Yii::app()->user->getState('ORDER')->ORDER_DISCOUNT_BY_PRODUCTO == 'true') {
                $VOrderSumaryItems = VOrderSumaryItemsActive::model()->findByAttributes(array('orderNumber' => $id));
            } else {
                $VOrderSumaryItems = VOrderSumaryActive::model()->findByAttributes(array('orderNumber' => $id));
            }
        }
        $this->renderPartial('_sumary', array(
            'orderSumary' => $VOrderSumaryItems,
            'OrderTax' => OrderTax::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $id)),
            'OrderDelRate' => $Delivery,
            'Discount' => BsHtml::listData(Discount::model()->findAll('r_d_s=1'), 'id', 'name'),
        ));
    }

    /**
     * Update Quty from one Order
     * @author Giosyst3m <me@giossyt3m.net>
     * @param ind $id ID OrdDetatil
     * @param int $qty New Quantity
     * @return json Result
     * 
     */
    function actionupdateDetailQuantity($id = 0, $qty = 0) {
        extract($_POST);
        try {
            if (empty($id)) {
                $response = $this->getJSon(400, 4005);
            } else if (empty($qty) or $qty < 0) {
                $response = $this->getJSon(500, 5016);
            } else {

                if (OrdDetail::model()->updateByPk($id, array('quantity' => $qty))) {
                    $model = $this->loadModel($id);
                    $this->addInventory($model->id_product, $model->quantity, 3, $model->id_order, $model->id, 1, false);
                    $response = $this->getJSon(200, 2003);
                } else {
                    $response = $this->getJSon(400, 4039);
                }
            }
        } catch (Exception $e) {
            $response = $this->getJSon(500, 'API', array(), $e->getCode(), $e->getMessage(), array(
                'line' => $e->getLine(),
                'file' => $e->getFile(),
                'previos' => $e->getPrevious(),
                'trace' => $e->getTrace(),
            ));
        }
        echo $response;
    }

    /**
     * Actualiza el Inventario de los pedidos, en el estado de Despacho, 
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @param int $id ID de la ORderDatail La línea (detalle) del Orden 
     * @param int $qty Cantidad Actual
     * @param int $req Cantidad Nueva
     * @return json resultado de la operación.
     */
    function actionApprovedRequest($id = 0, $qty = 0, $req = 0) {
        extract($_POST);
        try {
            if (empty($id)) {
                $response = $this->getJSon(400, 4005);
            } else if ($qty < 0) {
                $response = $this->getJSon(500, 5016);
            } else if ($req < 0) {
                $response = $this->getJSon(500, 5017);
            } else {
                $OrdDetail = OrdDetail::model()->findByPk($id);
                $model = $this->loadModel($id);
                $existencia = VProduct::model()->find('id=' . $model->id_product)->quantity;
                if ($OrdDetail->inv_in == 1) {
                    $existencia = $existencia + $qty;
                }
                if (($existencia) < $req) {
                    $response = $this->getJSon(500, Yii::t('json', 5018, ['{req}' => $req, '{quantity}' => $existencia]));
                } else {
                    if ($model->idOrder->id_status > 1) {
                        $ord = OrdDetail::model()->updateByPk($id, array('quantity' => $req, 'request' => $req, 'inv_in' => ($req > 0 ? 1 : 0)));
                        if ($ord) {
                            if ($model->idOrder->id_status > 1) {
                                if ($req == 0) {
                                    $priority = 4;
                                } elseif ($qty == $req) {
                                    $priority = 2;
                                } elseif ($req < $qty) {
                                    $priority = 3;
                                } else {
                                    $priority = 1;
                                }
                                $this->addCommentToOrder($model->id_order, Yii::t('json', 1010, [
                                            '{pedido_numero}' => $model->idOrder->number,
                                            '{status}' => $model->idOrder->idStatus->name,
                                            '{color}' => $model->idOrder->idStatus->color,
                                            '{producto_nombre}' => $model->idProduct->sku . ' - ' . $model->idProduct->barcode . ' - ' . $model->idProduct->name,
                                            '{qty}' => $qty,
                                            '{req}' => $req,
                                            '{user}' => Yii::app()->user->nombre,
                                        ]), $priority);

                                if ($OrdDetail->inv_in == 1) {
                                    $this->addInventory($model->id_product, $qty, 3, $model->id_order, $model->id, 1);
//                                    Product::model()->updateByPk($model->idProduct->id, ['stock'=> $model->idProduct->stock + $qty]);
                                }
                                $this->addInventory($model->id_product, $req, 3, $model->id_order, $model->id, 1, false);
                            }
                            $response = $this->getJSon(200, 2003, ['qty' => $req, 'req' => (int) $req, 'exi' => VProduct::model()->find('id=' . $model->id_product)->quantity, 'id' => $model->id_product]);
                        } else {
                            $response = $this->getJSon(400, 4039);
                        }
                    } else {
                        if (OrdDetail::model()->updateByPk($id, array('quantity' => $qty, 'inv_in' => ($req > 0 ? 1 : 0)))) {
                            $response = $this->getJSon(200, 2003, ['qty' => $req, 'req' => (int) $req, 'exi' => VProduct::model()->find('id=' . $model->id_product)->quantity, 'id' => $model->id_product]);
                        } else {
                            $response = $this->getJSon(500, 5003);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $response = $this->getJSon(500, 'API', array(), $e->getCode(), $e->getMessage(), array(
                'line' => $e->getLine(),
                'file' => $e->getFile(),
                'previos' => $e->getPrevious(),
                'trace' => $e->getTrace(),
            ));
        }
        echo $response;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {

        try {
            /** Comentarios * */
            $model3 = new Comment;
            $this->performAjaxValidation($model3);
            if (isset($_POST['Comment'])) {
                $model3->attributes = $_POST['Comment'];
                $File = CUploadedFile::getInstance($model3, 'file');
                if (!empty($File)) {
                    $model3->file = $this->generateRandomString(5) . '-' . $this->url_slug($File->name) . '.' . $File->getExtensionName();
                    $File->saveAs(Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('COMMENT')->COMMENT_ORDER_FILE_PATH . $model3->file);
                }
                if ($model3->save()) {
                    $CommentOrder = new CommentOrder();
                    $CommentOrder->id_comment = $model3->id;
                    $CommentOrder->id_order = $id;
                    $CommentOrder->save();
                    if (Yii::app()->user->getState('COMMENT')->COMMENT_ORDER_SEND_EMAIL === 'true') {
                        $VCommentOrder = VCommentOrder::model()->find("id=" . $model3->id);
                        $Order = Order::model()->findByPk($id);
                        if ($VCommentOrder && $Order) {
                            $email_list = Yii::app()->user->getState('email') . ',' . $Order->rcu->email;
                            $subjetc = Yii::t('email', 'Email-2003', array('{numero}' => $Order->id, '{title}' => $VCommentOrder->title, '{priority}' => $VCommentOrder->priority));
                            $emailError = Yii::t('email', 'Email-5002', array('{numero}' => $Order->id, '{title}' => $VCommentOrder->title));
                            $this->sendEmail($email_list, 'comment_order', $subjetc, $subjetc, $emailError, array('Order' => $Order, 'VCommentOrder' => $VCommentOrder, 'email' => $email_list));
                        }
                    }
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Comment'))));
                    $this->redirect(array('view', 'id' => $id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Comment'))));
                }
            }
            $VCommentOrderList = new VCommentOrder('search');
            $VCommentOrderList->unsetAttributes();  // clear any default values
            $VCommentOrderList->id_order = $id;
            if (isset($_GET['VCommentOrder']))
                $VCommentOrderList->attributes = $_GET['VCommentOrder'];
            /* +Commentaraio* */


            $model2 = new OrdDocument('search');
            $model2->unsetAttributes();
            $model2->id_order = $id;
            $model2->r_d_s = 1;
            if (isset($_GET['OrdDocument']))
                $model2->attributes = $_GET['OrdDocument'];

            $model4 = new Status('search');
            $model4->unsetAttributes();
            $model4->id_sta_group = 1;



            if (Yii::app()->user->getState('ORDER')->ORDER_DISCOUNT_BY_PRODUCTO == 'true') {
                $VOrderSumaryItems = VOrderSumaryItems::model()->findByAttributes(array('orderNumber' => $id));
            } else {
                $VOrderSumaryItems = VOrderSumary::model()->findByAttributes(array('orderNumber' => $id));
            }

            if (Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == 'true') {
                $Delivery = BsHtml::listData(VDelivery::model()->findAll(), 'id', 'service');
            } else {
                $Delivery = BsHtml::listData(Delivery::model()->findAll('r_d_s=1'), 'id', 'name');
            }
            if (Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == 'true') {
                $OrderDelRate = OrderDelRate::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $id));
            } else {
                $OrderDelRate = OrderDelivery::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $id));
            }

            $model = new VOrderDetail('search');
            $model->unsetAttributes();  // clear any default values
            $model->id = $id;   
            $model->dbCriteria->order = Yii::app()->user->getState('ORDER')->ORDER_PRODUCT_IN_ORDER;
            if (!($VOrderSumaryItems->id_status == 7) && !($VOrderSumaryItems->id_status == 8)) {
                $model->ord_detail_r_d_s = 1;
                if (Yii::app()->user->getState('ORDER')->ORDER_DISCOUNT_BY_PRODUCTO == 'true') {
                    $VOrderSumaryItems = VOrderSumaryItemsActive::model()->findByAttributes(array('orderNumber' => $id));
                } else {
                    $VOrderSumaryItems = VOrderSumaryActive::model()->findByAttributes(array('orderNumber' => $id));
                }
            }
            if (empty($VOrderSumaryItems)) {
                if (Yii::app()->user->getState('ORDER')->ORDER_DISCOUNT_BY_PRODUCTO == 'true') {
                    $VOrderSumaryItems = VOrderSumaryItems::model()->findByAttributes(array('orderNumber' => $id));
                } else {
                    $VOrderSumaryItems = VOrderSumary::model()->findByAttributes(array('orderNumber' => $id));
                }
            }
            $this->render('view', array(
                'model' => $model,
                'orderSumary' => $VOrderSumaryItems,
                'VClient'    => VClient::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['id'=>$VOrderSumaryItems->clientId]),
                'OrderTax' => OrderTax::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $id)),
                'OrderDelRate' => $OrderDelRate,
                'Bank' => BsHtml::listData(Bank::model()->findAll('r_d_s=1'), 'id', 'name'),
                'Document' => BsHtml::listData(Document::model()->findAll('r_d_s=1'), 'id', 'name'),
                'model2' => $model2,
                'model3' => $model3,
                'model4' => $model4,
                'Delivery' => $Delivery,
                'Discount' => BsHtml::listData(Discount::model()->findAll('r_d_s=1'), 'id', 'name'),
                'ComPriority' => BsHtml::listData(ComPriority::model()->findAll('r_d_s=1'), 'id', 'name'),
//                'VCommentOrder' => VCommentOrder::model()->findAllByAttributes(array(
//                    'r_d_s' => 1,
//                    'id_order' => $id
//                        ), array('order' => 'r_c_d DESC'))
                'VCommentOrder' => $VCommentOrderList,
                'VSisUsuario'=>  VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByAttributes(['id' => $VOrderSumaryItems->r_c_u])
            ));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new OrdDetail;
        $model2 = new OrdDetail('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['OrdDetail']))
            $model2->attributes = $_GET['OrdDetail'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['OrdDetail'])) {
            try {
                $model->attributes = $_POST['OrdDetail'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'OrdDetail'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'OrdDetail'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model,
            'model2' => $model2,
            'order' => CHtml::listData(Order::model()->findAll('r_d_s=1'), 'id', 'id'),
            'product' => CHtml::listData(VProduct::model()->findAll('r_d_s=1'), 'id', 'complete_name', 'father'),
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model2 = new OrdDetail('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['OrdDetail']))
            $model2->attributes = $_GET['OrdDetail'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['OrdDetail'])) {
            try {
                $model->attributes = $_POST['OrdDetail'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'OrdDetail'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'OrdDetail'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model, 'model2' => $model2
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id = 0, $reason = NULL) {
        extract($_POST);
        if (Yii::app()->user->checkAccess('OrdDetailDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    if (empty($reason)) {
                        echo $this->getJSon(400, 4019, array('{numero}' => $id));
                    } else {
                        $model = $this->loadModel($id);
                        $this->delete($model, $reason.' Cant: '.$model->quantity, true);
                        $this->addInventory($model->id_product, $model->quantity, 3, $model->id_order, $model->id, 1, true, $reason);
                        echo $this->getJSon(200, 2012);
                    }
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'OrdDetail'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//                if (!isset($_GET['ajax']))
//                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            } else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('OrdDetail');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new OrdDetail('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['OrdDetail']))
            $model->attributes = $_GET['OrdDetail'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return OrdDetail the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = OrdDetail::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param OrdDetail $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'ord-detail-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'comment-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Genera la Orden en PDF
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-07-10
     * @version 1.0.0
     * @param int $id ID del Pedido 
     * @return PDF Archivo pdf generado dinámicamente
     */
    public function actionpdf($id = 0) {
        //Asignar el layour para PDF
        $this->layout = '//layouts/pdf';
        # mPDF
        $mPDF1 = Yii::app()->ePdf->mpdf();

        # Load a stylesheet
        $stylesheet = file_get_contents(Yii::app()->request->hostInfo . Yii::app()->theme->baseUrl . '/css/bootstrap-custom.min.css');

        $mPDF1->WriteHTML($stylesheet, 1);
        $Order = Order::model()->findByPk($id);
        # renderPartial (only 'view' of current controller)
        if (Yii::app()->user->getState('ORDER')->ORDER_DISCOUNT_BY_PRODUCTO === 'true') {
            $VOrderSumaryItems = VOrderSumaryItems::model()->findByAttributes(array('orderNumber' => $id));
        } else {
            $VOrderSumaryItems = VOrderSumary::model()->findByAttributes(array('orderNumber' => $id));
        }
        if (Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == 'true') {
            $OrderDelRate = OrderDelRate::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $id));
        } else {
            $OrderDelRate = OrderDelivery::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $id));
        }
        $mPDF1->WriteHTML($this->render('order_pdf', array(
                    'Order' => $Order,
                    'OrdDetatil' => OrdDetail::model()->findAllByAttributes(array(
                        'r_d_s' => 1,
                        'id_order' => $id
                            )
                    ),
                    'orderSumary' => $VOrderSumaryItems,
                    'OrderTax' => OrderTax::model()->findAllByAttributes(array('r_d_s' => 1, 'id_order' => $id)),
                    'OrderDelRate' => $OrderDelRate,
                        ), true));

        # Outputs ready PDF
        $mPDF1->Output();
    }

}
