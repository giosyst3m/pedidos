<?php

class OrderController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index'),
                'roles' => array('OrderActionIndex'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
                'roles' => array('OrderActionView'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('createOrder'),
                'roles' => array('OrderActionCreateOrder',),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('getOrderByClientSumary'),
                'roles' => array('OrderActionGetOrderByClientSumary',),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
                'roles' => array('OrderActionCreate',),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
                'roles' => array('OrderActionUpdate'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
                'roles' => array('OrderActionAdmin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
                'roles' => array('OrderActionDelete'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('Step2'),
                'roles' => array('OrderActionStep2'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('Step3'),
                'roles' => array('OrderActionStep3'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('Step4', 'addDelivery'),
                'roles' => array('OrderActionStep4'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('Step5', 'getDocument', 'SaveDocument', 'deleteOrdDocument', 'deleteDelivery'),
                'roles' => array('OrderActionStep5'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('Step6'),
                'roles' => array('OrderActionStep6'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('Step7'),
                'roles' => array('OrderActionStep7'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('Step8'),
                'roles' => array('OrderActionStep8'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('Step9'),
                'roles' => array('OrderActionStep9'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('Step8'),
                'roles' => array('OrderActionStep8'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('saveDiscount'),
                'roles' => array('OrderActionSaveDiscount'),
            ),
            array('allow', // allow all users to perform 'close' 
                'actions' => array('bill'),
                'roles' => array('OrderActionBill'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionBill() {
        $model = VOrderDocumentStatus::model()->findAll('qty = pagada and id_document = 2');
        $arr = [];
        foreach ($model as $data) {
            if (Order::model()->updateAll(['id_status' => 6], 'id_status = 5 AND id='. $data->id_order)){
                $arr[] = $data->id_order;
            }
        }
        $model = new VOrderSumary('search');
        $model->unsetAttributes();  // clear any default values
        $model->dbCriteria->addInCondition('id', $arr);
        if (isset($_GET['VOrderSumary']))
            $model->attributes = $_GET['VOrderSumary'];

        $this->render('bill', array(
            'model' => $model,
        ));
    }
    
    function actiongetIdbyName($name) {
        $model = VProduct::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findByAttributes(array(
            'complete_name' => $name,
        ));
        if ($model) {
            return $model->id;
        } else {
            return false;
        }
    }

    function actiongetIdbyBarcode($barcode) {
        $model = VProduct::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findByAttributes(array(
            'barcode' => $barcode,
            'id_stock' => Yii::app()->user->getState('id_stock'),
        ));
        if ($model) {
            return $model->id;
        } else {
            return false;
        }
    }

    /**
     * Create order
     * @param int $id_product 
     * @param int $id_client
     */
    function actioncreateOrder($id_product = 0, $id_client = 0, $quantity = 0, $origin = 2, $getId = false, $barcode = 0) {
        extract($_POST);
        if ($getId) {
            $id_product = $this->actiongetIdbyName($id_product);
        }
        if (!empty($barcode)) {
            $id_product = $this->actiongetIdbyBarcode($barcode);
            $qProducto = VProduct::model()->findByAttributes(array('id' => $id_product))->quantity;
            if ($qProducto <= 0) {
                echo $this->getJSon(500, 5021);
                return;
            }
        }
        try {
            if (empty($id_client)) {
                $response = $this->getJSon(400, 4001);
            } else if (empty($id_product)) {
                $response = $this->getJSon(400, 4002);
            } else if (empty($quantity) or $quantity < 0) {
                $response = $this->getJSon(500, 5016);
            } else if (empty(Yii::app()->user->getState('id_company'))) {
                $response = $this->getJSon(400, 4036);
            } else {
                $order = $this->validOrderByClient($id_client);
                if ($order === 5005) {
                    $response = $this->getJSon(500, 5005);
                } elseif ($order === 5006) {
                    $response = $this->getJSon(500, 5006);
                } elseif (is_numeric($order)) {
                    $value = $this->addProduct($order, $id_product, $quantity, $origin);
                    if ($value === TRUE) {
                        $response = $this->getJSon(200, 2001, VOrderSumaryItems::model()->find('orderNumber=:order', array(':order' => $order)));
                    } else {
                        if ($value === 23000) {
                            $response = $this->getJSon(500, 5004);
                        } else {
                            $response = $this->getJSon(500, 5002, $value);
                        }
                    }
                } else {
                    $response = $this->getJSon(500, 5001, array('info' => $order));
                }
            }
        } catch (Exception $e) {
            $response = $this->getJSon(500, 'API', array(), $e->getCode(), $e->getMessage(), array(
                'line' => $e->getLine(),
                'file' => $e->getFile(),
                'previos' => $e->getPrevious(),
                'trace' => $e->getTrace(),
            ));
        }
        echo $response;
    }

    function actiongetOrderByClientSumary($id = 0, $print = true) {
        extract($_POST);
        try {
            if (empty($id)) {
                $response = $this->getJSon(400, 4001);
            } else {
                $model = VOrderSumaryItems::model()->find('clientId=:id', array(':id' => $id));
                if (!empty($model)) {
                    $response = $this->getJSon(200, 2002, $model);
                } else {
                    $response = $this->getJSon(400, 4004, array('clientId' => '', 'orderNumber' => '', 'totalItems' => '', 'quantityItems' => '', 'total' => '', 'total_grant' => '', 'discount' => ''));
                }
            }
        } catch (Exception $e) {
            $response = $this->getJSon(500, 'API', array(), $e->getCode(), $e->getMessage(), array(
                'line' => $e->getLine(),
                'file' => $e->getFile(),
                'previos' => $e->getPrevious(),
                'trace' => $e->getTrace(),
            ));
        }
        if ($print) {
            echo $response;
        } else {
            return $response;
        }
    }

    /**
     * Valida Cient hava any Order open
     * @param int $id
     * @return int
     */
    private function validOrderByClient($id) {
        try {
            $data = Order::model()->findByAttributes(array('id_client' => $id, 'id_status' => 1));

            if (empty($data)) {
                $model = new Order();
                $model->id_client = $id;
                $model->id_status = 1;
                $model->id_document = 14;
                $model->id_company = Yii::app()->user->getState('id_company');
                $model->number = $this->getNumber(14);
                $model->save();
                $order = $model->getPrimaryKey();
                if (Yii::app()->user->getState('ORDER')->ORDER_STEP1_SEND_EMAIL === 'true') {
                    $model = Order::model()->findByPk((int) $order);
                    $status = $model->idStatus->name;
                    $subject = Yii::t('app', 'Order Number') . ' ' . $model->number . ' ' . Yii::t('app', 'Change Status to') . ' ' . $status;
                    $to = $this->getEmailsListByPerfil(Yii::app()->user->getState('ORDER')->ORDER_STEP1_EMAIL_GROUP_USER);
                    $ok = Yii::t('email', 'Email-2002', array('{numero}' => $id, '{status}' => $status, '{grupo}' => Yii::app()->user->getState('ORDER')->ORDER_STEP1_EMAIL_GROUP_USER));
                    $error = Yii::t('email', 'Email-5001', array('{numero}' => $id));
                    $from = Yii::app()->user->nombre . '<' . Yii::app()->user->email . '>';
                    $this->layout = 'mail';
                    $msg = $this->render('../mail/order', ['id' => $id, 'status' => $status], true);
                    $this->sendEmail($from, $to, $subject, $msg, $ok, $error);
                }
                if (!$this->addTax($order)) {
                    return 5005;
                }
            } else {
                $order = $data->id;
            }
            return $order;
        } catch (Exception $e) {
            return array(500, 'API', array(), $e->getCode(), $e->getMessage(), array(
                    'line' => $e->getLine(),
                    'file' => $e->getFile(),
                    'previos' => $e->getPrevious(),
                    'trace' => $e->getTrace(),
                    'message' => $e->getMessage(),
            ));
        }
    }

    /**
     * Add Defaoult Delivery to Order
     * @param int $id_order
     * @return boolean
     */
    function addDelivery($id_order) {
        try {
            if (!empty(Yii::app()->user->ORDER->DEFAULT_DELIVERY)) {
                $model = new OrderDelRate();
                $model->id_del_rate = (int) Yii::app()->user->ORDER->DEFAULT_DELIVERY;
                $model->id_order = $id_order;
                return $model->save();
            }
            return true;
        } catch (Exception $e) {
            return array(500, 'API', array(), $e->getCode(), $e->getMessage(), array(
                    'line' => $e->getLine(),
                    'file' => $e->getFile(),
                    'previos' => $e->getPrevious(),
                    'trace' => $e->getTrace(),
                    'message' => $e->getMessage(),
            ));
        }
    }

    /**
     * Add prodcu on Order Detail
     * @param int $id_order
     * @param int $id_product
     * @param int $quantity
     * @return boolean
     */
    function addProduct($id_order, $id_product, $quantity, $origin) {
        try {
            $rate = ProductRate::model()->findByAttributes(array('id_product' => $id_product, 'id_rate' => 1));
            if ($rate) {
                $model = new OrdDetail();
                $model->id_order = $id_order;
                $model->id_product = $id_product;
                $model->quantity = $quantity;
                $model->price = $rate->price;
                $model->id_discount = $rate->id_discount;
                $model->id_inv_origin = $origin;
                if ($model->save()) {
                    if (Yii::app()->user->getState('ORDER')->ORDER_UPDATE_INVENTORY == 'true') {
                        $this->addInventory($id_product, $quantity, $origin, $id_order, $model->id, 1, FALSE);
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            if ($e->getCode() == 23000) {
                return $e->getCode();
            } else {
                return array(500, 'API', array(), $e->getCode(), $e->getMessage(), array(
                        'line' => $e->getLine(),
                        'file' => $e->getFile(),
                        'previos' => $e->getPrevious(),
                        'trace' => $e->getTrace(),
                        'message' => $e->getMessage(),
                ));
            }
        }
    }

    /**
     * Add Tax Vigente
     * @param int $id_order Number Order
     * @return boolean
     */
    function addTax($id_order) {
        try {
            $tax = Tax::model()->findAll('r_d_s=1 AND CURDATE() >= `from` and CURDATE() <= `to`');
            foreach ($tax as $value) {
                $model = new OrderTax();
                $model->id_order = $id_order;
                $model->id_tax = $value->id;
                $model->save();
            }
            return true;
        } catch (Exception $e) {
            return array(500, 'API', array(), $e->getCode(), $e->getMessage(), array(
                    'line' => $e->getLine(),
                    'file' => $e->getFile(),
                    'previos' => $e->getPrevious(),
                    'trace' => $e->getTrace(),
                    'message' => $e->getMessage(),
            ));
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Order;
        $model2 = new Order('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Order']))
            $model2->attributes = $_GET['Order'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Order'])) {
            try {
                $model->attributes = $_POST['Order'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'Order'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'Order'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model,
            'model2' => $model2,
            'client' => CHtml::listData(Client::model()->findAll('r_d_s=1'), 'id', 'name'),
            'status' => CHtml::listData(Status::model()->findAll('r_d_s=1'), 'id', 'name'),
            'delivery' => CHtml::listData(Delivery::model()->findAll('r_d_s=1'), 'id', 'name'),
            'VSisUsuario' => CHtml::listData(VSisUsuario::model()->findAll('r_d_s=1'), 'id', 'complete_name')
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model2 = new Order('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['Order']))
            $model2->attributes = $_GET['Order'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Order'])) {
            try {
                $model->attributes = $_POST['Order'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'Order'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'Order'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model, 'model2' => $model2,
            'client' => CHtml::listData(Client::model()->findAll('r_d_s=1'), 'id', 'name'),
            'status' => CHtml::listData(Status::model()->findAll('r_d_s=1'), 'id', 'name'),
            'delivery' => CHtml::listData(Delivery::model()->findAll('r_d_s=1'), 'id', 'name')
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->user->checkAccess('OrderDeleteButtonDelete')) {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                try {
                    $this->delete($this->loadModel($id));
                } catch (Exception $e) {
                    throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'Order'))));
                }
                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }else {
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } else {
            throw new CHttpException(400, Yii::t('auth', 'DENEY', array('{accion}' => 'Eliminar')));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Order');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Order('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Order']))
            $model->attributes = $_GET['Order'];

        $this->render('admin', array(
            'model2' => $model,
            'client' => CHtml::listData(Client::model()->findAll('r_d_s=1'), 'id', 'name'),
            'status' => CHtml::listData(Status::model()->findAll('r_d_s=1'), 'id', 'name'),
            'delivery' => CHtml::listData(Delivery::model()->findAll('r_d_s=1'), 'id', 'name'),
            'VSisUsuario' => CHtml::listData(VSisUsuario::model()->findAll('r_d_s=1'), 'id', 'complete_name')
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Order the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Order::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Order $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'order-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionStep2($id = 0, $status = 0) {
        extract($_POST);
        if ($this->updateStatus(2, $id)) {
            $VOrderSumary = VOrderSumary::model()->find('orderNumber=' . $id);
            $message = Yii::t('json', 1012, [
                        '{pedido_numero}' => $VOrderSumary->number,
                        '{color}' => $VOrderSumary->color,
                        '{status}' => $VOrderSumary->status,
                        '{user}' => Yii::app()->user->nombre
            ]);
            $this->addCommentToOrder($id, $message);
            if (Yii::app()->user->getState('ORDER')->ORDER_STEP2_SEND_EMAIL === 'true') {
                $model = VOrderSumary::model()->find('orderNumber=' . $id);
                $subject = Yii::t('app', 'Order Number') . ' ' . $model->number . ' ' . Yii::t('app', 'Change Status to') . ' ' . $model->status;
                $to = $this->getEmailsListByPerfil(Yii::app()->user->getState('ORDER')->ORDER_STEP2_EMAIL_GROUP_USER);
                $ok = Yii::t('email', 'Email-2002', array('{numero}' => $id, '{status}' => $model->status, '{grupo}' => Yii::app()->user->getState('ORDER')->ORDER_STEP2_EMAIL_GROUP_USER));
                $error = Yii::t('email', 'Email-5001', array('{numero}' => $id));
                $from = Yii::app()->user->nombre . '<' . Yii::app()->user->email . '>';
                $this->layout = 'mail';
                $msg = $this->render('../mail/order', ['id' => $id, 'status' => $model->status], true);
                $this->sendEmail($from, $to, $subject, $msg, $ok, $error);
            }
            echo $this->getJSon(200, 2010);
        } else {
            echo $this->getJSon(500, 5003);
        };
    }

    public function actionStep3($id = 0, $note = '') {//Bodega
        extract($_POST);
        if (!empty($note)) {
            $model = new OrdDocument();
            $model->id_document = 1;
            $model->note = $note;
            $model->id_order = $id;
            $model->number = $id . '-' . $this->generateRandomString(5, true);
            $model->save();
        }
        if ($this->updateStatus(3, $id)) {
            $VOrderSumary = VOrderSumary::model()->find('orderNumber=' . $id);
            $message = Yii::t('json', 1012, [
                        '{pedido_numero}' => $VOrderSumary->number,
                        '{color}' => $VOrderSumary->color,
                        '{status}' => $VOrderSumary->status,
                        '{user}' => Yii::app()->user->nombre
            ]);
            $this->addCommentToOrder($id, $message);
            if (Yii::app()->user->getState('ORDER')->ORDER_STEP3_SEND_EMAIL === 'true') {
                $model = VOrderSumary::model()->find('orderNumber=' . $id);
                $subject = Yii::t('app', 'Order Number') . ' ' . $model->number . ' ' . Yii::t('app', 'Change Status to') . ' ' . $model->status;
                $to = $this->getEmailsListByPerfil(Yii::app()->user->getState('ORDER')->ORDER_STEP3_EMAIL_GROUP_USER);
                $ok = Yii::t('email', 'Email-2002', array('{numero}' => $id, '{status}' => $model->status, '{grupo}' => Yii::app()->user->getState('ORDER')->ORDER_STEP3_EMAIL_GROUP_USER));
                $user = $this->getUuser($model->r_c_u);
                $cc = $user->complete_name . '<' . $user->email . '>';
                $error = Yii::t('email', 'Email-5001', array('{numero}' => $id));
                $from = Yii::app()->user->nombre . '<' . Yii::app()->user->email . '>';
                $this->layout = 'mail';
                $msg = $this->render('../mail/order', ['id' => $id, 'status' => $model->status], true);
                $this->sendEmail($from, $to, $subject, $msg, $ok, $error, $cc);
            }
            echo $this->getJSon(200, 2010);
        } else {
            echo $this->getJSon(500, 5003);
        }
    }

    public function actionStep4($id = 0, $note = '', $delivery = null, $price = 0) {//Facturar
        extract($_POST);
        if (!empty($note)) {
            $model = new OrdDocument();
            $model->note = $note;
            $model->id_order = $id;
            $model->id_document = 1;
            $model->number = $id . '-' . $this->generateRandomString(5, true);
            $model->save();
        }
        if ($this->updateStatus(4, $id)) {
            $VOrderSumary = VOrderSumary::model()->find('orderNumber=' . $id);
            $message = Yii::t('json', 1012, [
                        '{pedido_numero}' => $VOrderSumary->number,
                        '{color}' => $VOrderSumary->color,
                        '{status}' => $VOrderSumary->status,
                        '{user}' => Yii::app()->user->nombre
            ]);
            $this->addCommentToOrder($id, $message);
            if (Yii::app()->user->getState('ORDER')->ORDER_STEP4_SEND_EMAIL === 'true') {
                $model = VOrderSumary::model()->find('orderNumber=' . $id);
                $subject = Yii::t('app', 'Order Number') . ' ' . $model->number . ' ' . Yii::t('app', 'Change Status to') . ' ' . $model->status;
                $to = $this->getEmailsListByPerfil(Yii::app()->user->getState('ORDER')->ORDER_STEP4_EMAIL_GROUP_USER);
                $ok = Yii::t('email', 'Email-2002', array('{numero}' => $id, '{status}' => $model->status, '{grupo}' => Yii::app()->user->getState('ORDER')->ORDER_STEP4_EMAIL_GROUP_USER));
                $user = $this->getUuser($model->r_c_u);
                $cc = $user->complete_name . '<' . $user->email . '>';
                $error = Yii::t('email', 'Email-5001', array('{numero}' => $id));
                $from = Yii::app()->user->nombre . '<' . Yii::app()->user->email . '>';
                $this->layout = 'mail';
                $msg = $this->render('../mail/order', ['id' => $id, 'status' => $model->status], true);
                $this->sendEmail($from, $to, $subject, $msg, $ok, $error, $cc);
            }
            echo $this->getJSon(200, 2010);
        } else {
            echo $this->getJSon(500, 5003);
        }
    }

    public function actionaddDeliveryXXX($id = 0, $guide = '', $date = '', $note = '', $delivery = null, $price = 0) {
        extract($_POST);
        if (empty($delivery)) {
            echo $this->getJSon(400, 4033);
        } else if (empty($guide)) {
            echo $this->getJSon(400, 4007);
        } else if (empty($date)) {
            echo $this->getJSon(400, 4008);
        } else if (empty($price) && Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == 'false') {
            echo $this->getJSon(400, 4037);
        } else {
            Yii::app()->db
                    ->createCommand("update ord_detail set request = quantity where id_order = :id and (ISNULL(request) or request = 0) and r_d_s=1")
                    ->bindValues(array(':id' => $id))
                    ->execute();
            $model = OrdDetail::model()->findAllByAttributes(array(
                'id_order' => $id,
                'r_d_s' => 1,
            ));
            foreach ($model as $value) {
                $this->addInventory($value->id_product, $value->quantity, $value->id_inv_origin, $value->id_order, $value->id, 0, false);
            }
            $model = new OrdDocument();
            $model->number = $guide;
            $model->date = $date;
            $model->note = $note;
            $model->id_order = $id;
            $model->id_document = 13;
            if ($model->save()) {
                if (Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == 'true') {
                    OrderDelRate::model()->deleteAllByAttributes(array('id_order' => $id));
                    $model = new OrderDelRate();
                } else {
                    $model = new OrderDelivery();
                    $model->rate = $price;
                }
                $model->id_delivery = $delivery;
                $model->id_order = $id;
                if ($model->save()) {
                    if ($this->updateStatus(4, $id)) {
                        if (Yii::app()->user->getState('ORDER')->ORDER_STEP4_SEND_EMAIL === 'true') {
                            $model = CHtml::listData(SisUsuario::model()->findAll('r_d_s=1 AND id_auth_item=' . "'" . Yii::app()->user->getState('ORDER')->ORDER_STEP4_EMAIL_GROUP_USER . "'"), 'id', 'email');
                            if ($model) {
                                $emails = array_values($model);
                                $email_list = implode(',', $model);
                                $status = Order::model()->findByPk($id)->idStatus->name;
                                $subjetc = Yii::t('email', 'Email-2001', array('{numero}' => $id, '{status}' => $status));
                                $emailOK = Yii::t('email', 'Email-2002', array('{numero}' => $id, '{status}' => $status, '{grupo}' => Yii::app()->user->getState('ORDER')->ORDER_STEP4_EMAIL_GROUP_USER));
                                $emailError = Yii::t('email', 'Email-5001', array('{numero}' => $id));
                                $this->sendEmail($email_list, 'order', $subjetc, $emailOK, $emailError, array('id' => $id, 'status' => $status));
                            }
                        }
                    } else {
                        echo $this->getJSon(500, 5003);
                    }
                    echo $this->getJSon(200, 2010);
                } else {
                    echo $this->getJSon(500, 5003, $model->getErrors());
                }
            }
        }
    }

    public function actionaddDelivery($id = 0, $guide = '', $date = '', $note = '', $delivery = null, $price = 0) {
        extract($_POST);
        if (empty($delivery)) {
            echo $this->getJSon(400, 4033);
        } else if (empty($guide)) {
            echo $this->getJSon(400, 4007);
        } else if (empty($date)) {
            echo $this->getJSon(400, 4008);
        } else if (empty($price) && Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == 'false') {
            echo $this->getJSon(400, 4037);
        } else {
            $model = new OrdDocument();
            $model->number = $guide;
            $model->date = $date;
            $model->note = Yii::t('app', 'Delivery') . ' ' . Delivery::model()->findByPk($delivery)->name . (!empty($note) ? ' ' . Yii::t('app', 'Note') . ': ' . $note : '');
            $model->total = $price;
            $model->id_order = $id;
            $model->id_document = 13;
            if ($model->save()) {
                if (Yii::app()->user->getState('ORDER')->ORDER_DELIVERY_AND_PRICE_SETTING == 'true') {
                    OrderDelRate::model()->deleteAllByAttributes(array('id_order' => $id));
                    $model = new OrderDelRate();
                } else {
                    $model = new OrderDelivery();
                    $model->rate = $price;
                }
                $model->id_delivery = $delivery;
                $model->id_order = $id;
                if ($model->save()) {
                    echo $this->getJSon(200, 2010);
                } else {
                    echo $this->getJSon(500, 5003, $model->getErrors());
                }
            }
        }
    }

    public function actionSaveDocument($id = 0, $numero = '', $date = '', $total = 0, $note = '') {
        extract($_POST);
        if (empty($numero)) {
            echo $this->getJSon(400, 4009);
        } else if (empty($total)) {
            echo $this->getJSon(400, 4011);
        } else if (empty($date)) {
            echo $this->getJSon(400, 4010);
        } else {
            try {
                $model = new OrdDocument();
                $model->number = $numero;
                $model->date = $date;
                $model->note = $note;
                $model->id_order = $id;
                $model->total = $total;
                $model->id_document = 2;
                $model->id_status = 10; //Estatus Registrada
                if ($model->save()) {
                    echo $this->getJSon(200, 2010);
                } else {
                    echo $this->getJSon(500, 5003);
                }
            } catch (Exception $e) {
                if ($e->getCode() === 23000) {
                    echo $this->getJSon(500, 5012, array('error' => $e));
                } else {
                    echo $this->getJSon(500, 5013, array('error' => $e));
                }
            }
        }
    }

    public function actiondeleteOrdDocument($id = 0) {
        extract($_REQUEST);
        try {
            if (OrdDocument::model()->deleteByPk($id)) {
                echo $this->getJSon(200, 2012);
            }
        } catch (Exception $e) {
            echo $this->getJSon(500, 5008, array('error' => $e));
        }
    }

    public function actiondeleteDelivery($id = 0) {
        extract($_REQUEST);
        try {
            if (OrderDelivery::model()->deleteByPk($id)) {
                echo $this->getJSon(200, 2012);
            }
        } catch (Exception $e) {
            echo $this->getJSon(500, 5008, array('error' => $e));
        }
    }

    public function actionStep5($id = 0) {
        extract($_REQUEST);
        Yii::app()->db
                ->createCommand("update ord_detail set request = quantity where id_order = :id and (ISNULL(request) or request = 0) and r_d_s=1")
                ->bindValues(array(':id' => $id))
                ->execute();
        if (Yii::app()->user->getState('ORDER')->ORDER_UPDATE_INVENTORY === 'false') {

            $model = VOrderDetail::model()->findAllByAttributes(array(
                'id' => $id,
                'ord_detail_r_d_s' => 1,
            ));
            foreach ($model as $value) {
                if (empty($value->request)) {
                    $message = Yii::t('json', 1011, [
                                '{pedido_numero}' => $value->number,
                                '{status}' => $value->status,
                                '{color}' => $value->color,
                                '{producto_nombre} ' => $value->complete_name,
                                '{qty}' => $value->ord_detail_quantity,
                                '{req}' => $value->request,
                                '{user}' => Yii::app()->user->nombre,
                    ]);
                    $this->addCommentToOrder($value->id, $message, 4);
                    $this->delete(OrdDetail::model()->findByPk($value->ord_detail_id), $message);
                } else {
                    if($value->inv_in == 0){
                        $this->addInventory($value->id_product, $value->ord_detail_quantity, 3, $id, $value->ord_detail_id, 0, false);
                        OrdDetail::model()->updateByPk($value->id, ['inv_in'=>1]);
                    }
                }
            }
        }
        $model = OrdDocument::model()->findAllByAttributes(array(
            'id_order' => $id,
            'id_document' => 2
        ));

        if ($model) {
            $model = OrdDocument::model()->findBySql('SELECT sum(total) as total from ord_document where id_order=:id_order AND id_document=:id_document', array(
                ':id_order' => (int) $id,
                ':id_document' => 2
            ));
            if (Yii::app()->user->getState('ORDER')->ORDER_DISCOUNT_BY_PRODUCTO === 'true') {
                $Sumary = VOrderSumaryItems::model()->findByAttributes(array('orderNumber' => $id));
            } else {
                $Sumary = VOrderSumary::model()->findByAttributes(array('orderNumber' => $id));
            }

            if ($this->updateStatus(5, $id)) {
                $VOrderSumary = VOrderSumary::model()->find('orderNumber=' . $id);
                $message = Yii::t('json', 1012, [
                            '{pedido_numero}' => $VOrderSumary->number,
                            '{color}' => $VOrderSumary->color,
                            '{status}' => $VOrderSumary->status,
                            '{user}' => Yii::app()->user->nombre
                ]);
                $this->addCommentToOrder($id, $message);
                $model = VOrderSumary::model()->find('orderNumber=' . $id);
                echo $this->getJSon(200, 2010);
            } else {
                echo $this->getJSon(500, 5003);
            }
        } else {
            echo $this->getJSon(400, 4034);
        }
    }

    public function actiongetDocument($id = 0, $type = 1, $json = true) {
        extract($_POST);
        $model = new OrdDocument('search');
        $model->unsetAttributes();
        $model->id_document = $type;
        $model->id_order = $id;
        $model->r_d_s = 1;
        if ($json) {
            echo $this->getJSon(200, 2010, array('datos' => $this->renderPartial('_document', array('model' => $model), TRUE)));
        } else {
            echo $this->renderPartial('_document', array('model' => $model));
        }
    }

    public function actionStep6($id = 0, $date = '', $note = '') {
        extract($_POST);
        if (!empty($note)) {
            $model = new OrdDocument();
            $model->number = $id . '-' . $this->generateRandomString(5, true);
            $model->note = $note;
            $model->id_document = 1;
            $model->id_order = $id;
            $model->save();
        }
        
        OrdDocument::model()->updateAll(['id_status' => 11], 'id_order='.$id);
        
        if ($this->updateStatus(6, $id)) {
            $VOrderSumary = VOrderSumary::model()->find('orderNumber=' . $id);
            $message = Yii::t('json', 1012, [
                        '{pedido_numero}' => $VOrderSumary->number,
                        '{color}' => $VOrderSumary->color,
                        '{status}' => $VOrderSumary->status,
                        '{user}' => Yii::app()->user->nombre
            ]);
            $this->addCommentToOrder($id, $message);
            if (Yii::app()->user->getState('ORDER')->ORDER_STEP6_SEND_EMAIL === 'true') {
                $model = VOrderSumary::model()->find('orderNumber=' . $id);
                $subject = Yii::t('app', 'Order Number') . ' ' . $model->number . ' ' . Yii::t('app', 'Change Status to') . ' ' . $model->status;
                $to = $this->getEmailsListByPerfil(Yii::app()->user->getState('ORDER')->ORDER_STEP6_EMAIL_GROUP_USER);
                $ok = Yii::t('email', 'Email-2002', array('{numero}' => $id, '{status}' => $model->status, '{grupo}' => Yii::app()->user->getState('ORDER')->ORDER_STEP6_EMAIL_GROUP_USER));
                $user = $this->getUuser($model->r_c_u);
                $cc = $user->complete_name . '<' . $user->email . '>';
                $error = Yii::t('email', 'Email-5001', array('{numero}' => $id));
                $from = Yii::app()->user->nombre . '<' . Yii::app()->user->email . '>';
                $this->layout = 'mail';
                $msg = $this->render('../mail/order', ['id' => $id, 'status' => $model->status], true);
                $this->sendEmail($from, $to, $subject, $msg, $ok, $error, $cc);
            }
            echo $this->getJSon(200, 2010);
        } else {
            echo $this->getJSon(500, 5003);
        }
    }

    public function actionStep7($id = 0, $reason = '', $status = 7) {//Eliminar una Orden
        extract($_POST);
        if (empty($reason)) {
            echo $this->getJSon(400, ($status == 7 ? 4017 : 4020), array('{numero}' => $id));
        } else {
            if ($this->updateStatus($status, $id)) {
                $VOrderSumary = VOrderSumary::model()->find('orderNumber=' . $id);
                $message = Yii::t('json', 1012, [
                            '{pedido_numero}' => $VOrderSumary->number,
                            '{color}' => $VOrderSumary->color,
                            '{status}' => $VOrderSumary->status,
                            '{user}' => Yii::app()->user->nombre
                ]);
                $this->addCommentToOrder($id, $message);
                if ($this->delete($this->loadModel($id), $reason)) {
                    $model = OrdDetail::model()->findAllByAttributes(array(
                        'id_order' => $id,
                        'r_d_s' => 1,
                    ));
                    foreach ($model as $value) {
                        $this->addInventory($value->id_product, $value->quantity, 6, $value->id_order, $value->id, 0, true, $reason);
                    }
                    OrdDetail::model()->updateAll(array(
                        'r_d_s' => 0,
                        'r_d_d' => date('Y-m-d H:i:s'),
                        'r_d_u' => Yii::app()->user->id,
                        'r_d_i' => Yii::app()->request->userHostAddress,
                        'r_d_r' => $reason,
                            ), 'id_order=:order AND r_d_s=1', array(':order' => $id));
                    if (Yii::app()->user->getState('ORDER')->ORDER_STEP7_SEND_EMAIL === 'true') {
                        $model = VOrderSumary::model()->find('orderNumber=' . $id);
                        $subject = Yii::t('app', 'Order Number') . ' ' . $model->number . ' ' . Yii::t('app', 'Change Status to') . ' ' . $model->status;
                        $cc = $this->getEmailsListByPerfil(Yii::app()->user->getState('ORDER')->ORDER_STEP7_EMAIL_GROUP_USER);
                        $ok = Yii::t('email', 'Email-2002', array('{numero}' => $id, '{status}' => $model->status, '{grupo}' => Yii::app()->user->getState('ORDER')->ORDER_STEP7_EMAIL_GROUP_USER));
                        $user = $this->getUuser($model->r_c_u);
                        $to = $user->complete_name . '<' . $user->email . '>';
                        $error = Yii::t('email', 'Email-5001', array('{numero}' => $id));
                        $from = Yii::app()->user->nombre . '<' . Yii::app()->user->email . '>';
                        $this->layout = 'mail';
                        $msg = $this->render('../mail/order', ['id' => $id, 'status' => $model->status], true);
                        $this->sendEmail($from, $to, $subject, $msg, $ok, $error, $cc);
                    }
                    echo $this->getJSon(200, 2011, array('{numero}' => $id));
                    Yii::app()->user->setFlash('success', Yii::t('json', '2011', array('{numero}' => $id)));
                } else {
                    echo $this->getJSon(500, 5008);
                }
            } else {
                echo $this->getJSon(500, 5003);
            }
        }
    }

    public function actionStep8($id = 0, $reason = '') {//Elimnar un producto
        extract($_POST);
        $this->actionStep7($id, $reason, 8);
        $VOrderSumary = VOrderSumary::model()->find('orderNumber=' . $id);
        $message = Yii::t('json', 1012, [
                    '{pedido_numero}' => $VOrderSumary->number,
                    '{color}' => $VOrderSumary->color,
                    '{status}' => $VOrderSumary->status,
                    '{user}' => Yii::app()->user->nombre
        ]);
        $this->addCommentToOrder($id, $message);
        if (Yii::app()->user->getState('ORDER')->ORDER_STEP8_SEND_EMAIL === 'true') {
            $model = VOrderSumary::model()->find('orderNumber=' . $id);
            $subject = Yii::t('app', 'Order Number') . ' ' . $model->number . ' ' . Yii::t('app', 'Change Status to') . ' ' . $model->status;
            $cc = $this->getEmailsListByPerfil(Yii::app()->user->getState('ORDER')->ORDER_STEP8_EMAIL_GROUP_USER);
            $ok = Yii::t('email', 'Email-2002', array('{numero}' => $id, '{status}' => $model->status, '{grupo}' => Yii::app()->user->getState('ORDER')->ORDER_STEP8_EMAIL_GROUP_USER));
            $user = $this->getUuser($model->r_c_u);
            $to = $user->complete_name . '<' . $user->email . '>';
            $error = Yii::t('email', 'Email-5001', array('{numero}' => $id));
            $from = Yii::app()->user->nombre . '<' . Yii::app()->user->email . '>';
            $this->layout = 'mail';
            $msg = $this->render('../mail/order', ['id' => $id, 'status' => $model->status], true);
            $this->sendEmail($from, $to, $subject, $msg, $ok, $error, $cc);
        }
    }

    public function actionStep9($id = 0, $reason = '') {//Reabrir una Orden rechazada
        extract($_POST);
        if (empty($reason)) {
            echo $this->getJSon(400, 4038);
        } else {
            $Order = Order::model()->findByPk($id);
            $model = new order();
            $model->id_client = $Order->id_client;
            $model->id_company = $Order->id_company;
            $model->id_discount = $Order->id_discount;
            $model->id_document = $Order->id_document;
            $model->id_ralate = $Order->id;
            $model->id_status = 9;
            $model->number = $this->getNumber(14);
            if ($model->save()) {
                Order::model()->updateByPk((int) $model->getPrimaryKey(), ['r_c_u' => $Order->r_c_u]);
                $OrdDetail = OrdDetail::model()->findAllByAttributes(array('id_order' => $id));
                foreach ($OrdDetail as $value) {
                    $this->addProduct($model->id, $value->id_product, $value->quantity, 2);
                }
                $this->addTax($model->id);
                $msg = Yii::t('json', 2017, array('{number_new}' => $model->number, '{number_old}' => $Order->number));
                $model3 = new Comment();
                $model3->id_com_priority = 4;
                $model3->message = $reason;
                $model3->title = $msg;
                $model3->save();
                $model4 = new CommentOrder();
                $model4->id_order = $model->id;
                $model4->id_comment = $model3->id;
                $model4->save();
                $model5 = new CommentOrder();
                $model5->id_order = $Order->id;
                $model5->id_comment = $model3->id;
                $model5->save();
                $VOrderSumary = VOrderSumary::model()->find('orderNumber=' . $id);
                $message = Yii::t('json', 1012, [
                            '{pedido_numero}' => $VOrderSumary->number,
                            '{color}' => $VOrderSumary->color,
                            '{status}' => $VOrderSumary->status,
                            '{user}' => Yii::app()->user->nombre
                ]);
                $this->addCommentToOrder($id, $message);
                if (Yii::app()->user->getState('ORDER')->ORDER_STEP9_SEND_EMAIL === 'true') {
                    $model = VOrderSumary::model()->find('orderNumber=' . $id);
                    $subject = Yii::t('app', 'Order Number') . ' ' . $model->number . ' ' . Yii::t('app', 'Change Status to') . ' ' . $model->status;
                    $cc = $this->getEmailsListByPerfil(Yii::app()->user->getState('ORDER')->ORDER_STEP9_EMAIL_GROUP_USER);
                    $ok = Yii::t('email', 'Email-2002', array('{numero}' => $id, '{status}' => $model->status, '{grupo}' => Yii::app()->user->getState('ORDER')->ORDER_STEP9_EMAIL_GROUP_USER));
                    $user = $this->getUuser($model->r_c_u);
                    $to = $user->complete_name . '<' . $user->email . '>';
                    $error = Yii::t('email', 'Email-5001', array('{numero}' => $id));
                    $from = Yii::app()->user->nombre . '<' . Yii::app()->user->email . '>';
                    $this->layout = 'mail';
                    $msg = $this->render('../mail/order', ['id' => $id, 'status' => $model->status], true);
                    $this->sendEmail($from, $to, $subject, $msg, $ok, $error, $cc);
                }
                echo $this->getJSon(200, $msg);
            }
        }
    }

    public function actionsaveDiscount($id = 0, $id_discount = 0) {
        extract($_REQUEST);
        if (empty($id)) {
            echo $this->getJSon(400, 4031);
        } elseif (empty($id_discount)) {
            echo $this->getJSon(400, 4032);
        } else {
            try {
                $model = Order::model()->findByPk($id);
                $model->id_discount = $id_discount;
                $VOrderSumary = VOrderSumary::model()->find('orderNumber=' . $id);
                if ($model->update()) {
                    $message = Yii::t('json', 1014, [
                                '{pedido_numero}' => $VOrderSumary->number,
                                '{color}' => $VOrderSumary->color,
                                '{status}' => $VOrderSumary->status,
                                '{user}' => Yii::app()->user->nombre,
                                '{descuento_actual}' => $VOrderSumary->discount_name,
                                '{descuento_nuevo}' => Discount::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByPk($id_discount)->name
                    ]);
                    $this->addCommentToOrder($id, $message, 3);
                    echo $this->getJSon(200, 2015);
                } else {
                    echo $this->getJSon(500, 5013);
                }
            } catch (Exception $e) {
                echo $this->getJSon(500, 'API', array(), $e->getCode(), $e->getMessage(), array(
                    'line' => $e->getLine(),
                    'file' => $e->getFile(),
                    'previos' => $e->getPrevious(),
                    'trace' => $e->getTrace(),
                ));
            }
        }
    }

}
