<?php

class CatalogController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index', 'filterProduct'),
                'roles' => array('CatalogActionIndex'),
            ),
            array('allow', // allow all users to perform 'index' 
                'actions' => array('QuickCatalog', 'QuickCatalogView'),
                'roles' => array('CatalogActionQuickCatalog'),
            ),
            array('allow', // allow all users to perform 'index' 
                'actions' => array('Simple'),
                'roles' => array('CatalogActionSimple'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($id = '', $order = 0, $page = 0) {
        
        if (!empty($id)) {
            $Client['name'] = VZoneClient::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->find('id=' . $id)->full_name;
        } else {
            $Client['name'] = '';
        }
        $Client['id'] = $id;
        $Client['order'] = $order;
        $this->render('index', array(
            'page' => 0,
            'Client' => $Client,
            'pages' => 1,
            'total' => 10,
            'ProGroup' => BsHtml::listData(ProGroup::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Brand' => BsHtml::listData(Brand::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Condition' => BsHtml::listData(Condition::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAll('r_d_s=1'), 'id', 'name'),
            'Category' => BsHtml::listData(VCategory::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAll(), 'id', 'name', 'father_name'),
                )
        );
    }

    public function actionfilterProduct($progroup = NULL, $brand = NULL, $condition = NULL, $category = NULL, $content = NULL, $last = 0, $page = 0) {
        extract($_POST);

        $criteria = new CDbCriteria;
        $criteria->addCondition('r_d_s=1 AND quantity > 0 AND id_stock=' . Yii::app()->user->getState('id_stock'));
        if (!empty($progroup)) {
            $criteria->addInCondition('id_pro_group', $progroup);
        }
        if (!empty($brand)) {
            $criteria->addInCondition('id_brand', $brand);
        }
        if (!empty($condition)) {
            $criteria->addInCondition('id_condition', $condition);
        }
        if (!empty($content)) {
            $content = explode(',', $content);
            foreach ($content as $value) {
                $criteria->addSearchCondition('complete_name', trim($value), TRUE);
            }
        }

        if (!empty($category)) {
            $criteria->addInCondition('id_category', $category);
        }
        $criteria->order = Yii::app()->user->getState('CATALOG')->CATALOG_ORDER_PRODUCT;
        $total = VProduct::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->count($criteria);
        $pages = new CPagination($total);
        $pages->pageSize = Yii::app()->user->getState('PRODUCT')->NUMBER_PRODUCT_INFINITE_SCROLL;

        $pages->currentPage = $page;
        $pages->applyLimit($criteria);
        $pages->params = [$criteria];
        $model = VProduct::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_CATALOG_TIME)->findAll($criteria);

        $html = $this->renderPartial('_index', array(
            'model' => $model,
            'page' => $page,
            'total'=>$total,
            'pages' => $pages->pageCount,
                ), TRUE
        );

        echo $this->getJSon(100, 1004, array('html' => $html, 'pages' => $pages->pageCount, 'total' => $total));
    }

    public function actionQuickCatalog($id = '', $order = 0) {
        if (!empty($id)) {
            $Client['name'] = VZoneClient::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->find('id=' . $id)->full_name;
        } else {
            $Client['name'] = '';
        }
        $Client['id'] = $id;
        $Client['order'] = $order;
        $this->render('quick_catalog', ['Client' => $Client,]);
    }

    public function actionQuickCatalogView($id = 0) {
        extract($_POST);
        $criteria = new CDbCriteria();
        $criteria->addColumnCondition([
            'id_order' => $id,
            'r_d_s' => 1,
        ]);
        $criteria->order = 'r_c_d DESC';
        $dataProvider = new CActiveDataProvider('OrdDetail', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));

        $this->renderPartial('_quick_catalog', array('model' => $dataProvider));
    }

    function actionSimple($id = '', $order = 0) {
        if (!empty($id)) {
            $Client['name'] = VZoneClient::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->find('id=' . $id)->full_name;
        } else {
            $Client['name'] = '';
        }
        $Client['id'] = $id;
        $Client['order'] = $order;
        $this->render('simple', ['Client' => $Client,]);
    }

}
