<?php

class ReportController extends Controller
{
	 /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('Catalog'),
                'roles' => array('ReportActionCatalog'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionCatalog() {
        $this->render('catalog',[
            'ProGroup'=>  BsHtml::listData(ProGroup::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll('r_d_s=1 order by orden'), 'id', 'name'),
        ]);
    }
}