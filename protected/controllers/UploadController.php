<?php

class UploadController extends Controller {

    private $data = array();
    private $type = 0;
    private $product = array();

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    private $file = '';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'update' 
                'actions' => array('product', 'saveUploadProduct', 'processFileExcelProduct'),
                'roles' => array('UploadActionProduct'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('file', 'saveUploadFile', 'processFileExcel'),
                'roles' => array('UploadActionProduct'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('ImagenProduct', 'saveUploadImagenProduct', 'processFileZipProduct'),
                'roles' => array('UploadActionImagenProduct'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('client', 'saveUploadClient', 'processFileExcelClient'),
                'roles' => array('UploadActionClient'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('bill', 'saveUploadBill', 'processFileExcelBill'),
                'roles' => array('UploadActionBill'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionfile($type = 0) {
        $this->type = $type;
        Yii::app()->user->setState('type', $type);
        $this->render('file', array('type' => $type, 'title' => Yii::t('json', $type)));
    }

    public function actionsaveUploadFile() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_EXCEL_PATH; // folder for uploaded files
        $allowedExtensions = array("xls", 'xlsx'); //array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = Yii::app()->user->getState('FILE')->FILES_UPLOAD_MAX; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
        $fileName = $result['filename']; //GETTING FILE NAME
        Yii::app()->user->setState('file', NULL);
        echo $return; // it's array
    }

    public function actionprocessFileExcel($file = null, $type = 0) {
        extract($_POST);
        if (Yii::app()->user->getState('file') === NULL) {
            $this->file = $file;
            ini_set('max_execution_time', Yii::app()->user->getState('PRODUCT')->PRODUCT_EXCEL_MAX_EXECUTION_TIME);
            $file_path = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_EXCEL_PATH . $this->file;
            $sheet_array = Yii::app()->yexcel->readActiveSheet($file_path);
            $index = 1;
            foreach ($sheet_array as $row) {
                $a = array('id' => $index, 'result' => $this->saveFileExcel($type, $row));
                $b = array_merge($a, $row);
                $this->data[] = $b;
                $index++;
            }
            Yii::app()->user->setState('file', $this->data);
        }
        $arrayDataProvider = new CArrayDataProvider(Yii::app()->user->getState('file'), array(
            'id' => 'id',
            'pagination' => false
        ));
        if ($type == 1015) {
            $this->renderPartial('_result_excel_cartera', array(
                'arrayDataProvider' => $arrayDataProvider,
            ));
        } else {
            echo 0;
        }
    }

    public function saveFileExcel($type, $row) {
        switch ($type) {
            case 1015:
                return $this->processCartera($row);

                break;

            default:
                break;
        }
    }

    public function processCartera($row) {
        if (empty($row['G'])) {
            return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4040));
        }
        if (empty($row['H'])) {
            return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4041));
        }
        if (empty($row['M'])) {
            return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4042));
        }
        $suc = explode('Suc :', $row['H']);
        $CDbCriteria = new CDbCriteria();
        $CDbCriteria->addSearchCondition('code', $row['G']);
//        $CDbCriteria->addColumnCondition(['correlative'=>(int)$suc[1]]);
        if (!Client::model()->find($CDbCriteria)) {
            unset($CDbCriteria);
            return BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('json', 5022));
        }
        $document = explode('-', $row['M']);
        if ($document[0] == 'F') {
            $VOrdDocument = VOrderDocument::model()->findAllByAttributes(['number' => (int) $document[2]]);
            if (!$VOrdDocument) {
                return BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('json', 5023));
            } else {
                $index = 0;
                $pedido = '';
                $ok = true;
                foreach ($VOrdDocument as $value) {
                    $index++;
                    $PedidoNumber[$index] = $value->order_number;
                    $pedido .= $index . ' - ' . Yii::t('app', 'Order') . ': ' . $value->order_number . ' Total: ' . number_format($value->total, Yii::app()->user->getState("SYSTEM")->SYSTEM_DECIMALS) . ' ';
                    if ($value->total == $row['R']) {
                        $pedido .= BsHtml::icon(BsHtml::GLYPHICON_OK);
                    } else {
                        $pedido .= BsHtml::icon(BsHtml::GLYPHICON_REMOVE) . '<br>';
                        $pedido .= Yii::t('json', 4043);
                        $ok = FALSE;
                    }
                    $pedido .= '<br>';
                }
                if ($index > 1) {
                    return BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('json', 5024)) . BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, $pedido);
                }
                if ($ok) {
                    return BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, 'Pendiente validar con el cliente') . BsHtml::alert(BsHtml::ALERT_COLOR_INFO, $pedido);
                } else {
                    return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, $pedido);
                }
                unset($VOrdDocument);
            }
        } else {
            return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4044));
        }
    }

    public function actionProduct($type = 0) {
        $this->type = $type;
        Yii::app()->user->setState('type', $type);
        $model=new VOrderSumary('search');
        $model->unsetAttributes();  // clear any default values
        $model->dbCriteria->addInCondition('id_status', explode(',',Yii::app()->user->getState('ORDER')->ORDER_BEFORE_UPLOAD_INVENTORY));
        
        if(isset($_GET['FieType']))
                $model->attributes=$_GET['FieType'];
        
        $this->render('product', array(
            'type'  => $type, 
            'data'  => BsHtml::listData(VOrderSumary::model()->findAll('id_status in('.Yii::app()->user->getState('ORDER')->ORDER_BEFORE_UPLOAD_INVENTORY.')'), 'orderNumber', 'client'),
            'title' => Yii::t('json', $type), 
            'model' => $model,
            'Status' => Status::model()->findAll(' id in ('.Yii::app()->user->getState('ORDER')->ORDER_BEFORE_UPLOAD_INVENTORY.')'),
            ));
    }

    public function actionsaveUploadProduct() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_EXCEL_PATH; // folder for uploaded files
        $allowedExtensions = array("xls", 'xlsx'); //array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = Yii::app()->user->getState('FILE')->FILES_UPLOAD_MAX; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
        $fileName = $result['filename']; //GETTING FILE NAME
        Yii::app()->user->setState('file', NULL);
        echo $return; // it's array
    }

    /**
     * Process Excel File after Upload
     * @param string $file
     * @author Giovanni Ariza <giosyst3m@gmail.com>
     * @version 1.0.0
     */
    public function actionprocessFileExcelProduct($file = null) {
        extract($_POST);
        if (Yii::app()->user->getState('file') === NULL) {
            $this->file = $file;
            ini_set('max_execution_time', Yii::app()->user->getState('PRODUCT')->PRODUCT_EXCEL_MAX_EXECUTION_TIME);
            $file_path = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_EXCEL_PATH . $this->file;
            $sheet_array = Yii::app()->yexcel->readActiveSheet($file_path);
            $index = 1;
            $this->updateInventory();
            foreach ($sheet_array as $row) {
                $a = array('id' => $index, 'result' => $this->getProduct($row));
                $b = array_merge($a, $row);
                $this->data[] = $b;
                $index++;
            }
            Yii::app()->user->setState('file', $this->data);
        }
        $arrayDataProvider = new CArrayDataProvider(Yii::app()->user->getState('file'), array(
            'id' => 'id',
            'pagination' => false
        ));
        if (isset($_GET['ajax'])) {
            $this->renderPartial('_result_excel_product', array(
                'arrayDataProvider' => $arrayDataProvider,
            ));
        } else {
            $this->renderPartial('_result_excel_product', array(
                'arrayDataProvider' => $arrayDataProvider,
            ));
        }
    }

    protected function getProduct($row) {
        $id_brand = $this->getBrand($row['E']);
        if (!$id_brand) {
            return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4021));
        }
        $id_pro_group = $this->getProGroup($row['B']);
        if (!$id_pro_group) {
            return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4022, array('{id}' => $row['B'])));
        }
        $id_category = $this->getCategory((int) $row['K']);
        if (!$id_category) {
            return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4023, array('{id}' => $row['K'])));
        }
        $id_pro_type = $this->getProType($row['A']);
        if (!$id_pro_type) {
            return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4024, array('{id}' => $row['A'])));
        }
        if ($row['I'] <= 0) {
            return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4027, array('{unidades}' => $row['I'])));
        }
        $Product = Product::model()->findByAttributes(array(
            Yii::app()->user->getState('PRODUCT')->PRODUCT_FIELD_KEY => trim($row['F']),
        ));
        try {
            if (!$Product) {
                $Product = new Product();
                $Product->sku = $row['D'];
                $Product->name = $row['E'];
                $Product->stock = $row['I'];
                $Product->slug = $this->url_slug($row['F'] . '-' . $row['D'] . '-' . $row['E']);
                $Product->barcode = $row['F'];
                $Product->id_brand = $id_brand;
                $Product->id_condition = Yii::app()->user->getState('PRODUCT')->PRODUCT_CONDTION_DEFAULT;
                $Product->id_pro_type = Yii::app()->user->getState('PRODUCT')->PRODUCT_TYPE_DEFAULT;
                $Product->id_pro_group = $id_pro_group;
                $Product->r_c_o = 'excel';
                $Product->r_c_f = $this->file;
                $Product->photo = Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_DEFAULT;
                if (trim(Yii::app()->user->getState('PRODUCT')->PRODUCT_CODE_ALTERNATIVE_1) === 'SIGO') {
                    $Product->code_alternative_1 = $row['A'] . str_pad($row['B'], 4, "0", STR_PAD_LEFT) . str_pad($row['C'], 6, '0', STR_PAD_LEFT);
                }
                if ($Product->save()) {
                    $this->saveProductCateogry($id_category, $Product->id);
                    $this->getRate($Product->id, $row['H']);
                    return $this->processProduct($Product, $row, TRUE);
                } else {
                    return BsHtml::errorSummary($Product);
                }
            } else {
                $Product->sku = $row['D'];
                $Product->name = $row['E'];
                $Product->barcode = $row['F'];
                $Product->id_brand = $id_brand;
                $Product->stock = $row['I'];
                $Product->r_u_o = 'excel';
                $Product->r_u_f = $this->file;
                $Product->r_d_s = 1;
                $Product->slug = $this->url_slug($row['F'] . '-' . $row['D'] . '-' . $row['E']);
                if (trim(Yii::app()->user->getState('PRODUCT')->PRODUCT_CODE_ALTERNATIVE_1) === 'SIGO') {
                    $Product->code_alternative_1 = str_pad($row['A'], 4, "0", STR_PAD_RIGHT) . $row['B'] . str_pad($row['C'], 6, '0', STR_PAD_LEFT);
                }
                if ($Product->update()) {
                    $this->saveProductCateogry($id_category, $Product->id);
                    $this->getRate($Product->id, $row['H']);
                    return $this->processProduct($Product, $row, false);
                } else {
                    return BsHtml::errorSummary($Product);
                }
            }
        } catch (Exception $exc) {
            return Yii::t('json', $exc->getCode());
        }
    }

    protected function processProduct($Product, $row, $insert = true) {
        switch (Yii::app()->user->getState('type')) {
            case 1005:
                if ($this->addInventory($Product->id, $row['I'], 10, 0, 0, 0, TRUE, '', FALSE)) {
                    if ($insert) {
                        return BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, Yii::t('json', 2004));
                    } else {
                        return BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, Yii::t('json', 2005));
                    }
                } else {
                    return BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('json', 5009));
                }
                break;
            case 1007:
            case 1006:
                if ($insert) {
                    if ($this->addInventory($Product->id, $row['I'], 5)) {
                        return BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, Yii::t('json', 2004));
                    } else {
                        return BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('json', 5009));
                    }
                } else {
                    if (isset($this->product[trim($row['F'])])) {
                        if ($this->product[$row['F']] < 0) {
                            return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4026));
                        } else {
                            if ($this->product[$row['F']] == $row['I']) {
                                return BsHtml::alert(BsHtml::ALERT_COLOR_INFO, Yii::t('json', 1008, array('{cantidad_actual}' => $this->product[$row['F']], '{cantidad_nueva}' => $row['I'])));
                            } elseif ($row['I'] > $this->product[$row['F']]) {
                                $diferencia = $row['I'] - $this->product[$row['F']];
                                $this->addInventory($Product->id, $diferencia, Yii::app()->user->getState('inventory'), 0, 0, 0, true, ' File: ' . $this->file);
                                return BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, Yii::t('json', 2014, array('{cantidad_actual}' => $this->product[$row['F']], '{cantidad_nueva}' => $row['I'], '{cantidad_diferencia}' => $diferencia)));
                            } else {
                                $diferencia = $this->product[$row['F']] - $row['I'];
                                $this->addInventory($Product->id, $diferencia, Yii::app()->user->getState('inventory'), 0, 0, 0, false, ' File: ' . $this->file);
                                return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4028, array('{cantidad_actual}' => $this->product[$row['F']], '{cantidad_nueva}' => $row['I'], '{cantidad_diferencia}' => $diferencia)));
                            }
                        }
                    } else {
                        return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4029));
                    }
                }
                break;

            default:
                return false;
                break;
        }
    }

    protected function updateInventory() {

        switch (Yii::app()->user->getState('type')) {
            case 1005:
                $this->desactiveAllProduct();
                $this->passProductoToCero();
                break;
            case 1006:
                $this->product = BsHtml::listData(VProduct::model()->findAll(), 'barcode', 'quantity');
                $this->desactiveAllProduct();
                Yii::app()->user->setState('inventory', 5);
                break;
            case 1007:
                $this->product = BsHtml::listData(VProduct::model()->findAll(), 'barcode', 'quantity');
                $this->desactiveAllProduct();
                Yii::app()->user->setState('inventory', 12);
                break;
            default:
                return false;
                break;
        }
    }

    protected function passProductoToCero() {
        $model = Product::model()->findAll();
        if ($model) {
            foreach ($model as $value) {
                if ($value->stock > 0) {
                    $this->addInventory($value->id, $value->stock, 10, 0, 0, Yii::app()->user->getState('PRODUCT')->PRODUCT_STOCK_DEFAULT, false, Yii::t('json', 1005) . ' file:' . $this->file, FALSE);
                } else {
                    $this->addInventory($value->id, $value->stock * -1, 10, 0, 0, Yii::app()->user->getState('PRODUCT')->PRODUCT_STOCK_DEFAULT, true, Yii::t('json', 1005) . ' file:' . $this->file, FALSE);
                }
            }
            Product::model()->updateAll(['stock' => 0]);
            return true;
        } else {
            return false;
        }
    }

    protected function desactiveAllProduct() {
        return Product::model()->updateAll(array(
                    'r_d_s' => 0,
                    'r_d_d' => date('Y-m-d H:i:s'),
                    'r_d_u' => Yii::app()->user->id,
                    'r_d_i' => Yii::app()->request->userHostAddress,
                    'r_d_o' => 'excel',
                    'r_d_f' => $this->file,
                    'r_d_m' => Yii::t('json', 2012) . ' ' . Yii::t('json', 1005),
                        ), 'r_d_s=1');
    }

    protected function getProType($id) {
        $model = ProType::model()->findByPk($id);
        if ($model) {
            return $model->id;
        } else {
            return FALSE;
        }
    }

    protected function getProGroup($id) {
        $model = ProGroup::model()->findByPk($id);
        if ($model) {
            return $model->id;
        } else {
            return FALSE;
        }
    }

    protected function getBrand($name = null) {
        $output_array = array();
        preg_match("/\((.*?)\)/", $name, $output_array);
        if (!empty($output_array[1])) {
            $model = Brand::model()->findByAttributes(array(
                'slug' => $this->url_slug($output_array[1])
            ));
            if ($model) {
                $model->name = trim($output_array[1]);
                $model->save();
                return $model->id;
            } else {
                $model = new Brand();
                $model->name = trim($output_array[1]);
                $model->slug = $this->url_slug($output_array[1]);
                $model->r_c_o = 'excel';
                $model->r_c_f = $this->file;
                $model->save();
                return $model->getPrimaryKey();
            }
        } else {
            return false;
        }
    }

    protected function getCategory($id) {
        $model = Category::model()->findByPk($id);
        if ($model) {
            return $model->id;
        } else {
            return false;
        }
    }

    protected function saveProductCateogry($id_category, $id_product) {
        try {
            ProductCategory::model()->deleteAllByAttributes(array(
                'id_product' => $id_product
            ));
            $model = new ProductCategory;
            $model->id_category = $id_category;
            $model->id_product = $id_product;
            $model->r_c_o = 'excel';
            $model->r_c_f = $this->file;
            $model->save();
        } catch (Exception $exc) {
            return;
            $exc->getTraceAsString();
        }
    }

    protected function XgetCategory($row = array(), $id_product = 0) {
        try {
            ProductCategory::model()->deleteAllByAttributes(array(
                'id_product' => $id_product
            ));
            $category = explode(',', $row['F']);
            if (is_array($category)) {
                foreach ($category as $value) {
                    $cateModel = Category::model()->findByAttributes(array(
                        'slug' => $this->url_slug($value)
                    ));
                    if (!$cateModel) {
                        $cateModel = new Category;
                        $cateModel->name = $value;
                        $cateModel->slug = $this->url_slug($value);
                        $cateModel->father = 1;
                        $cateModel->r_c_o = 'excel';
                        $cateModel->r_c_f = $this->file;
                        $cateModel->save();
                    }
                    $model = new ProductCategory;
                    $model->id_category = $cateModel->id;
                    $model->id_product = $id_product;
                    $model->r_c_o = 'excel';
                    $model->r_c_f = $this->file;
                    $model->save();
                }
            } else {
                $cateModel = Category::model()->findByAttributes(array(
                    'slug' => $this->url_slug($value)
                ));
                if (!$cateModel) {
                    $cateModel = new Category;
                    $cateModel->name = $row['F'];
                    $cateModel->slug = $this->url_slug($row['F']);
                    $cateModel->father = 1;
                    $cateModel->r_c_o = 'excel';
                    $cateModel->r_c_f = $this->file;
                    $cateModel->save();
                }
                $model = new ProductCategory;
                $model->id_category = $cateModel->id;
                $model->id_product = $id_product;
                $model->r_c_o = 'excel';
                $model->r_c_f = $this->file;
                $model->save();
            }
        } catch (Exception $ex) {
            return;
            $exc->getTraceAsString();
        }
    }

    protected function getVechiculos($row = array(), $id_product = 0) {
        try {
            $category = explode(',', $row['G']);
            if (is_array($category)) {
                foreach ($category as $value) {
                    $cateModel = Category::model()->findByAttributes(array(
                        'slug' => $this->url_slug($value)
                    ));
                    if (!$cateModel) {
                        $cateModel = new Category;
                        $cateModel->name = $value;
                        $cateModel->slug = $this->url_slug($value);
                        $cateModel->father = 2;
                        $cateModel->r_c_o = 'excel';
                        $cateModel->r_c_f = $this->file;
                        $cateModel->save();
                    }
                    $model = new ProductCategory;
                    $model->id_category = $cateModel->id;
                    $model->id_product = $id_product;
                    $model->r_c_o = 'excel';
                    $model->r_c_f = $this->file;
                    $model->save();
                }
            } else {
                $cateModel = Category::model()->findByAttributes(array(
                    'slug' => $this->url_slug($value)
                ));
                if (!$cateModel) {
                    $cateModel = new Category;
                    $cateModel->name = $row['G'];
                    $cateModel->slug = $this->url_slug($row['G']);
                    $cateModel->father = 2;
                    $cateModel->r_c_o = 'excel';
                    $cateModel->r_c_f = $this->file;
                    $cateModel->save();
                }
                $model = new ProductCategory;
                $model->id_category = $cateModel->id;
                $model->id_product = $id_product;
                $model->r_c_o = 'excel';
                $model->r_c_f = $this->file;
                $model->save();
            }
        } catch (Exception $exc) {
            return;
            $exc->getTraceAsString();
        }
    }

    protected function getRate($id_product = 0, $price = 0) {
        $ProductRate = ProductRate::model()->findByAttributes(array(
            'id_product' => $id_product,
            'r_d_s' => 1,
            'id_rate' => Yii::app()->user->getState('PRODUCT')->PRODUCT_RATE_DEFAULT,
        ));
        if ($ProductRate) {
            $ProductRate->price = $price;
            $ProductRate->r_u_o = 'excel';
            $ProductRate->r_u_f = $this->file;
            $ProductRate->update();
        } else {
            $ProductRate = new ProductRate();
            $ProductRate->id_product = $id_product;
            $ProductRate->id_rate = Yii::app()->user->getState('PRODUCT')->PRODUCT_RATE_DEFAULT;
            $ProductRate->id_tax = Yii::app()->user->getState('PRODUCT')->PRODUCT_TAX_DEFAULT;
            $ProductRate->id_discount = Yii::app()->user->getState('PRODUCT')->PRODUCT_DISCOUNT_DEFAULT;
            $ProductRate->price = $price;
            $ProductRate->r_c_o = 'excel';
            $ProductRate->r_c_f = $this->file;
            $ProductRate->save();
        }
    }

    public function actionImagenProduct() {
        $this->render('imagen_product');
    }

    public function actionsaveUploadImagenProduct() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_FILE_PATH; // folder for uploaded files
        $allowedExtensions = array("zip"); //array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = Yii::app()->user->getState('FILE')->FILES_UPLOAD_MAX; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
        $fileName = $result['filename']; //GETTING FILE NAME
        Yii::app()->user->setState('file', NULL);
        echo $return; // it's array
    }

    public function actionprocessFileZipProduct($file = null) {
        extract($_POST);
        $zip = Yii::app()->zip;
        $folder = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_FILE_PATH . $file;
        $data = array_keys($zip->infosZip($folder, false));
        $tmp = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('SYSTEM')->SYSTEM_TMP_PATH . $this->generateRandomString() . '/';
        mkdir($tmp, 07777);
        $zip->extractZip($folder, $tmp);
        $this->saveImagenfromZip($data, $tmp);
    }

    protected function saveImagenfromZip($data, $tmp) {
        $index = 0;
        foreach ($data as $key => $img) {
            if (!preg_match('/__MACOSX/', $img)) {
                $imgtemp = explode('.', $img);
                $imgs = explode('-', $imgtemp[0]);
                $Product = Product::model()->findByAttributes(array(
                    'r_d_s' => 1,
                    Yii::app()->user->getState('PRODUCT')->PRODUCT_FIELD_KEY => $imgs[0],
                        )
                );
                $result[$index]['id'] = $index;
                $result[$index]['sku'] = isset($Product->sku) ? $Product->sku : NULL;
                $result[$index]['barcode'] = isset($Product->barcode) ? $Product->barcode : null;
                $result[$index]['name'] = isset($Product->name) ? $Product->name : $this->url_slug($img);
                if (isset($imgtemp[1])) {
                    $result[$index]['save'] = $imgeName = $this->url_slug($result[$index]['sku']) . '-' . $this->url_slug($result[$index]['name']) . '-' . $this->generateRandomString(5) . '.' . $imgtemp[1];
                } else {
                    $result[$index]['save'] = null;
                }
                $result[$index]['imagen'] = $img;
                if (!isset($imgs[1]) && $Product) {
                    $Product->photo = $imgeName;
                    if ($Product->update()) {
                        rename($tmp . $img, Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH . $imgeName);
                        $result[$index]['result'] = BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, Yii::t('json', 2008));
                    } else {
                        $result[$index]['result'] = BsHtml::errorSummary($Product);
                    }
                } elseif ($Product) {
                    $model = new ProImagen();
                    $model->id_product = $Product->id;
                    $model->title = $Product->sku . ' - ' . $Product->name;
                    $model->image = $imgeName;
                    if ($model->save()) {
                        rename($tmp . $img, Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('PRODUCT')->PRODUCT_IMAGEN_PATH . $imgeName);
                        $result[$index]['result'] = BsHtml::alert(BsHtml::ALERT_COLOR_INFO, Yii::t('json', 2009));
                    } else {
                        $result[$index]['result'] = BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, BsHtml::errorSummary($model));
                    }
                } else {
                    $result[$index]['result'] = BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json', 4006));
                }
            }
            $index++;
        }
        $arrayDataProvider = new CArrayDataProvider($result, array(
            'id' => 'id',
            'pagination' => false
        ));
        if (isset($_GET['ajax'])) {
            $this->renderPartial('_result_zip_producto', array(
                'arrayDataProvider' => $arrayDataProvider,
            ));
        } else {
            $this->renderPartial('_result_zip_producto', array(
                'arrayDataProvider' => $arrayDataProvider,
            ));
        }
    }
/**
     * Subir archvio de excel de SIGO
     * @param int $type 1 = SIGO
     */
    public function actionBill() {
        $this->render('bill', [
            'System' => [
                1 => 'SIGO'
            ]
        ]);
    }
    
    /**
     * Guarda el archivo en la ruta requerida
     * @author Giovanni Ariza <giosyst3m@gmail.com>
     * @version 1.0.0
     */
    public function actionsaveUploadBill() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('BILL')->BILL_EXCEL_PATH; // folder for uploaded files
        $allowedExtensions = array("xls", 'xlsx'); //array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = Yii::app()->user->getState('FILE')->FILES_UPLOAD_MAX; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
        $fileName = $result['filename']; //GETTING FILE NAME
        Yii::app()->user->setState('file', NULL);
        echo $return; // it's array
    }
    
    /**
     * Process Bill's Excel File after upload
     * @param string $file nombre del archivo
     * @param int $system tipo de sistema
     * @author Giovanni Ariza <sistemas@giosyst3m.com>
     * @version 1.0.0
     * 
     */
    public function actionprocessFileExcelBill($file = null, $system = 0) {
        extract($_POST);
        if (!empty($system)) {
            if (Yii::app()->user->getState('file') === NULL) {
                $this->file = $file;
                ini_set('max_execution_time', Yii::app()->user->getState('BILL')->BILL_EXCEL_MAX_EXECUTION_TIME);
                $file_path = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('BILL')->BILL_EXCEL_PATH . $this->file;
                $sheet_array = Yii::app()->yexcel->readActiveSheet($file_path);
                $index = 1;
                foreach ($sheet_array as $row) {
                    $a = array('id' => $index, 'result' => $this->saveBillFromExcel($row, $system));
                    $b = array_merge($a, $row);
                    $this->data[] = $b;
                    $index++;
                }
                Yii::app()->user->setState('file', $this->data);
            }
            $arrayDataProvider = new CArrayDataProvider(Yii::app()->user->getState('file'), array(
                'id' => 'id',
                'pagination' => false
            ));
            if (isset($_GET['ajax'])) {
                $this->renderPartial('_result_excel_bill', array(
                    'arrayDataProvider' => $arrayDataProvider,
                ));
            } else {
                $this->renderPartial('_result_excel_bill', array(
                    'arrayDataProvider' => $arrayDataProvider,
                ));
            }
        } else {
            echo BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('json', 5025));
        }
    }
    
    protected function saveBillFromExcel($row, $system = 0) {
        $qty = 0;
        $total = 0;
        $save = null;
        $order = '<table class="items table table-condensed table-bordered table-striped"><thead><tr><th>#</th><th>Estado</th><th>Cliente</th><th>Total</th></tr></thead><tbody>';
        if($system == 1){ //SIGO
            $data = explode('-', $row['D']);
            if(!empty($data[1])){
                 $bill = (int)$data[1];
                $orDocumento = VOrderDocument::model()->findAllByAttributes(['number' => $bill]);
                if($orDocumento){
                    foreach ($orDocumento as $data) {
                        $qty++;
                        $total = $total +  $data->total;
                        $save = OrdDocument::model()->updateAll(['id_status'=>11,'r_u_o'=> 'excel', 'r_u_f'=> $this->file,'r_u_m'=> Yii::t('json', 2019)], 'id_status = 10 and id='. $data->id);
                        
                        $order = $order.'<tr>' 
                                . '<td>'.$data->orderNumber.'</td>'
                                .'<td>'.($save?'<i class="fa fa-check"></i>':'<i class="fa fa-ban"></i>').'</td>'
                                . '<td>'.$data->client.'</td>'
                                . '<td class="text-right">'.number_format($data->total, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS).'</td>'
                                . '</tr>';
                    }
                    $order = $order.'</tr>'
                            . '<tfoot><tr class="btn-teal">'
                            . '<td colspan="2">Totales:</td>'
                            . "<td>$qty</td>"
                            . "<td class='text-right'>".number_format($total, Yii::app()->user->getState('SYSTEM')->SYSTEM_DECIMALS)."</td>"
                            . '</tr></tfoot></tbody></table>';
                }else{
                    
                    return BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('json',5026,['{bill}'=>$bill]));
                }
                return BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, Yii::t('json', 2019,['{bill}'=> $bill ])).$order;
                
            }else{
                return BsHtml::alert(BsHtml::ALERT_COLOR_WARNING, Yii::t('json',4045));
            }

        }
        return BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, Yii::t('json', 2006));
    }
    
    
    
    public function actionClient() {
        $this->render('client', array(
            'Zone' => BsHtml::listData(Zone::model()->findAll('r_d_s=1'), 'id', 'name'),
                )
        );
    }
    
    /**
     * Process Excel File after Upload
     * @param string $file
     * @author Giovanni Ariza <giosyst3m@gmail.com>
     * @version 1.0.0
     */
    public function actionprocessFileExcelClient($file = null, $zone = 0) {
        extract($_POST);
        if (!empty($zone)) {
            if (Yii::app()->user->getState('file') === NULL) {
                $this->file = $file;
                ini_set('max_execution_time', Yii::app()->user->getState('CLIENT')->CLIENT_EXCEL_MAX_EXECUTION_TIME);
                $file_path = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('CLIENT')->CLIENT_EXCEL_PATH . $this->file;
                $sheet_array = Yii::app()->yexcel->readActiveSheet($file_path);
                $index = 1;
                $this->desactiveAllClient($zone);
                foreach ($sheet_array as $row) {
                    $a = array('id' => $index, 'result' => $this->saveClientFromExcel($row, $zone));
                    $b = array_merge($a, $row);
                    $this->data[] = $b;
                    $index++;
                }
                Yii::app()->user->setState('file', $this->data);
            }
            $arrayDataProvider = new CArrayDataProvider(Yii::app()->user->getState('file'), array(
                'id' => 'id',
                'pagination' => false
            ));
            if (isset($_GET['ajax'])) {
                $this->renderPartial('_result_excel_client', array(
                    'arrayDataProvider' => $arrayDataProvider,
                ));
            } else {
                $this->renderPartial('_result_excel_client', array(
                    'arrayDataProvider' => $arrayDataProvider,
                ));
            }
        } else {
            echo BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('json', 5019));
        }
    }

    protected function desactiveAllClient($zone = 0) {
        $ZoneClient = BsHtml::listData(ZoneClient::model()->findAllByAttributes([
                            'id_zone' => $zone,
                        ]), 'id_client', 'id_client');
        $CDbCriteria = new CDbCriteria();
        $CDbCriteria->addInCondition('id', $ZoneClient);
        $CDbCriteria->addColumnCondition([
            'r_d_s' => 1
        ]);
        Client::model()->updateAll(array(
            'r_d_s' => 0,
            'r_d_d' => date('Y-m-d H:i:s'),
            'r_d_u' => Yii::app()->user->id,
            'r_d_i' => Yii::app()->request->userHostAddress,
            'r_d_o' => 'excel',
            'r_d_f' => $this->file,
            'r_d_m' => Yii::t('json', 2012) . ' ' . Yii::t('json', 1009),
                ), $CDbCriteria);
        ZoneClient::model()->updateAll(array('r_d_s' => 0), 'id_zone=:zone', array(':zone' => $zone));
    }

    public function actionsaveUploadClient() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = Yii::getPathOfAlias('webroot') . Yii::app()->user->getState('CLIENT')->CLIENT_EXCEL_PATH; // folder for uploaded files
        $allowedExtensions = array("xls", 'xlsx'); //array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = Yii::app()->user->getState('FILE')->FILES_UPLOAD_MAX; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
        $fileName = $result['filename']; //GETTING FILE NAME
        Yii::app()->user->setState('file', NULL);
        echo $return; // it's array
    }

    protected function saveClientFromExcel($row, $zone = 0) {
        $Client = Client::model()->findByAttributes(array(
            Yii::app()->user->getState('CLIENT')->CLIENT_FIELD_KEY => trim($row['A']) . '-' . $row['C'],
            'correlative' => $row['B']
        ));
        try {
            if ($Client) {
                $Client->name = $row['D'];
                $Client->phone = $row['I'];
                $Client->address = $row['F'];
                $Client->id_city = $this->getCity($row['G']);
                $Client->r_u_o = 'excel';
                $Client->r_u_f = $this->file;
                $Client->r_d_s = 1;
                if (!$Client->update()) {
                    return BsHtml::errorSummary($Client);
                } else {
                    $this->saveZoneClient($Client->id, $zone);
                    $this->saveClientData($Client->id, $row);
                    return BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, Yii::t('json', 2007));
                }
            } else {
                $Client = New Client();
                $Client->code = trim($row['A']) . '-' . $row['C'];
                $Client->correlative = empty($row['B']) ? 0 : (int) $row['B'];
                $Client->name = $row['D'];
                $Client->address = $row['F'];
                $Client->id_city = $this->getCity($row['G']);
                $Client->phone = $row['I'];
                $Client->r_c_o = 'excel';
                $Client->r_c_f = $this->file;
                if (!$Client->save()) {
                    return BsHtml::errorSummary($Client);
                } else {
                    $this->saveZoneClient($Client->id, $zone);
                    return BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, Yii::t('json', 2006));
                }
            }
        } catch (Exception $exc) {
            return Yii::t('json', $exc->getCode());
        }
    }

    protected function saveZoneClient($id, $zone) {
        try {
            if (!ZoneClient::model()->updateAll(array('r_d_s' => 1), 'id_client=:client', array(':client' => $id))) {
                $model = new ZoneClient();
                $model->id_client = $id;
                $model->id_zone = $zone;
                $model->save();
            }
        } catch (Exception $exc) {
            
        }
    }

    protected function saveClientData($id, $row) {
        $model = ClientField::model()->findAll('r_d_s=1');
        $CliData = CliData::model()->deleteAllByAttributes(array(
            'id_client' => $id,
        ));
        foreach ($model as $data) {
            switch ($data->id) {
                case 1:
                    $value = $row['B'];
                    break;
                case 2:
                    $value = $row['C'];
                    break;
                case 3:
                    $value = $row['E'];
                    break;
                case 3:
                    $value = $row['E'];
                    break;
                case 4:
                    $value = $row['H'];
                    break;
                case 5:
                    $value = $row['J'];
                    break;
                case 6:
                    $value = $row['K'];
                case 7:
                    $value = $row['L'];
                    break;
                case 8:
                    $value = $row['M'];
                    break;
                case 9:
                    $value = $row['N'];
                    break;
                case 10:
                    $value = $row['O'];
                    break;
                case 11:
                    $value = $row['P'];
                    break;

                default:
                    $value = null;
                    break;
            }
            $CliData = new CliData();
            $CliData->id_client = $id;
            $CliData->id_field_client = $data->id;
            $CliData->value = $value;
            $CliData->r_c_o = 'excel';
            $CliData->r_c_f = $this->file;
            $CliData->save();
        }
    }

    protected function getCity($name) {
        $City = City::model()->findByAttributes(array('slug' => $this->url_slug($name)));
        if (!$City) {
            $City = new City();
            $City->name = $name;
            $City->id_state = 1;
            $City->slug = $this->url_slug($name);
            $City->save();
            return $City->id;
        }
        return $City->id;
    }

}
