<?php
/**
 * The following variables are available in this template:
 * - $this: the BootstrapCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form BSActiveForm */
<?php echo "?>\n"; ?>

<?php echo "<?php \$form=\$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'" . $this->class2id($this->modelClass) . "-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    )
)); ?>\n"; ?>

    <p class="help-block"><?php echo " <?php echo  Yii::t('app','Fields with');?>"?> <span class="required">*</span> <?php echo "<?php echo Yii::t('app','are required.')?>";?></p>

    <?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

<?php
foreach ($this->tableSchema->columns as $column) :
    if ($column->autoIncrement) {
        continue;
    }
    ?>
    <?php echo "<?php echo " . $this->generateActiveControlGroup($this->modelClass, $column) . "; ?>\n"; ?>
<?php endforeach; ?>
    
    <?php echo "<?php 
    if(Yii::app()->user->checkAccess('".$this->modelClass."CreateStatusChange') || Yii::app()->user->checkAccess('".$this->modelClass."UpdateStatusChange') ){
        echo \$form->dropDownListControlGroup(\$model, 'r_d_s', array(1=>'Activo',0=>'Inactivo'),array('data-style'=>'btn-info','class'=>'selectpicker show-tick'));
    }?>" ?>
    <?php echo "<?php 
    if(Yii::app()->user->checkAccess('".$this->modelClass."CreateButtonSave') || Yii::app()->user->checkAccess('".$this->modelClass."UpdateButtonSave')){
        echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); 
    }?>"?>
    
    <?php echo "<?php 
    if(Yii::app()->user->checkAccess('".$this->modelClass."CreateButtonNew') || Yii::app()->user->checkAccess('".$this->modelClass."UpdateButtonNew')){
        echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('".$this->modelClass."/create'),array('class'=>  'btn btn-primary')); 
    }?>"?>
  
<?php echo "<?php \$this->endWidget(); ?>\n"; ?>