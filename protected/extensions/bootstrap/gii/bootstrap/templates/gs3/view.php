<?php
/**
 * The following variables are available in this template:
 * - $this: the BootstrapCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
<?php echo "?>\n"; ?>

<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn},
);\n";
?>

?>

<?php echo "<?php echo BsHtml::pageHeader('View','$this->modelClass '.\$model->{$this->tableSchema->primaryKey}) ?>\n"; ?>
<?php echo "<?php 
 \$reg = array();
if(Yii::app()->user->checkAccess('".$this->modelClass."ViewAuthView')){
    \$reg = array(	
				";
foreach ($this->tableSchema->columns as $column) {
    echo "\t\t'" . $column->name . "',\n";
};
echo "
        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> \$model->RegistroEstado( \$model->r_d_s)
        ),
        
    );
}else{
    \$reg = array(	
		";
foreach ($this->tableSchema->columns as $column) {
    echo "\t\t'" . $column->name . "',\n";
}
echo "
        array(
            'name'=>'r_d_s',
            'type'=>'raw',
            'value'=> \$model->RegistroEstado( \$model->r_d_s)
        ),
    );
}
?>"?>
<?php echo "<?php"; ?> $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>$reg
<?php
//foreach ($this->tableSchema->columns as $column) {
//    echo "\t\t'" . $column->name . "',\n";
//}
?>
	,
)); ?>

<?php echo "<?php echo BsHtml::button(Yii::t('app', 'Back'), array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick' => 'history.go(-1)',
                )
        );
 echo BsHtml::link(Yii::t('app','New'), Yii::app()->createAbsoluteUrl('".$this->modelClass."/create'),array('class'=>  'btn btn-primary')); ?>";?>