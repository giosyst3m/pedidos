<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>

<div class="form">

<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'id'=>'".$this->class2id($this->modelClass)."-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>\n"; ?>

    <div class="notif g16">
        <?php echo "<?php echo Yii::t('app','Fields with');?>";?>*
        <?php echo "<?php echo Yii::t('app','are required.');?>\n";?>
        <span class="icon">D</span>
    </div>

	<?php echo "<?php echo \$form->errorSummary(\$model,null,null,array('class'=>'notif red auto  g16')); ?>\n"; ?>
    <br/>
    <br/>
    <hr>
<?php
foreach($this->tableSchema->columns as $column)
{
    //if($column->name != 'ip'){
	if($column->autoIncrement)
		continue;
?>
        <div class="box-body form">
                <?php echo  "<span class='label input g4'><?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?></span>\n"; ?>
                <?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
                <?php echo "<?php echo \$form->error(\$model,'{$column->name}',array('class'=>'notif red g16')); ?>\n"; ?>
        </div>

<?php
    //}
    }
?>
	<div class="row buttons">
		<?php echo "<?php echo CHtml::submitButton(\$model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Save'),array('class'=>'green btn-m flt-r g3')); ?>\n"; ?>
	</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>

</div><!-- form -->