<?php

/**
 * This is the model class for table "v_comment_order".
 *
 * The followings are the available columns in table 'v_comment_order':
 * @property string $message
 * @property string $file
 * @property string $r_c_d
 * @property string $complete_name
 * @property string $email
 * @property string $photo
 * @property string $sis_usu_tipo_nombre
 * @property integer $id_order
 * @property integer $r_d_s
 * @property string $title
 * @property string $priority
 * @property string $color
 * @property integer $id
 * @property integer $id_status
 * @property integer $order_r_c_u
 * @property integer $id_company
 * @property integer $number
 */
class VCommentOrder extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_comment_order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message, email, sis_usu_tipo_nombre, id_order, title, priority, color, id_status, id_company, number', 'required'),
			array('id_order, r_d_s, id, id_status, order_r_c_u, id_company, number', 'numerical', 'integerOnly'=>true),
			array('complete_name', 'length', 'max'=>101),
			array('email, sis_usu_tipo_nombre', 'length', 'max'=>50),
			array('photo, priority, color', 'length', 'max'=>255),
			array('title', 'length', 'max'=>200),
			array('file, r_c_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('message, file, r_c_d, complete_name, email, photo, sis_usu_tipo_nombre, id_order, r_d_s, title, priority, color, id, id_status, order_r_c_u, id_company, number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'message' => Yii::t('app','Message'),
			'file' => Yii::t('app','File'),
			'r_c_d' => Yii::t('app','R C D'),
			'complete_name' => Yii::t('app','Complete Name'),
			'email' => Yii::t('app','Email'),
			'photo' => Yii::t('app','Photo'),
			'sis_usu_tipo_nombre' => Yii::t('app','Sis Usu Tipo Nombre'),
			'id_order' => Yii::t('app','Id Order'),
			'r_d_s' => Yii::t('app','R D S'),
			'title' => Yii::t('app','Title'),
			'priority' => Yii::t('app','Priority'),
			'color' => Yii::t('app','Color'),
			'id' => Yii::t('app','ID'),
			'id_status' => Yii::t('app','Id Status'),
			'order_r_c_u' => Yii::t('app','Order R C U'),
			'id_company' => Yii::t('app','Id Company'),
			'number' => Yii::t('app','Number'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('message',$this->message,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('complete_name',$this->complete_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('sis_usu_tipo_nombre',$this->sis_usu_tipo_nombre,true);
		$criteria->compare('id_order',$this->id_order);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('priority',$this->priority,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('order_r_c_u',$this->order_r_c_u);
		$criteria->compare('id_company',$this->id_company);
		$criteria->compare('number',$this->number);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VCommentOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
