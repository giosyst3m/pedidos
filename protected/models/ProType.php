<?php

/**
 * This is the model class for table "pro_type".
 *
 * The followings are the available columns in table 'pro_type':
 * @property integer $id
 * @property string $name
 * @property string $descripction
 * @property integer $r_d_s
 *
 * The followings are the available model relations:
 * @property ProTypeField[] $proTypeFields
 * @property Product[] $products
 */
class ProType extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pro_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('r_d_s', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
			array('descripction', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, descripction, r_d_s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proTypeFields' => array(self::HAS_MANY, 'ProTypeField', 'id_pro_type'),
			'products' => array(self::HAS_MANY, 'Product', 'id_pro_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'name' => Yii::t('app','Name'),
			'descripction' => Yii::t('app','Descripction'),
			'r_d_s' => Yii::t('app','R D S'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('descripction',$this->descripction,true);
		$criteria->compare('r_d_s',$this->r_d_s);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
