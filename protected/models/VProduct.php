<?php

/**
 * This is the model class for table "v_product".
 *
 * The followings are the available columns in table 'v_product':
 * @property integer $id
 * @property integer $sku_father
 * @property string $sku
 * @property string $name
 * @property string $complete_name
 * @property string $type
 * @property integer $r_d_s
 * @property string $descripction
 * @property integer $id_brand
 * @property string $brand
 * @property string $photo
 * @property string $description
 * @property string $rate
 * @property integer $id_rate
 * @property string $price
 * @property string $discount_name
 * @property integer $discount
 * @property integer $id_discount
 * @property string $barcode
 * @property string $quantity
 * @property string $conditiona
 * @property integer $id_condition
 * @property integer $id_pro_group
 * @property string $pro_group
 * @property integer $id_stock
 * @property integer $id_category
 * @property string $category_name
 * @property integer $category_orden
 * @property integer $brand_orden
 * @property integer $pro_group_orden
 * @property string $pro_group_imagen
 * @property string $code_alternative_1
 * @property string $code_alternative_2
 * @property string $code_alternative_3
 */
class VProduct extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sku, name, complete_name, type, brand, rate, id_rate, price, discount_name, discount, id_discount, barcode, conditiona, id_condition, id_pro_group, pro_group, id_stock, id_category, category_name', 'required'),
			array('id, sku_father, r_d_s, id_brand, id_rate, discount, id_discount, id_condition, id_pro_group, id_stock, id_category, category_orden, brand_orden, pro_group_orden', 'numerical', 'integerOnly'=>true),
			array('sku, name, brand, photo, rate, discount_name, barcode, conditiona, pro_group, category_name, pro_group_imagen, code_alternative_1, code_alternative_2, code_alternative_3', 'length', 'max'=>255),
			array('type', 'length', 'max'=>50),
			array('price', 'length', 'max'=>11),
			array('quantity', 'length', 'max'=>32),
			array('descripction, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sku_father, sku, name, complete_name, type, r_d_s, descripction, id_brand, brand, photo, description, rate, id_rate, price, discount_name, discount, id_discount, barcode, quantity, conditiona, id_condition, id_pro_group, pro_group, id_stock, id_category, category_name, category_orden, brand_orden, pro_group_orden, pro_group_imagen, code_alternative_1, code_alternative_2, code_alternative_3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'sku_father' => Yii::t('app','Sku Father'),
			'sku' => Yii::t('app','Sku'),
			'name' => Yii::t('app','Name'),
			'complete_name' => Yii::t('app','Complete Name'),
			'type' => Yii::t('app','Type'),
			'r_d_s' => Yii::t('app','R D S'),
			'descripction' => Yii::t('app','Descripction'),
			'id_brand' => Yii::t('app','Id Brand'),
			'brand' => Yii::t('app','Brand'),
			'photo' => Yii::t('app','Photo'),
			'description' => Yii::t('app','Description'),
			'rate' => Yii::t('app','Rate'),
			'id_rate' => Yii::t('app','Id Rate'),
			'price' => Yii::t('app','Price'),
			'discount_name' => Yii::t('app','Discount Name'),
			'discount' => Yii::t('app','Discount'),
			'id_discount' => Yii::t('app','Id Discount'),
			'barcode' => Yii::t('app','Barcode'),
			'quantity' => Yii::t('app','Quantity'),
			'conditiona' => Yii::t('app','Conditiona'),
			'id_condition' => Yii::t('app','Id Condition'),
			'id_pro_group' => Yii::t('app','Id Pro Group'),
			'pro_group' => Yii::t('app','Pro Group'),
			'id_stock' => Yii::t('app','Id Stock'),
			'id_category' => Yii::t('app','Id Category'),
			'category_name' => Yii::t('app','Category Name'),
			'category_orden' => Yii::t('app','Category Orden'),
			'brand_orden' => Yii::t('app','Brand Orden'),
			'pro_group_orden' => Yii::t('app','Pro Group Orden'),
			'pro_group_imagen' => Yii::t('app','Pro Group Imagen'),
			'code_alternative_1' => Yii::t('app','Code Alternative 1'),
			'code_alternative_2' => Yii::t('app','Code Alternative 2'),
			'code_alternative_3' => Yii::t('app','Code Alternative 3'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sku_father',$this->sku_father);
		$criteria->compare('sku',$this->sku,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('complete_name',$this->complete_name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('descripction',$this->descripction,true);
		$criteria->compare('id_brand',$this->id_brand);
		$criteria->compare('brand',$this->brand,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('rate',$this->rate,true);
		$criteria->compare('id_rate',$this->id_rate);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('discount_name',$this->discount_name,true);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('id_discount',$this->id_discount);
		$criteria->compare('barcode',$this->barcode,true);
		$criteria->compare('quantity',$this->quantity,true);
		$criteria->compare('conditiona',$this->conditiona,true);
		$criteria->compare('id_condition',$this->id_condition);
		$criteria->compare('id_pro_group',$this->id_pro_group);
		$criteria->compare('pro_group',$this->pro_group,true);
		$criteria->compare('id_stock',$this->id_stock);
		$criteria->compare('id_category',$this->id_category);
		$criteria->compare('category_name',$this->category_name,true);
		$criteria->compare('category_orden',$this->category_orden);
		$criteria->compare('brand_orden',$this->brand_orden);
		$criteria->compare('pro_group_orden',$this->pro_group_orden);
		$criteria->compare('pro_group_imagen',$this->pro_group_imagen,true);
		$criteria->compare('code_alternative_1',$this->code_alternative_1,true);
		$criteria->compare('code_alternative_2',$this->code_alternative_2,true);
		$criteria->compare('code_alternative_3',$this->code_alternative_3,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
