<?php

/**
 * This is the model class for table "product_rate".
 *
 * The followings are the available columns in table 'product_rate':
 * @property integer $id
 * @property integer $id_product
 * @property integer $id_rate
 * @property string $price
 * @property integer $id_discount
 * @property integer $id_tax
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property string $r_c_o
 * @property string $r_c_f
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property string $r_u_o
 * @property string $r_u_f
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 *
 * The followings are the available model relations:
 * @property Product $idProduct
 * @property Rate $idRate
 * @property Discount $idDiscount
 * @property Tax $idTax
 */
class ProductRate extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product_rate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_product, id_rate, price, id_discount, id_tax', 'required'),
			array('id_product, id_rate, id_discount, id_tax, r_c_u, r_u_u, r_d_u, r_d_s', 'numerical', 'integerOnly'=>true),
			array('price', 'length', 'max'=>11),
			array('r_c_i, r_c_o, r_c_f, r_u_i, r_u_o, r_u_f, r_d_i', 'length', 'max'=>255),
			array('r_c_d, r_u_d, r_d_d', 'safe'),
                        array('price','numerical',
                            'integerOnly'=>false,
                            'min'=>1,
                            'max'=>9999999,
                            'tooSmall'=>'You must order at least 1 piece',
                            'tooBig'=>'You cannot order more than 9.999.999 pieces at once'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_product, id_rate, price, id_discount, id_tax, r_c_u, r_c_d, r_c_i, r_c_o, r_c_f, r_u_u, r_u_d, r_u_i, r_u_o, r_u_f, r_d_u, r_d_d, r_d_i, r_d_s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProduct' => array(self::BELONGS_TO, 'Product', 'id_product'),
			'idRate' => array(self::BELONGS_TO, 'Rate', 'id_rate'),
			'idDiscount' => array(self::BELONGS_TO, 'Discount', 'id_discount'),
			'idTax' => array(self::BELONGS_TO, 'Tax', 'id_tax'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'id_product' => Yii::t('app','Id Product'),
			'id_rate' => Yii::t('app','Id Rate'),
			'price' => Yii::t('app','Price'),
			'id_discount' => Yii::t('app','Id Discount'),
			'id_tax' => Yii::t('app','Id Tax'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_c_o' => Yii::t('app','R C O'),
			'r_c_f' => Yii::t('app','R C F'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_u_o' => Yii::t('app','R U O'),
			'r_u_f' => Yii::t('app','R U F'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_product',$this->id_product);
		$criteria->compare('id_rate',$this->id_rate);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('id_discount',$this->id_discount);
		$criteria->compare('id_tax',$this->id_tax);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_c_o',$this->r_c_o,true);
		$criteria->compare('r_c_f',$this->r_c_f,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_u_o',$this->r_u_o,true);
		$criteria->compare('r_u_f',$this->r_u_f,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductRate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
