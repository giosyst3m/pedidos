<?php

/**
 * This is the model class for table "v_sys_user_plan".
 *
 * The followings are the available columns in table 'v_sys_user_plan':
 * @property string $company
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $sub_domain
 * @property string $plan
 * @property string $price
 * @property string $frequency
 * @property string $country
 * @property string $licen_from
 * @property string $licence_to
 * @property string $licence_numbre
 * @property string $paid
 * @property integer $r_d_s
 * @property string $program
 * @property integer $id_program
 * @property integer $id_frequency
 * @property integer $id_country
 * @property integer $id_plan
 * @property integer $id_sys_user
 * @property string $icon
 * @property integer $id
 */
class VSysUserPlan extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_sys_user_plan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company, name, last_name, email, sub_domain, plan, price, frequency, country, licen_from, licence_to, licence_numbre, paid, program, id_program, id_frequency, id_country, id_plan, id_sys_user', 'required'),
			array('r_d_s, id_program, id_frequency, id_country, id_plan, id_sys_user, id', 'numerical', 'integerOnly'=>true),
			array('company, name, last_name, email, plan, frequency, country, licence_numbre, program, icon', 'length', 'max'=>255),
			array('sub_domain, price, paid', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('company, name, last_name, email, sub_domain, plan, price, frequency, country, licen_from, licence_to, licence_numbre, paid, r_d_s, program, id_program, id_frequency, id_country, id_plan, id_sys_user, icon, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'company' => Yii::t('app','Company'),
			'name' => Yii::t('app','Name'),
			'last_name' => Yii::t('app','Last Name'),
			'email' => Yii::t('app','Email'),
			'sub_domain' => Yii::t('app','Sub Domain'),
			'plan' => Yii::t('app','Plan'),
			'price' => Yii::t('app','Price'),
			'frequency' => Yii::t('app','Frequency'),
			'country' => Yii::t('app','Country'),
			'licen_from' => Yii::t('app','Licen From'),
			'licence_to' => Yii::t('app','Licence To'),
			'licence_numbre' => Yii::t('app','Licence Numbre'),
			'paid' => Yii::t('app','Paid'),
			'r_d_s' => Yii::t('app','R D S'),
			'program' => Yii::t('app','Program'),
			'id_program' => Yii::t('app','Id Program'),
			'id_frequency' => Yii::t('app','Id Frequency'),
			'id_country' => Yii::t('app','Id Country'),
			'id_plan' => Yii::t('app','Id Plan'),
			'id_sys_user' => Yii::t('app','Id Sys User'),
			'icon' => Yii::t('app','Icon'),
			'id' => Yii::t('app','ID'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('company',$this->company,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('sub_domain',$this->sub_domain,true);
		$criteria->compare('plan',$this->plan,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('frequency',$this->frequency,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('licen_from',$this->licen_from,true);
		$criteria->compare('licence_to',$this->licence_to,true);
		$criteria->compare('licence_numbre',$this->licence_numbre,true);
		$criteria->compare('paid',$this->paid,true);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('program',$this->program,true);
		$criteria->compare('id_program',$this->id_program);
		$criteria->compare('id_frequency',$this->id_frequency);
		$criteria->compare('id_country',$this->id_country);
		$criteria->compare('id_plan',$this->id_plan);
		$criteria->compare('id_sys_user',$this->id_sys_user);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VSysUserPlan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
