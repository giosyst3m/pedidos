<?php

/**
 * This is the model class for table "v_product_attribiute".
 *
 * The followings are the available columns in table 'v_product_attribiute':
 * @property integer $id
 * @property integer $pro_type_field_r_d_s
 * @property string $label
 * @property string $description
 * @property string $value
 * @property integer $require
 * @property integer $field_r_d_s
 * @property integer $id_fie_type
 * @property integer $id_field
 * @property integer $id_pro_type
 * @property string $fie_type_name
 * @property string $pro_type_name
 * @property string $descripction
 * @property integer $pro_type_r_d_s
 * @property integer $fie_type_r_d_s
 */
class VProductAttribiute extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_product_attribiute';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('label, require, id_fie_type, id_field, id_pro_type, fie_type_name, pro_type_name', 'required'),
			array('id, pro_type_field_r_d_s, require, field_r_d_s, id_fie_type, id_field, id_pro_type, pro_type_r_d_s, fie_type_r_d_s', 'numerical', 'integerOnly'=>true),
			array('label, description', 'length', 'max'=>255),
			array('fie_type_name, pro_type_name', 'length', 'max'=>50),
			array('value, descripction', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pro_type_field_r_d_s, label, description, value, require, field_r_d_s, id_fie_type, id_field, id_pro_type, fie_type_name, pro_type_name, descripction, pro_type_r_d_s, fie_type_r_d_s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'pro_type_field_r_d_s' => Yii::t('app','Pro Type Field R D S'),
			'label' => Yii::t('app','Label'),
			'description' => Yii::t('app','Description'),
			'value' => Yii::t('app','Value'),
			'require' => Yii::t('app','Require'),
			'field_r_d_s' => Yii::t('app','Field R D S'),
			'id_fie_type' => Yii::t('app','Id Fie Type'),
			'id_field' => Yii::t('app','Id Field'),
			'id_pro_type' => Yii::t('app','Id Pro Type'),
			'fie_type_name' => Yii::t('app','Fie Type Name'),
			'pro_type_name' => Yii::t('app','Pro Type Name'),
			'descripction' => Yii::t('app','Descripction'),
			'pro_type_r_d_s' => Yii::t('app','Pro Type R D S'),
			'fie_type_r_d_s' => Yii::t('app','Fie Type R D S'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pro_type_field_r_d_s',$this->pro_type_field_r_d_s);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('require',$this->require);
		$criteria->compare('field_r_d_s',$this->field_r_d_s);
		$criteria->compare('id_fie_type',$this->id_fie_type);
		$criteria->compare('id_field',$this->id_field);
		$criteria->compare('id_pro_type',$this->id_pro_type);
		$criteria->compare('fie_type_name',$this->fie_type_name,true);
		$criteria->compare('pro_type_name',$this->pro_type_name,true);
		$criteria->compare('descripction',$this->descripction,true);
		$criteria->compare('pro_type_r_d_s',$this->pro_type_r_d_s);
		$criteria->compare('fie_type_r_d_s',$this->fie_type_r_d_s);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VProductAttribiute the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
