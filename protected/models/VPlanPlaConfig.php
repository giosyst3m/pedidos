<?php

/**
 * This is the model class for table "v_plan_pla_config".
 *
 * The followings are the available columns in table 'v_plan_pla_config':
 * @property string $name
 * @property string $plan
 * @property string $default
 * @property integer $id_plan
 * @property integer $id_pla_config
 * @property integer $id_program
 * @property string $programv
 * @property string $description
 * @property integer $show
 * @property integer $r_d_s
 */
class VPlanPlaConfig extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_plan_pla_config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, plan, default, id_plan, id_pla_config, id_program, programv, description', 'required'),
			array('id_plan, id_pla_config, id_program, show, r_d_s', 'numerical', 'integerOnly'=>true),
			array('name, plan, default, programv, description', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('name, plan, default, id_plan, id_pla_config, id_program, programv, description, show, r_d_s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name' => Yii::t('app','Name'),
			'plan' => Yii::t('app','Plan'),
			'default' => Yii::t('app','Default'),
			'id_plan' => Yii::t('app','Id Plan'),
			'id_pla_config' => Yii::t('app','Id Pla Config'),
			'id_program' => Yii::t('app','Id Program'),
			'programv' => Yii::t('app','Programv'),
			'description' => Yii::t('app','Description'),
			'show' => Yii::t('app','Show'),
			'r_d_s' => Yii::t('app','R D S'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name,true);
		$criteria->compare('plan',$this->plan,true);
		$criteria->compare('default',$this->default,true);
		$criteria->compare('id_plan',$this->id_plan);
		$criteria->compare('id_pla_config',$this->id_pla_config);
		$criteria->compare('id_program',$this->id_program);
		$criteria->compare('programv',$this->programv,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('show',$this->show);
		$criteria->compare('r_d_s',$this->r_d_s);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VPlanPlaConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
