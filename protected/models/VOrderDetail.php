<?php

/**
 * This is the model class for table "v_order_detail".
 *
 * The followings are the available columns in table 'v_order_detail':
 * @property integer $id
 * @property integer $number
 * @property integer $id_client
 * @property string $code
 * @property integer $correlative
 * @property string $client_name
 * @property string $phone
 * @property string $mobil
 * @property string $address
 * @property string $conctact
 * @property string $email
 * @property integer $id_city
 * @property string $city_name
 * @property integer $id_product
 * @property integer $id_status
 * @property string $status
 * @property string $status_description
 * @property string $color
 * @property string $boton_color
 * @property string $boton_lcolor
 * @property string $icon
 * @property string $sku
 * @property string $name
 * @property string $complete_name
 * @property string $type
 * @property integer $r_d_s
 * @property string $descripction
 * @property integer $id_brand
 * @property string $brand
 * @property string $photo
 * @property string $description
 * @property string $rate
 * @property integer $id_rate
 * @property string $price
 * @property string $discount_name
 * @property integer $discount
 * @property integer $id_discount
 * @property string $barcode
 * @property string $quantity
 * @property string $conditiona
 * @property integer $id_condition
 * @property integer $id_pro_group
 * @property string $pro_group
 * @property integer $id_stock
 * @property integer $ord_detail_r_d_s
 * @property integer $request
 * @property string $user_complete_name
 * @property string $r_c_d
 * @property integer $id_category
 * @property string $category_name
 * @property integer $category_orden
 * @property integer $brand_orden
 * @property integer $pro_group_orden
 * @property integer $inv_in
 * @property integer $id_ord_detail
 * @property string $r_d_d
 * @property string $r_d_r
 * @property string $ord_detail_price
 * @property integer $ord_detail_id
 * @property integer $ord_detail_quantity
 * @property string $code_alternative_1
 * @property string $code_alternative_2
 * @property string $code_alternative_3
 * @property integer $r_c_u
 */
class VOrderDetail extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_order_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('number, id_client, code, client_name, address, id_city, city_name, id_product, id_status, status, sku, name, complete_name, type, brand, rate, id_rate, price, discount_name, discount, id_discount, barcode, conditiona, id_condition, id_pro_group, pro_group, id_stock, id_category, category_name, ord_detail_price, ord_detail_quantity', 'required'),
			array('id, number, id_client, correlative, id_city, id_product, id_status, r_d_s, id_brand, id_rate, discount, id_discount, id_condition, id_pro_group, id_stock, ord_detail_r_d_s, request, id_category, category_orden, brand_orden, pro_group_orden, inv_in, id_ord_detail, ord_detail_id, ord_detail_quantity, r_c_u', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>25),
			array('client_name, mobil, conctact, email, city_name, status, color, boton_color, boton_lcolor, icon, sku, name, brand, photo, rate, discount_name, barcode, conditiona, pro_group, category_name, r_d_r, code_alternative_1, code_alternative_2, code_alternative_3', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>15),
			array('type', 'length', 'max'=>50),
			array('price, ord_detail_price', 'length', 'max'=>11),
			array('quantity', 'length', 'max'=>32),
			array('user_complete_name', 'length', 'max'=>101),
			array('status_description, descripction, description, r_c_d, r_d_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, number, id_client, code, correlative, client_name, phone, mobil, address, conctact, email, id_city, city_name, id_product, id_status, status, status_description, color, boton_color, boton_lcolor, icon, sku, name, complete_name, type, r_d_s, descripction, id_brand, brand, photo, description, rate, id_rate, price, discount_name, discount, id_discount, barcode, quantity, conditiona, id_condition, id_pro_group, pro_group, id_stock, ord_detail_r_d_s, request, user_complete_name, r_c_d, id_category, category_name, category_orden, brand_orden, pro_group_orden, inv_in, id_ord_detail, r_d_d, r_d_r, ord_detail_price, ord_detail_id, ord_detail_quantity, code_alternative_1, code_alternative_2, code_alternative_3, r_c_u', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'number' => Yii::t('app','Number'),
			'id_client' => Yii::t('app','Id Client'),
			'code' => Yii::t('app','Code'),
			'correlative' => Yii::t('app','Correlative'),
			'client_name' => Yii::t('app','Client Name'),
			'phone' => Yii::t('app','Phone'),
			'mobil' => Yii::t('app','Mobil'),
			'address' => Yii::t('app','Address'),
			'conctact' => Yii::t('app','Conctact'),
			'email' => Yii::t('app','Email'),
			'id_city' => Yii::t('app','Id City'),
			'city_name' => Yii::t('app','City Name'),
			'id_product' => Yii::t('app','Id Product'),
			'id_status' => Yii::t('app','Id Status'),
			'status' => Yii::t('app','Status'),
			'status_description' => Yii::t('app','Status Description'),
			'color' => Yii::t('app','Color'),
			'boton_color' => Yii::t('app','Boton Color'),
			'boton_lcolor' => Yii::t('app','Boton Lcolor'),
			'icon' => Yii::t('app','Icon'),
			'sku' => Yii::t('app','Sku'),
			'name' => Yii::t('app','Name'),
			'complete_name' => Yii::t('app','Complete Name'),
			'type' => Yii::t('app','Type'),
			'r_d_s' => Yii::t('app','R D S'),
			'descripction' => Yii::t('app','Descripction'),
			'id_brand' => Yii::t('app','Id Brand'),
			'brand' => Yii::t('app','Brand'),
			'photo' => Yii::t('app','Photo'),
			'description' => Yii::t('app','Description'),
			'rate' => Yii::t('app','Rate'),
			'id_rate' => Yii::t('app','Id Rate'),
			'price' => Yii::t('app','Price'),
			'discount_name' => Yii::t('app','Discount Name'),
			'discount' => Yii::t('app','Discount'),
			'id_discount' => Yii::t('app','Id Discount'),
			'barcode' => Yii::t('app','Barcode'),
			'quantity' => Yii::t('app','Quantity'),
			'conditiona' => Yii::t('app','Conditiona'),
			'id_condition' => Yii::t('app','Id Condition'),
			'id_pro_group' => Yii::t('app','Id Pro Group'),
			'pro_group' => Yii::t('app','Pro Group'),
			'id_stock' => Yii::t('app','Id Stock'),
			'ord_detail_r_d_s' => Yii::t('app','Ord Detail R D S'),
			'request' => Yii::t('app','Request'),
			'user_complete_name' => Yii::t('app','User Complete Name'),
			'r_c_d' => Yii::t('app','R C D'),
			'id_category' => Yii::t('app','Id Category'),
			'category_name' => Yii::t('app','Category Name'),
			'category_orden' => Yii::t('app','Category Orden'),
			'brand_orden' => Yii::t('app','Brand Orden'),
			'pro_group_orden' => Yii::t('app','Pro Group Orden'),
			'inv_in' => Yii::t('app','Inv In'),
			'id_ord_detail' => Yii::t('app','Id Ord Detail'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_r' => Yii::t('app','R D R'),
			'ord_detail_price' => Yii::t('app','Ord Detail Price'),
			'ord_detail_id' => Yii::t('app','Ord Detail'),
			'ord_detail_quantity' => Yii::t('app','Ord Detail Quantity'),
			'code_alternative_1' => Yii::t('app','Code Alternative 1'),
			'code_alternative_2' => Yii::t('app','Code Alternative 2'),
			'code_alternative_3' => Yii::t('app','Code Alternative 3'),
			'r_c_u' => Yii::t('app','R C U'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($page = 10)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('number',$this->number);
		$criteria->compare('id_client',$this->id_client);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('correlative',$this->correlative);
		$criteria->compare('client_name',$this->client_name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobil',$this->mobil,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('conctact',$this->conctact,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('id_city',$this->id_city);
		$criteria->compare('city_name',$this->city_name,true);
		$criteria->compare('id_product',$this->id_product);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('status_description',$this->status_description,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('boton_color',$this->boton_color,true);
		$criteria->compare('boton_lcolor',$this->boton_lcolor,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('sku',$this->sku,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('complete_name',$this->complete_name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('descripction',$this->descripction,true);
		$criteria->compare('id_brand',$this->id_brand);
		$criteria->compare('brand',$this->brand,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('rate',$this->rate,true);
		$criteria->compare('id_rate',$this->id_rate);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('discount_name',$this->discount_name,true);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('id_discount',$this->id_discount);
		$criteria->compare('barcode',$this->barcode,true);
		$criteria->compare('quantity',$this->quantity,true);
		$criteria->compare('conditiona',$this->conditiona,true);
		$criteria->compare('id_condition',$this->id_condition);
		$criteria->compare('id_pro_group',$this->id_pro_group);
		$criteria->compare('pro_group',$this->pro_group,true);
		$criteria->compare('id_stock',$this->id_stock);
		$criteria->compare('ord_detail_r_d_s',$this->ord_detail_r_d_s);
		$criteria->compare('request',$this->request);
		$criteria->compare('user_complete_name',$this->user_complete_name,true);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('id_category',$this->id_category);
		$criteria->compare('category_name',$this->category_name,true);
		$criteria->compare('category_orden',$this->category_orden);
		$criteria->compare('brand_orden',$this->brand_orden);
		$criteria->compare('pro_group_orden',$this->pro_group_orden);
		$criteria->compare('inv_in',$this->inv_in);
		$criteria->compare('id_ord_detail',$this->id_ord_detail);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_r',$this->r_d_r,true);
		$criteria->compare('ord_detail_price',$this->ord_detail_price,true);
		$criteria->compare('ord_detail_id',$this->ord_detail_id);
		$criteria->compare('ord_detail_quantity',$this->ord_detail_quantity);
		$criteria->compare('code_alternative_1',$this->code_alternative_1,true);
		$criteria->compare('code_alternative_2',$this->code_alternative_2,true);
		$criteria->compare('code_alternative_3',$this->code_alternative_3,true);
		$criteria->compare('r_c_u',$this->r_c_u);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'Pagination' => array (
                                'PageSize' => $page,
                        ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VOrderDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
