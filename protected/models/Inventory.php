<?php

/**
 * This is the model class for table "inventory".
 *
 * The followings are the available columns in table 'inventory':
 * @property integer $id
 * @property integer $id_product
 * @property integer $quantity
 * @property integer $id_stock
 * @property integer $type
 * @property integer $id_inv_origin
 * @property string $comment
 * @property integer $id_order
 * @property integer $id_ord_detail
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 *
 * The followings are the available model relations:
 * @property OrdDetail $idOrdDetail
 * @property Product $idProduct
 * @property Stock $idStock
 * @property InvOrigin $idInvOrigin
 * @property Order $idOrder
 */
class Inventory extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inventory';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_product, quantity, id_stock, type, id_inv_origin, comment', 'required'),
			array('id_product, quantity, id_stock, type, id_inv_origin, id_order, id_ord_detail, r_c_u, r_u_u, r_d_u, r_d_s', 'numerical', 'integerOnly'=>true),
			array('r_c_i, r_u_i, r_d_i', 'length', 'max'=>255),
			array('r_c_d, r_u_d, r_d_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_product, quantity, id_stock, type, id_inv_origin, comment, id_order, id_ord_detail, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_d_u, r_d_d, r_d_i, r_d_s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idOrdDetail' => array(self::BELONGS_TO, 'OrdDetail', 'id_ord_detail'),
			'idProduct' => array(self::BELONGS_TO, 'Product', 'id_product'),
			'idStock' => array(self::BELONGS_TO, 'Stock', 'id_stock'),
			'idInvOrigin' => array(self::BELONGS_TO, 'InvOrigin', 'id_inv_origin'),
			'idOrder' => array(self::BELONGS_TO, 'Order', 'id_order'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'id_product' => Yii::t('app','Id Product'),
			'quantity' => Yii::t('app','Quantity'),
			'id_stock' => Yii::t('app','Id Stock'),
			'type' => Yii::t('app','Type'),
			'id_inv_origin' => Yii::t('app','Id Inv Origin'),
			'comment' => Yii::t('app','Comment'),
			'id_order' => Yii::t('app','Id Order'),
			'id_ord_detail' => Yii::t('app','Id Ord Detail'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_product',$this->id_product);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('id_stock',$this->id_stock);
		$criteria->compare('type',$this->type);
		$criteria->compare('id_inv_origin',$this->id_inv_origin);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('id_order',$this->id_order);
		$criteria->compare('id_ord_detail',$this->id_ord_detail);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Inventory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
