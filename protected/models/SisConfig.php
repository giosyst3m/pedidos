<?php

/**
 * This is the model class for table "sis_config".
 *
 * The followings are the available columns in table 'sis_config':
 * @property integer $id
 * @property string $name
 * @property string $default
 * @property integer $id_sis_con_group
 * @property string $description
 * @property integer $id_fie_type
 * @property string $value
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 *
 * The followings are the available model relations:
 * @property FieType $idFieType
 * @property SisConGroup $idSisConGroup
 * @property SisConfigCompany[] $sisConfigCompanies
 */
class SisConfig extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sis_config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, default, id_sis_con_group, description, id_fie_type', 'required'),
			array('id_sis_con_group, id_fie_type, r_c_u, r_u_u, r_d_u, r_d_s', 'numerical', 'integerOnly'=>true),
			array('name, default, value, r_c_i, r_u_i, r_d_i', 'length', 'max'=>255),
			array('r_c_d, r_u_d, r_d_d', 'safe'),
                        array('name','unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, default, id_sis_con_group, description, id_fie_type, value, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_d_u, r_d_d, r_d_i, r_d_s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idFieType' => array(self::BELONGS_TO, 'FieType', 'id_fie_type'),
			'idSisConGroup' => array(self::BELONGS_TO, 'SisConGroup', 'id_sis_con_group'),
			'sisConfigCompanies' => array(self::HAS_MANY, 'SisConfigCompany', 'id_sis_config'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'name' => Yii::t('app','Name'),
			'default' => Yii::t('app','Default'),
			'id_sis_con_group' => Yii::t('app','Id Sis Con Group'),
			'description' => Yii::t('app','Description'),
			'id_fie_type' => Yii::t('app','Id Fie Type'),
			'value' => Yii::t('app','Value'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('default',$this->default,true);
		$criteria->compare('id_sis_con_group',$this->id_sis_con_group);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('id_fie_type',$this->id_fie_type);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SisConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
