<?php

/**
 * This is the model class for table "v_zone_client".
 *
 * The followings are the available columns in table 'v_zone_client':
 * @property string $name
 * @property string $phone
 * @property string $mobil
 * @property string $address
 * @property string $conctact
 * @property string $email
 * @property string $logo
 * @property integer $id_city
 * @property string $full_name
 * @property integer $id
 * @property string $code
 * @property string $city
 * @property string $stade
 * @property string $country
 * @property integer $correlative
 * @property integer $client_r_d_s
 * @property string $zone
 * @property integer $id_zone
 * @property integer $id_client
 * @property integer $zone_client_r_d_s
 * @property integer $zone_r_d_s
 */
class VZoneClient extends GS3CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'v_zone_client';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, address, id_city, code, city, stade, country, zone, id_zone, id_client', 'required'),
            array('id_city, id, correlative, client_r_d_s, id_zone, id_client, zone_client_r_d_s, zone_r_d_s', 'numerical', 'integerOnly' => true),
            array('name, mobil, conctact, email, logo, city, stade, country, zone', 'length', 'max' => 255),
            array('phone', 'length', 'max' => 15),
            array('code', 'length', 'max' => 25),
            array('full_name', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('name, phone, mobil, address, conctact, email, logo, id_city, full_name, id, code, city, stade, country, correlative, client_r_d_s, zone, id_zone, id_client, zone_client_r_d_s, zone_r_d_s', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'mobil' => Yii::t('app', 'Mobil'),
            'address' => Yii::t('app', 'Address'),
            'conctact' => Yii::t('app', 'Conctact'),
            'email' => Yii::t('app', 'Email'),
            'logo' => Yii::t('app', 'Logo'),
            'id_city' => Yii::t('app', 'Id City'),
            'full_name' => Yii::t('app', 'Full Name'),
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'city' => Yii::t('app', 'City'),
            'stade' => Yii::t('app', 'Stade'),
            'country' => Yii::t('app', 'Country'),
            'correlative' => Yii::t('app', 'Correlative'),
            'client_r_d_s' => Yii::t('app', 'Client R D S'),
            'zone' => Yii::t('app', 'Zone'),
            'id_zone' => Yii::t('app', 'Id Zone'),
            'id_client' => Yii::t('app', 'Id Client'),
            'zone_client_r_d_s' => Yii::t('app', 'Zone Client R D S'),
            'zone_r_d_s' => Yii::t('app', 'Zone R D S'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('name', $this->name, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('mobil', $this->mobil, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('conctact', $this->conctact, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('logo', $this->logo, true);
        $criteria->compare('id_city', $this->id_city);
        $criteria->compare('full_name', $this->full_name, true);
        $criteria->compare('id', $this->id);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('stade', $this->stade, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('correlative', $this->correlative);
        $criteria->compare('client_r_d_s', $this->client_r_d_s);
        $criteria->compare('zone', $this->zone, true);
        $criteria->compare('id_zone', $this->id_zone);
        $criteria->compare('id_client', $this->id_client);
        $criteria->compare('zone_client_r_d_s', $this->zone_client_r_d_s);
        $criteria->compare('zone_r_d_s', $this->zone_r_d_s);
        if (!Yii::app()->user->checkAccess('CatalogSeeAllClient')) {
            $ZoneSisUsuario = BsHtml::listData(ZoneSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes([
                'id_sis_usuario'=>  Yii::app()->user->id,
            ]), 'id_zone', 'id_zone');
            $criteria->addInCondition('id_zone', $ZoneSisUsuario);
        }
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return VZoneClient the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
