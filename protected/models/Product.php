<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $id
 * @property integer $sku_father
 * @property string $sku
 * @property string $name
 * @property string $description
 * @property string $stock
 * @property integer $id_pro_type
 * @property integer $id_brand
 * @property string $photo
 * @property string $barcode
 * @property integer $id_condition
 * @property integer $id_pro_group
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_description
 * @property string $frendly_url
 * @property string $code_alternative_1
 * @property string $code_alternative_2
 * @property string $code_alternative_3
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property string $r_c_o
 * @property string $r_c_f
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property string $r_u_o
 * @property string $r_u_f
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 * @property string $r_d_m
 * @property string $r_d_o
 * @property string $r_d_f
 *
 * The followings are the available model relations:
 * @property Caracteristic[] $caracteristics
 * @property Inventory[] $inventories
 * @property OrdDetail[] $ordDetails
 * @property ProImagen[] $proImagens
 * @property ProType $idProType
 * @property Product $skuFather
 * @property Product[] $products
 * @property Brand $idBrand
 * @property Condition $idCondition
 * @property ProGroup $idProGroup
 * @property ProductCategory[] $productCategories
 * @property ProductManufacture[] $productManufactures
 * @property ProductRate[] $productRates
 * @property ProductSplit[] $productSplits
 * @property ProductSupplier[] $productSuppliers
 * @property ProductTag[] $productTags
 */
class Product extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sku, name, id_pro_type, barcode, id_condition, id_pro_group, slug', 'required'),
			array('sku_father, id_pro_type, id_brand, id_condition, id_pro_group, r_c_u, r_u_u, r_d_u, r_d_s', 'numerical', 'integerOnly'=>true),
			array('sku, name, photo, barcode, slug, frendly_url, code_alternative_1, code_alternative_2, code_alternative_3, r_c_i, r_c_o, r_c_f, r_u_i, r_u_o, r_u_f, r_d_i, r_d_m, r_d_o, r_d_f', 'length', 'max'=>255),
			array('stock', 'length', 'max'=>10),
			array('meta_title', 'length', 'max'=>70),
			array('meta_description', 'length', 'max'=>160),
			array('description, r_c_d, r_u_d, r_d_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sku_father, sku, name, description, stock, id_pro_type, id_brand, photo, barcode, id_condition, id_pro_group, slug, meta_title, meta_description, frendly_url, code_alternative_1, code_alternative_2, code_alternative_3, r_c_u, r_c_d, r_c_i, r_c_o, r_c_f, r_u_u, r_u_d, r_u_i, r_u_o, r_u_f, r_d_u, r_d_d, r_d_i, r_d_s, r_d_m, r_d_o, r_d_f', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'caracteristics' => array(self::HAS_MANY, 'Caracteristic', 'id_product'),
			'inventories' => array(self::HAS_MANY, 'Inventory', 'id_product'),
			'ordDetails' => array(self::HAS_MANY, 'OrdDetail', 'id_product'),
			'proImagens' => array(self::HAS_MANY, 'ProImagen', 'id_product'),
			'idProType' => array(self::BELONGS_TO, 'ProType', 'id_pro_type'),
			'skuFather' => array(self::BELONGS_TO, 'Product', 'sku_father'),
			'products' => array(self::HAS_MANY, 'Product', 'sku_father'),
			'idBrand' => array(self::BELONGS_TO, 'Brand', 'id_brand'),
			'idCondition' => array(self::BELONGS_TO, 'Condition', 'id_condition'),
			'idProGroup' => array(self::BELONGS_TO, 'ProGroup', 'id_pro_group'),
			'productCategories' => array(self::HAS_MANY, 'ProductCategory', 'id_product'),
			'productManufactures' => array(self::HAS_MANY, 'ProductManufacture', 'id_product'),
			'productRates' => array(self::HAS_MANY, 'ProductRate', 'id_product'),
			'productSplits' => array(self::HAS_MANY, 'ProductSplit', 'id_product'),
			'productSuppliers' => array(self::HAS_MANY, 'ProductSupplier', 'id_product'),
			'productTags' => array(self::HAS_MANY, 'ProductTag', 'id_product'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'sku_father' => Yii::t('app','Sku Father'),
			'sku' => Yii::t('app','Sku'),
			'name' => Yii::t('app','Name'),
			'description' => Yii::t('app','Description'),
			'stock' => Yii::t('app','Stock'),
			'id_pro_type' => Yii::t('app','Id Pro Type'),
			'id_brand' => Yii::t('app','Id Brand'),
			'photo' => Yii::t('app','Photo'),
			'barcode' => Yii::t('app','Barcode'),
			'id_condition' => Yii::t('app','Id Condition'),
			'id_pro_group' => Yii::t('app','Id Pro Group'),
			'slug' => Yii::t('app','Slug'),
			'meta_title' => Yii::t('app','Meta Title'),
			'meta_description' => Yii::t('app','Meta Description'),
			'frendly_url' => Yii::t('app','Frendly Url'),
			'code_alternative_1' => Yii::t('app','Code Alternative 1'),
			'code_alternative_2' => Yii::t('app','Code Alternative 2'),
			'code_alternative_3' => Yii::t('app','Code Alternative 3'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_c_o' => Yii::t('app','R C O'),
			'r_c_f' => Yii::t('app','R C F'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_u_o' => Yii::t('app','R U O'),
			'r_u_f' => Yii::t('app','R U F'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
			'r_d_m' => Yii::t('app','R D M'),
			'r_d_o' => Yii::t('app','R D O'),
			'r_d_f' => Yii::t('app','R D F'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sku_father',$this->sku_father);
		$criteria->compare('sku',$this->sku,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('stock',$this->stock,true);
		$criteria->compare('id_pro_type',$this->id_pro_type);
		$criteria->compare('id_brand',$this->id_brand);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('barcode',$this->barcode,true);
		$criteria->compare('id_condition',$this->id_condition);
		$criteria->compare('id_pro_group',$this->id_pro_group);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('frendly_url',$this->frendly_url,true);
		$criteria->compare('code_alternative_1',$this->code_alternative_1,true);
		$criteria->compare('code_alternative_2',$this->code_alternative_2,true);
		$criteria->compare('code_alternative_3',$this->code_alternative_3,true);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_c_o',$this->r_c_o,true);
		$criteria->compare('r_c_f',$this->r_c_f,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_u_o',$this->r_u_o,true);
		$criteria->compare('r_u_f',$this->r_u_f,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('r_d_m',$this->r_d_m,true);
		$criteria->compare('r_d_o',$this->r_d_o,true);
		$criteria->compare('r_d_f',$this->r_d_f,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
