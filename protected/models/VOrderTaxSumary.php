<?php

/**
 * This is the model class for table "v_order_tax_sumary".
 *
 * The followings are the available columns in table 'v_order_tax_sumary':
 * @property integer $id
 * @property string $sub_total_mas_delivery
 * @property integer $tax
 * @property string $total
 */
class VOrderTaxSumary extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_order_tax_sumary';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tax', 'required'),
			array('id, tax', 'numerical', 'integerOnly'=>true),
			array('sub_total_mas_delivery', 'length', 'max'=>59),
			array('total', 'length', 'max'=>65),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sub_total_mas_delivery, tax, total', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'sub_total_mas_delivery' => Yii::t('app','Sub Total Mas Delivery'),
			'tax' => Yii::t('app','Tax'),
			'total' => Yii::t('app','Total'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sub_total_mas_delivery',$this->sub_total_mas_delivery,true);
		$criteria->compare('tax',$this->tax);
		$criteria->compare('total',$this->total,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VOrderTaxSumary the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
