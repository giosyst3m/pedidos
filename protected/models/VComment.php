<?php

/**
 * This is the model class for table "v_comment".
 *
 * The followings are the available columns in table 'v_comment':
 * @property integer $id
 * @property string $messages
 * @property string $file
 * @property integer $id_priority
 * @property integer $id_activity
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property integer $r_u_i
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 * @property string $priority
 * @property integer $order
 * @property string $color
 * @property string $boton_color
 * @property string $boton_lcolor
 * @property string $icon
 * @property integer $activity_r_c_u
 * @property string $activity
 */
class VComment extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('messages, id_activity, priority, activity', 'required'),
			array('id, id_priority, id_activity, r_c_u, r_u_u, r_u_i, r_d_u, r_d_s, order, activity_r_c_u', 'numerical', 'integerOnly'=>true),
			array('file, r_c_i, r_d_i, priority, color, boton_color, boton_lcolor, icon, activity', 'length', 'max'=>255),
			array('r_c_d, r_u_d, r_d_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, messages, file, id_priority, id_activity, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_d_u, r_d_d, r_d_i, r_d_s, priority, order, color, boton_color, boton_lcolor, icon, activity_r_c_u, activity', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'messages' => Yii::t('app','Messages'),
			'file' => Yii::t('app','File'),
			'id_priority' => Yii::t('app','Id Priority'),
			'id_activity' => Yii::t('app','Id Activity'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
			'priority' => Yii::t('app','Priority'),
			'order' => Yii::t('app','Order'),
			'color' => Yii::t('app','Color'),
			'boton_color' => Yii::t('app','Boton Color'),
			'boton_lcolor' => Yii::t('app','Boton Lcolor'),
			'icon' => Yii::t('app','Icon'),
			'activity_r_c_u' => Yii::t('app','Activity R C U'),
			'activity' => Yii::t('app','Activity'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('messages',$this->messages,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('id_priority',$this->id_priority);
		$criteria->compare('id_activity',$this->id_activity);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('priority',$this->priority,true);
		$criteria->compare('order',$this->order);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('boton_color',$this->boton_color,true);
		$criteria->compare('boton_lcolor',$this->boton_lcolor,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('activity_r_c_u',$this->activity_r_c_u);
		$criteria->compare('activity',$this->activity,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db3;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VComment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
