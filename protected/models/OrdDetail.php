<?php

/**
 * This is the model class for table "ord_detail".
 *
 * The followings are the available columns in table 'ord_detail':
 * @property integer $id
 * @property integer $id_order
 * @property integer $id_product
 * @property integer $quantity
 * @property integer $request
 * @property string $price
 * @property integer $id_discount
 * @property integer $id_inv_origin
 * @property integer $inv_in
 * @property string $note
 * @property integer $type
 * @property integer $id_money
 * @property string $trm
 * @property integer $id_category
 * @property string $date
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 * @property string $r_d_r
 *
 * The followings are the available model relations:
 * @property Inventory[] $inventories
 * @property Order $idOrder
 * @property Product $idProduct
 * @property Discount $idDiscount
 * @property InvOrigin $idInvOrigin
 * @property Category $idCategory
 */
class OrdDetail extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ord_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_order, id_product, quantity, price, id_discount, id_inv_origin', 'required'),
			array('id_order, id_product, quantity, request, id_discount, id_inv_origin, inv_in, type, id_money, id_category, r_c_u, r_u_u, r_d_u, r_d_s', 'numerical', 'integerOnly'=>true),
                        array('id_order, id_product, quantity, price, id_discount, id_inv_origin, id_category, date', 'required', 'on' => 'finance'),
			array('price', 'length', 'max'=>11),
			array('trm', 'length', 'max'=>10),
			array('r_c_i, r_u_i, r_d_i, r_d_r', 'length', 'max'=>255),
			array('note, date, r_c_d, r_u_d, r_d_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_order, id_product, quantity, request, price, id_discount, id_inv_origin, inv_in, note, type, id_money, trm, id_category, date, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_d_u, r_d_d, r_d_i, r_d_s, r_d_r', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'inventories' => array(self::HAS_MANY, 'Inventory', 'id_ord_detail'),
			'idOrder' => array(self::BELONGS_TO, 'Order', 'id_order'),
			'idProduct' => array(self::BELONGS_TO, 'Product', 'id_product'),
			'idDiscount' => array(self::BELONGS_TO, 'Discount', 'id_discount'),
			'idInvOrigin' => array(self::BELONGS_TO, 'InvOrigin', 'id_inv_origin'),
			'idCategory' => array(self::BELONGS_TO, 'Category', 'id_category'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'id_order' => Yii::t('app','Id Order'),
			'id_product' => Yii::t('app','Id Product'),
			'quantity' => Yii::t('app','Quantity'),
			'request' => Yii::t('app','Request'),
			'price' => Yii::t('app','Price'),
			'id_discount' => Yii::t('app','Id Discount'),
			'id_inv_origin' => Yii::t('app','Id Inv Origin'),
			'inv_in' => Yii::t('app','Inv In'),
			'note' => Yii::t('app','Note'),
			'type' => Yii::t('app','Type'),
			'id_money' => Yii::t('app','Id Money'),
			'trm' => Yii::t('app','Trm'),
			'id_category' => Yii::t('app','Id Category'),
			'date' => Yii::t('app','Date'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
			'r_d_r' => Yii::t('app','R D R'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_order',$this->id_order);
		$criteria->compare('id_product',$this->id_product);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('request',$this->request);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('id_discount',$this->id_discount);
		$criteria->compare('id_inv_origin',$this->id_inv_origin);
		$criteria->compare('inv_in',$this->inv_in);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('id_money',$this->id_money);
		$criteria->compare('trm',$this->trm,true);
		$criteria->compare('id_category',$this->id_category);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('r_d_r',$this->r_d_r,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrdDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
