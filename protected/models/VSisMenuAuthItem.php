<?php

/**
 * This is the model class for table "v_sis_menu_auth_item".
 *
 * The followings are the available columns in table 'v_sis_menu_auth_item':
 * @property integer $id
 * @property integer $padre
 * @property string $nombre
 * @property string $link
 * @property string $icono
 * @property string $orden
 * @property integer $id_programa
 * @property string $name_auth_item
 * @property integer $r_d_s
 * @property integer $shortcut
 * @property integer $sis_menu_r_d_s
 * @property string $target
 */
class VSisMenuAuthItem extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_sis_menu_auth_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_auth_item', 'required'),
			array('id, padre, id_programa, r_d_s, shortcut, sis_menu_r_d_s', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>25),
			array('link, icono, orden', 'length', 'max'=>50),
			array('name_auth_item', 'length', 'max'=>64),
			array('target', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, padre, nombre, link, icono, orden, id_programa, name_auth_item, r_d_s, shortcut, sis_menu_r_d_s, target', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'padre' => Yii::t('app','Padre'),
			'nombre' => Yii::t('app','Nombre'),
			'link' => Yii::t('app','Link'),
			'icono' => Yii::t('app','Icono'),
			'orden' => Yii::t('app','Orden'),
			'id_programa' => Yii::t('app','Id Programa'),
			'name_auth_item' => Yii::t('app','Name Auth Item'),
			'r_d_s' => Yii::t('app','R D S'),
			'shortcut' => Yii::t('app','Shortcut'),
			'sis_menu_r_d_s' => Yii::t('app','Sis Menu R D S'),
			'target' => Yii::t('app','Target'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('padre',$this->padre);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('icono',$this->icono,true);
		$criteria->compare('orden',$this->orden,true);
		$criteria->compare('id_programa',$this->id_programa);
		$criteria->compare('name_auth_item',$this->name_auth_item,true);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('shortcut',$this->shortcut);
		$criteria->compare('sis_menu_r_d_s',$this->sis_menu_r_d_s);
		$criteria->compare('target',$this->target,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VSisMenuAuthItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
