<?php

/**
 * This is the model class for table "ord_document".
 *
 * The followings are the available columns in table 'ord_document':
 * @property integer $id
 * @property string $number
 * @property string $date
 * @property string $total
 * @property string $note
 * @property integer $id_document
 * @property integer $id_order
 * @property integer $id_bank
 * @property integer $id_status
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property string $r_u_o
 * @property string $r_u_f
 * @property string $r_u_m
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 *
 * The followings are the available model relations:
 * @property Document $idDocument
 * @property Order $idOrder
 * @property Bank $idBank
 * @property Status $idStatus
 */
class OrdDocument extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ord_document';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('number, id_document, id_order, id_status', 'required'),
			array('id_document, id_order, id_bank, id_status, r_c_u, r_u_u, r_d_u, r_d_s', 'numerical', 'integerOnly'=>true),
			array('number', 'length', 'max'=>100),
			array('total', 'length', 'max'=>12),
			array('r_c_i, r_u_i, r_u_o, r_u_f, r_u_m, r_d_i', 'length', 'max'=>255),
			array('date, note, r_c_d, r_u_d, r_d_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, number, date, total, note, id_document, id_order, id_bank, id_status, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_u_o, r_u_f, r_u_m, r_d_u, r_d_d, r_d_i, r_d_s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDocument' => array(self::BELONGS_TO, 'Document', 'id_document'),
			'idOrder' => array(self::BELONGS_TO, 'Order', 'id_order'),
			'idBank' => array(self::BELONGS_TO, 'Bank', 'id_bank'),
			'idStatus' => array(self::BELONGS_TO, 'Status', 'id_status'),
                        'rcu'=>array(self::BELONGS_TO,'SisUsuario','r_c_u')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'number' => Yii::t('app','Number'),
			'date' => Yii::t('app','Date'),
			'total' => Yii::t('app','Total'),
			'note' => Yii::t('app','Note'),
			'id_document' => Yii::t('app','Id Document'),
			'id_order' => Yii::t('app','Id Order'),
			'id_bank' => Yii::t('app','Id Bank'),
			'id_status' => Yii::t('app','Id Status'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_u_o' => Yii::t('app','R U O'),
			'r_u_f' => Yii::t('app','R U F'),
			'r_u_m' => Yii::t('app','R U M'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('id_document',$this->id_document);
		$criteria->compare('id_order',$this->id_order);
		$criteria->compare('id_bank',$this->id_bank);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_u_o',$this->r_u_o,true);
		$criteria->compare('r_u_f',$this->r_u_f,true);
		$criteria->compare('r_u_m',$this->r_u_m,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrdDocument the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
