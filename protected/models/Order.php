<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $id
 * @property integer $id_ralate
 * @property integer $number
 * @property integer $id_client
 * @property integer $id_status
 * @property integer $id_discount
 * @property integer $id_company
 * @property integer $id_document
 * @property integer $id_account
 * @property string $note
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 * @property string $r_d_r
 *
 * The followings are the available model relations:
 * @property CommentOrder[] $commentOrders
 * @property Inventory[] $inventories
 * @property OrdDetail[] $ordDetails
 * @property OrdDocument[] $ordDocuments
 * @property Client $idClient
 * @property Status $idStatus
 * @property Discount $idDiscount
 * @property Company $idCompany
 * @property Document $idDocument
 * @property Account $idAccount
 * @property OrderDelRate[] $orderDelRates
 * @property OrderDelivery[] $orderDeliveries
 * @property OrderTax[] $orderTaxes
 */
class Order extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('number, id_client, id_status, id_company, id_document', 'required'),
			array('id_ralate, number, id_client, id_status, id_discount, id_company, id_document, id_account, r_c_u, r_u_u, r_d_u, r_d_s', 'numerical', 'integerOnly'=>true),
			array('r_c_i, r_u_i, r_d_i, r_d_r', 'length', 'max'=>255),
			array('note, r_c_d, r_u_d, r_d_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_ralate, number, id_client, id_status, id_discount, id_company, id_document, id_account, note, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_d_u, r_d_d, r_d_i, r_d_s, r_d_r', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'commentOrders' => array(self::HAS_MANY, 'CommentOrder', 'id_order'),
			'inventories' => array(self::HAS_MANY, 'Inventory', 'id_order'),
			'ordDetails' => array(self::HAS_MANY, 'OrdDetail', 'id_order'),
			'ordDocuments' => array(self::HAS_MANY, 'OrdDocument', 'id_order'),
			'idClient' => array(self::BELONGS_TO, 'Client', 'id_client'),
			'idStatus' => array(self::BELONGS_TO, 'Status', 'id_status'),
			'idDiscount' => array(self::BELONGS_TO, 'Discount', 'id_discount'),
			'idCompany' => array(self::BELONGS_TO, 'Company', 'id_company'),
			'idDocument' => array(self::BELONGS_TO, 'Document', 'id_document'),
			'idAccount' => array(self::BELONGS_TO, 'Account', 'id_account'),
			'orderDelRates' => array(self::HAS_MANY, 'OrderDelRate', 'id_order'),
			'orderDeliveries' => array(self::HAS_MANY, 'OrderDelivery', 'id_order'),
			'orderTaxes' => array(self::HAS_MANY, 'OrderTax', 'id_order'),
			'rcu' => array(self::BELONGS_TO, 'SisUsuario', 'r_c_u'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'id_ralate' => Yii::t('app','Id Ralate'),
			'number' => Yii::t('app','Number'),
			'id_client' => Yii::t('app','Id Client'),
			'id_status' => Yii::t('app','Id Status'),
			'id_discount' => Yii::t('app','Id Discount'),
			'id_company' => Yii::t('app','Id Company'),
			'id_document' => Yii::t('app','Id Document'),
			'id_account' => Yii::t('app','Id Account'),
			'note' => Yii::t('app','Note'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
			'r_d_r' => Yii::t('app','R D R'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_ralate',$this->id_ralate);
		$criteria->compare('number',$this->number);
		$criteria->compare('id_client',$this->id_client);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('id_discount',$this->id_discount);
		$criteria->compare('id_company',$this->id_company);
		$criteria->compare('id_document',$this->id_document);
		$criteria->compare('id_account',$this->id_account);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('r_d_r',$this->r_d_r,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
