<?php

/**
 * This is the model class for table "ticket".
 *
 * The followings are the available columns in table 'ticket':
 * @property integer $id
 * @property integer $id_priority
 * @property integer $id_product
 * @property integer $id_status
 * @property integer $id_type
 * @property integer $id_sis_usuario
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property string $r_d_u
 *
 * The followings are the available model relations:
 * @property Commet[] $commets
 * @property Priority $idPriority
 * @property Product $idProduct
 * @property Status $idStatus
 * @property Type $idType
 * @property User $idSisUsuario
 * @property TicketTag[] $ticketTags
 * @property Work[] $works
 */
class Ticket extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ticket';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_priority, id_product, id_status, id_type, id_sis_usuario, r_c_d', 'required'),
			array('id_priority, id_product, id_status, id_type, id_sis_usuario, r_c_u, r_u_u', 'numerical', 'integerOnly'=>true),
			array('r_c_i, r_u_i, r_d_u', 'length', 'max'=>255),
			array('r_u_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_priority, id_product, id_status, id_type, id_sis_usuario, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_d_u', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'commets' => array(self::HAS_MANY, 'Commet', 'id_ticket'),
			'idPriority' => array(self::BELONGS_TO, 'Priority', 'id_priority'),
			'idProduct' => array(self::BELONGS_TO, 'Product', 'id_product'),
			'idStatus' => array(self::BELONGS_TO, 'Status', 'id_status'),
			'idType' => array(self::BELONGS_TO, 'Type', 'id_type'),
			'idSisUsuario' => array(self::BELONGS_TO, 'User', 'id_sis_usuario'),
			'ticketTags' => array(self::HAS_MANY, 'TicketTag', 'id_ticket'),
			'works' => array(self::HAS_MANY, 'Work', 'id_ticket'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'id_priority' => Yii::t('app','Id Priority'),
			'id_product' => Yii::t('app','Id Product'),
			'id_status' => Yii::t('app','Id Status'),
			'id_type' => Yii::t('app','Id Type'),
			'id_sis_usuario' => Yii::t('app','Id Sis Usuario'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_d_u' => Yii::t('app','R D U'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_priority',$this->id_priority);
		$criteria->compare('id_product',$this->id_product);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('id_type',$this->id_type);
		$criteria->compare('id_sis_usuario',$this->id_sis_usuario);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_d_u',$this->r_d_u,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ticket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
