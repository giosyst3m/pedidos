<?php

/**
 * This is the model class for table "pro_type_field".
 *
 * The followings are the available columns in table 'pro_type_field':
 * @property integer $id
 * @property integer $id_field
 * @property integer $id_pro_type
 * @property integer $r_d_s
 *
 * The followings are the available model relations:
 * @property Caracteristic[] $caracteristics
 * @property Field $idField
 * @property ProType $idProType
 */
class ProTypeField extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pro_type_field';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_field, id_pro_type', 'required'),
			array('id_field, id_pro_type, r_d_s', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_field, id_pro_type, r_d_s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'caracteristics' => array(self::HAS_MANY, 'Caracteristic', 'id_pro_type_field'),
			'idField' => array(self::BELONGS_TO, 'Field', 'id_field'),
			'idProType' => array(self::BELONGS_TO, 'ProType', 'id_pro_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'id_field' => Yii::t('app','Id Field'),
			'id_pro_type' => Yii::t('app','Id Pro Type'),
			'r_d_s' => Yii::t('app','R D S'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_field',$this->id_field);
		$criteria->compare('id_pro_type',$this->id_pro_type);
		$criteria->compare('r_d_s',$this->r_d_s);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProTypeField the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
