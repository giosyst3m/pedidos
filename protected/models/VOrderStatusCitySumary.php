<?php

/**
 * This is the model class for table "v_order_status_city_sumary".
 *
 * The followings are the available columns in table 'v_order_status_city_sumary':
 * @property integer $id
 * @property string $city_name
 * @property integer $id_status
 * @property string $status_name
 * @property string $color
 * @property string $boton_color
 * @property string $boton_lcolor
 * @property string $icon
 * @property integer $id_company
 * @property string $quantity
 * @property string $total
 */
class VOrderStatusCitySumary extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_order_status_city_sumary';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('city_name, status_name, id_company', 'required'),
			array('id, id_status, id_company', 'numerical', 'integerOnly'=>true),
			array('city_name, status_name, color, boton_color, boton_lcolor, icon', 'length', 'max'=>255),
			array('quantity', 'length', 'max'=>21),
			array('total', 'length', 'max'=>65),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, city_name, id_status, status_name, color, boton_color, boton_lcolor, icon, id_company, quantity, total', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'city_name' => Yii::t('app','City Name'),
			'id_status' => Yii::t('app','Id Status'),
			'status_name' => Yii::t('app','Status Name'),
			'color' => Yii::t('app','Color'),
			'boton_color' => Yii::t('app','Boton Color'),
			'boton_lcolor' => Yii::t('app','Boton Lcolor'),
			'icon' => Yii::t('app','Icon'),
			'id_company' => Yii::t('app','Id Company'),
			'quantity' => Yii::t('app','Quantity'),
			'total' => Yii::t('app','Total'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('city_name',$this->city_name,true);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('status_name',$this->status_name,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('boton_color',$this->boton_color,true);
		$criteria->compare('boton_lcolor',$this->boton_lcolor,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('id_company',$this->id_company);
		$criteria->compare('quantity',$this->quantity,true);
		$criteria->compare('total',$this->total,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VOrderStatusCitySumary the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
