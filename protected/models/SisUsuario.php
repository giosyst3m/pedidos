<?php

/**
 * This is the model class for table "sis_usuario".
 *
 * The followings are the available columns in table 'sis_usuario':
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property string $email
 * @property string $username
 * @property string $clave
 * @property string $clave2
 * @property string $id_auth_item
 * @property integer $id_sis_cargo
 * @property integer $id_sis_usu_tipo
 * @property integer $acceso
 * @property string $foto
 * @property integer $id_client
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 *
 * The followings are the available model relations:
 * @property CajMovimientos[] $cajMovimientoses
 * @property CajaSisUsuario[] $cajaSisUsuarios
 * @property CuentaSisUsuario[] $cuentaSisUsuarios
 * @property SisUsuCargo $idSisCargo
 * @property SisUsuTipo $idSisUsuTipo
 * @property AuthItem $idAuthItem
 * @property Vale[] $vales
 */
class SisUsuario extends GS3CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sis_usuario';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, apellido, username, email,id_auth_item, id_sis_cargo, id_sis_usu_tipo, r_d_s, acceso, clave', 'required', 'on' => 'registre'),
            array('nombre, apellido, username, email, id_auth_item, id_sis_cargo, id_sis_usu_tipo, r_d_s, acceso', 'required', 'on' => 'registre_email'),
            array('username, email', 'unique'),
            array(
                'username',
                'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9_-]/',
                'message' => 'No se permiten espacios en blanco, caracteres especiales, solo letras sin acentos, numéros, guión - y guión abajo _',
            ),
            array('nombre, apellido, email', 'required', 'on' => 'perfil'),
            array('clave, clave2', 'required', 'on' => 'chagePassword'),
            array('id_sis_cargo, id_sis_usu_tipo, acceso, id_client, r_c_u, r_u_u, r_d_u, r_d_s', 'numerical', 'integerOnly' => true),
            array('nombre, apellido, email', 'length', 'max' => 50, 'on' => 'registre, perfil'),
            array('username, clave, clave2, foto, r_c_i, r_u_i, r_d_i', 'length', 'max' => 255),
            array('clave, clave2', 'length', 'max' => 50, 'min' => 8, 'on' => 'chagePassword, registre'),
            array('email', 'length', 'min' => 2, 'on' => 'registre, perfil'),
            array('email', 'email', 'on' => 'registre, perfil'),
//            array('email', 'unique', 'on' => 'registre, perfil'),
            array('clave2', 'compare', 'compareAttribute' => 'clave', 'on' => ' chagePassword'),
            array('id_auth_item', 'length', 'max' => 64),
            array('r_u_d, r_d_d', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, apellido, email, username, clave, clave2, id_auth_item, id_sis_cargo, id_sis_usu_tipo, acceso,foto, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_d_u, r_d_d, r_d_i, r_d_s', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idSisCargo' => array(self::BELONGS_TO, 'SisUsuCargo', 'id_sis_cargo'),
            'idSisUsuTipo' => array(self::BELONGS_TO, 'SisUsuTipo', 'id_sis_usu_tipo'),
            'idAuthItem' => array(self::BELONGS_TO, 'AuthItem', 'id_auth_item'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'apellido' => Yii::t('app', 'Apellido'),
            'email' => Yii::t('app', 'Email'),
            'username' => Yii::t('app', 'Username'),
            'clave' => Yii::t('app', 'Clave'),
            'clave2' => Yii::t('app', 'Clave2'),
            'id_auth_item' => Yii::t('app', 'Id Auth Item'),
            'id_sis_cargo' => Yii::t('app', 'Id Sis Cargo'),
            'id_sis_usu_tipo' => Yii::t('app', 'Id Sis Usu Tipo'),
            'acceso' => Yii::t('app', 'Acceso'),
            'foto' => Yii::t('app', 'Foto'),
            'r_c_u' => Yii::t('app', 'R C U'),
            'r_c_d' => Yii::t('app', 'R C D'),
            'r_c_i' => Yii::t('app', 'R C I'),
            'r_u_u' => Yii::t('app', 'R U U'),
            'r_u_d' => Yii::t('app', 'R U D'),
            'r_u_i' => Yii::t('app', 'R U I'),
            'r_d_u' => Yii::t('app', 'R D U'),
            'r_d_d' => Yii::t('app', 'R D D'),
            'r_d_i' => Yii::t('app', 'R D I'),
            'r_d_s' => Yii::t('app', 'R D S'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('apellido', $this->apellido, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('clave', $this->clave, true);
        $criteria->compare('clave2', $this->clave2, true);
        $criteria->compare('id_auth_item', $this->id_auth_item, true);
        $criteria->compare('id_sis_cargo', $this->id_sis_cargo);
        $criteria->compare('id_sis_usu_tipo', $this->id_sis_usu_tipo);
        $criteria->compare('acceso', $this->acceso);
        $criteria->compare('foto', $this->foto, true);
        $criteria->compare('r_c_u', $this->r_c_u);
        $criteria->compare('r_c_d', $this->r_c_d, true);
        $criteria->compare('r_c_i', $this->r_c_i, true);
        $criteria->compare('r_u_u', $this->r_u_u);
        $criteria->compare('r_u_d', $this->r_u_d, true);
        $criteria->compare('r_u_i', $this->r_u_i, true);
        $criteria->compare('r_d_u', $this->r_d_u);
        $criteria->compare('r_d_d', $this->r_d_d, true);
        $criteria->compare('r_d_i', $this->r_d_i, true);
        $criteria->compare('r_d_s', $this->r_d_s);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SisUsuario the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
