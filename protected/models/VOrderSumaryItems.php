<?php

/**
 * This is the model class for table "v_order_sumary_items".
 *
 * The followings are the available columns in table 'v_order_sumary_items':
 * @property integer $clientId
 * @property string $client
 * @property integer $orderNumber
 * @property string $quantityItems
 * @property string $sub_total_productos
 * @property string $discount_name
 * @property string $discount
 * @property string $sub_total_menos_descuento
 * @property string $total_delivery
 * @property string $sub_total_mas_delivery
 * @property string $total_tax
 * @property string $total
 * @property integer $id_status
 * @property string $code
 * @property integer $correlative
 * @property string $status
 * @property string $complete_name
 * @property string $color
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $semestre
 * @property string $cuatrimestre
 * @property string $trimestre
 * @property string $bimestre
 * @property string $mes
 * @property integer $id_discount
 * @property integer $number
 * @property integer $id_company
 * @property integer $id_document
 * @property integer $id
 * @property string $status_description
 * @property string $boton_color
 * @property string $boton_lcolor
 * @property string $icon
 */
class VOrderSumaryItems extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_order_sumary_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('clientId, client, orderNumber, discount_name, id_status, code, status, id_discount, number, id_company, id_document', 'required'),
			array('clientId, orderNumber, id_status, correlative, r_c_u, id_discount, number, id_company, id_document, id', 'numerical', 'integerOnly'=>true),
			array('client, discount_name, status, color, boton_color, boton_lcolor, icon', 'length', 'max'=>255),
			array('quantityItems, total_delivery', 'length', 'max'=>32),
			array('sub_total_productos', 'length', 'max'=>43),
			array('discount', 'length', 'max'=>57),
			array('sub_total_menos_descuento', 'length', 'max'=>58),
			array('sub_total_mas_delivery', 'length', 'max'=>59),
			array('total_tax, total', 'length', 'max'=>65),
			array('code', 'length', 'max'=>25),
			array('complete_name', 'length', 'max'=>101),
			array('r_c_d, semestre, cuatrimestre, trimestre, bimestre, mes, status_description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('clientId, client, orderNumber, quantityItems, sub_total_productos, discount_name, discount, sub_total_menos_descuento, total_delivery, sub_total_mas_delivery, total_tax, total, id_status, code, correlative, status, complete_name, color, r_c_u, r_c_d, semestre, cuatrimestre, trimestre, bimestre, mes, id_discount, number, id_company, id_document, id, status_description, boton_color, boton_lcolor, icon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'clientId' => Yii::t('app','Client'),
			'client' => Yii::t('app','Client'),
			'orderNumber' => Yii::t('app','Order Number'),
			'quantityItems' => Yii::t('app','Quantity Items'),
			'sub_total_productos' => Yii::t('app','Sub Total Productos'),
			'discount_name' => Yii::t('app','Discount Name'),
			'discount' => Yii::t('app','Discount'),
			'sub_total_menos_descuento' => Yii::t('app','Sub Total Menos Descuento'),
			'total_delivery' => Yii::t('app','Total Delivery'),
			'sub_total_mas_delivery' => Yii::t('app','Sub Total Mas Delivery'),
			'total_tax' => Yii::t('app','Total Tax'),
			'total' => Yii::t('app','Total'),
			'id_status' => Yii::t('app','Id Status'),
			'code' => Yii::t('app','Code'),
			'correlative' => Yii::t('app','Correlative'),
			'status' => Yii::t('app','Status'),
			'complete_name' => Yii::t('app','Complete Name'),
			'color' => Yii::t('app','Color'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'semestre' => Yii::t('app','Semestre'),
			'cuatrimestre' => Yii::t('app','Cuatrimestre'),
			'trimestre' => Yii::t('app','Trimestre'),
			'bimestre' => Yii::t('app','Bimestre'),
			'mes' => Yii::t('app','Mes'),
			'id_discount' => Yii::t('app','Id Discount'),
			'number' => Yii::t('app','Number'),
			'id_company' => Yii::t('app','Id Company'),
			'id_document' => Yii::t('app','Id Document'),
			'id' => Yii::t('app','ID'),
			'status_description' => Yii::t('app','Status Description'),
			'boton_color' => Yii::t('app','Boton Color'),
			'boton_lcolor' => Yii::t('app','Boton Lcolor'),
			'icon' => Yii::t('app','Icon'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('clientId',$this->clientId);
		$criteria->compare('client',$this->client,true);
		$criteria->compare('orderNumber',$this->orderNumber);
		$criteria->compare('quantityItems',$this->quantityItems,true);
		$criteria->compare('sub_total_productos',$this->sub_total_productos,true);
		$criteria->compare('discount_name',$this->discount_name,true);
		$criteria->compare('discount',$this->discount,true);
		$criteria->compare('sub_total_menos_descuento',$this->sub_total_menos_descuento,true);
		$criteria->compare('total_delivery',$this->total_delivery,true);
		$criteria->compare('sub_total_mas_delivery',$this->sub_total_mas_delivery,true);
		$criteria->compare('total_tax',$this->total_tax,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('correlative',$this->correlative);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('complete_name',$this->complete_name,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('semestre',$this->semestre,true);
		$criteria->compare('cuatrimestre',$this->cuatrimestre,true);
		$criteria->compare('trimestre',$this->trimestre,true);
		$criteria->compare('bimestre',$this->bimestre,true);
		$criteria->compare('mes',$this->mes,true);
		$criteria->compare('id_discount',$this->id_discount);
		$criteria->compare('number',$this->number);
		$criteria->compare('id_company',$this->id_company);
		$criteria->compare('id_document',$this->id_document);
		$criteria->compare('id',$this->id);
		$criteria->compare('status_description',$this->status_description,true);
		$criteria->compare('boton_color',$this->boton_color,true);
		$criteria->compare('boton_lcolor',$this->boton_lcolor,true);
		$criteria->compare('icon',$this->icon,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VOrderSumaryItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
