<?php

/**
 * This is the model class for table "v_sis_usuario".
 *
 * The followings are the available columns in table 'v_sis_usuario':
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property string $nombre_completo
 * @property string $email
 * @property string $id_auth_item
 * @property integer $id_sis_cargo
 * @property integer $id_sis_usu_tipo
 * @property integer $acceso
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 * @property string $sis_usu_tipo_nombre
 * @property string $sis_usu_cargo_nombre
 * @property integer $sis_usu_tipo_r_d_s
 * @property integer $sis_usu_cargo_r_d_s
 * @property string $foto
 */
class VSisUsuario extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_sis_usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, apellido, email, id_auth_item, id_sis_cargo, id_sis_usu_tipo, acceso, sis_usu_tipo_nombre, sis_usu_cargo_nombre', 'required'),
			array('id, id_sis_cargo, id_sis_usu_tipo, acceso, r_c_u, r_u_u, r_d_u, r_d_s, sis_usu_tipo_r_d_s, sis_usu_cargo_r_d_s', 'numerical', 'integerOnly'=>true),
			array('nombre, apellido, email, sis_usu_tipo_nombre', 'length', 'max'=>50),
			array('nombre_completo', 'length', 'max'=>101),
			array('id_auth_item', 'length', 'max'=>64),
			array('r_c_i, r_u_i, r_d_i, foto', 'length', 'max'=>255),
			array('sis_usu_cargo_nombre', 'length', 'max'=>25),
			array('r_c_d, r_u_d, r_d_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, apellido, nombre_completo, email, id_auth_item, id_sis_cargo, id_sis_usu_tipo, acceso, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_d_u, r_d_d, r_d_i, r_d_s, sis_usu_tipo_nombre, sis_usu_cargo_nombre, sis_usu_tipo_r_d_s, sis_usu_cargo_r_d_s, foto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'nombre' => Yii::t('app','Nombre'),
			'apellido' => Yii::t('app','Apellido'),
			'nombre_completo' => Yii::t('app','Nombre Completo'),
			'email' => Yii::t('app','Email'),
			'id_auth_item' => Yii::t('app','Id Auth Item'),
			'id_sis_cargo' => Yii::t('app','Id Sis Cargo'),
			'id_sis_usu_tipo' => Yii::t('app','Id Sis Usu Tipo'),
			'acceso' => Yii::t('app','Acceso'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
			'sis_usu_tipo_nombre' => Yii::t('app','Sis Usu Tipo Nombre'),
			'sis_usu_cargo_nombre' => Yii::t('app','Sis Usu Cargo Nombre'),
			'sis_usu_tipo_r_d_s' => Yii::t('app','Sis Usu Tipo R D S'),
			'sis_usu_cargo_r_d_s' => Yii::t('app','Sis Usu Cargo R D S'),
			'foto' => Yii::t('app','Foto'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('apellido',$this->apellido,true);
		$criteria->compare('nombre_completo',$this->nombre_completo,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('id_auth_item',$this->id_auth_item,true);
		$criteria->compare('id_sis_cargo',$this->id_sis_cargo);
		$criteria->compare('id_sis_usu_tipo',$this->id_sis_usu_tipo);
		$criteria->compare('acceso',$this->acceso);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('sis_usu_tipo_nombre',$this->sis_usu_tipo_nombre,true);
		$criteria->compare('sis_usu_cargo_nombre',$this->sis_usu_cargo_nombre,true);
		$criteria->compare('sis_usu_tipo_r_d_s',$this->sis_usu_tipo_r_d_s);
		$criteria->compare('sis_usu_cargo_r_d_s',$this->sis_usu_cargo_r_d_s);
		$criteria->compare('foto',$this->foto,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VSisUsuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
