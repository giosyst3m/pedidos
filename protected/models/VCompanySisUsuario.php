<?php

/**
 * This is the model class for table "v_company_sis_usuario".
 *
 * The followings are the available columns in table 'v_company_sis_usuario':
 * @property integer $id
 * @property integer $id_company
 * @property integer $id_sis_usuario
 * @property string $complete_name
 * @property string $company
 * @property string $address
 * @property string $company_address
 */
class VCompanySisUsuario extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_company_sis_usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_company, id_sis_usuario, company', 'required'),
			array('id, id_company, id_sis_usuario', 'numerical', 'integerOnly'=>true),
			array('complete_name', 'length', 'max'=>101),
			array('company', 'length', 'max'=>255),
			array('address, company_address', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_company, id_sis_usuario, complete_name, company, address, company_address', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'id_company' => Yii::t('app','Id Company'),
			'id_sis_usuario' => Yii::t('app','Id Sis Usuario'),
			'complete_name' => Yii::t('app','Complete Name'),
			'company' => Yii::t('app','Company'),
			'address' => Yii::t('app','Address'),
			'company_address' => Yii::t('app','Company Address'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_company',$this->id_company);
		$criteria->compare('id_sis_usuario',$this->id_sis_usuario);
		$criteria->compare('complete_name',$this->complete_name,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('company_address',$this->company_address,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VCompanySisUsuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
