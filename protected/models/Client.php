<?php

/**
 * This is the model class for table "client".
 *
 * The followings are the available columns in table 'client':
 * @property integer $id
 * @property string $code
 * @property integer $correlative
 * @property string $name
 * @property string $phone
 * @property string $mobil
 * @property string $address
 * @property string $conctact
 * @property string $email
 * @property string $logo
 * @property integer $id_cli_type
 * @property integer $id_city
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property string $r_c_o
 * @property string $r_c_f
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property string $r_u_o
 * @property string $r_u_f
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 * @property string $r_d_o
 * @property string $r_d_f
 * @property string $r_d_m
 *
 * The followings are the available model relations:
 * @property CliData[] $cliDatas
 * @property CliType $idCliType
 * @property City $idCity
 * @property Department[] $departments
 * @property Order[] $orders
 * @property SisUsuario[] $sisUsuarios
 * @property ZoneClient[] $zoneClients
 */
class Client extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'client';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, name, address, id_city, correlative', 'required'),
			array('correlative, id_cli_type, id_city, r_c_u, r_u_u, r_d_u, r_d_s', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>25),
			array('name, mobil, conctact, email, logo, r_c_i, r_c_o, r_c_f, r_u_i, r_u_o, r_u_f, r_d_i, r_d_o, r_d_f, r_d_m', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>15),
			array('r_c_d, r_u_d, r_d_d', 'safe'),
                        array('email','email'),
                        array('logo', 'file', 'allowEmpty' => TRUE, 'types'=>'jpg, gif, jpeg, png',
                            'maxSize'=> 5120000, 
                            'maxFiles'=> 1, 
                            'tooLarge'=> Yii::t('app', 'ERROR_FILE_SIZE_500KB'),
                            'wrongMimeType'=> YII::t('app', 'ERROR_FILE_SIZE_TYPE_IMAGEN')
                        ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, correlative, name, phone, mobil, address, conctact, email, logo, id_cli_type, id_city, r_c_u, r_c_d, r_c_i, r_c_o, r_c_f, r_u_u, r_u_d, r_u_i, r_u_o, r_u_f, r_d_u, r_d_d, r_d_i, r_d_s, r_d_o, r_d_f, r_d_m', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cliDatas' => array(self::HAS_MANY, 'CliData', 'id_client'),
			'idCliType' => array(self::BELONGS_TO, 'CliType', 'id_cli_type'),
			'idCity' => array(self::BELONGS_TO, 'City', 'id_city'),
			'departments' => array(self::HAS_MANY, 'Department', 'id_client'),
			'orders' => array(self::HAS_MANY, 'Order', 'id_client'),
			'sisUsuarios' => array(self::HAS_MANY, 'SisUsuario', 'id_client'),
			'zoneClients' => array(self::HAS_MANY, 'ZoneClient', 'id_client'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'code' => Yii::t('app','Code'),
			'correlative' => Yii::t('app','Correlative'),
			'name' => Yii::t('app','Name'),
			'phone' => Yii::t('app','Phone'),
			'mobil' => Yii::t('app','Mobil'),
			'address' => Yii::t('app','Address'),
			'conctact' => Yii::t('app','Conctact'),
			'email' => Yii::t('app','Email'),
			'logo' => Yii::t('app','Logo'),
			'id_cli_type' => Yii::t('app','Id Cli Type'),
			'id_city' => Yii::t('app','Id City'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_c_o' => Yii::t('app','R C O'),
			'r_c_f' => Yii::t('app','R C F'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_u_o' => Yii::t('app','R U O'),
			'r_u_f' => Yii::t('app','R U F'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
			'r_d_o' => Yii::t('app','R D O'),
			'r_d_f' => Yii::t('app','R D F'),
			'r_d_m' => Yii::t('app','R D M'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('correlative',$this->correlative);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobil',$this->mobil,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('conctact',$this->conctact,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('id_cli_type',$this->id_cli_type);
		$criteria->compare('id_city',$this->id_city);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_c_o',$this->r_c_o,true);
		$criteria->compare('r_c_f',$this->r_c_f,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_u_o',$this->r_u_o,true);
		$criteria->compare('r_u_f',$this->r_u_f,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('r_d_o',$this->r_d_o,true);
		$criteria->compare('r_d_f',$this->r_d_f,true);
		$criteria->compare('r_d_m',$this->r_d_m,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
