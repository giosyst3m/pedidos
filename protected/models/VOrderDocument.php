<?php

/**
 * This is the model class for table "v_order_document".
 *
 * The followings are the available columns in table 'v_order_document':
 * @property integer $id
 * @property string $number
 * @property string $date
 * @property string $total
 * @property string $note
 * @property integer $id_document
 * @property integer $id_order
 * @property integer $orderNumber
 * @property integer $id_bank
 * @property integer $r_c_u
 * @property string $r_c_d
 * @property string $r_c_i
 * @property integer $r_u_u
 * @property string $r_u_d
 * @property string $r_u_i
 * @property integer $r_d_u
 * @property string $r_d_d
 * @property string $r_d_i
 * @property integer $r_d_s
 * @property integer $order_number
 * @property integer $id_status
 * @property string $status_name
 * @property string $description
 * @property string $next_step
 * @property string $next_description
 * @property integer $orden
 * @property string $color
 * @property string $boton_color
 * @property string $boton_lcolor
 * @property string $icon
 * @property string $client
 * @property string $total_0
 * @property string $document_name
 */
class VOrderDocument extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_order_document';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('number, id_document, id_order, orderNumber, order_number, id_status, status_name, orden, client, document_name', 'required'),
			array('id, id_document, id_order, orderNumber, id_bank, r_c_u, r_u_u, r_d_u, r_d_s, order_number, id_status, orden', 'numerical', 'integerOnly'=>true),
			array('number', 'length', 'max'=>100),
			array('total', 'length', 'max'=>12),
			array('r_c_i, r_u_i, r_d_i, status_name, next_step, color, boton_color, boton_lcolor, icon, client, document_name', 'length', 'max'=>255),
			array('total_0', 'length', 'max'=>65),
			array('date, note, r_c_d, r_u_d, r_d_d, description, next_description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, number, date, total, note, id_document, id_order, orderNumber, id_bank, r_c_u, r_c_d, r_c_i, r_u_u, r_u_d, r_u_i, r_d_u, r_d_d, r_d_i, r_d_s, order_number, id_status, status_name, description, next_step, next_description, orden, color, boton_color, boton_lcolor, icon, client, total_0, document_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'number' => Yii::t('app','Number'),
			'date' => Yii::t('app','Date'),
			'total' => Yii::t('app','Total'),
			'note' => Yii::t('app','Note'),
			'id_document' => Yii::t('app','Id Document'),
			'id_order' => Yii::t('app','Id Order'),
			'orderNumber' => Yii::t('app','Order Number'),
			'id_bank' => Yii::t('app','Id Bank'),
			'r_c_u' => Yii::t('app','R C U'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_c_i' => Yii::t('app','R C I'),
			'r_u_u' => Yii::t('app','R U U'),
			'r_u_d' => Yii::t('app','R U D'),
			'r_u_i' => Yii::t('app','R U I'),
			'r_d_u' => Yii::t('app','R D U'),
			'r_d_d' => Yii::t('app','R D D'),
			'r_d_i' => Yii::t('app','R D I'),
			'r_d_s' => Yii::t('app','R D S'),
			'order_number' => Yii::t('app','Order Number'),
			'id_status' => Yii::t('app','Id Status'),
			'status_name' => Yii::t('app','Status Name'),
			'description' => Yii::t('app','Description'),
			'next_step' => Yii::t('app','Next Step'),
			'next_description' => Yii::t('app','Next Description'),
			'orden' => Yii::t('app','Orden'),
			'color' => Yii::t('app','Color'),
			'boton_color' => Yii::t('app','Boton Color'),
			'boton_lcolor' => Yii::t('app','Boton Lcolor'),
			'icon' => Yii::t('app','Icon'),
			'client' => Yii::t('app','Client'),
			'total_0' => Yii::t('app','Total 0'),
			'document_name' => Yii::t('app','Document Name'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('id_document',$this->id_document);
		$criteria->compare('id_order',$this->id_order);
		$criteria->compare('orderNumber',$this->orderNumber);
		$criteria->compare('id_bank',$this->id_bank);
		$criteria->compare('r_c_u',$this->r_c_u);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_c_i',$this->r_c_i,true);
		$criteria->compare('r_u_u',$this->r_u_u);
		$criteria->compare('r_u_d',$this->r_u_d,true);
		$criteria->compare('r_u_i',$this->r_u_i,true);
		$criteria->compare('r_d_u',$this->r_d_u);
		$criteria->compare('r_d_d',$this->r_d_d,true);
		$criteria->compare('r_d_i',$this->r_d_i,true);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('order_number',$this->order_number);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('status_name',$this->status_name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('next_step',$this->next_step,true);
		$criteria->compare('next_description',$this->next_description,true);
		$criteria->compare('orden',$this->orden);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('boton_color',$this->boton_color,true);
		$criteria->compare('boton_lcolor',$this->boton_lcolor,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('client',$this->client,true);
		$criteria->compare('total_0',$this->total_0,true);
		$criteria->compare('document_name',$this->document_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VOrderDocument the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
