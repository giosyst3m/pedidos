<?php

/**
 * This is the model class for table "v_client".
 *
 * The followings are the available columns in table 'v_client':
 * @property string $name
 * @property string $phone
 * @property string $mobil
 * @property string $address
 * @property string $conctact
 * @property string $email
 * @property string $logo
 * @property integer $id_city
 * @property string $full_name
 * @property integer $id
 * @property string $code
 * @property string $city
 * @property string $stade
 * @property string $country
 * @property integer $correlative
 * @property integer $r_d_s
 * @property string $zone
 * @property string $seller
 * @property integer $id_cli_type
 * @property string $name_0
 */
class VClient extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_client';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, address, id_city, code, city, stade, country', 'required'),
			array('id_city, id, correlative, r_d_s, id_cli_type', 'numerical', 'integerOnly'=>true),
			array('name, mobil, conctact, email, logo, city, stade, country, zone, name_0', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>15),
			array('code', 'length', 'max'=>25),
			array('seller', 'length', 'max'=>101),
			array('full_name', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('name, phone, mobil, address, conctact, email, logo, id_city, full_name, id, code, city, stade, country, correlative, r_d_s, zone, seller, id_cli_type, name_0', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name' => Yii::t('app','Name'),
			'phone' => Yii::t('app','Phone'),
			'mobil' => Yii::t('app','Mobil'),
			'address' => Yii::t('app','Address'),
			'conctact' => Yii::t('app','Conctact'),
			'email' => Yii::t('app','Email'),
			'logo' => Yii::t('app','Logo'),
			'id_city' => Yii::t('app','Id City'),
			'full_name' => Yii::t('app','Full Name'),
			'id' => Yii::t('app','ID'),
			'code' => Yii::t('app','Code'),
			'city' => Yii::t('app','City'),
			'stade' => Yii::t('app','Stade'),
			'country' => Yii::t('app','Country'),
			'correlative' => Yii::t('app','Correlative'),
			'r_d_s' => Yii::t('app','R D S'),
			'zone' => Yii::t('app','Zone'),
			'seller' => Yii::t('app','Seller'),
			'id_cli_type' => Yii::t('app','Id Cli Type'),
			'name_0' => Yii::t('app','Name 0'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobil',$this->mobil,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('conctact',$this->conctact,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('id_city',$this->id_city);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('stade',$this->stade,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('correlative',$this->correlative);
		$criteria->compare('r_d_s',$this->r_d_s);
		$criteria->compare('zone',$this->zone,true);
		$criteria->compare('seller',$this->seller,true);
		$criteria->compare('id_cli_type',$this->id_cli_type);
		$criteria->compare('name_0',$this->name_0,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VClient the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
