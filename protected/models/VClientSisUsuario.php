<?php

/**
 * This is the model class for table "v_client_sis_usuario".
 *
 * The followings are the available columns in table 'v_client_sis_usuario':
 * @property integer $id
 * @property integer $id_client
 * @property integer $id_sis_usuario
 * @property string $client
 * @property string $user
 * @property string $code
 * @property string $full_name
 */
class VClientSisUsuario extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'v_client_sis_usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_client, id_sis_usuario, client, code', 'required'),
			array('id, id_client, id_sis_usuario', 'numerical', 'integerOnly'=>true),
			array('client', 'length', 'max'=>255),
			array('user', 'length', 'max'=>101),
			array('code', 'length', 'max'=>25),
			array('full_name', 'length', 'max'=>281),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_client, id_sis_usuario, client, user, code, full_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'id_client' => Yii::t('app','Id Client'),
			'id_sis_usuario' => Yii::t('app','Id Sis Usuario'),
			'client' => Yii::t('app','Client'),
			'user' => Yii::t('app','User'),
			'code' => Yii::t('app','Code'),
			'full_name' => Yii::t('app','Full Name'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_client',$this->id_client);
		$criteria->compare('id_sis_usuario',$this->id_sis_usuario);
		$criteria->compare('client',$this->client,true);
		$criteria->compare('user',$this->user,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('full_name',$this->full_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VClientSisUsuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
